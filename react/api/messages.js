import request from '../utils/request'

export default {
  list: () => {
    return request({
      method: 'get',
      path: '/articles/App/getAll'
    })
  }
}