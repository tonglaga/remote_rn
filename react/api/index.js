import categories from './categories'
import categoriesm from './categoriesm'
// import releasedMessages from './releasedMessages'
// import comments from './comments'
import farmer from './farmer'
import drugstore from './drugstore'
import expert from './expert'
import user from './user'
import introduces from './introduces'
import messages from './messages'
import file from './file'
import feedback from './feedback'
import grade from './grade'
import gradem from './gradem'
import collects from './collects'
export default {
       user,
       categories,
       categoriesm,
    // releasedMessages,
    // comments,
      collects,
      introduces,
      feedback,
      messages,
      file,
      farmer,
      drugstore,
      expert,
      grade,
      gradem,
}