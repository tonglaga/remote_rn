import request from '../utils/request'

export default {
  upload: (file) => {
    return request({
        method: 'file',
        path: '/files/',
        data: {
            image: file
        }
    })
  }
}