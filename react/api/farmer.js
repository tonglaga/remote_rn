import request from '../utils/request'

export default {
    getByUid: (data) => {
    return request({
      method: 'get',
      path: '/newherdsmans/App/getByUid',
      data: data
    })
  },
  user: (data) => {
    return request({
      method: 'get',
      path: '/users/current',
      data: data
    })
  },
  userponalhan: () => {
    return request({
      method: 'get',
      path: '/newherdsmans/newherdsman/han/'
    })
  },
  diagnose: (data) => {
    return request({
      method: 'get',
      path: '/taskms/AppList',
      data: data
    })
  },
  //牧民个人信息
  mumin: (data) => {
    return request({
      method: 'get',
      path: '/newherdsmans/newherdsman/han/'+data.id,
    })
  },
  zhuanjia: (data) => {
    return request({
      method: 'get',
      path: '/newherdsmans/newherdsman/han/'+data.id,
   
    })
  },
  yaodian: (data) => {
    return request({
      method: 'get',
      path: '/newherdsmans/newherdsman/han/'+data.id,
     
    })
  },
  //获取地区接口
  diqu: () => {
    return request({
      method: 'get',
      path: '/districts/App/getAll',
     
    })
  },
  //获取对应诊断药房数据
  prescriptionms: (data) => {
    return request({
      method: 'get',
      path: '/prescriptionms/App/getP/'+data.id,
      data: data
    })
  },
}
