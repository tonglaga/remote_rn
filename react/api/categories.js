import request from '../utils/request'

//获取专家列表
 export default {
  hanzhuanjia: () => {
    return request({
      method: 'get',
      path: '/app/expertinfo/ten/1',
    })
  },
  //牧民点击视频通话，请求通话
  videoconnects: (data) => {
    return request({
      method: 'get',
      path: '/videoconnects/vc/',
      data:data
    })
  },
  //返回专家状态
  videoreturn: (data) => {
    return request({
      method: 'get',
      path: '/videoconnects/vc/return',
      data:data
    })
  },
  //牧民提前挂断
  notwait: (data) => {
    return request({
      method: 'get',
      path: '/videoconnects/vc/notwait',
      data:data
    })
  },
  //汉语牧民提交文字资料
  appAddTask: (data) => {
    return request({
      method: 'Post',
      path: '/newherdsmans/AppAddTask',
      data:data
    })
  },
   //获取请求通话汉语牧民名称和头像
   newherdsman: (data) => {
    return request({
      method: 'get',
      path: '/newherdsmans/newherdsman/han/'+data.hid,
      data:data
    })
  },
}