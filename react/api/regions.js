import request from '../utils/request'
import config from '../config'

export default {
  cities: () => {
    return request({
      method: 'get',
      path: 'regions/cities'
    })
  },
  list: (cityId) => {
    return request({
      method: 'get',
      path: 'regions',
      data: {
        city_id: cityId
      }
    })
  },
  coordsTrans: (location) => {
    return request({
      method: 'get',
      path: 'https://api.map.baidu.com/geocoder/v2/',
      data: {
        location,
        output: 'json',
        ak: config.baiduMap.ak,
        mcode: config.baiduMap.mcode
      }
    })
  }
}