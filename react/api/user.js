import request from '../utils/request'

export default {
  login: (data) => {
    return request({
      method: 'post',
      path: '/sys/login/restful',
      data: data
    })
  },
  user: (data) => {
    return request({
      method: 'get',
      path: '/users/current',
      data: data
    })
  },
  userponalhan: () => {
    return request({
      method: 'get',
      path: '/newherdsmans/newherdsman/han/'
    })
  },
  deviceId: (data) => {
    return request({
      method: 'get',
      path: '/app/getRegsId',
      data: data
    })
  },
  //牧民个人信息
  mumin: (data) => {
    return request({
      method: 'get',
      path: '/newherdsmans/newherdsman/han/'+data.id,
    })
  },
  //专家个人信息
  zhuanjia: (data) => {
    return request({
      method: 'get',
      path: '/expertinfos/AppGet',
   
    })
  },
  yaodian: (data) => {
    return request({
      method: 'get',
      path: '/drugstoreinfos/AppGet',
     
    })
  },
  //获取短信
  sendMsg: (data) => {
    return request({
      method: 'get',
      path: '/app/sendMsg/'+data.tel,
     
    })
  },
  //注册接口
  checkMsg: (data) => {
    return request({
      method: 'post',
      path: '/app/checkMsg',
      data: data
    })
  },
  //忘记密码
  ChangPassword: (data) => {
    return request({
      method: 'post',
      path: '/app/ChangPassword',
      data: data
    })
  },
}
