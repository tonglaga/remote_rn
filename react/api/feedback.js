import request from '../utils/request'

export default {
  submit: (data) => {
    return request({
      method: 'post',
      path: '/feedbacks/App/save',
      data: data
    })
  }
}