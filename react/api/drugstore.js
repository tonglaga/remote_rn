import request from '../utils/request'

export default {
    
  diagnosef: (data) => {
    return request({
      method: 'get',
      path: '/prescriptions/App/drugsGetList',
      data: data
    })
  },

  yaodian: (data) => {
    return request({
      method: 'get',
      path: '/newherdsmans/newherdsman/han/'+data.id,
     
    })
  },

  //获取对应诊断药房数据
  prescriptionms: (data) => {
    return request({
      method: 'get',
      path: '/prescriptionms/App/getP/'+data.id,
      data: data
    })
  },
}
