import request from '../utils/request'

export default {
  
  diagnose: (data) => {
    return request({
      method: 'get',
      path: '/taskms/AppList',
      data: data
    })
  },
   //获取对应诊断药房数据
   prescriptionms: (data) => {
    return request({
      method: 'get',
      path: '/prescriptionms/App/getP/'+data.id,
      data: data
    })
  },
   //获取对应牧民十个药店
   drugstoreinfos: (data) => {
    return request({
      method: 'get',
      path: 'drugstoreinfos/App/getByTaskIdM/'+data.id,
      data: data
    })
  },
  //获取病分类
  illnesscategorys: (data) => {
    return request({
      method: 'get',
      path: '/illnesscategorys/AppList',
      data: data
    })
  },
  //完善诊断接口
  expertinfoszhen: (data) => {
    return request({
      method: 'post',
      path: '/expertinfos/App/updateTaskm?login-token='+localStorage.token,
      data: data
    })
  },
  //查找对应药店药品
  drugstui: (data) => {
    return request({
      method: 'get',
      path: '/drugs/App/getByDid',
      data: data
    })
  },
  //获取 文章列表
  studyarticlems: (data) => {
    return request({
      method: 'get',
      path: '/studyarticlems/App/getByUid?login-token='+localStorage.token,
      data: data
    })
  },
  

}
