import request from '../utils/request'

export default {
  list: (query) => {
    return request({
      method: 'get',
      path: 'released_messages',
      data: query
    })
  },
  show: (id) => {
    return request({
      method: 'get',
      path: 'released_messages/' + id
    })
  },
  myList: (query) => {
    return request({
      method: 'get',
      path: 'released_messages/my',
      data: query
    })
  },
  changeStatus: (data) => {
    return request({
      method: 'post',
      path: 'released_messages/release',
      data: data
    })
  }
}