import request from '../utils/request'

export default {
  list: (messageId) => {
    return request({
      method: 'get',
      path: 'comments',
      data: {
        released_message_id: messageId
      }
    })
  },
  submit: (data) => {
    return request({
      method: 'post',
      path: 'comments',
      data: data
    })
  }
}