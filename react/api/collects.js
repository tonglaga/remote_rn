import request from '../utils/request'

export default {
  articles: (data) => {
    return request({
      method: 'get',
      path: '/collectionss/App/look?cateId=1',
      data:data
    })
  },
  videos: (data) => {
    return request({
      method: 'get',
      path: '/collectionss/App/look?cateId=2',
      data:data
    })
  },
  experts: (data) => {
    return request({
      method: 'get',
      path: '/collectionss/App/look?cateId=3',
      data:data
    })
  },
  cases: (data) => {
    return request({
      method: 'get',
      path: '/collectionss/App/look?cateId=4',
      data:data

    })
  },
  quipments: (data) => {
    return request({
      method: 'get',
      path: '/collectionss/App/look?cateId=5',
      data:data
    })
  },
}