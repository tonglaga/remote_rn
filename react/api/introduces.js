import request from '../utils/request'

export default {
  getIntroduces: () => {
    return request({
      method: 'get',
      path: '/introduces/App/getAll',
    })
  },
}
