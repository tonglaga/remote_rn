import request from '../utils/request'

export default {
  // submit: (data) => {
  //   return request({
  //     method: 'post',
  //     path: 'grades',
  //     data: data
  //   })
  //}
  // 首页幻灯图片
  slidem: () => {
    return request({
      method: 'get',
      path: '/app/getSlidemList',
    })
  },
  //三条专家
  expertinfos: () => {
    return request({
      method: 'get',
      path: '/app/expertinfo/three',
    })
  },
  //三条学习
  studyvideoms: () => {
    return request({
      method: 'get',
      path: '/app/studyvideo/frist',
    })
  },
  //三条机械
  medicalequipmentms: () => {
    return request({
      method: 'get',
      path: '/app/medicalequipmentm/all',
    })
  },
  //药店列表数据
  drugstoreinfo: () => {
    return request({
      method: 'get',
      path: '/app/drugstoreinfo/ten/1',
    })
  },
  //学习视频
  Videom: () => {
    return request({
      method: 'get',
      path: '/app/getStudyVideo',
    })
  },
  //学习文章
  Articlem: () => {
    return request({
      method: 'get',
      path: '/app/getStudyArticle',
    })
  },
  //优秀病例
  Taskms: () => {
    return request({
      method: 'get',
      path: '/app/getTask',
    })
  },
  //对应药店药品数据
  Drug: (data) => {
    return request({
      method: 'get',
      path: '/app/getDrug/'+data.uid,
    })
  },
}