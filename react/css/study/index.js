import { 
    StyleSheet 
} from 'react-native'

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    studys: {
    },
    tabs: {
        width: '100%',
        backgroundColor: '#fff',
	    height: 50,
	    borderBottomWidth: 1,
	    borderBottomColor: '#eee',
	    flexDirection: 'row',
	    justifyContent: 'space-around',
	    alignItems: 'center'
    },
    headeritem: {
    	width: '33.3%',
    	alignItems: 'center',
	    justifyContent: 'center',
	    height: 30
    },
    headeritemborder: {
    	borderRightWidth: 1,
    	borderRightColor: '#eee'
    },
    drugstoreimg: {
        width: 150,
        height: 120,
        marginTop: 10,
        borderRadius: 8,
        marginLeft: 6, 
    },
    content: {
        marginLeft:4,
        marginTop:4,
        marginRight:4,
        flexDirection:'row',
        backgroundColor: '#fff',
        height:140,
    },
    info: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:12,
        marginTop:-20,
    },
    articletitle: {
        color: '#2fa4e7',
        fontSize: 16,
        padding:10,
    },
    casetitle:{
    	justifyContent: 'center',
        alignItems: 'center',
        color: '#2fa4e7',
        fontSize: 16,
        padding:10,
    },
    caseinfo: {
    	width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:-20,
    },
});