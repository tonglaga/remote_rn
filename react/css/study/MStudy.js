import { 
    StyleSheet,
    Dimensions
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    studys: {
    	height:height-122,
    },
    tabs: {
    	flex:1,
	    borderBottomWidth: 1,
	    borderBottomColor: '#eee',
	    position:'absolute',
    },
    contents: {
    	// flex:6,
    	// height:height-122,
    	// backgroundColor: '#fff'
    },
    coverPhoto: {
    	flex:4
    },
    headeritem: {
    	width:50,
    	alignItems: 'center',
	    justifyContent: 'center',
	    height: (height-122)/3,
        backgroundColor: '#fff',
    },
    headeritemborder: {
    	borderBottomWidth: 1,
    	borderBottomColor: '#ccc'
    },
    drugstoreimg: {
        width: 70,
        height: 70,
        marginTop: 10,
        borderRadius: 8,
        marginLeft: 5, 
    },
    content: {
        marginLeft:4,
        marginTop:4,
        marginRight:4,
        backgroundColor: '#fff',
        height:height-122,
    	width:80,
    },
    info: {
    	// flex:3,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        padding:8,
    },
    articletitle: {
    	color: '#2fa4e7',
        fontSize: 16,
    	position: 'relative',
    	left:20/2-(height-222)/2,
    	top:(height-222)/2-20/2,
    	width:height-222,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    reading: {
    	position: 'relative',
    	left:20/2-(height-222)/2,
        top:(height-222)/2-20/2,
    	width:height-222,
    	fontSize:13,
    	fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    casetitle:{
    	color: '#2fa4e7',
        fontSize: 16,
    	position: 'relative',
        left:20/2-(height-145)/2,
        top:(height-145)/2-20/2,
    	width:height-145,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    cause: {
    	position: 'relative',
        left:20/2-(height-145)/2,
        top:(height-145)/2-20/2,
    	width:height-145,
    	fontSize:13,
    	fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    caseinfo: {
    	width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:-20,
    },
    video: {
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    article: {
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    task: {
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
});