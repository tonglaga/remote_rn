import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    headerText:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    content: {
    	backgroundColor: '#fff',
    	margin:4,
    	borderRadius:6,
    	padding:6,
    },
	infors: {
	    flexDirection:'row',
	    justifyContent : 'space-between',
	    paddingTop:5,
	},
	title: {
	 	fontWeight: '600',
	 	fontSize:16,
	 	color: '#000',
	 	alignSelf: 'center'
	},
	coverImage: {
		height:180,
		width:'80%',
	 	alignSelf: 'center',
	 	marginTop:15,
	 	marginBottom:15,
	 	borderRadius:6,
	}
});