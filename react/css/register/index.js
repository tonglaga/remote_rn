import { 
    StyleSheet 
} from 'react-native'

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    loginInformation: {
    	flexDirection:'row',
    	backgroundColor: '#fff',
    	height:55,
    	justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#ccc',
        borderBottomWidth:1,
    },
    loginText: {
    	paddingLeft:20,
    },
    LoginInput: {
    	flex:3,
    },
    verificationInput: {
    	flex:1,
    },
    handles: {
    	backgroundColor: '#fff',
    	marginTop: 12,
    	paddingTop:15,
    	paddingBottom:15,
    },
    loginbtn: {
    	justifyContent: 'center',
        alignItems: 'center',
    },
    verificationBtn: {
    	color:'#2fa4e7'
    },
    loginbtnText: {
    	backgroundColor: '#1e6ec3',
    	color: '#fff',
    	paddingLeft:'40%',
    	paddingRight: '40%',
    	paddingTop:8,
    	paddingBottom: 8,
    	borderRadius:6,
    },
    handle: {
    	flexDirection:'row',
    	justifyContent: 'center',
        alignItems: 'center',
    },
    registerText: {
    	color: '#1e6ec3',
    	padding:12,
    	paddingTop:18,
    	paddingLeft:30,
    },
    forgotpasswordText: {
    	color: '#1e6ec3',
    	padding:12,
    	paddingTop:18,
    	paddingRight:30,
    },
    uprightText: {
    	paddingTop:6,
    },
    Mongolia: {
        fontFamily:'MongolianWhitePuaMirror',
    }
});
