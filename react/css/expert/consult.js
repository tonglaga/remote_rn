import { 
    StyleSheet 
} from 'react-native'

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    }, 
    consultcontent: {
        backgroundColor: '#fff',
        padding:6,
        paddingBottom:20,
    },
    consultinput: {
        borderRadius:6,
        borderColor: 'gray', 
        borderWidth: 1,
        backgroundColor: '#f7f7f7',
    },
    submitbtn: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:20,
    },
    submitbtnText: {
        backgroundColor: '#1e6ec3',
        color: '#fff',
        paddingLeft:'40%',
        paddingRight: '40%',
        paddingTop:8,
        paddingBottom: 8,
        borderRadius:6,
    },
    prompt: {
        padding:3,
        textAlign:'right'
    }
});
