import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    titleBox: {
	    width: '100%',
	    height: height-25,
	 },
	 titleCommon: {
	 	flex:1,
	 	alignItems: 'center',
	 	paddingTop:90,
	 },
	 headerImg: {
	 	borderRadius:100,
	 	height:100,
	 	width:100
	 },
	 name:{
	 	color: '#fff',
	 	padding:15,
	 	fontSize:18,
	 	fontWeight: '600',
	 },
	 operates: {
	 	flexDirection:'row',
	 	paddingTop:150,
	 },
	 connect: {
	 	backgroundColor: 'green',
	 	padding:20,
	 	paddingLeft:16,
	 	paddingRight:16,
	 	borderRadius:100,
	 	color: '#fff'
	 },
	 hangup: {
	 	backgroundColor: 'red',
	 	padding:20,
	 	paddingLeft:15,
	 	paddingRight:15,
	 	borderRadius:100,
	 	color: '#fff',
	 	marginLeft:120,
	 },
});