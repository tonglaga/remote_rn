import { 
    StyleSheet,
    Dimensions
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    experts: {
        marginTop: 4,
        backgroundColor: '#fff',
        height: 145,
        flexDirection:'row',
    },
    portrait: {
        flex:3
    },
    infomation: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
    },
    expertintro: {
        flex:2,
        justifyContent: 'center',
    },
    expertimg: {
        width: 150,
        height: 120,
        marginTop: 10,
        borderRadius: 8,
        marginLeft: 6, 
    },
    dealing: {
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
        height:70,
    },
    handle: {
        flexDirection:'row',
        borderTopWidth:1,
        borderColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn: {
        height:25,
        width:25,
    },
    status: {
        position: 'absolute',
        zIndex: 99,
        right: 0,
        top: -40,
        width: 42,
        color: '#FFF',
        padding: 5,
        borderRadius: 8,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    online: {
        backgroundColor: '#1b9e1b',
    },
    offline: {
        backgroundColor: 'gray'
    },
    busy: {
        backgroundColor: '#dc2424'
    },
    name: {
        flex:1,
        color: '#2fa4e7',
        fontSize: 16,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    }, 
    department: {
        flex:1,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    video: {
        padding:5
    },
    phone: {
        padding:5
    },
    message: {
        padding:5
    }, 
    expertinfomation: {
        marginTop:12,
        height:height-235,
    },
    expertinformationTop: {
        position:'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        height:height-235,
        width:35,
        backgroundColor: '#fffddd',
        borderRightColor: '#ffe5b6',
        borderRightWidth: 1,
    },
    Einformation: {
        position:'relative',
        flexDirection:'row',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        marginLeft:40,
    },
    dealingP: {

        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    }
});
