import { 
    StyleSheet 
} from 'react-native'

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    experts: {
        marginTop: 4,
        backgroundColor: '#fff',
        height: 145,
        flexDirection:'row',
    },
    portrait: {
        flex:3
    },
    infomation: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    expertintro: {
        flex:2,
        justifyContent: 'center',
        // alignItems: 'center',
    },
    expertimg: {
        width: 150,
        height: 120,
        marginTop: 10,
        borderRadius: 8,
        marginLeft: 6, 
    },
    dealing: {
        flexDirection:'row',
        padding:5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    handle: {
        flexDirection:'row',
        borderTopWidth:1,
        borderColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn: {
        height:25,
        width:25,
    },
    status: {
        position: 'absolute',
        zIndex: 99,
        right: 8,
        top: -46,
        width: 24,
        color: '#FFF',
        padding: 5,
        borderRadius: 8,
    },
    online: {
        backgroundColor: '#1b9e1b',
    },
    offline: {
        backgroundColor: 'gray'
    },
    busy: {
        backgroundColor: '#dc2424'
    },
    name: {
        color: '#2fa4e7',
        fontSize: 16,
    },
    department: {

    },
    video: {
        padding:5
    },
    phone: {
        padding:5
    },
    message: {
        padding:5
    },
    expertinfomation: {
        backgroundColor: '#fff',
        marginTop:12,
    },
    expertinformationTop: {
        justifyContent: 'center',
        alignItems: 'center',
        height:35,
        backgroundColor: '#fffddd',
        borderBottomColor: '#ffe5b6',
        borderBottomWidth: 1,
    },
    Einformation: {
        padding:12,
    }
});
