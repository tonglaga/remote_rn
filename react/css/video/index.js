import { 
    StyleSheet 
} from 'react-native'

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    handles: {
    	backgroundColor: '#fff',
    	marginTop: 12,
    	paddingTop:15,
    	paddingBottom:15,
    },
    loginbtn: {
        
    	justifyContent: 'center',
        alignItems: 'center',
    },
    verificationBtn: {
    	color:'#2fa4e7'
    },
    loginbtnText: {
        width:'100%',
        height:50, 
    	backgroundColor: '#f00',
    	color: '#fff',
        textAlign:'center',
        fontSize:16,
    	textAlignVertical:'center',
    },
});
