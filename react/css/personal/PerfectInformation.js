import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    headerText:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    }, 
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600',
        marginLeft:-30,
    },
    informations: {
    	padding:8,
    },
    information: {
    	flexDirection:'row',
    	height:35,
    	justifyContent: 'center',
        alignItems: 'center',
        marginTop:2,
        marginBottom:2,
    },
    nameText: {
    	flex:2,
    	alignItems: 'center',
    },
    nameInput: {
    	flex:6,
    	backgroundColor: '#fff',
    	padding:2,
    	borderRadius:3,
    	paddingLeft:8,
    },
    infor: {
    	height:60,
    	flexDirection:'row',
    	justifyContent: 'center',
        alignItems: 'center',
        marginTop:4,
        marginBottom:3,
    },
    inforInput: {
    	flex:6,
    	backgroundColor: '#fff',
    	padding:5,
    	borderRadius:3,
    	paddingLeft:8,
    },
    IDCard: {
    	height:90,
    	flexDirection:'row',
    	justifyContent: 'center',
        alignItems: 'center',
        marginTop:3,
        marginBottom:3,
    },
    headerPic: {
        height:70,
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:3,
        marginBottom:3,
    },
    headerImage: {
        width: 60,
        height: 60,
        borderRadius: 50,
    },
    headerImageContainer: {
        flex:6,
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 10,
    },
    IdCardImage: {
        width: 110,
        height: 80,
    }
});