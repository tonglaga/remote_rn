import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    headerText:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    titleBorder: {
    	borderBottomWidth:1,
    	borderBottomColor:'#ccc'
    },
    title: {
    	alignSelf: 'center',
    	height:35,
    	lineHeight:30,
    },
    content: {
    	padding:10,
    	borderTopWidth:1,
    	borderTopColor:'#fff'
    }
});