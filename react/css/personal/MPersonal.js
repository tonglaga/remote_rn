import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    titleBox: {
	    width: '100%',
	    height: 180,
	    flexDirection:'row',
	 },
	 titleCommon: {
	 	flex:1,
	 	justifyContent: 'center',
	 	alignItems: 'center',
	 },
	 login: {
	 	backgroundColor: '#2685c9',
	 	color: '#fff',
	 	paddingLeft:12,
	 	paddingRight:12,
	 	paddingTop:8,
	 	paddingBottom:3,
	 	borderColor: '#f3f8fc',
	 	borderWidth:1,
	 	borderRadius:6,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
	 },
	 leave: {
	 	backgroundColor: '#2685c9',
	 	color: '#fff',
	 	paddingLeft:12,
	 	paddingRight:12,
	 	paddingTop:8,
	 	paddingBottom:3,
	 	borderColor: '#f3f8fc',
	 	borderWidth:1,
	 	borderRadius:6,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
	 },
	 headerImg: {
	 	borderRadius:100,
	 	height:100,
	 	width:100
	 },
	 personalList: {
	 	height:height-270,
	 	marginTop:15,
	 	backgroundColor: '#fff',
	 	flexDirection:'row',
	 	paddingTop:8,
	 },
	 lists: {
	 	flex:1,
	 	alignItems: 'center',
	 },
	 modelIcon: {
	 	height: 50,
	 	width:50,
	 },
	 borderRi: {
	 	borderRightWidth: 1,
	 	borderRightColor: '#ccc'
	 },
	 modelText: {
	 	padding:10,
	 	marginTop:100,
	 	width:height-400,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
	 }
});