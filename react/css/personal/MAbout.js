import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
	 personalList: {
	 	height:height-82,
	 	// backgroundColor: '#fff',
	 	flexDirection:'row',
	 	
	 },
	 lists: {
	 	flex:1,
	 	alignItems: 'center',
	 	backgroundColor: '#fff',
	 	margin:5,
	 	paddingTop:16,
	 },
	 modelIcon: {
	 	height: 50,
	 	width:50,
	 },
	 borderRi: {
	 	borderRightWidth: 1,
	 	borderRightColor: '#ccc'
	 },
	 modelText: {
	 	padding:10,
	 	marginTop:100,
	 	width:height-400,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
	 }
});