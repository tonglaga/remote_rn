import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    titleBox: {
	    width: '100%',
	    height: 180,
	    flexDirection:'row',
	 },
	 titleCommon: {
	 	flex:1,
	 	justifyContent: 'center',
	 	alignItems: 'center',
	 },
	 login: {
	 	backgroundColor: '#2685c9',
	 	color: '#fff',
	 	paddingTop:12,
	 	paddingBottom:12,
	 	width:30,
	 	paddingLeft:8,
	 	paddingRight:3,
	 	borderColor: '#f3f8fc',
	 	borderWidth:1,
	 	borderRadius:6,
	 },
	 leave: {
	 	backgroundColor: '#2685c9',
	 	color: '#fff',
	 	paddingTop:12,
	 	paddingBottom:12,
	 	width:30,
	 	paddingLeft:8,
	 	paddingRight:3,
	 	borderColor: '#f3f8fc',
	 	borderWidth:1,
	 	borderRadius:6,
	 },
	 headerImg: {
	 	borderRadius:100,
	 	height:100,
	 	width:100
	 },
	 personalList: {
	 	height:height-270,
	 	marginTop:15,
	 	backgroundColor: '#fff',
	 	flexDirection:'row',
	 	paddingTop:8,
	 },
	 lists: {
	 	flex:1,
	 	alignItems: 'center',
	 },
	 modelIcon: {
	 	height: 50,
	 	width:50,
	 },
	 borderRi: {
	 	borderRightWidth: 1,
	 	borderRightColor: '#ccc'
	 },
	 modelText: {
	 	padding:10,
	 	width:38,
	 }
});