import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    headerText:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
	 messages: {
	 	marginTop:8,
	 },
	 message: {
	 	backgroundColor: '#fff',
	 	padding:12,
	 	margin:4,
	 	borderRadius:6,
	 },
	 title: {
	 	fontWeight: '600',
	 	fontSize:16,
	 	color: '#000',
	 },
     dtitle: {
        alignSelf: 'center',
        fontWeight: '600',
        fontSize:16,
        color: '#000',
     },
     dreading: {
        paddingRight:18,
     },
     infors: {
        flexDirection:'row',
        paddingTop:5,
     },
     dcontent: {
        paddingTop:15,
     },
     tabs: {
        width: '100%',
        backgroundColor: '#fff',
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    coverImage: {
        height:50,
        flex:1,
    },
    listTop: {
        flexDirection: 'row',
    },
    articleTitle: {
        flex:5,
        fontWeight: '600',
        fontSize:16,
        color: '#000',
        paddingLeft:10,
        paddingRight:10,
        lineHeight:24,
    }
});