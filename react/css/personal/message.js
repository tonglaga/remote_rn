import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    headerText:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600',
        marginLeft:-33,
    },
	 messages: {
	 	marginTop:8,
	 },
	 message: {
	 	backgroundColor: '#fff',
	 	padding:12,
	 	margin:4,
	 	borderRadius:6,
	 },
	 title: {
	 	fontWeight: '600',
	 	fontSize:16,
	 	color: '#000',
	 },
     dtitle: {
        alignSelf: 'center',
        fontWeight: '600',
        fontSize:16,
        color: '#000',
     },
     dcontent: {
        paddingTop:15,
     },
     time:{
        color:'#aaa',
        fontSize:13,
        alignSelf:'flex-end'
     },
});