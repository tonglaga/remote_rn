import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    headerText:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600',
        marginLeft:-30,
    },
	list: {
	 	flexDirection:'row',
	 	backgroundColor: '#fff',
	 	marginTop:13,
	 	padding:5,
	},
	modelIcon: {
	 	height: 40,
	 	width:40,
	},
	modelText: {
	 	lineHeight:30,
	 	paddingLeft:15,
	}
});