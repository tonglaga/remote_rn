import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    headerText:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600',
        marginLeft:-30,
    },
	 messages: {
	 	marginTop:8,
	 },
	 message: {
	 	backgroundColor: '#fff',
	 	padding:10,
	 	margin:1,
	 	flexDirection:'row',
	 	justifyContent: 'space-between',
	 },
	 title: {
	 	color: '#333', 
	 },
	 time: {
	 	color: '#aaa',
	 	fontSize:13,
	 },
     dtitle: {
        alignSelf: 'center',
        fontWeight: '600',
        fontSize:16,
        color: '#000',
     },
     messageContent: {
        backgroundColor: '#fff',
        padding:10,
     },
     infors: {
        alignSelf: 'flex-end',
     },
     dcontent: {
        padding:5,
        lineHeight:20,
     },
     informations: {
        padding:8,
    },
    information: {
        flexDirection:'row',
        height:35,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:2,
        marginBottom:2,
    },
    nameText: {
        flex:2,
        alignItems: 'center',
    },
    nameInput: {
        flex:6,
        padding:5,
        borderRadius:3,
        borderWidth:1,
        borderColor: '#ccc'
    },
    infor: {
        height:60,
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:4,
        marginBottom:3,
    },
    inforInput: {
        flex:6,
        padding:5,
        borderRadius:3,
        borderWidth:1,
        borderColor: '#ccc'
    },
    coverImage: {
        flex:6,
    },
    headerPic: {
        height:70,
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:3,
        marginBottom:3,
    },
    headerImageContainer: {
        flex:6,
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 10,
    },
    IdCardImage: {
        width: 60,
        height: 60,
        borderRadius: 8,
    },
    referbtn: {
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: '#3385ff',
        width:'80%',
        borderRadius: 3,
        marginTop:10,
    },
    refer: {
        padding:6,
        color: '#fff',
    }
});