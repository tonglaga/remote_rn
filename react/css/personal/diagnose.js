import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    headerText:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600',
        marginLeft:-30,
    },
	 messages: {
	 	marginTop:8,
	 },
	 message: {
	 	backgroundColor: '#fff',
	 	padding:12,
	 	margin:1,
	 	
	 },
	 title: {
	 	fontWeight: '600',
	 	fontSize:16,
	 	color: '#000',
	 },
     dtitle: {
        alignSelf: 'center',
        fontWeight: '600',
        fontSize:16,
        color: '#000',
     },
     dreading: {
        paddingRight:18,
     },
     infors: {
        flexDirection:'row',
        paddingTop:5,
     },
     dcontent: {
        paddingTop:15,
     },
     tabs: {
        width: '100%',
        backgroundColor: '#fff',
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    listTop: {
    	flexDirection:'row',
        marginTop:4,
    },
    causesLabel: {
        color: '#333',
    },
    infors: {
    	alignSelf: 'flex-end',
    },
    phoneNumber: {
    	fontSize: 12,
    	color: '#aaa'
    },
    prescription: {
        backgroundColor: '#fff',
        margin: 4,
        borderRadius:6,
        padding:6,
        width:width,
        // height:height-80,
    },
    medicinal: {
        paddingLeft:15,
    },
    recommend: {
        backgroundColor: '#4395ff',
        color: '#fff',
        alignSelf: 'flex-start',
        padding:4,
        borderRadius:3,
    },
    prescript: {
        borderWidth:1,
        borderColor:'#4395ff',
        margin:4,
        padding:6,
    },
    drugstore: {
        marginTop:8,
    },
    btnStyle: {
        borderWidth:1,
        borderColor:'#ccc',
        width:120,
    },
    selectStyle: {
        borderWidth:1,
        borderColor:'#ccc',
        width:120,
        marginTop:1,
        marginLeft:-1,
    },
    labeltext: {
        flex:2,
    },
    infor: {
        flex:6,
        padding:5,
    },
    inputBorder: {
        borderWidth:1,
        borderColor:'#ccc',
    },
    border: {
        borderBottomWidth:1,
        borderBottomColor:'#ccc',
        padding:8,
    },
    label:{
        width:80
    },
    labelText:{
        width:width-82
    }
});