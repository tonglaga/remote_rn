import { 
    Dimensions,
    StyleSheet 
} from 'react-native'
// const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    banner:{
        // height:width * 40 / 78,
        backgroundColor: '#9bebe5',
    },
    wrpaper: {
        // width: width,
        // height:width * 40 / 78,
    },
    paginationStyle: {
        bottom: 6,
    },
    dotStyle: {
        width: 22,
        height: 3,
        backgroundColor: '#fff',
        opacity: 0.4,
        borderRadius: 0,
    },
    activeDotStyle: {
        width: 22,
        height: 3,
        backgroundColor: '#fff',
        borderRadius: 0,
     },
    bannerImg: {
        width: '100%',
        height: 200,
        flex: 1
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft:80,
    },
    language: {
        flex:2,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    languageText: {
        fontSize:14,
        color: '#FFF',
    },
    Models: {
        backgroundColor:'#FFF',
        height:72,
        marginTop: 15,
        flexDirection:'row',
    },
    Model: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
        padding:20,
    },
    modelText: {
        fontFamily:'MongolianWhite',
        transform:[
			{rotateZ:'90deg'}
		],
    },
    MImg: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    Mmtext: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    MText: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    Micon: {
        height:36,
        width:36,
    },
    experts: {
        backgroundColor: '#fff',
        marginTop:15,
        height: 180,
        flexDirection:'row',
    },
    expertText: {
        height:180,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
    },
    Etitle: {
    	fontFamily:'MongolianWhite',
        transform:[
			{rotateZ:'90deg'}
		],
		height:14,
    },
    expertimg: {
        flexDirection:'row',
        left:28,
    },
    expertF: {
        padding:5,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    expertSc: {
        padding:5,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    expertT: {
        padding:5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    Eimage: {
        height:145,
        width:100,
        borderRadius:6,
    },
    name: {
        position: 'absolute',
        zIndex: 99,
        right: -60,
        top: -130,
        backgroundColor: '#000',
        opacity: 0.5,
        color: '#FFF',
        padding: 5,
        fontFamily:'MongolianWhite',
        transform:[
			{rotateZ:'90deg'}
		],
    },
    player: {
        position: 'absolute',
        zIndex: 10,
        left:35,
        top: -85,
        opacity: 0.5,
        width:35,
        height:35,
    }

})