import { 
    StyleSheet 
} from 'react-native'

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    drugstore: {
        marginTop: 4,
        backgroundColor: '#fff',
        height: 145,
        flexDirection:'row',
    },
    storename: {
        color: '#2fa4e7',
        fontSize: 16,
        fontWeight: '600'
    },
    drugstoreimg: {
        width: 150,
        height: 120,
        marginTop: 10,
        borderRadius: 8,
        marginLeft: 6, 
    },
    drugstoreintro: {
        paddingLeft:15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    medicinalList: {
    	backgroundColor: '#fff',
    	marginTop:12,
    	// width: '100%',
    },
    medicinalListTop: {
    	justifyContent: 'center',
        alignItems: 'center',
        height:35,
        backgroundColor: '#fffddd',
        borderBottomColor: '#ffe5b6',
        borderBottomWidth: 1,
    },
    medicinals: {
    	padding:8,
    	flexDirection:'row',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    medicinalpadding: {
    	paddingTop: 15,
    },
    medicinal: {
    	width: '50%',
    	justifyContent: 'center',
        alignItems: 'center',
    },
    medicinalImg: {
    	width:'80%',
    	height:98,
        borderRadius:8,
    },
    medicinalname: {
    	color: '#000',
        fontSize: 16,
        fontWeight: '600'
    },
    numControllStyle: {
        flexDirection:'row',
        marginTop:12,
        alignItems:'flex-end'
    },
    numberStyle: {
        fontSize:18,
        padding:3,
        borderColor: '#ccc',
        borderWidth: 1,
        paddingLeft:10,
        paddingRight:10,
        borderLeftWidth:0,
        borderRightWidth:0,
    }
});
