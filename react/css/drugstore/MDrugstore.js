import { 
    StyleSheet,
    Dimensions
} from 'react-native'
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
	container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    backImg: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:8,
        paddingRight:8,
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    drugstore: {
        marginTop: 4,
        backgroundColor: '#fff',
        height: 145,
        flexDirection:'row',
    },
    storename: {
        color: '#2fa4e7',
        fontSize:16,
        fontFamily:'MongolianWhitePuaMirror',
        transform:[
            {rotateX:'180deg'},
            {rotateZ:'270deg'}
        ],
    },
    drugstoreimg: {
        width: 150,
        height: 120,
        marginTop: 10,
        borderRadius: 8,
        marginLeft: 6, 
    },
    drugstoreintro: {
        paddingLeft:15,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row',
    },
    medicinalList: {
    	// backgroundColor: '#fff',
    	marginTop:12,
        height:height-235,
    },
    medicinalListTop: {
        position:'absolute',
    	justifyContent: 'center',
        alignItems: 'center',
        height:height-235,
        width:35,
        backgroundColor: '#fffddd',
        borderRightColor: '#ffe5b6',
        borderRightWidth: 1,
    },
    medicinals: {
        position:'relative',
    	// padding:8,
    	flexDirection:'row',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        marginLeft:40,
    },
    medicinalpadding: {
    	paddingTop: 15,
        flexDirection:'row',
    },
    medicinal: {
    	width: 80,
        height:height-235,
        backgroundColor: '#fff',
        padding:5,
        margin:3,
    	// justifyContent: 'center',
     //    alignItems: 'center',
    },
    medicinalImg: {
    	width:'100%',
    	height:70,
        borderRadius:8,
    },
    medicinalname: {
    	color: '#000',
        fontSize: 16,
        fontWeight: '600'
    },
    numControllStyle: {
        flexDirection:'row',
        marginTop:12,
        alignItems:'flex-end'
    },
    numberStyle: {
        fontSize:18,
        padding:3,
        borderColor: '#ccc',
        borderWidth: 1,
        paddingLeft:10,
        paddingRight:10,
        borderLeftWidth:0,
        borderRightWidth:0,
    }
});
