import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    DeviceEventEmitter,
    TouchableOpacity,
    ToastAndroid,
    ImageBackground
} from 'react-native';
import PersonalCSS from '../../css/personal';
import { getToken } from '../../utils/token'
import { removeToken } from '../../utils/token'
import { getUsername,getPassword,getHeadImgUrl} from '../../utils/user'
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
        headerTitle: '个人',
        tabBarLabel: '个人',
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../img/gs.png')}
            style={{ width: 20, height: 20, tintColor: tintColor }}
          />
        )
    };
    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state
        this.state = {
            headImgUrl: require('../../img/a8.jpg'),
            loginedstatus:null,
            loginbtn:null,
            siginoutbtn:null,
            username:null,
        }
    }
    //跳转个人中心 
    gogo(){
       
        //判断token,失效重新登录
        getToken().then(token => {
           
            if(token !== null) {
               
                if(localStorage.type==1){
                    this.props.navigation.navigate('FCenter');
                   }else if(localStorage.type == 2){
                    this.props.navigation.navigate('ECenter');
                   }else if(localStorage.type == 3){
                    this.props.navigation.navigate('DCenter');
                 }
            }
        }).catch(err => {
           
            ToastAndroid.show('请先登录！', ToastAndroid.SHORT);
        })
    
       
    }
    componentDidMount() {
        //监听登陆完返回过来的数据
        DeviceEventEmitter.addListener('headImgUrl', (message) => {
            this.getMessage(message); 
            
        })

        //判断用户登录状态
        getToken().then(token => {
            
            if(token !== null ) {
                    //获取头像
                    getHeadImgUrl().then(img => {
                    
                        this.setState({
                            headImgUrl:img
                        })
                    }).catch(err => {  
                    
                        this.setState({
                            headImgUrl:require('../../img/a8.jpg')
                        })
                    })
                    //获取用户名
                    getUsername().then(username => {
                    
                        this.setState({
                    
                            username:username
                        })
                    }).catch(err => {  
                        this.setState({
                            username:require('../../img/a8.jpg')
                        })
                    })
                    //设置已登录样式
                    this.setState({
                        loginedstatus:1,
                        siginoutbtn: <TouchableOpacity style={[PersonalCSS.titleCommon]}  onPress={() => this.siginout()}>
                        <Text style={[PersonalCSS.leave]}>退出</Text>
                    </TouchableOpacity>
                    })
                
               }
           }).catch(err => {
           
                    this.setState({
                        loginedstatus:0,
                        loginbtn: <TouchableOpacity style={[PersonalCSS.titleCommon]} onPress={() => this.siginin()}>
                    <Text style={[PersonalCSS.leave]}>登录</Text>
                    </TouchableOpacity>
                    })
            })
      
       
     
    }
    //跳转登陆
    siginin(){
        this.props.navigation.navigate('Login');
    }
    //退出登录
    siginout(){
        removeToken();
        this.setState({
            siginoutbtn: 0,
            siginoutbtn: null,
            loginbtn: <TouchableOpacity style={[PersonalCSS.titleCommon]} onPress={() => this.siginin()}>
            <Text style={[PersonalCSS.leave]}>登录</Text>
            </TouchableOpacity>
        })
    }
    getMessage(message) {
       // console.warn(message)
        this.setState({
            headImgUrl: message,
            siginoutbtn: <TouchableOpacity style={[PersonalCSS.titleCommon]} onPress={() => this.siginout()}>
            <Text style={[PersonalCSS.leave]}>退出</Text>
        </TouchableOpacity>,
           loginbtn: null
        })
       
    }
    //跳转消息中心
      goMessage(){
        //判断token,失效重新登录
        getToken().then(token => {
            if(token !== null) {
             this.props.navigation.navigate('Message');
                
            }
        }).catch(err => {
            ToastAndroid.show('请先登录！', ToastAndroid.SHORT);
        })
    
       
    }
     //跳转收藏中心
     goCollect(){
        //判断token,失效重新登录
        getToken().then(token => {

            if(token !== null) {
                    this.props.navigation.navigate('Collect');          
            }

        }).catch(err => {
            ToastAndroid.show('请先登录！', ToastAndroid.SHORT);
           // this.props.navigation.navigate('Login');
        })
    
       
    }
    render() {
        
        const { navigate } = this.props.navigation;
       
    
        return (
            <ScrollView>
                <View style={PersonalCSS.container}>
                    <ImageBackground
                        source={require('../../img/bg-top.png')}
                        style={[PersonalCSS.titleBox]}
                      >
                        <View style={[PersonalCSS.titleCommon]}>
                            {this.state.loginbtn}
                        </View>
                        <View style={[PersonalCSS.titleCommon]}>
                          <Image source={this.state.headImgUrl} style={[PersonalCSS.headerImg]} />
                           <Text style={{color:'#fff'}}>{this.state.username}</Text>
                        </View>
                        <View style={[PersonalCSS.titleCommon]}>
                            {this.state.siginoutbtn}
                        </View>
                    </ImageBackground>
                    <View  style={PersonalCSS.personalList}>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]} onPress={() =>this.gogo()}>
                            <Image source={require('../../img/center.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>中心</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]}  onPress={() =>this.goMessage()}>
                            <Image source={require('../../img/message.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>消息</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]} onPress={() => this.goCollect()}>
                            <Image source={require('../../img/collect.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>收藏</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists]} onPress={() => navigate('About')}>
                            <Image source={require('../../img/about.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>平台</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
