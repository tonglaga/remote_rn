import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    ToastAndroid,
    DeviceEventEmitter
} from 'react-native';
import api from '../../../api'
import LoginCSS from '../../../css/register';
export default class Expert extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state;
        this.state = {
        form:{
            username:params.tel,
            code: params.code,
            pass:""
         }
        }
    }
    submit(){
        if(this.state.form.pass == ""){
            ToastAndroid.show('密码不能为空', ToastAndroid.SHORT);
            return; 
        }
        
        api.user.ChangPassword(this.state.form).then(response => {
            //console.warn(response)  
            if(response.code == 401){
               ToastAndroid.show(response.message, ToastAndroid.SHORT);
               //DeviceEventEmitter.emit('register', {username:this.state.form.username,pass:this.state.form.pass})
              // this.props.navigation.goBack(null);
            }else if(response.code == 400){
                ToastAndroid.show(response.message, ToastAndroid.SHORT);
            }else{
               ToastAndroid.show("修改成功，重新登录", ToastAndroid.SHORT);
              // DeviceEventEmitter.emit('register', {username:this.state.form.username,pass:this.state.form.pass})
               this.props.navigation.navigate('login');
            }
           
       })  
    }
    render() {
        const { navigate } = this.props.navigation;
        const {pass} = this.state.form;
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}>设置密码</Text>
                        </View>         
                    </View>
                    <View>
                        <Text style={[LoginCSS.loginText,{paddingLeft:0,padding:10,color:'#aaa',fontSize:12,}]}>新密码：</Text>
                        <View style={[LoginCSS.loginInformation]}>
                            <TextInput
                                 placeholder="请输入新密码"
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput]}
                                value={pass} onChangeText={(value)=>{
                                    let form = this.state.form
                                    form.pass = value
                                    this.setState({
                                   
                                        form: form
                                       
                                    })
                                  }}
                            ></TextInput>
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]}  onPress={() => this.submit()}>
                                <Text style={[LoginCSS.loginbtnText]}>确定</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}