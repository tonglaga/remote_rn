import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    ToastAndroid,
    DeviceEventEmitter
} from 'react-native';
import LoginCSS from '../../../css/register';
export default class Expert extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };
    
  
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        this.state = {
            
            form: {
                username: '', 
            }
        }
       
     
    }
    render() {
        const { navigate } = this.props.navigation;
        const {username} = this.state.form;
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}>短信验证</Text>
                        </View>         
                    </View>
                    <View>
                        <Text style={[LoginCSS.loginText,{paddingLeft:0,padding:10,color:'#aaa',fontSize:12,}]}>为保护您的账户安全，请输入完整的注册手机号码</Text>
                        <View style={[LoginCSS.loginInformation]}>
                            <TextInput
                                 placeholder="请输入手机号"
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput]}
                                value={username} onChangeText={(value)=>{
                                    let form = this.state.form
                                       form.username = value
                                    this.setState({
                                   
                                        form: form
                                       
                                    })
                                  }}
                            ></TextInput>
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]} onPress={() => navigate('Verification',{tel:this.state.form.username})}>
                                <Text style={[LoginCSS.loginbtnText]}>下一步</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}