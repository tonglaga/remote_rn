import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    ToastAndroid,
    DeviceEventEmitter
} from 'react-native';
import LoginCSS from '../../../css/register';
import api from '../../../api'
import CountDownButton from 'react-native-smscode-count-down'
export default class Expert extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state;
        this.state = {
        form:{
            username:params.tel,
            code: "",
         }
        }
    }
    render() {
        const {username} = this.state.form;
        const { navigate } = this.props.navigation;
        const {code} = this.state.form;
    
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}>短信验证</Text>
                        </View>         
                    </View>
                    <View>
                        <Text style={[LoginCSS.loginText,{paddingLeft:0,padding:10,color:'#aaa',fontSize:12,}]}>您的注册手机号码是{username}，请点击“获取验证码”</Text>
                        <View style={[LoginCSS.loginInformation]}>
                            <TextInput
                                 placeholder="请输入验证码"
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput,{flex:3}]}
                                value={code} onChangeText={(value)=>{
                                    let form = this.state.form
                                       form.code = value
                                    this.setState({
                                   
                                        form: form
                                       
                                    })
                                  }}
                            ></TextInput>
                            <TouchableOpacity style={{flex:2}}>
                            <CountDownButton
                                style={{width: 110,marginRight: 10}}
                                textStyle={{color: 'blue'}}
                                enable={username != ""}
                                timerCount={60}
                                timerTitle={'点击获取验证码'}
                                timerActiveTitle={['（','s）后重试']}
                                onClick={(shouldStartCounting)=>{
                                    //随机模拟发送验证码成功或失败
                                   // const requestSucc = Math.random() + 0.5 > 1; 
                                 
                                   api.user.sendMsg({tel:username}).then(response => {
                                     if(response.code == 400){
                                         ToastAndroid.show(response.message, ToastAndroid.SHORT);
                                        //DeviceEventEmitter.emit('register', {username:this.state.form.username,pass:this.state.form.pass})
                                      //  this.props.navigation.goBack(null);
                                      }
                                   })
                                   shouldStartCounting(true);
                                   //this._requestAPI(shouldStartCounting)
                                    
                                }}
                                timerEnd={()=>{
                                    this.setState({
                                        state: '倒计时结束'
                                    })
                                }}/>
                            </TouchableOpacity>
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]} onPress={() => navigate('NewPassword',{tel:username,code:code})}>
                                <Text style={[LoginCSS.loginbtnText]}>下一步</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}