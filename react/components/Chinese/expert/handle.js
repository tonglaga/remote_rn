import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import HandleCSS from '../../../css/expert/handle';
import api from '../../../api';
import config from '../../../config';
import Sound from 'react-native-sound';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {

    }; 

    constructor(props) {
        super(props);
      
        //播放铃声
        let demoAudio = require('../../../Video/Ringtone/Ringtone.mp3');//支持众多格式
        this.state = {
            img:null,
            name:null,
            loopingSound: new Sound(demoAudio, (e) => {
                this.state.loopingSound.play(() => this.state.loopingSound.release());
            }),
            data:30
        }
        //定时挂断功能
        this._index=30;
        this._timer=setInterval(()=>{this.setState({data:this._index--});
        if(this.state.data<=0){
            this.state.loopingSound.stop(() => this.state.loopingSound.release());
            this._timer && clearInterval(this._timer);
            api.categories.notwait({id:localStorage.roomid,'login-token':localStorage.token}).then(response => {
                this.props.navigation.goBack(null)
            })
          }
         },1000);
      } 
    componentDidMount() {
        this.getList()
    }
    //获取请求通话牧民名称和头像
    getList() {
        api.categories.newherdsman({hid:localStorage.hid,'login-token':localStorage.token}).then(response => {
         
          this.setState({
            name: response.name,
            img: config.img_url+response.headerUrl,
          })
        })
      }
    //点击接受或者挂断时发生的事件  
    getVideo(h) {
       
        api.categories.videoreturn({hid:localStorage.hid,'login-token':localStorage.token,ifOrNot:h}).then(response => {
        })
        //挂断音频播放
        this.state.loopingSound.stop(() => this.state.loopingSound.release());
        if(h==1){
            this.props.navigation.navigate('Videocall',{key:"'"+localStorage.roomurl+"'"}); 
          }else{
            this.props.navigation.navigate('Home'); 
          }
      }
    
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={HandleCSS.container}>
                    <ImageBackground
                        source={require('../../../img/waiting.png')}
                        style={[HandleCSS.titleBox]}
                      >
                        <View style={[HandleCSS.titleCommon]}>
                            <Image source={{uri:this.state.img}} style={[HandleCSS.headerImg]} />
                            <Text style={[HandleCSS.name]}>{this.state.name}</Text>
                            <Text style={{color:'#ffffff'}}>{this.state.data}秒以后自动挂断！</Text>
                            <View style={[HandleCSS.operates]}>
                                <TouchableOpacity style={[HandleCSS.operate]}  onPress={() =>this.getVideo(1)} >
                                    <Text style={[HandleCSS.connect]}>接通</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[HandleCSS.operate]} onPress={() =>this.getVideo(2)}>
                                <Text style={[HandleCSS.hangup]}>挂断</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ImageBackground>
                </View>
            </ScrollView>
        );
    }
}