import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import Sound from 'react-native-sound';
import WaitingCSS from '../../../css/expert/waiting';
import api from '../../../api';
import config from '../../../config';
export default class Personal extends Component {

    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
       
    }; 
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        //播放铃声
        let demoAudio = require('../../../Video/Ringtone/Ringtone.mp3');//支持众多格式
        this.state = {
            id:params.id,
            img:config.img_url+params.img,
            name: params.name,
            loopingSound: new Sound(demoAudio, (e) => {
                     
                if (e) {
                     console.warn('播放失败');
                    return;
                }
                this.state.loopingSound.play(() => this.state.loopingSound.release());
            }),
            data:30
        }
      
       
      
     
        //  this.setState({loopingSound: s});
          //定时挂断功能
          this._index=30;
          this._timer=setInterval(()=>{this.setState({data:this._index--});
          
           if(this.state.data<=0){
            this.state.loopingSound.stop(() => this.state.loopingSound.release());
              this._timer && clearInterval(this._timer);
              api.categories.notwait({id:localStorage.roomid,'login-token':localStorage.token}).then(response => {   
                  this.props.navigation.goBack(null)
                })
          }
         },1000);
      } 
      
      stopTime(){
          this._timer && clearInterval(this._timer);
      }  
      componentWillUnmount() {
        this._timer && clearInterval(this._timer);
      }  
      componentDidMount() {
        this.getList()
      }

      getList() {
         
        api.categories.videoconnects({eid:this.state.id,'login-token':localStorage.token,type:2}).then(response => {
         // console.warn(response)
          localStorage.roomurl=response.roomid; 
          localStorage.roomid=response.id; 
        })
      }
      notwait(){
        api.categories.notwait({id:localStorage.roomid,'login-token':localStorage.token}).then(response => {
            
          })
          this.state.loopingSound.stop(() => this.state.loopingSound.release());
            this.props.navigation.navigate('Home')
      }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={WaitingCSS.container}>
                    <ImageBackground
                        source={require('../../../img/waiting.png')}
                        style={[WaitingCSS.titleBox]}
                      >
                        <View style={[WaitingCSS.titleCommon]}>
                            <Image source={{uri:this.state.img}} style={[WaitingCSS.headerImg]} />
                            <Text style={[WaitingCSS.name]}>{this.state.name}</Text>
                            <Text style={{color:'#ffffff'}}>{this.state.data}秒以后自动挂断！</Text>
                            <View style={[WaitingCSS.operates]}>
                                <TouchableOpacity style={[WaitingCSS.operate]} onPress={() =>this.notwait()}>
                                <Text style={[WaitingCSS.hangup]}>挂断</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ImageBackground>
                </View>
            </ScrollView>
        );
    }
}