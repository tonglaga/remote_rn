import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import ConsultCSS from '../../../css/expert/consult';
import api from '../../../api';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
            form: {
                expertId:params.id,
                content: null
            }
        }
       
    }
    //提交文字
    submitbtnText(){
        //console.warn(localStorage.token)
        api.categories.appAddTask({'loken-token':localStorage.token,expertId:this.state.form.expertId,content:this.state.form.content}).then(response => {
            ToastAndroid.show('发送成功，耐心等待专家回复！', ToastAndroid.SHORT);
             this.props.navigation.goBack(null)
        })
    }
    
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={ConsultCSS.container}>
                    <View style={ConsultCSS.header}>
                        <TouchableOpacity style={ConsultCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ConsultCSS.title}>
                            <Text style={ConsultCSS.titleText}>咨询</Text>
                        </View>         
                    </View>
                    <View style={[ConsultCSS.consultcontent]}>
                        <TextInput
                            multiline={true}
                            placeholder="请输入要咨询的问题"
                            underlineColorAndroid='transparent'
                            numberOfLines={5}
                            maxLength ={100}
                            style={[ConsultCSS.consultinput]}
                            onChangeText={(text) => this.setState({text})}
                        />
                        <Text style={[ConsultCSS.prompt]}>最多可输入100字</Text>
                        <TouchableOpacity style={[ConsultCSS.submitbtn]}>
                            <Text style={[ConsultCSS.submitbtnText]} onPress={() => this.submitbtnText()}>提交</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}