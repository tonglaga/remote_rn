import React, { Component } from 'react';
import {

    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import DrugstoreCSS from '../../../css/drugstore'
import api from '../../../api';
import config from '../../../config';
export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，
    static navigationOptions = {
        
    };
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
                data:[],
                lists:[]
             
        }
       
    }
    componentDidMount() {
        this.getList()
    }
    
    getList() {
        api.grade.Drug({uid:this.state.con.uid}).then(response => {
         // console.warn(response)
          this.setState({
            data: response
          })
          this.getlisst();
        })
      }
    getlisst(){
        
        let list= [];
        let listt=[];
        let i=0;
        this.state.data.map((item,id) => {


            if(id >0 && id % 2 != 0){
             list[i]=<TouchableOpacity style={DrugstoreCSS.medicinal} key={id}  onPress={() =>this.props.navigation.navigate('Medicinal',{itm:item})}>
                <Image style={DrugstoreCSS.medicinalImg}  source={{uri:config.img_url+item.drugUrl}} />
                <Text>{item.title}</Text>
               </TouchableOpacity>;    
             i=0;
             listt[listt.length]=<View style={[DrugstoreCSS.medicinals, DrugstoreCSS.medicinalpadding]} >
               {
                   list.map((it,j)=>{
                       return(it);
                   })
               }

              </View>
                
            }else{
            list[i]=<TouchableOpacity style={DrugstoreCSS.medicinal} key={id}  onPress={() =>this.props.navigation.navigate('Medicinal',{itm:item})}>
            <Image style={DrugstoreCSS.medicinalImg}  source={{uri:config.img_url+item.drugUrl}} />
            <Text>{item.title}</Text>
           </TouchableOpacity>; 
           i++;
           }
           

        })
        this.setState({
            lists:listt
        })
        //console.warn(listt)
    }  
    render() {
        const { navigate } = this.props.navigation;
       
        return (
            <ScrollView style={DrugstoreCSS.mainStyle}>
                <View style={DrugstoreCSS.container}>
                    <View style={DrugstoreCSS.header}>
                        <TouchableOpacity style={DrugstoreCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DrugstoreCSS.title}>
                            <Text style={DrugstoreCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={DrugstoreCSS.drugstores}>
                        <View style={DrugstoreCSS.drugstore}>
                            <View style={DrugstoreCSS.portrait}>
                                <Image source={{uri:config.img_url+this.state.con.headerUrl}} style={DrugstoreCSS.drugstoreimg} />
                            </View>
                            <View style={DrugstoreCSS.drugstoreintro}>
                                <Text style={DrugstoreCSS.storename}>{this.state.con.pharmacyName}</Text>
                                <Text>{this.state.con.provinceName} {this.state.con.cityName} {this.state.con.areaName}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={DrugstoreCSS.medicinalList}>
                        <View style={DrugstoreCSS.medicinalListTop}>
                            <Text>该店药品</Text>
                        </View>
                       
                      {
                  
                        this.state.lists.map((item,id)=>{
                            return(
                                <View key={id}>{item}</View> 
                            )
                        })
                          
                         //id=id+1;

                         // console.warn(config.img_url+item.headerUrl);

                         // var img=config.img_url+item.headerUrl;

                         // console.warn('qing:'+id)

                      //  if(id > 0 && id % 2 != 0){
                          //console.warn('hou:'+id)

                         

                    
                    //       return (
                           
                    //         
                            
                    //         )
                           
    
                            
                    //  //   }
                         
                        
                     
                        
                    // })
                    
                   }
                     
                    </View>
                </View>
            </ScrollView>
        );
    }
}