import React, { Component } from 'react';
import {
    AppRegistry,
    DrugstoreCSSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import DrugstoreCSS from '../../../css/drugstore';
import config from '../../../config';
export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，
    static navigationOptions = {
        
    };
    
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
        }
       
    }
   

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={DrugstoreCSS.mainStyle}>
                <View style={DrugstoreCSS.container}>
                    <View style={DrugstoreCSS.header}>
                        <TouchableOpacity style={DrugstoreCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DrugstoreCSS.title}>
                            <Text style={DrugstoreCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={DrugstoreCSS.drugstores}>
                        <View style={DrugstoreCSS.drugstore}>
                            <View style={DrugstoreCSS.portrait}>
                                <Image source={{uri:config.img_url+this.state.con.drugUrl}} style={DrugstoreCSS.drugstoreimg} />
                            </View>
                            <View style={DrugstoreCSS.drugstoreintro}>
                                <Text style={DrugstoreCSS.medicinalname}>{this.state.con.title}</Text>
                                <Text>药品数量：{this.state.con.number}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={DrugstoreCSS.medicinalList}>
                        <View style={DrugstoreCSS.medicinalListTop}>
                            <Text>药品介绍</Text>
                        </View>
                        <View style={DrugstoreCSS.medicinals}>
                            <Text>{this.state.con.content}</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
} 