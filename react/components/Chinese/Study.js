import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import StudyCSS from '../../css/study'
import api from '../../api';
import config from '../../config';
export default class Study extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
        headerTitle: '学习',
        tabBarLabel: '学习',
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../img/xs.png')}
            style={{ width: 20, height: 20, tintColor: tintColor }}
          />
        )
    };

    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state
        this.state = {
          filterNum: 1,
          StudyArticlem:[],
          StudyVideom:[],
          Taskm:[]
        }
    }
    componentDidMount() {
        this.getList()
    }
    getList() {
        api.grade.Videom().then(response => {
         // console.warn(response)
          this.setState({
            StudyVideom: response
          })
        })
        api.grade.Articlem().then(response => {
            // console.warn(response)
             this.setState({
                StudyArticlem: response
             })
           })
        api.grade.Taskms().then(response => {
            // console.warn(response)
             this.setState({
                Taskm: response
             })
           })   
      }

    getFilter() {
        const { navigate } = this.props.navigation;
        if(this.state.filterNum === 1) {
            return (
                <View style={StudyCSS.contents}>

                     {
                      this.state.StudyVideom.map((item,id) => {
                         
                    return (
                    <TouchableOpacity style={StudyCSS.content} onPress={() => navigate('Video',{itm:item})} key={id}>
                        <View style={StudyCSS.coverPhoto}>
                            <TouchableOpacity onPress={this._onPressButton}>
                                <Image  source={{uri:config.img_url+item.coverPhoto}} style={StudyCSS.drugstoreimg} />
                            </TouchableOpacity>
                        </View>
                        <View style={StudyCSS.info}>
                            <Text style={StudyCSS.articletitle}>{item.title}</Text>
                            <Text style={StudyCSS.reading}>阅读量：{item.clicks}</Text>
                        </View>
                    </TouchableOpacity>
                           )
                        })
                       }

                </View>
            )
        } else if(this.state.filterNum === 2) {
            return (
                
                <View style={StudyCSS.contents}>
                    {
                    this.state.StudyArticlem.map((item,id) => {
                     return (
                    <TouchableOpacity style={StudyCSS.content} onPress={() => navigate('Article',{itm:item})} key={id}>
                        <View style={StudyCSS.coverPhoto}>
                            <Image source={{uri:config.img_url+item.coverPhoto}} style={StudyCSS.drugstoreimg} />
                        </View>
                        <View style={StudyCSS.info}>
                            <Text style={StudyCSS.articletitle}>{item.title}</Text>
                            <Text style={StudyCSS.reading}>阅读量：{item.clicks}</Text>
                        </View>
                    </TouchableOpacity>
                       )
                    })
                   }
                </View>
                
            )
        } else if(this.state.filterNum === 3) {
            return (
                <View style={StudyCSS.contents}>
                    {
                    this.state.Taskm.map((item,id) => {
                     return (
                     <TouchableOpacity style={StudyCSS.content} onPress={() => navigate('Task',{itm:item})} key={id}>
                        <View style={StudyCSS.caseinfo}>
                            <Text style={StudyCSS.casetitle}>{item.title}</Text>
                            <Text style={StudyCSS.cause}>{item.content}</Text>
                        </View>
                    </TouchableOpacity>
                     )
                    })
                   }
                   

                </View>
            )
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={StudyCSS.mainstyle}>
                <View style={StudyCSS.container}>
                    <View style={StudyCSS.header}>
                        <View style={StudyCSS.title}>
                            <Text style={StudyCSS.titleText}>学习</Text>
                        </View>         
                    </View>
                    <View style={StudyCSS.studys}>
                        <View style={StudyCSS.tabs}>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 1 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.video}>视频</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 2 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.article}>图文</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 3 }) }} style={StudyCSS.headeritem}>
                                <Text style={StudyCSS.task}>病例</Text>
                            </TouchableOpacity>
                        </View>
                        { this.getFilter() }
                    </View>
                </View>
            </ScrollView>
        );
    }
}    