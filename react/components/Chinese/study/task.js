import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import DiagnoseCSS from '../../../css/personal/diagnose';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
     static navigationOptions = {
     }; 
     constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
        }
       
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={DiagnoseCSS.container}>
                    <View style={[DiagnoseCSS.header]}>
                        <TouchableOpacity style={[DiagnoseCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                             
                    </View>
                    <View style={[DiagnoseCSS.prescription]}>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text>病例标题：</Text>
                            <Text>标题名</Text>
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text>病状分类：</Text>
                            <Text>7</Text>
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text style={[DiagnoseCSS.label]}>病情描述：</Text>
                            <Text style={[DiagnoseCSS.listTop]}>病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述病情描述描述</Text>
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text>诊断方案：</Text>
                            <Text>专断方案诊断方案</Text>
                        </View>
                        <View style={{marginTop:4}}>
                            <Text style={[DiagnoseCSS.recommend]}>推荐药品</Text>
                            <View style={[DiagnoseCSS.prescript]}>
                                <View style={[DiagnoseCSS.listTop,DiagnoseCSS.drugstore]}>
                                    <Text>药店名称：</Text>
                                    <Text>药店名药店名</Text>
                                </View>
                                <View style={[DiagnoseCSS.medicinal]}>
                                    <Text>药品1   2盒   每日两次/每次3片</Text>
                                    <Text>药品2   2盒   每日两次/每次3片</Text>
                                    <Text>药品3   2盒   每日两次/每次3片</Text>
                                    <Text>药品4   2盒   每日两次/每次3片</Text>
                                    <Text>药品5   2盒   每日两次/每次3片</Text>
                                </View>
                                <View style={[DiagnoseCSS.listTop,DiagnoseCSS.drugstore]}>
                                    <Text>药店名称：</Text>
                                    <Text>药店名药店名</Text>
                                </View>
                                <View style={[DiagnoseCSS.medicinal]}>
                                    <Text>药品1   2盒   每日两次/每次3片</Text>
                                    <Text>药品2   2盒   每日两次/每次3片</Text>
                                    <Text>药品3   2盒   每日两次/每次3片</Text>
                                    <Text>药品4   2盒   每日两次/每次3片</Text>
                                    <Text>药品5   2盒   每日两次/每次3片</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}