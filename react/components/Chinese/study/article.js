import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import ArticleCSS from '../../../css/study/article';
import config from '../../../config';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
        }
       
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={ArticleCSS.container}>
                    <View style={[ArticleCSS.header]}>
                        <TouchableOpacity style={[ArticleCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ArticleCSS.headerText}>
                            <Text style={[ArticleCSS.titleText]}>图文</Text>
                        </View>         
                    </View>
                    <View style={[ArticleCSS.content]}>
                            <Text style={[ArticleCSS.title]}>{this.state.con.title}</Text>
                            <View style={[ArticleCSS.infors]}>
                                <Text style={[ArticleCSS.reading]}>阅读量：{this.state.con.clicks}</Text>
                                <Text style={[ArticleCSS.time]}></Text>
                            </View>
                            <Image source={{uri:config.img_url+this.state.con.coverPhoto}} style={[ArticleCSS.coverImage]}/>
                            <Text>
                            {this.state.con.content}

                            </Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}