import React, { Component } from 'react';
import {
   WebView,
   StyleSheet,
    View,
    Text,
    TextInput,
    DeviceEventEmitter
} from 'react-native';

export default class Meng extends Component {
    

    constructor(props) {
        super(props);
      //  const number=10;
      //  const string='-270deg';
        this.state = {
            text: null
        }
      }

    componentDidMount() {

        DeviceEventEmitter.addListener('imeMsg', (message) => {
            this.getMessage(message)
        })
        
    }

    getMessage(message) {
        this.setState({
            text: message
        })
    }

    showIme() {
        this.props.navigation.navigate('Ime')
    }

    render() 
       {
        return (
                 <View style={styles.container}>
                       <Text style={[styles.ziti,{transform:[{translateX:10},{translateY:180},{rotateZ:'90deg'}]}]}> ᠮᠤᠩᠭᠤᠯ  ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ1 ᠄ </Text>
                       <Text style={[styles.ziti,{transform:[{translateX:20},{translateY:180},{rotateZ:'90deg'}]}]}> ᠮᠤᠩᠭᠤᠯ  ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ2 ᠄ </Text>
                       <Text style={[styles.ziti,{transform:[{translateX:30},{translateY:180},{rotateZ:'90deg'}]}]}> ᠮᠤᠩᠭᠤᠯ  ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ 3᠄ </Text>
                       <Text style={[styles.ziti,{transform:[{translateX:40},{translateY:180},{rotateZ:'90deg'}]}]}> ᠮᠤᠩᠭᠤᠯ  ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ4 ᠄ </Text>
                       <Text style={[styles.ziti,{transform:[{translateX:50},{translateY:180},{rotateZ:'90deg'}]}]}> ᠮᠤᠩᠭᠤᠯ  ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ 5᠄ </Text>
                       <TextInput   style={[styles.ziti,{transform:[{translateX:10},{translateY:240},{rotateZ:'90deg'}]}]}>{this.state.text}</TextInput> 
                       <TextInput 
                        multiline={true}
                        placeholder="ᠮᠤᠩᠭᠤᠯ  ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ 6"
                        underlineColorAndroid='transparent'
                        numberOfLines={5}
                        maxLength ={100}
                       style={[styles.ziti,{transform:[{translateX:50},{translateY:240},{rotateZ:'90deg'}]}]}>ᠮᠤᠩᠭᠤᠯ  ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ 6 /n ᠮᠤᠩᠭᠤᠯ  ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ 7 ᠮᠤᠩᠭᠤᠯ \n ᠪᠠᠷ ᠪᠢᠴᠢᠬᠦ 8</TextInput>
                </View>
                  );
       }
}
//onFocus={() => { this.showIme() }}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    header:{
        flexDirection:'row',
        height:48, 
        backgroundColor:"#2fa4e7",
    },
    title:{
        flex:4,
        backgroundColor:"#2fa4e7",
        justifyContent: 'center',
        alignItems: 'center',
    },
    titleText: {
        fontSize: 18,
        color: '#FFF',
        fontWeight: '600'
    },
    experts: {
        marginTop: 4,
        backgroundColor: '#fff',
        height: 145,
        flexDirection:'row',
    },
    portrait: {
        flex:3
    },
    infomation: {
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    expertintro: {
        flex:2,
        justifyContent: 'center',
        // alignItems: 'center',
    },
    expertimg: {
        width: 150,
        height: 120,
        marginTop: 10,
        borderRadius: 8,
        marginLeft: 6, 
    },
    dealing: {
        flexDirection:'row',
        padding:5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    handle: {
        flexDirection:'row',
        borderTopWidth:1,
        borderColor: '#ccc',
        alignItems: 'center',
        justifyContent: 'center',
    },
    btn: {
        height:25,
        width:25,
    },
    status: {
        position: 'absolute',
        zIndex: 99,
        right: 8,
        top: -46,
        width: 24,
        color: '#FFF',
        padding: 5,
        borderRadius: 8,
    },
    online: {
        backgroundColor: '#1b9e1b',
    },
    offline: {
        backgroundColor: 'gray'
    },
    busy: {
        backgroundColor: '#dc2424'
    },
    name: {
        color: '#2fa4e7',
        fontSize: 16,
    },
    department: {

    },
    video: {
        padding:5
    },
    phone: {
        padding:5
    },
    message: {
        padding:5
    },
    ziti:{
     // writingDirection:'ltr',
      fontFamily:'OrhonChaganTig',
      color:'#000000'
    }

});