import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import MessageCSS from '../../../css/personal/message';
import api from '../../../api';



export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    
    constructor(props) {
        super(props)
        this.state = {
            data:[]
        }
    }

    componentDidMount() {
        
        this.getList()
    } 

    getList() {

        api.messages.list().then(response => {

            this.setState({
                data: response
            })
        })
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={MessageCSS.container}>
                    <View style={[MessageCSS.header]}>
                        <TouchableOpacity style={[MessageCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={MessageCSS.headerText}>
                            <Text style={[MessageCSS.titleText]}>消息</Text>
                        </View>         
                    </View>
                    <View style={[MessageCSS.messages]}>
                    { 
                        this.state.data.map((item,id) => {
                            return (
                                <TouchableOpacity style={[MessageCSS.message]} key={id} onPress={() => navigate('MessageDetail',{title:item.title,content:item.content,time:item.createTime})}>
                                    <Text style={[MessageCSS.title]}>{item.title}</Text>
                                    <Text style={[MessageCSS.time]}>{item.createTime}</Text>
                                    <Text style={[MessageCSS.content]}>{item.content}</Text>
                                </TouchableOpacity>
                            )
                        })
                    }   
                    </View>
                </View>
            </ScrollView>
        );
    }
} 