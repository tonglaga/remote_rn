import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground
} from 'react-native'; 
import AboutCSS from '../../../css/personal/introduces';
import api from '../../../api';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    
    constructor(props) {
        super(props)
        this.state = {
          title: null,
          introduce: null,
        }
    }

    componentDidMount() {
        this.getList()
    } 

    getList() {
        api.introduces.getIntroduces().then(response => {
            this.setState({
                title: response[0].title,
                introduce:  response[0].introduce,
            })
        })
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={AboutCSS.container}>
                    <View style={[AboutCSS.header]}>
                        <TouchableOpacity style={[AboutCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={AboutCSS.headerText}>
                            <Text style={[AboutCSS.titleText]}>功能介绍</Text>
                        </View>      
                    </View>
                    <View style={[AboutCSS.lists]}>
                        <View style={[AboutCSS.list]}>
                            <View style={[AboutCSS.titleBorder]}>
                                <Text style={[AboutCSS.title]}>{this.state.title}</Text>
                            </View>
                            <Text style={[AboutCSS.content]}>{this.state.introduce}</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}