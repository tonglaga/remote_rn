import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import CollectCSS from '../../../css/personal/collect';
import api from '../../../api'
import config from '../../../config'
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 

    constructor(props) {
        super(props)
      //  const { params } = this.props.navigation.state
        this.state = {
          filterNum: 1,
          articles:[],
          videos:[],
          cases:[],
        }
    }

    componentDidMount() {
        this.getArticles()
    }  

    getArticles() {
        api.collects.articles({'login-token':localStorage.token}).then(response => {
            //console.warn(response)
            if(response.code != 400){
                this.setState({
                    articles: response
               })
            }
            
        })
        api.collects.videos({'login-token':localStorage.token}).then(response => {
            if(response.code != 400){
                this.setState({
                    videos: response
                })
            }
           
        })
     
        api.collects.cases({'login-token':localStorage.token}).then(response => {
            if(response.code != 400){
                this.setState({
                    cases: response
                })
            }
           
        })
     
    }
 
    getFilter() {
        if(this.state.filterNum === 1) {
            return (
                <View style={[CollectCSS.messages]}>
                   {
                    this.state.articles.map((item,id) => {
                     return (
                    <View style={[CollectCSS.message]} key={id}  onPress={() =>this.props.navigation.navigate('Article',{itm:item})}>
                        <View style={[CollectCSS.listTop]}>
                            <Image source={{uri:config.img_url+item.coverPhoto}} style={[CollectCSS.coverImage]}/>
                            <Text style={[CollectCSS.articleTitle]}>{item.title}</Text>
                        </View>
                        <View style={[CollectCSS.infors]}>
                            <Text style={[CollectCSS.dreading]}>阅读量</Text>
                            <Text style={[CollectCSS.dtime]}>{item.clicks}</Text>
                        </View>
                    </View>
                     )
                    })
                   }

                   
                </View>
            )
        } else if(this.state.filterNum === 2) {
            return (
                <View style={[CollectCSS.messages]}>
                     {
                      this.state.videos.map((item,id) => {
                        return (
                    <View style={[CollectCSS.message]} key={id} onPress={() =>this.props.navigation.navigate('Video',{itm:item})}>
                        <View style={[CollectCSS.listTop]}>
                            <Image source={{uri:config.img_url+item.coverPhoto}} style={[CollectCSS.coverImage]}/>
                            <Text style={[CollectCSS.articleTitle]}>{item.title}</Text>
                        </View>
                        <View style={[CollectCSS.infors]}>
                            <Text style={[CollectCSS.dreading]}>阅读量：</Text>
                            <Text style={[CollectCSS.dtime]}>{item.clicks}</Text>
                        </View>
                    </View>
                     )
                    })
                   }
                </View>
            )
        } else if(this.state.filterNum === 3) {
            return (
                <View style={[CollectCSS.messages]}>
                    {
                    this.state.cases.map((item,id) => {
                     return (
                    <View style={[CollectCSS.message]} key={id} onPress={() =>this.props.navigation.navigate('Task',{itm:item})}>
                        <Text style={[CollectCSS.title]}>{item.title}</Text>
                        <Text style={[CollectCSS.content]}>{item.content}</Text>
                        <View style={[CollectCSS.infors]}>
                            <Text style={[CollectCSS.dreading]}></Text>
                            <Text style={[CollectCSS.dtime]}></Text>
                        </View>
                    </View>
                        )
                    })
                   }
                  
                  
                </View>
            )
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={CollectCSS.container}>
                    <View style={[CollectCSS.header]}>
                        <TouchableOpacity style={[CollectCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={CollectCSS.headerText}>
                            <Text style={[CollectCSS.titleText]}>收藏</Text>
                        </View>         
                    </View>
                    <View style={CollectCSS.studys}>
                        <View style={CollectCSS.tabs}>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 1 }) }} style={[CollectCSS.headeritem,CollectCSS.headeritemborder]}>
                                <Text style={CollectCSS.article}>图文</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 2 }) }} style={[CollectCSS.headeritem,CollectCSS.headeritemborder]}>
                                <Text style={CollectCSS.video}>视频</Text>
                            </TouchableOpacity>
                     
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 3 }) }} style={[CollectCSS.headeritem,CollectCSS.headeritemborder]}>
                                <Text style={CollectCSS.task}>案例</Text>
                            </TouchableOpacity>
                          
                        </View>
                    </View>
                    { this.getFilter() }
                    
                </View>
            </ScrollView>
        );
    }
}