import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import ConsultCSS from '../../../css/expert/consult';
import api from '../../../api'

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    }; 

    constructor(props) {
        super(props);
        this.state = {
            opinion: null
        };
    }

    submit() {
        api.feedback.submit({opinion:this.state.opinion}).then(response => {
            ToastAndroid.show('发送成功', ToastAndroid.SHORT);
            this.props.navigation.goBack(null)
        })
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={ConsultCSS.container}>
                    <View style={ConsultCSS.header}>
                        <TouchableOpacity style={ConsultCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ConsultCSS.title}>
                            <Text style={ConsultCSS.titleText}>意见反馈</Text>
                        </View>      
                    </View>
                    <View style={[ConsultCSS.consultcontent]}>
                        <TextInput
                            multiline={true}
                            placeholder="请输入您的宝贵意见"
                            underlineColorAndroid='transparent'
                            numberOfLines={5}
                            maxLength ={100}
                            style={[ConsultCSS.consultinput]}
                            onChangeText={(opinion) => this.setState({ opinion })}
                        />
                        <Text style={[ConsultCSS.prompt]}>最多可输入100字</Text>
                        <TouchableOpacity style={[ConsultCSS.submitbtn]} onPress={() => this.submit()}>
                            <Text style={[ConsultCSS.submitbtnText]}>提交</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}