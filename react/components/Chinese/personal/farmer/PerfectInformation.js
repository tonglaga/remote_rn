import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Dimensions,
    ListView
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
//import ImagePicker from 'react-native-image-picker'


const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let {width, height} = Dimensions.get('window');
import MessageCSS from '../../../../css/personal/PerfectInformation';
import RadioGroup  from '../../../../common/RadioGroup';
import api from '../../../../api';
import config from '../../../../config';
import Picker from 'react-native-roll-picker'
import cityCode from './ChinaCityCode';
var options = { // 弹出框配置
    title:'请选择',  
    cancelButtonTitle:'取消',
    takePhotoButtonTitle:'拍照',
    chooseFromLibraryButtonTitle:'选择相册',
    quality:0.75,
    allowsEditing:true,
    noData:false,
    storageOptions: {
        skipBackup: true,
        path:'images'
    }
};
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props)
        
        this.state = {
            avatarSource: null,
            sexArray: [
                {
                    title: '男',
                    image:  require('../../../../img/hs.png'),
                    image2:require('../../../../img/h.png'),
                },
                {
                    title: '女',
                    image:  require('../../../../img/hs.png'),
                    image2:require('../../../../img/h.png'),
                }
            ],
            data: {
                name: null,
                avatar: null,
                avatar_file: null,
                sign: {
                    sex: 0,
                    address: null,
                    sign: null
                }
            },
            from: {
                ziliaoid: null,
                createTime: null,
                updateTime: null,
                uid: null,
                province: this.rowIndex0,
                city: this.rowIndex1,
                area: this.rowIndex2,
                address: null,
                sex: null,
                cardNumber: null,
                headerUrl: require('../../../../img/Icon.png'),
                name: null,
               
            },
            regiondata:[],
           
        };
        this._imagePicker = this._imagePicker.bind(this); // bind
       
    }
    componentDidMount() {
        api.farmer.getByUid({'login-token':localStorage.token}).then(response => {
            //  console.warn(response)
               if(typeof(response)!=undefined){
                this.setState({
                from:{  
                ziliaoid: response.id,
                createTime: response.createTime,
                updateTime: response.updateTime,
                uid: response.uid,
                province: response.province,
                city: response.city,
                area: response.area,
                address: response.address,
                sex: response.sex,
                cardNumber: response.cardNumber,
                headerUrl: config.img_url+response.headerUrl,
                name: response.name,
                }
                })    
            }
        })

        // api.farmer.diqu().then(response => {
        //     this.setState({u
        //         regiondata=response,
        //     })
        // })
    }
    _imagePicker() {
       // console.warn("yunxing")
        ImagePicker.showImagePicker(options,(res) => {
            console.warn("liuliu")
            if (res.didCancel) {  // 返回
                return
            } else {
                let source;  // 保存选中的图片
                source = {uri: 'data:image/jpeg;base64,' + res.data};
    
                if (Platform.OS === 'android') {
                    source = { uri: res.uri };
                } else {
                    source = { uri: res.uri.replace('file://','') };
                }
    
                this.setState({
                    avatarSource: source
                });
            }
        })
    }
  
    changeHeadImage() {
        //this._imagePicker();
        // console.warn(1111)
        // ImagePicker.openPicker({
        //    width: 300,
        //    height: 400,
        //    cropping: true
       //  }).then(image => {
         //   console.log("22222" + image['data']);
            //console.warn(image)
        //   api.file.upload({uri: image.path, name: 'image.jpg', type: image.mime}).then(response => {
            //   console.warn(response)
                // api.user.update({avatar: response.id}).then(() => {
                //     this.getData()
                //     DeviceEventEmitter.emit('userStatusChange')
                // })
        // })
      // })
    //   ImagePicker.openCropper({  
    //     path:require('../../../../img/Icon.png'),  
    //     width: 300,  
    //     height: 400  
    //   }).then(image => {  
    //     console.warn(image);  
    //   });
    // ImagePicker.openPicker({  
    //     multiple: true  
    //   }).then(images => {  
    //     console.warn(images);  
    //   });  
    ImagePicker.openCamera({  
        width: 300,  
        height: 400,  
        cropping: true  
      }).then(image => {  
        console.warn(image);  
      });
    }
    createHeaderImage() {
        // if(this.state.data.avatar_file === null) {
       
            return (
                <Image
                    style={MessageCSS.headerImage}
                    source={require('../../../../img/Icon.png')}  >

                </Image>
            )
        // } else {
        //     return (
        //         <Image
        //             style={MessageCSS.headerImage}
        //             source={{ uri: this.state.data.avatar_file.path }}  >

        //         </Image>
        //     )
        // }
    }
    setusername(username){
        let from = this.state.from
        from.name = username
        this.setState({
            from: from
        })
    }
    setaddressval(addressval){
        console.warn(addressval)
        let from = this.state.from
        from.address = addressval
        this.setState({
            from: from
        })
    }
    setcardNumberval(Numberval){
        let from = this.state.from
        from.cardNumber = Numberval
        this.setState({
            from: from
        })
    }
    setsex(sex){
        let from = this.state.from
        from.sex = sex
        this.setState({
            from: from
        })
    }
    setProvince(p,c,a){
        let from = this.state.from
        from.province = p
        from.city = c
        from.area = a
        this.setState({
            from: from
        })
    }
    setcity(c,a){
        let from = this.state.from
        from.city = c
        from.area = a
        this.setState({
            from: from
        })
    }
    setarea(a){
        let from = this.state.from
        from.area = a
        this.setState({
            from: from
        })
    }
    //提交数据
    postsubmit(){
        console.warn(this.state.from)
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={MessageCSS.container}>
                    <View style={[MessageCSS.header]}>
                        <TouchableOpacity style={[MessageCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={MessageCSS.headerText}>
                            <Text style={[MessageCSS.titleText]}>完善资料</Text>
                        </View>  
                        <TouchableOpacity style={[MessageCSS.backImg]} onPress={() => { this.postsubmit() }}>
                            <Text style={{color:'#ffffff'}}>发送</Text>
                        </TouchableOpacity>        
                    </View>
                    <TouchableOpacity onPress={() => this.changeHeadImage()} style={[MessageCSS.headerPic]}>
                           
                            <View style={MessageCSS.inforInput}>
                                {this.createHeaderImage()}
                            </View>

                    </TouchableOpacity>
                    <View  style={[MessageCSS.informations]}>
                        <View style={[MessageCSS.information]}>
                            <TextInput placeholder="请输入姓名"
                                secureTextEntry={false}
                                value={this.state.from.name}
                                underlineColorAndroid='transparent'
                                onChangeText={(username) =>this.setusername(username)}
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>

                        <View style={[MessageCSS.information]}>
                            
                            <View style={{height: 44, flex: 6}}>
                                <RadioGroup
                                    style={{flexDirection: 'row',}}//整个组件的样式----这样可以垂直和水平
                                    conTainStyle={{height: 44, width: 60}}//图片和文字的容器样式
                                    imageStyle={{width: 25, height: 25}}//图片样式
                                    textStyle={{color: '#666'}}//文字样式
                                    selectIndex={this.state.from.sex}//空字符串,表示不选中,数组索引表示默认选中
                                    data={this.state.sexArray}//数据源
                                    onPress={(index, item)=> {
                                        if(index==0){
                                           index=2;
                                        }
                                       this.setsex(index);
                                    }}
                                />
                            </View>
                        </View>
                        <View style={[MessageCSS.information]}>
                           
                            <TextInput placeholder="请输入身份证号"
                                secureTextEntry={false}
                                maxLength={18}
                                value={this.state.from.cardNumber}
                                underlineColorAndroid='transparent'
                                onChangeText={(cardNumberval) => this.setcardNumberval(cardNumberval)}
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style = {{height: 225, flexDirection: 'row'}}>
                <View style = {{flex: 0.9}}>
                    <Picker 
                        data = {cityCode.CityZoneCode.China.Province}
                        ref = '_Picker0'
                        name = 'name'
                        onRowChange = {index => {
                            this.setProvince(index,0,0); 
                           
                            this.refs._Picker1.setDataSource(cityCode.CityZoneCode.China.Province[this.state.from.province].City); 
                            this.refs._Picker2.setDataSource(cityCode.CityZoneCode.China.Province[this.state.from.province].City[0].Area)}}
                    />
                </View>
                <View style = {{flex: 1}}>
                    <Picker 
                        data = {cityCode.CityZoneCode.China.Province[0].City} 
                        ref = '_Picker1'
                        name = 'name'
                        onRowChange = {index => {
                            this.setcity(index,0)
                           
                            this.refs._Picker2.setDataSource(cityCode.CityZoneCode.China.Province[this.state.from.province].City[this.state.from.city].Area)}}
                    />
                </View>
                <View style = {{flex: 1}}>
                    <Picker 
                        data = {cityCode.CityZoneCode.China.Province[0].City[0].Area}
                        ref = '_Picker2'
                        name = 'name'
                        onRowChange = {index => this.state.from.area = index}
                    />
                </View>
            </View>
                    
                        
                        <View style={[MessageCSS.information]}>
                            
                            <TextInput 
                                placeholder="请输入地区" 
                                value={this.state.from.address}
                                secureTextEntry={false}
                                underlineColorAndroid='transparent'
                                onChangeText={(addressval) => this.setaddressval(addressval)}
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        
                    </View>
                </View>
            </ScrollView>
        );
    }
}