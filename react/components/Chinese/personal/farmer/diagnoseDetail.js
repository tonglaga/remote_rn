import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';

import DiagnoseCSS from '../../../../css/personal/diagnose';
import api from '../../../../api';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    // static navigationOptions = {
    // }; 
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
                data:[]
        }
       
    }
    componentDidMount() {
        this.getList()
      }

      getList() {
        api.farmer.prescriptionms({'login-token':localStorage.token,id:this.state.con.id}).then(response => {
         
          this.setState({
            data: response
          })
        })
      }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={DiagnoseCSS.container}>
                    <View style={[DiagnoseCSS.header]}>
                        <TouchableOpacity style={[DiagnoseCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DiagnoseCSS.headerText}>
                            <Text style={[DiagnoseCSS.titleText]}>已处理</Text>
                        </View>         
                    </View>
                    <View style={[DiagnoseCSS.prescription]}>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text>病例标题：</Text>
                            <Text>{this.state.con.title}</Text>
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text>病状分类：</Text>
                            <Text>{this.state.con.illnessCategoryId}</Text>
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text style={[DiagnoseCSS.label]}>病情描述：</Text>
                            <Text style={[DiagnoseCSS.listTop]}>{this.state.con.content}</Text>
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text>诊断方案：</Text>
                            <Text>{this.state.con.processingMethod}</Text>
                        </View>
                        <View style={{marginTop:4}}>
                            <Text style={[DiagnoseCSS.recommend]}>推荐药品</Text>
                            <View style={[DiagnoseCSS.prescript]}>
                            {
                             this.state.data.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                        
                        return (
                              <View key={id}>
                                <View style={[DiagnoseCSS.listTop,DiagnoseCSS.drugstore]} >
                                    <Text>药店名称：{item.drugstoreinfo.pharmacyName}</Text>
                                    <Text>药店电话：{item.drugstoreinfo.username}</Text>
                                </View>
                                {
                                item.prescriptionmDtos.map((m,j)=>{
                                 return (  
                                <View style={[DiagnoseCSS.medicinal]} key={j}>
                                    <Text>药名：{m.drugName}       数量： {m.drugNumber}盒</Text>
                                  
                                </View>
                                   )
                                  })
                                }
                                </View>
                                 )
                               
                                })
        
                               }

                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}