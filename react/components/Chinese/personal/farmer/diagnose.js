import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import DiagnoseCSS from '../../../../css/personal/diagnose';
import api from '../../../../api';
//import config from '../../../../config';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    // static navigationOptions = {
    // };
    constructor(props) {
        super(props)
        this.state = {
          data: []
        }
      }
    
      componentDidMount() {
        this.getList()
      }

      getList() {
        api.farmer.diagnose({'login-token':localStorage.token}).then(response => {
         
          this.setState({
            data: response
          })
        })
      }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={DiagnoseCSS.container}>
                    <View style={[DiagnoseCSS.header]}>
                        <TouchableOpacity style={[DiagnoseCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DiagnoseCSS.headerText}>
                            <Text style={[DiagnoseCSS.titleText]}>诊断管理</Text>
                        </View>         
                    </View>
                    <View style={[DiagnoseCSS.messages]}>
                    {
                      this.state.data.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                         if(item.status==2){
                           
                    return (
	                    <TouchableOpacity key={id} onPress={() => navigate('FarmerDiagnoseDetail',{itm:item})} style={[DiagnoseCSS.message]}>
	                        <Text style={[DiagnoseCSS.causesLabel]}>{item.title}</Text>
	                    </TouchableOpacity>
                            )
                        }
                        })

                       }
	                </View>
                </View>
            </ScrollView>
        );
    }
}