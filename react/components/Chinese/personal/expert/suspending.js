import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    DeviceEventEmitter,
    TextInput,
} from 'react-native';
import DiagnoseCSS from '../../../../css/personal/diagnose';
import ModalDropdown from 'react-native-modal-dropdown';
import api from '../../../../api';
import PopupDialog, { SlideAnimation,DialogTitle } from 'react-native-popup-dialog';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    // static navigationOptions = {
    // }; 
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        
        this.state = { 
            con: {
                json:null,
                id:params.itm.id,
                herdsmanId:params.itm.herdsmanId,
                expertId:params.itm.expertId,
                title:params.itm.title,
                content:params.itm.content,
                illnessCategoryId:params.itm.illnesscategoryid,
                processingMethod:params.itm.pronessingmethod,
                type:params.itm.type,
                good:params.itm.good,
                enclosure:""
            },
               // con:params.itm,
                data:[],
                fenlei:[],
                yaopin:[]
        }
       
    }
    componentDidMount() {
        this.getList();
        DeviceEventEmitter.addListener('zuizhong', (message) => {
            
            let yaopin=this.state.yaopin;
            yaopin[yaopin.length]=message;
            this.setState({
                yaopin:yaopin,
               
            })
            //console.warn(yaopin)
            //this.getMessage(message); 
        })
      }

      getList() {
        api.expert.drugstoreinfos({'login-token':localStorage.token,'id':this.state.con.id}).then(response => {
         
          this.setState({
            data: response
          })
        })
        api.expert.illnesscategorys({'login-token':localStorage.token}).then(response => {
        
             this.setState({
               fenlei: response
             })
          })
      }
      _dropdown_2_renderButtonText(rowData) {
        const {id, name} = rowData;

        return `${name}`;
      }
    
      _dropdown_2_renderRow(rowData, rowID, highlighted) {
     //   let icon = highlighted ? require('./images/heart.png') : require('./images/flower.png');
        let evenRow = rowID % 2;
        return (
          <TouchableHighlight underlayColor='cornflowerblue'>
            <View style={[DiagnoseCSS.dropdown_2_row, {backgroundColor: evenRow ? 'lemonchiffon' : 'white'}]}>
              
              <Text style={[DiagnoseCSS.dropdown_2_row_text, highlighted && {color: 'mediumaquamarine'}]}>
                {`${rowData.name}`}
              </Text>
            </View>
          </TouchableHighlight>
        );
      }
    
      _dropdown_2_renderSeparator(sectionID, rowID, adjacentRowHighlighted) {
        
       // if (rowID == DEMO_OPTIONS_1.length - 1) return;
        let key = `spr_${rowID}`;
        return (<View style={DiagnoseCSS.dropdown_2_separator}
                      key={key}
        />);
      }
      _dropdown_6_onSelect(idx, value) {
      //  console.warn('idx:'+idx)
       // console.warn(value.id) 
       let con=this.state.con;
       con.illnessCategoryId=value.id;
         this.setState({
           con:con
        })
      }
      setStatetitle(text){
        let con=this.state.con;
       con.title=text;
         this.setState({
           con:con
        })   
      }
      setStatecontent(text){
        let con=this.state.con;
       con.content=text;
         this.setState({
           con:con
        })   
      }
      setStateprocessingMethod(text){
        let con=this.state.con;
       con.processingMethod=text;
         this.setState({
           con:con
        })   
      }
      //选择药品
      goChooseMedicinal(item){
        this.popupDialog.dismiss();
        this.props.navigation.navigate('ChooseMedicinal',{itm:item}) 
      }
      //提交诊断接口
      gosubmit(){
        let json=[];  
        let k=0;
        this.state.yaopin.map((n,i)=>{
            n.prescriptionmDtos.map((m,j)=>{
                json[k]=m;
                k++;
            })
           
        })  
        let con=this.state.con;
        con.json=JSON.stringify(json);
          this.setState({
            con:con
         })
         console.warn(this.state.con)
        api.expert.expertinfoszhen(this.state.con).then(response => {
         console.warn(response) 
       
          })
         this.props.navigation.navigate("Diagnose");
      }
    render() {
        const { navigate } = this.props.navigation;
        const slideAnimation = new SlideAnimation({
		  slideFrom: 'bottom',
		});
        return (
            <ScrollView>
                <View style={DiagnoseCSS.container}>
                	<PopupDialog
					    dialogTitle={<DialogTitle title="选择药店" />}
					    ref={popupDialog => {
					        this.popupDialog = popupDialog;
					    }}
					    width={300}
					    height={540}
					>
					    <View>
                        {
                      this.state.data.map((item,id) => {
                        
                    return (
					        <TouchableOpacity key={id} style={[DiagnoseCSS.border]}  onPress={() => this.goChooseMedicinal(item)}>

                                <Text style={[DiagnoseCSS.store]}>{item.pharmacyName}</Text>
                            </TouchableOpacity>
                              )
                           
                            })
        
                           }
					    </View>

					</PopupDialog>
                    <View style={[DiagnoseCSS.header]}>
                        <TouchableOpacity style={[DiagnoseCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DiagnoseCSS.headerText}>
                            <Text style={[DiagnoseCSS.titleText]}>未处理</Text>
                        </View> 
                        <TouchableOpacity style={[DiagnoseCSS.backImg]} onPress={() => { this.gosubmit() }}>
                            <Text style={{color:'#ffffff'}}>发送</Text>
                        </TouchableOpacity>        
                    </View>
                    <View style={[DiagnoseCSS.prescription]}>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text style={[DiagnoseCSS.labeltext]}>病例标题：</Text>
                            <TextInput
                                placeholder="请输入标题名"
                                value={this.state.con.title}
                                underlineColorAndroid='transparent'
                                style={[DiagnoseCSS.infor,DiagnoseCSS.inputBorder]}
                                onChangeText={(text) => this.setStatetitle(text)}
                            />
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text style={[DiagnoseCSS.labeltext]}>病状分类：</Text>
                            <ModalDropdown style={[DiagnoseCSS.btnStyle,DiagnoseCSS.infor]} dropdownStyle={[DiagnoseCSS.selectStyle]} 
                            defaultValue='请选择...' 
                            options={this.state.fenlei}
                            renderButtonText={(rowData) => this._dropdown_2_renderButtonText(rowData)}
                            renderRow={this._dropdown_2_renderRow.bind(this)}
                            renderSeparator={(sectionID, rowID, adjacentRowHighlighted) => this._dropdown_2_renderSeparator(sectionID, rowID, adjacentRowHighlighted)}
                            onSelect={(idx, value) => this._dropdown_6_onSelect(idx, value)}
                           />
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text style={[DiagnoseCSS.labeltext]}>病情描述：</Text>
                            <TextInput
                                multiline={true}
                                placeholder="请输入病情描述"
                                value={this.state.con.content}
                                underlineColorAndroid='transparent'
                                numberOfLines={5}
                                maxLength ={100}
                                style={[DiagnoseCSS.infor,DiagnoseCSS.inputBorder]}
                                onChangeText={(text) => this.setStatecontent(text)}
                            />
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text style={[DiagnoseCSS.labeltext]}>诊断方案：</Text>
                            <TextInput
                                multiline={true}
                                placeholder="请输入诊断方案"
                                value={this.state.con.processingMethod}
                                underlineColorAndroid='transparent'
                                numberOfLines={5}
                                maxLength ={100}
                                style={[DiagnoseCSS.infor,DiagnoseCSS.inputBorder]}
                                onChangeText={(text) => this.setStateprocessingMethod(text)}
                            />
                        </View>
                        <View style={{marginTop:4,}}>
                        	<TouchableOpacity style={[DiagnoseCSS.backImg]} onPress={() => { this.popupDialog.show(); }}>
	                            <Text style={[DiagnoseCSS.recommend]}>推荐药品</Text>
	                        </TouchableOpacity>
                        </View>
                        <View style={{marginTop:4}}>
                           
                            <View style={[DiagnoseCSS.prescript]}>
                            {
                             this.state.yaopin.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                        
                        return (
                              <View key={id}>
                                <View style={[DiagnoseCSS.listTop,DiagnoseCSS.drugstore]} >
                                    <Text>药店名称：{item.pharmacyName}</Text>
                                  
                                </View>
                                {
                                item.prescriptionmDtos.map((m,j)=>{
                                 return (  
                                <View style={[DiagnoseCSS.medicinal]} key={j}>
                                    <Text>药名：{m.drugName}       数量： {m.drugNumber}盒</Text>
                                  
                                </View>
                                   )
                                  })
                                }
                                </View>
                                 )
                               
                                })
        
                               }
                                
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}