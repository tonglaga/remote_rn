import React, { Component } from 'react';
import {
    AppRegistry,
    ArticleCSSheet,
    Text,
    Image,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    ListView,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let {width, height} = Dimensions.get('window');
import ArticleCSS from '../../../../css/personal/articleManagement';
import RadioGroup  from '../../../../common/RadioGroup';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
   
    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state;
        this.state = {
            data: {
                con:params.itm,
                name: null,
                avatar: null,
                avatar_file: null,
                sign: {
                    sex: 0,
                    address: null,
                    sign: null
                }
            }
        };
    }
    changeHeadImage() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            api.file.upload({uri: image.path, name: 'image.jpg', type: image.mime}).then(response => {
                api.user.update({avatar: response.id}).then(() => {
                    this.getData()
                    DeviceEventEmitter.emit('userStatusChange')
                })
            })
        })
    }
    createHeaderImage() {
        if(this.state.data.avatar_file === null) {
            return (
                <Image
                    style={ArticleCSS.IdCardImage}
                    source={require('../../../../img/Icon.png')}  >

                </Image>
            )
        } else {
            return (
                <Image
                    style={ArticleCSS.IdCardImage}
                    source={{ uri: this.state.data.avatar_file.path }}  >

                </Image>
            )
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={ArticleCSS.container}>
                    <View style={[ArticleCSS.header]}>
                        <TouchableOpacity style={[ArticleCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ArticleCSS.headerText}>
                            <Text style={[ArticleCSS.titleText]}>编辑文章</Text>
                        </View>         
                    </View>
                    <View  style={[ArticleCSS.informations]}>
                        <View style={[ArticleCSS.information]}>
                            <Text style={[ArticleCSS.nameText]}>文章标题</Text>
                            <TextInput 
                            	placeholder="文章标题"
                                underlineColorAndroid='transparent'
                                style={[ArticleCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[ArticleCSS.infor]}>
                            <Text style={[ArticleCSS.nameText]}>文章内容</Text>
                            <TextInput
                                multiline={true}
                                placeholder="文章内容"
                                underlineColorAndroid='transparent'
                                numberOfLines={5}
                                maxLength ={100}
                                style={[ArticleCSS.inforInput]}
                                onChangeText={(text) => this.setState({text})}
                            />
                        </View>
                        <TouchableOpacity onPress={() => this.changeHeadImage()} style={[ArticleCSS.headerPic]}>
                            <Text style={[ArticleCSS.nameText]}>文章封面</Text>
                            <View style={ArticleCSS.coverImage}>
                                {this.createHeaderImage()}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={ArticleCSS.referbtn}>
                            <Text style={ArticleCSS.refer}>提交</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}