import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import ArticleCSS from '../../../../css/personal/articleManagement';
import api from '../../../../api';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props) 
        this.state = {
          filterNum: 1,
          data:[]
        }
    }

    componentDidMount() {

        this.getList();

      }

      getList() {
        api.expert.studyarticlems({'login-token':localStorage.token}).then(response => {  
          //  console.warn(response); 
            if(response.code != 401){
                this.setState({
                    data: response
                  })
            }else {
                ToastAndroid.show(response.message, ToastAndroid.SHORT)
            }
       
        })
      }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={ArticleCSS.container}>
                    <View style={[ArticleCSS.header]}>
                        <TouchableOpacity style={[ArticleCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ArticleCSS.headerText}>
                            <Text style={[ArticleCSS.titleText]}>文章管理</Text>
                        </View> 
                        <TouchableOpacity style={[ArticleCSS.backImg]} onPress={() => navigate('AddArticle')}>
                        <Text style={[ArticleCSS.title]}>添加</Text>
                        </TouchableOpacity>        
                    </View>
                    <View style={[ArticleCSS.messages]}>
                    {
                      this.state.data.map((item,id) => {
                        
                       
                    return (
                        <View style={[ArticleCSS.message]} key={id}>

                            <TouchableOpacity onPress={() => navigate('ArticleDetail',{itm:item})} >
                                <Text style={[ArticleCSS.title]}>{item.title}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => navigate('EditArticle',{itm:item})}>
                                <Text  style={[ArticleCSS.title]} >修改</Text>
                            </TouchableOpacity>
                        </View>
                        )
                    
                    })

                   }
                    </View>
                </View>
            </ScrollView>
        );
    }
}