import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import MessageCSS from '../../../../css/personal/articleManagement';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
      
        this.state = {
            con:params.itm,
            
        }
    } 
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={MessageCSS.container}>
                    <View style={[MessageCSS.header]}>
                        <TouchableOpacity style={[MessageCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={MessageCSS.headerText}>
                            <Text style={[MessageCSS.titleText]}>文章详情</Text>
                        </View>         
                    </View>
                    <View style={[MessageCSS.messages]}>
                        <View style={[MessageCSS.messageContent]}>
                            <Text style={[MessageCSS.dtitle]}>{this.state.con.title}</Text>
                            <View style={[MessageCSS.infors]}>
                                <Text style={[MessageCSS.time]}>{this.state.con.createTime}</Text>
                            </View>
                            <Text style={[MessageCSS.dcontent]}>
                            {this.state.con.content}
                            </Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}