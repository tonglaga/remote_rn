import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    DeviceEventEmitter,
    TouchableHighlight
} from 'react-native';
import DrugstoreCSS from '../../../../css/drugstore'
import api from '../../../../api';
import config from '../../../../config';
const checkedImage=require('../../../../img/hs.png');
const checkImage=require('../../../../img/h.png');

export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，
    static navigationOptions = {
        
    };
     
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
            con:params.itm,
            zuizhong:{
                id:params.itm.uid,
                pharmacyName:params.itm.pharmacyName,
                prescriptionmDtos:[]
            },
            data:[]
        };
    }
    componentDidMount() {
        this.getList()
      }

      getList() {
        // console.warn({'login-token':localStorage.token,'Did':this.state.con.id}) 
        api.expert.drugstui({'login-token':localStorage.token,'Did':this.state.con.id}).then(response => {
           // console.warn(response)
            response.map((o,i)=>{
                response[i]["drugName"]= response[i]["title"],
                response[i]["isChecked"] = false;
                response[i]["drugNumber"] = 0;
          })
        
           this.setState({
             data: response
           })
        })
       
      }
    // getChecked() {
    //     return this.state.isChecked;
    // }
 
    // setChecked(isChecked) {
    //     this.setState({
    //         isChecked: isChecked
    //     });
    // }
 
    checkClick(e) {
        let data=this.state.data;
        if(data[e].isChecked == true){
            data[e].isChecked=false;
        }else{
            data[e].isChecked=true;
        }
      
        this.setState({
            data:data
        }); 
    }
    jianClick(e) {
        let data=this.state.data;
        if(data[e].drugNumber==0){

            data[e].drugNumber = 0;
            data[e].isChecked=false;

        }else{
            data[e].drugNumber = data[e].drugNumber-1;
        }
        
      
        this.setState({
            data:data
        }); 
    }
    jiaClick(e) {
        
        let data=this.state.data;
        if(data[e].isChecked==false){
            data[e].isChecked=true;
        }
        if(data[e].drugNumber == data[e].number){
            data[e].drugNumber = data[e].drugNumber;
        }else{
            data[e].drugNumber = data[e].drugNumber+1;
        }

        this.setState({
            data:data
        }); 
    }

    gohome(){
      let data=this.state.data;
      let zuizhong=this.state.zuizhong;
      let i=0;
      data.map((item,id)=>{
         if(item.isChecked == true && item.drugNumber > 0){
            zuizhong.prescriptionmDtos[i]=item;
            //zuizhong.prescriptionmDtos[i]=item;
            i++;
         }
      })
      this.setState({
        zuizhong:zuizhong
    }); 
      DeviceEventEmitter.emit('zuizhong',zuizhong)
      this.props.navigation.goBack(null);
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={DrugstoreCSS.mainStyle}>

                <View style={DrugstoreCSS.container}>
                    <View style={DrugstoreCSS.header}>
                        <TouchableOpacity style={DrugstoreCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DrugstoreCSS.title}>
                            <Text style={DrugstoreCSS.titleText}></Text>
                        </View> 
                        <TouchableOpacity style={DrugstoreCSS.backImg}  onPress={() =>this.gohome()}>
                        <Text style={{color:'#ffffff'}}>添加</Text>
                        </TouchableOpacity>        
                    </View>
                    <View style={DrugstoreCSS.medicinalList}>
                    {
                      this.state.data.map((item,id) => {
                        
                    return (
                        <View style={[DrugstoreCSS.medicinals, DrugstoreCSS.medicinalpadding]} key={id}>
                            <TouchableHighlight underlayColor={'transparent'} onPress={() => this.checkClick(id)} style={{flex:1}}>
                                <Image source={item.isChecked == true ? checkImage : checkedImage} style={{ marginLeft: 5,height: 15, width: 15}}/>
                            </TouchableHighlight>
                            <TouchableOpacity style={DrugstoreCSS.medicinal} style={{flex:4}}>
                                <Image style={DrugstoreCSS.medicinalImg} source={{uri:config.img_url+item.drugUrl}} />
                            </TouchableOpacity>
                            <View style={{flex:3}}>
                                <Text style={{color:'#000',fontSize:16,fontWeight:'600',}}>{item.title}</Text>
                                <View style={ DrugstoreCSS.numControllStyle }>
                                    <TouchableOpacity style={ DrugstoreCSS.reduceStyle } onPress={() => this.jianClick(id)}>
                                        <Text style={{color:'red',fontSize:18,padding:3,borderColor: '#ccc',borderWidth: 1,paddingLeft:10,paddingRight:10,}}>-</Text>
                                    </TouchableOpacity>
                                    <View style={ DrugstoreCSS.numberViewStyle }>
                                        <Text style={ DrugstoreCSS.numberStyle}>{item.drugNumber}</Text>
                                    </View>
                                    <TouchableOpacity style={ DrugstoreCSS.increaseStyle } onPress={() => this.jiaClick(id)}>
                                        <Text style={{color:'#000',fontSize:18,padding:3,borderColor: '#ccc',borderWidth: 1,paddingLeft:10,paddingRight:10,}}>+</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                         )
                           
                        })
    
                       }
                    </View>
                </View>

            </ScrollView>
        );
    }
}