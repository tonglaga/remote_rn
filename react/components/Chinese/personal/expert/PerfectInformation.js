import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Dimensions,
    ListView,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let {width, height} = Dimensions.get('window');
import MessageCSS from '../../../../css/personal/PerfectInformation';
import RadioGroup  from '../../../../common/RadioGroup';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props)
        this.state = {
            sexArray: [
                {
                    title: '男',
                    image:  require('../../../../img/xuan.png'),
                    image2:require('../../../../img/yixuan.png'),
                },
                {
                    title: '女',
                    image:  require('../../../../img/xuan.png'),
                    image2:require('../../../../img/yixuan.png'),
                }
            ],
            data: {
                name: null,
                avatar: null,
                avatar_file: null,
                sign: {
                    sex: 0,
                    address: null,
                    sign: null
                }
            },
            avatarSource: null,
            videoSource: null
        };
    }
    //选择图片
    selectPhotoTapped() {
        const options = {
            title: '选择图片', 
            cancelButtonTitle: '取消',
            takePhotoButtonTitle: '拍照', 
            chooseFromLibraryButtonTitle: '选择照片', 
            customButtons: [
                {name: 'fb', title: 'Choose Photo from Facebook'},
              ],
            cameraType: 'back',
            mediaType: 'photo',
            videoQuality: 'high', 
            durationLimit: 10, 
            maxWidth: 300,
            maxHeight: 300,
            quality: 0.8, 
            angle: 0,
            allowsEditing: false, 
            noData: false,
            storageOptions: {
                skipBackup: true  
            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source
                });
            }
        });
    }


    changeHeadImage() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            api.file.upload({uri: image.path, name: 'image.jpg', type: image.mime}).then(response => {
                console.warn(response);
                // api.user.update({avatar: response.id}).then(() => {
                //     this.getData()
                //     DeviceEventEmitter.emit('userStatusChange')
                // })
            })
        })
    }
    createHeaderImage() {
        if(this.state.data.avatar_file === null) {
            return (
                <Image
                    style={MessageCSS.headerImage}
                    source={require('../../../../img/Icon.png')}  >

                </Image>
            )
        } else {
            return (
                <Image
                    style={MessageCSS.headerImage}
                    source={{ uri: this.state.data.avatar_file.path }}  >

                </Image>
            )
        }
    }
    createIdCard() {
        if(this.state.data.avatar_file === null) {
            return (
                <Image
                    style={MessageCSS.IdCardImage}
                    source={require('../../../../img/a8.jpg')}  >

                </Image>
            )
        } else {
            return (
                <Image
                    style={MessageCSS.headerImage}
                    source={{ uri: this.state.data.avatar_file.path }}  >

                </Image>
            )
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={MessageCSS.container}>
                    <View style={[MessageCSS.header]}>
                        <TouchableOpacity style={[MessageCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={MessageCSS.headerText}>
                            <Text style={[MessageCSS.titleText]}>完善资料</Text>
                        </View>         
                    </View>
                    <View  style={[MessageCSS.informations]}>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>姓名</Text>
                            <TextInput placeholder="请输入姓名"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>性别</Text>
                            <View style={{height: 44, flex: 6}}>
                                <RadioGroup
                                    style={{flexDirection: 'row',}}//整个组件的样式----这样可以垂直和水平
                                    conTainStyle={{height: 44, width: 60}}//图片和文字的容器样式
                                    imageStyle={{width: 25, height: 25}}//图片样式
                                    textStyle={{color: '#666'}}//文字样式
                                    selectIndex={''}//空字符串,表示不选中,数组索引表示默认选中
                                    data={this.state.sexArray}//数据源
                                    onPress={(index, item)=> {
                                    }}
                                />
                            </View>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>学历</Text>
                            <TextInput placeholder="请选择"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>专长</Text>
                            <TextInput placeholder="请输入专长"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>专业</Text>
                            <TextInput placeholder="请选择"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>身份证号</Text>
                            <TextInput placeholder="请输入身份证号"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>地区</Text>
                            <TextInput placeholder="请输入地区"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.infor]}>
                            <Text style={[MessageCSS.nameText]}>介绍</Text>
                            <TextInput
                                multiline={true}
                                placeholder="请输入个人介绍"
                                underlineColorAndroid='transparent'
                                numberOfLines={5}
                                maxLength ={100}
                                style={[MessageCSS.inforInput]}
                                onChangeText={(text) => this.setState({text})}
                            />
                        </View>
                        <TouchableOpacity onPress={this.selectPhotoTapped.bind(this)}>
                    <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 30}]}>
                        { this.state.avatarSource === null ? <Text>选择照片</Text> :
                            <Image style={styles.avatar} source={this.state.avatarSource} />
                        }
                    </View>
                </TouchableOpacity>

                        {/* <TouchableOpacity onPress={() => this.changeHeadImage()} style={[MessageCSS.headerPic]}>
                            <Text style={[MessageCSS.nameText]}>头像</Text>
                            <View style={MessageCSS.inforInput}>
                                {this.createHeaderImage()}
                            </View>
                        </TouchableOpacity> */}
                        <TouchableOpacity style={[MessageCSS.IDCard]}>
                            <Text style={[MessageCSS.nameText]}>身份证（正）</Text>
                            <View style={MessageCSS.inforInput}>
                                {this.createIdCard()}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity  style={[MessageCSS.IDCard]}>
                            <Text style={[MessageCSS.nameText]}>身份证（反）</Text>
                            <View style={MessageCSS.inforInput}>
                                {this.createIdCard()}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={[MessageCSS.IDCard]}>
                            <Text style={[MessageCSS.nameText]}>学历证书</Text>
                            <View style={MessageCSS.inforInput}>
                                {this.createIdCard()}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={[MessageCSS.IDCard]}>
                            <Text style={[MessageCSS.nameText]}>资格证书</Text>
                            <View style={MessageCSS.inforInput}>
                                {this.createIdCard()}
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}