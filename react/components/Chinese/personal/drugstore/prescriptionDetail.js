import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';

import DiagnoseCSS from '../../../../css/personal/diagnose';
export default class PrescriptionDetail extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    // static navigationOptions = {
    // }; 
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
        }
       
    }
 
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={DiagnoseCSS.container}>
                    <View style={[DiagnoseCSS.header]}>
                        <TouchableOpacity style={[DiagnoseCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DiagnoseCSS.headerText}>
                            <Text style={[DiagnoseCSS.titleText]}>订单详情</Text>
                        </View>         
                    </View>
                    <View style={[DiagnoseCSS.prescription]}>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text>牧民姓名：</Text>
                            <Text>{this.state.con.name}</Text>
                        </View>
                        <View style={[DiagnoseCSS.listTop]}>
                            <Text>牧民电话：</Text>
                            <Text>{this.state.con.username}</Text>
                        </View>
                       
                        
                        <View style={{marginTop:4}}>
                            <Text style={[DiagnoseCSS.recommend]}>推荐药品账单</Text>
                            <View style={[DiagnoseCSS.prescript]}>
                            {
                             this.state.con.prescriptions.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                        
                        
                                 return (  
                                <View style={[DiagnoseCSS.medicinal]} key={id}>
                                    <Text>药名：{item.drugName}       数量： {item.drugNumber}盒</Text>
                                  
                                </View>
                                   )
                               
                               
                                })
        
                               }

                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}