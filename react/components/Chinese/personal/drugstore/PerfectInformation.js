import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
    Dimensions,
    ListView,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let {width, height} = Dimensions.get('window');
import MessageCSS from '../../../../css/personal/PerfectInformation';
import RadioGroup  from '../../../../common/RadioGroup';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props)
        this.state = {
            sexArray: [
                {
                    title: '男',
                    image:  require('../../../../img/xuan.png'),
                    image2:require('../../../../img/yixuan.png'),
                },
                {
                    title: '女',
                    image:  require('../../../../img/xuan.png'),
                    image2:require('../../../../img/yixuan.png'),
                }
            ],
            data: {
                name: null,
                avatar: null,
                avatar_file: null,
                sign: {
                    sex: 0,
                    address: null,
                    sign: null
                }
            }
        };
    }
    changeHeadImage() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            api.file.upload({uri: image.path, name: 'image.jpg', type: image.mime}).then(response => {
                api.user.update({avatar: response.id}).then(() => {
                    this.getData()
                    DeviceEventEmitter.emit('userStatusChange')
                })
            })
        })
    }
    createHeaderImage() {
        if(this.state.data.avatar_file === null) {
            return (
                <Image
                    style={MessageCSS.headerImage}
                    source={require('../../../../img/Icon.png')}  >

                </Image>
            )
        } else {
            return (
                <Image
                    style={MessageCSS.headerImage}
                    source={{ uri: this.state.data.avatar_file.path }}  >

                </Image>
            )
        }
    }
    createIdCard() {
        if(this.state.data.avatar_file === null) {
            return (
                <Image
                    style={MessageCSS.IdCardImage}
                    source={require('../../../../img/a8.jpg')}  >

                </Image>
            )
        } else {
            return (
                <Image
                    style={MessageCSS.headerImage}
                    source={{ uri: this.state.data.avatar_file.path }}  >

                </Image>
            )
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={MessageCSS.container}>
                    <View style={[MessageCSS.header]}>
                        <TouchableOpacity style={[MessageCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={MessageCSS.headerText}>
                            <Text style={[MessageCSS.titleText]}>完善资料</Text>
                        </View>         
                    </View>
                    <View  style={[MessageCSS.informations]}>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>药店名称</Text>
                            <TextInput placeholder="请输入药店名称"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>省</Text>
                            <TextInput placeholder="请选择"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>市</Text>
                            <TextInput placeholder="请选择"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>区</Text>
                            <TextInput placeholder="请选择"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.information]}>
                            <Text style={[MessageCSS.nameText]}>地址</Text>
                            <TextInput placeholder="请输入地址"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[MessageCSS.nameInput]}></TextInput>
                        </View>
                        <View style={[MessageCSS.infor]}>
                            <Text style={[MessageCSS.nameText]}>药店信息</Text>
                            <TextInput
                                multiline={true}
                                placeholder="请输入药店信息"
                                underlineColorAndroid='transparent'
                                numberOfLines={5}
                                maxLength ={100}
                                style={[MessageCSS.inforInput]}
                                onChangeText={(text) => this.setState({text})}
                            />
                        </View>
                        <TouchableOpacity onPress={() => this.changeHeadImage()} style={[MessageCSS.headerPic]}>
                            <Text style={[MessageCSS.nameText]}>头像</Text>
                            <View style={MessageCSS.inforInput}>
                                {this.createHeaderImage()}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.changeHeadImage()} style={[MessageCSS.IDCard]}>
                            <Text style={[MessageCSS.nameText]}>药店许可证</Text>
                            <View style={MessageCSS.inforInput}>
                                {this.createIdCard()}
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}