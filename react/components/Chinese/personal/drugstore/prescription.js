import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import MessageCSS from '../../../../css/personal/message';
import api from '../../../../api';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
        
    }; 
    constructor(props) {
        super(props)
        this.state = {
          data: []
        }
      }
    
      componentDidMount() {
        this.getList()
      }

      getList() {
         
        api.drugstore.diagnosef({'login-token':localStorage.token}).then(response => {
           
          this.setState({
            data: response
          })
        })
      }
      
    render() {

        return (
            <ScrollView>
                <View style={MessageCSS.container}>
                    <View style={[MessageCSS.header]}>
                        <TouchableOpacity style={[MessageCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={MessageCSS.headerText}>
                            <Text style={[MessageCSS.titleText]}>推荐订单列表</Text>
                        </View>         
                    </View>
                    <View  style={[MessageCSS.messages]}> 
                    {
                      this.state.data.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                     
                    return (
                        
                        <TouchableOpacity key={id} style={[MessageCSS.message]}  onPress={() =>this.props.navigation.navigate('prescriptionDetail',{itm:item})}>
                            <Text style={[MessageCSS.title]}>{item.name}</Text>
                        </TouchableOpacity>
                        
                            )
                       
                        })

                       }
                       </View>
                </View>
            </ScrollView>
        );
    }
}