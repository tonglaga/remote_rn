import React, { Component } from 'react';
import {
    AppRegistry,
    ArticleCSSheet,
    Text,
    Image,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    ListView,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
let {width, height} = Dimensions.get('window');
import ArticleCSS from '../../../../css/personal/articleManagement';
import RadioGroup  from '../../../../common/RadioGroup';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props)
         this.state = {
            sexArray: [
                {
                    title: '上架',
                    image:  require('../../../../img/hs.png'),
                    image2:require('../../../../img/h.png'),
                },
                {
                    title: '下架',
                    image:  require('../../../../img/hs.png'),
                    image2:require('../../../../img/h.png'),
                }
            ],
            data: {
                name: null,
                avatar: null,
                avatar_file: null,
                sign: {
                    sex: 0,
                    address: null,
                    sign: null
                }
            }
        };
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={ArticleCSS.container} style={{backgroundColor: '#fff'}}>
                    <View style={[ArticleCSS.header]}>
                        <TouchableOpacity style={[ArticleCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ArticleCSS.headerText}>
                            <Text style={[ArticleCSS.titleText]}>编辑药品</Text>
                        </View>         
                    </View>
                    <View  style={[ArticleCSS.informations]}>
                        <View style={[ArticleCSS.information]}>
                            <Text style={[ArticleCSS.nameText]}>修改药品数量</Text>
                            <TextInput 
                            	placeholder="请输入药品数量"
                                underlineColorAndroid='transparent'
                                style={{flex:4,padding:5,borderRadius:3,borderWidth:1, borderColor: '#ccc'}}></TextInput>
                            <TouchableOpacity style={{flex:2}}>
                                <Text style={{backgroundColor:'blue',color:'#fff',alignSelf:'center',borderRadius:3,padding:3,paddingLeft:12,paddingRight:12,}}>确定</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={[ArticleCSS.information]}>
                            <Text style={[ArticleCSS.nameText]}>药品状态</Text>
                            <View style={{height: 44, flex: 4}}>
                                <RadioGroup
                                    style={{flexDirection: 'row',}}//整个组件的样式----这样可以垂直和水平
                                    conTainStyle={{height: 44, width: 80}}//图片和文字的容器样式
                                    imageStyle={{width: 25, height: 25}}//图片样式
                                    textStyle={{color: '#666'}}//文字样式
                                    selectIndex={''}//空字符串,表示不选中,数组索引表示默认选中
                                    data={this.state.sexArray}//数据源
                                    onPress={(index, item)=> {
                                    }}
                                />
                            </View>
                            <TouchableOpacity style={{flex:2}}>
                                <Text style={{backgroundColor:'blue',color:'#fff',alignSelf:'center',borderRadius:3,padding:3,paddingLeft:12,paddingRight:12,}}>确定</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}