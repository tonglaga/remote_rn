import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import AboutCSS from '../../../../css/personal/about';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={AboutCSS.container}>
                    <View style={[AboutCSS.header]}>
                        <TouchableOpacity style={[AboutCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={AboutCSS.headerText}>
                            <Text style={[AboutCSS.titleText]}>中心</Text>
                        </View>         
                    </View>
                    <View style={[AboutCSS.lists]}>
                        <TouchableOpacity style={[AboutCSS.list]} onPress={() => navigate('Prescription')}>
                            <Image source={require('../../../../img/setting.png')} style={[AboutCSS.modelIcon]} />
                            <Text style={[AboutCSS.modelText]}>收到药方</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[AboutCSS.list]} onPress={() => navigate('DrugstoreMedicinal')}>
                            <Image source={require('../../../../img/collect.png')} style={[AboutCSS.modelIcon]} />
                            <Text style={[AboutCSS.modelText]}>药品管理</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[AboutCSS.list]} onPress={() => navigate('DrugstorePerfectInformation')}>
                            <Image source={require('../../../../img/about.png')} style={[AboutCSS.modelIcon]} />
                            <Text style={[AboutCSS.modelText]}>完善资料</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}