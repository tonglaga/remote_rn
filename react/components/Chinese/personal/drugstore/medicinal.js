import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import DrugstoreCSS from '../../../../css/drugstore'
import AboutCSS from '../../../../css/personal/about';

export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
        headerTitle: '药店',
        tabBarLabel: '药店',
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../../../img/ys.png')}
            style={{ width: 20, height: 20, tintColor: tintColor }}
          />
        )
    };
    _onPressButton() {
        console.log("You tapped the button!");
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={DrugstoreCSS.mainStyle}>
                <View style={DrugstoreCSS.container}>
                    <View style={[DrugstoreCSS.header]}>
                        <TouchableOpacity style={[AboutCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={AboutCSS.headerText}>
                            <Text style={[AboutCSS.titleText]}>药品管理</Text>
                        </View>  
                        <TouchableOpacity style={[AboutCSS.backImg]}  onPress={() => navigate('AddMedicinal')}>
                            <Text style={{color:'#ffffff'}}>添加</Text>
                        </TouchableOpacity>          
                    </View>
                    <View style={DrugstoreCSS.drugstores}>

                        <View style={DrugstoreCSS.drugstore}>
                            <View style={DrugstoreCSS.portrait}>
                                <TouchableOpacity onPress={() => navigate('Drug')}>
                                    <Image source={require('../../../../img/a9.jpg')} style={DrugstoreCSS.drugstoreimg} />
                                </TouchableOpacity>
                            </View>
                            <View style={DrugstoreCSS.drugstoreintro}>
                                <Text style={DrugstoreCSS.storename}>某某药品</Text>
                                <Text>数量：15</Text>
                                <Text>状态：下架</Text>
                            </View>
                            <View style={{flex:2,justifyContent: 'center', alignItems: 'center',}}>
                            <TouchableOpacity onPress={() => navigate('EditMedicinal')}>
                                <Text style={{backgroundColor:'blue',color:'#fff',alignSelf:'center',borderRadius:3,padding:3,paddingLeft:12,paddingRight:12,}}>编辑</Text>
                            </TouchableOpacity>
                            <TouchableOpacity>    
                                <Text style={{marginTop:8,backgroundColor:'blue',color:'#fff',alignSelf:'center',borderRadius:3,padding:3,paddingLeft:12,paddingRight:12,}}>删除</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                        
                    </View>
                </View>
            </ScrollView>
        );
    }
}