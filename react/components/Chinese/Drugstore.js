import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import DrugstoreCSS from '../../css/drugstore'
import api from '../../api';
import config from '../../config';
export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
        headerTitle: '药店',
        tabBarLabel: '药店',
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../img/ys.png')}
            style={{ width: 20, height: 20, tintColor: tintColor }}
          />
        )
    };
    constructor(props) {
        super(props)
        this.state = {
          data: []
        }
      }
    _onPressButton() {
        console.log("You tapped the button!");
    }
    componentDidMount() {
        this.getList()
    }
    
    getList() {
        api.grade.drugstoreinfo().then(response => {
         // console.warn(response)
          this.setState({
            data: response
          })
        })
      }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={DrugstoreCSS.mainStyle}>
                <View style={DrugstoreCSS.container}>
                    <View style={DrugstoreCSS.header}>
                        <View style={DrugstoreCSS.title}>
                            <Text style={DrugstoreCSS.titleText}>药店</Text>
                        </View>         
                    </View>
                    <View style={DrugstoreCSS.drugstores}>
                    {
                      this.state.data.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                      return (
                        <View style={DrugstoreCSS.drugstore} key={id}>
                            <View style={DrugstoreCSS.portrait}>
                                <TouchableOpacity onPress={() => navigate('Drug',{itm:item})}>
                                    <Image source={{uri:config.img_url+item.headerUrl}} style={DrugstoreCSS.drugstoreimg} />
                                </TouchableOpacity>
                            </View>
                            <View style={DrugstoreCSS.drugstoreintro}>
                                <Text style={DrugstoreCSS.storename}>{item.pharmacyName}</Text>
                                <Text>{item.provinceName} {item.cityName} {item.areaName}</Text>
                            </View>
                        </View>
                            )
                        })
                       }
                      
                    </View>
                </View>
            </ScrollView>
        );
    }
}