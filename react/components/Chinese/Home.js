import React, { Component } from 'react';
import {
    Platform,
    Text,
    View,
    Image,
    Dimensions,
    ScrollView,
    TouchableOpacity,
    NativeAppEventEmitter,
} from 'react-native';

import JPushModule from 'jpush-react-native'
import Swiper from 'react-native-swiper';
import HomeCSS from '../../css/home'
import api from '../../api';
import config from '../../config';
import { getLanguage, setLanguage } from '../../utils/language'
const { width, height } = Dimensions.get('window');



export default class Home extends Component {
    static navigationOptions = {
        headerTitle: '首页',
        tabBarLabel: '首页',
        tabBarIcon: ({ focused, tintColor }) => (
            <Image
                source={require('../../img/h.png')}
                style={{ width: 20, height: 20, tintColor: tintColor }}
            />
        ),
    };
    
    constructor(props) {
        super(props);
        this.state = {
            swiperShow: false,
             bg: '#ffffff',
             appkey: 'AppKey',
             imei: 'IMEI',
             package: 'PackageName',
             deviceId: 'DeviceId',
             version: 'Version',
             pushMsg: 'PushMessage',
             registrationId: 'registrationId',
             tag: '',
             alias: '',
             expertinfo:[],
             studyvideom:[],
             medicalequipmentm:[]
        };
       
    }
    //幻燈图片
    renderBanner() {
        if (this.state.swiperShow) {
            // console.log ('返回值' + this.state.swiperShow);
            return (
                <Swiper
                   style={HomeCSS.wrapper}
                   height={width * 40 / 75}
                   showsButtons={false}
                   removeClippedSubviews={false} //这个很主要啊，解决白屏问题
                   autoplay={true}
                   horizontal ={true}
                   paginationStyle={HomeCSS.paginationStyle}
                   dotStyle={HomeCSS.dotStyle}
                   activeDotStyle={HomeCSS.activeDotStyle}
                >
                  
                    <Image source={require('../../img/1.jpg')} style={HomeCSS.bannerImg} />
                    <Image source={require('../../img/1.jpg')} style={HomeCSS.bannerImg} />
                    <Image source={require('../../img/1.jpg')} style={HomeCSS.bannerImg} />
                    <Image source={require('../../img/1.jpg')} style={HomeCSS.bannerImg} />
                </Swiper>
            );
        } else {
            return (
                <View style={HomeCSS.wrapper}>
                    <Image source={require('../../img/1.jpg')} style={HomeCSS.bannerImg} />
                </View>
            );
        }
    }
    changeLanguage() {
        getLanguage().then(lang => {
          let change = config.language.list.mn
          setLanguage(change)
        })
      }
    //幻灯片请求函数
    getSlidemList() {
        // api.grade.slidem().then(response => {
        //  // console.warn(response)
        //   this.setState({
        //     data: response
        //   })
        // })
        //三条专家
        api.grade.expertinfos().then(response => {
            
             this.setState({
                expertinfo: response.result
             })
           });

        //三条学习
        api.grade.studyvideoms().then(response => {
            //console.warn(response)
             this.setState({
                 studyvideom: response
              })
           });
        //三条机械
        api.grade.medicalequipmentms().then(response => {
           //  console.warn(response)
             this.setState({
                medicalequipmentm: response
             })
           });
      }

      componentDidMount() {
        setTimeout(() => {
            this.setState({
                swiperShow: true,
            });
        }, 0)
        //幻灯片请求函数
        this.getSlidemList();
        
        if (Platform.OS === 'android') {
               JPushModule.initPush()
               JPushModule.getInfo(map => {
               this.setState({
                appkey: map.myAppKey,
                imei: map.myImei,
                package: map.myPackageName,
                deviceId: map.myDeviceId,
                version: map.myVersion
              })
            })
            
            JPushModule.notifyJSDidLoad(resultCode => {
               // console.warn(resultCode)
              if (resultCode === 0) {
                 // this.props.navigation.navigate('Home')
              }
            })
          } else {
            JPushModule.setupPush()
          }
        

          
          
      JPushModule.addReceiveCustomMsgListener(map => {
      datajie = JSON.parse(map.content);       
      console.warn(datajie)

      //接受请求视频通话数据
      if(datajie.type == "NEW_VIDEO"){
              //专家存储房间路径，id,牧民id 
               localStorage.roomurl=datajie.videoconnect.roomid;
               localStorage.roomid=datajie.videoconnect.id;
               localStorage.hid=datajie.videoconnect.hid;
               this.props.navigation.navigate('Handle');
              //生成json数据
             //   var zuotis = { defaultValues : [{
             //             "id" :datajie.videoconnect.id,
             //             "createTime" :datajie.videoconnect.createTime,
             //             "updateTime" :datajie.videoconnect.updateTime,
             //             "eid" :datajie.videoconnect.eid,
             //             "hid" :datajie.videoconnect.hid,
             //             "roomid":datajie.videoconnect.roomid,
             //             "url":datajie.videoconnect.url,
             //             "time" :datajie.videoconnect.time,
             //             "type" :datajie.videoconnect.type,
             //             "headurl" :"ddd",
             //             "username" :"fff"
             //             }]}
             //把数据存储在tuisong数据组件里    
             //  datatui.newData(zuotis);
             //获取最新一调数据
             // row=datatui.getLastRow().toJson();
             //专家打开通知页面
             //  wind.open({
             //     params:{
             //         rowdata:row,
             //           }
             //       });
      }
     //牧民接受專家通知以后推过来的状态		
      if(datajie.type== "VIDEO_RETURN"){
              //如果点击了接受按钮以后
              if(datajie.returnVc.ifOrNot == 1){
                //   ddd=dddd.comp("windowContainer3");
                //   var jinshi=ddd.getInnerModel();
                //   jinshi.count=30;
                //   jinshi.comp('jindengtai').close();
                //   //让牧民 进入 视频通话界面
                //   justep.Shell.showPage("shiroom");	
               // console.warn(localStorage.roomurl)
               //console.warn('mumin'+localStorage.roomurl)	 
                this.props.navigation.navigate('Videocall',{key:"'"+localStorage.roomurl+"'"});               
              } else{	
                this.props.navigation.navigate('Home');	                
                //  justep.Util.hint("专家拒接接受！");
                //  ddd=dddd.comp("windowContainer3");
                //  var jinshi=ddd.getInnerModel();
                //  jinshi.count=30;
                //  //关闭等待中的页面
                //  jinshi.comp('jindengtai').close();
              }  
      }
     //视频通话牧民点击挂断以后的状态
     if(datajie.type == "VIDEO_STOP_H"){
        this.props.navigation.navigate('Home');	 
             //关闭专家视频房间页面
            //  justep.Shell.closeAllOpendedPages();
            //  justep.Util.hint("本次视频通话结束！");
            //  //并让专家回到首页    
            //  justep.Shell.showPage("main");		           
        }
     //专家点击挂断以后的状态
     if(datajie.type == "VIDEO_STOP_E"){
        this.props.navigation.navigate('Home');	 
             //关闭牧民视频房间页面
            //  justep.Shell.closeAllOpendedPages();
            //  justep.Util.hint("本次视频通话结束！");
            //  //并让牧民回到首页
            //  justep.Shell.showPage("main");	           		           
     }
       //牧民 专家之前挂断或者 无人接听挂断情况
      if(datajie.type == "VC_NOTWAIT"){
        this.props.navigation.navigate('Home');	
             //关闭牧民视频房间页面
            //  ddd=dddd.comp("windowContainer3");
            //  var jinshi=ddd.getInnerModel();
            //  jinshi.comp('jindengtai').close();	           		           
     } 
       
       
       //牧民发送蒙文文件资料，专家收到
       if(datajie.type == "ADD_TASKM"){
        getLanguage().then(lang => {
            let change = config.language.list.mn
            setLanguage(change)
          })
        this.props.navigation.navigate('MSuspending');	
            //  window.menghan=1;
            //  justep.Shell.showPage("zgzhenguan");	           		           
       }
       //牧民发送汉文文件资料，专家收到
       if(datajie.type == "ADD_TASK"){
        this.props.navigation.navigate('Suspending');
            //  window.menghan=2;
            //  justep.Shell.showPage("zgzhenguan");	           		           
       }
       //专家完善蒙文诊断资料，牧民收到
        if(datajie.type == "FINAL_TASKM"){
            getLanguage().then(lang => {
                let change = config.language.list.mn
                setLanguage(change)
              })
            this.props.navigation.navigate('MDiagnose');
            //  window.menghan=1;
            //  justep.Shell.showPage("mmgzhenduan");	           		           
       }
       //专家完善蒙文诊断资料，药店收到
        if(datajie.type == "LOOK_DRUGM"){
            getLanguage().then(lang => {
                let change = config.language.list.mn
                setLanguage(change)
              })
            this.props.navigation.navigate('MPrescription');
            //  window.menghan=1;
            //  justep.Shell.showPage("ygyaofang");	           		           
       }
       //专家完善汉语诊断资料，牧民收到
        if(datajie.type == "FINAL_TASK"){
            this.props.navigation.navigate('Diagnose');
            //  window.menghan=2;
            //  justep.Shell.showPage("mmgzhenduan");	           		           
       }
       //专家完善汉语诊断资料，药店收到
        if(datajie.type == " LOOK_DRUG"){
            this.props.navigation.navigate('Prescription');
            //  window.menghan=2;
            //  justep.Shell.showPage("ygyaofang");	           		           
       }
          })
      
          JPushModule.addReceiveNotificationListener(map => {
           console.warn('alertContent: ' + map.alertContent)
           // console.warn('extras: ' + map.extras)
            
          //  console.warn('content: ' + map.content)

           
            // var extra = JSON.parse(map.extras);
            // console.log(extra.key + ": " + extra.value);
          })
      
          JPushModule.addReceiveOpenNotificationListener(map => {
           // console.warn('Opening notification!')
           // console.warn('map.extra: ' + map.extras)
          //  this.jumpSecondActivity()
            // JPushModule.jumpToPushActivity("SecondActivity");
          })
      
          JPushModule.addGetRegistrationIdListener(registrationId => {
            console.warn('Device register succeed, registrationId ' + registrationId)
          })
      
          // var notification = {
          //   buildId: 1,
          //   id: 5,
          //   title: 'jpush',
          //   content: 'This is a test!!!!',
          //   extra: {
          //     key1: 'value1',
          //     key2: 'value2'
          //   },
          //   fireTime: 2000
          // }
         //  JPushModule.sendLocalNotification(notification)
          
    }
    goVideo(){
        //this.props.navigation.navigate('Handle'); 
    }
    componentWillMount () {}

   
  
    componentWillUnmount () {
      //JPushModule.removeReceiveCustomMsgListener(receiveCustomMsgEvent)
     // JPushModule.removeReceiveNotificationListener(receiveNotificationEvent)
     // JPushModule.removeReceiveOpenNotificationListener(openNotificationEvent)
     // JPushModule.removeGetRegistrationIdListener(getRegistrationIdEvent)
     // console.warn('Will clear all notifications')
    //  JPushModule.clearAllNotifications()
    }
    render() {
       
       // localStorage.deviceId=this.state.deviceId.DeviceId;
      // console.warn('registrationId1111: ' +this.state.registrationId);
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={HomeCSS.mainStyle}>
                <View style={HomeCSS.container}>
                    <View style={HomeCSS.header}>
                        <View style={HomeCSS.title}>
                            <Text style={HomeCSS.titleText}>远程治疗</Text>
                        </View>
                        <View style={HomeCSS.language}>
                            <TouchableOpacity onPress={() =>this.changeLanguage()}>
                                <Text style={HomeCSS.languageText}>切换蒙文版</Text>
                            </TouchableOpacity>
                        </View>          
                    </View>
                    <View style={HomeCSS.banner}>
                        {this.renderBanner()}
                    </View>
                    <View style={HomeCSS.Models}>
                    <TouchableOpacity onPress={() =>this.props.navigation.navigate('Videocall',{key:'665231'}) } style={HomeCSS.Model}>
                        {/* <TouchableOpacity onPress={() =>this.props.navigation.navigate('Expert')} style={HomeCSS.Model}> */}
                            <View style={HomeCSS.MImg}>
                                    <Image source={require('../../img/shipin.png')} style={HomeCSS.Micon} />
                            </View>
                            <View style={HomeCSS.MText}>
                                <Text style={HomeCSS.modelText}>诊断</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity  onPress={() => this.props.navigation.navigate('Study')} style={HomeCSS.Model}>
                            <View style={HomeCSS.MImg}>
                                    <Image source={require('../../img/xuexi.png')} style={HomeCSS.Micon} />
                            </View>
                            <View style={HomeCSS.MText}>
                                <Text style={HomeCSS.modelText}>学习</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Drugstore')} style={HomeCSS.Model}>
                            <View style={HomeCSS.MImg}>
                                    <Image source={require('../../img/yao.png')} style={HomeCSS.Micon} />
                            </View>
                            <View style={HomeCSS.MText}>
                                <Text style={HomeCSS.modelText}>药店</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={HomeCSS.experts}>
                        <View style={HomeCSS.expertText}>
                            <Text style={HomeCSS.Etitle}>专家</Text>
                        </View>
                        <View style={HomeCSS.expertimg}>
                        {
                      this.state.expertinfo.map((item,id) => {
                        
                          return (
                            <View style={HomeCSS.expertF}  key={id}>
                                <TouchableOpacity onPress={() => navigate('ExpertDetail',{itm:item})}>
                                    <Image source={{uri:config.img_url+item.headerUrl}} style={HomeCSS.Eimage} />
                                </TouchableOpacity>
                                <View><Text style={HomeCSS.name}>{item.name}</Text></View>
                            </View>
                                   )
                                })
                               }
                        </View>
                    </View>
                    <View style={HomeCSS.experts}>
                        <View style={HomeCSS.expertText}>
                            <Text style={HomeCSS.Etitle}>学习</Text>
                        </View>
                        <View style={HomeCSS.expertimg}>
                        {
                      this.state.studyvideom.map((item,id) => {
                        
                          return (
                            <View style={HomeCSS.expertF}  key={id}>
                                <TouchableOpacity  onPress={() => navigate('Video',{itm:item})}>
                                    <Image source={{uri:config.img_url+item.coverPhoto}} style={HomeCSS.Eimage} />
                                    <View><Image source={require('../../img/player.png')} style={HomeCSS.player} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                               )
                            })
                           }
                           
                        </View>
                    </View>
                    <View style={HomeCSS.experts}>
                        <View style={HomeCSS.expertText}>
                            <Text style={HomeCSS.Etitle}>器材</Text>
                        </View>
                        <View style={HomeCSS.expertimg}>
                        {
                      this.state.medicalequipmentm.map((item,id) => {
                          return (
                            <View style={HomeCSS.expertF}  key={id}>
                                <TouchableOpacity onPress={() => navigate('Appliance',{itm:item})}>
                                    <Image source={{uri:config.img_url+item.image}} style={HomeCSS.Eimage} />
                                </TouchableOpacity>
                            </View>
                            )
                        })
                       }
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
}
