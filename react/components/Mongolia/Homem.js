import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    Image,
    View,
    Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
        headerTitle: '个人',
        tabBarLabel: '个人',
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../img/gs.png')}
            style={{ width: 20, height: 20, tintColor: tintColor }}
          />
        )
    }; 
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{flex:1}}>

            <View style={{flex: 2,backgroundColor: 'powderblue'}}>   
               <Text>通拉嘎汉语侧测</Text>
            </View>

            <View style={[PersonalCSS.xuanzhuan,{flex:4}]}>

                <View style={{flex: 2,backgroundColor: 'skyblue'}}>
                    <Text style={{width:400,fontFamily:'MongolianWhitePuaMirror',transform:[
            {rotateX:'180deg'},
            {translateX:-10}
		]}}>1ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  2 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 3 
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  4 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 5
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  6 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 7
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  8 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 9
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  10 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 11
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  12 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 13
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  10 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 11
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  12 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 13
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  10 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 11
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  12 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 13
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  10 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 11
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  12 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 13
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  10 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 11
        ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ  12 ᠲᠤᠩᠭᠠᠯᠠᠭ  ᠲᠤᠷᠰᠢᠪᠠᠵᠤ  ᠪᠠᠢᠨ᠎ᠠ 13
        </Text>
                </View>

                <View style={{flex:2,backgroundColor: 'steelblue'}}> 

                </View>

            </View>
            </View>
        );
    }
}
const PersonalCSS = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e7ebec',
    },
    titleBox: {
	    width: '100%',
	    height: 180,
	    flexDirection:'row',
	 },
	 titleCommon: {
	 	flex:1,
	 	justifyContent: 'center',
	 	alignItems: 'center',
	 },
	 login: {
	 	backgroundColor: '#2685c9',
	 	color: '#fff',
	 	paddingTop:12,
	 	paddingBottom:12,
	 	width:30,
	 	paddingLeft:8,
	 	paddingRight:3,
	 	borderColor: '#f3f8fc',
	 	borderWidth:1,
	 	borderRadius:6,
	 },
	 leave: {
	 	backgroundColor: '#2685c9',
	 	color: '#fff',
	 	paddingTop:12,
	 	paddingBottom:12,
	 	width:30,
	 	paddingLeft:8,
	 	paddingRight:3,
	 	borderColor: '#f3f8fc',
	 	borderWidth:1,
	 	borderRadius:6,
	 },
	 headerImg: {
	 	borderRadius:100,
	 	height:100,
	 	width:100
	 },
	 personalList: {
        flex:1,
	 //	height:30,
	 	marginTop:15,
	 	backgroundColor: '#fff',
	 	flexDirection:'row',
	 	paddingTop:8,
	 },
	 lists: {
        // height:
         flex:1,
         marginTop:10
	 	//alignItems: 'center',
	 },
	 modelIcon: {
	 	//height: 50,
	 	//width:50,
	 },
	 borderRi: {
	 	borderRightWidth: 1,
	 	borderRightColor: '#ccc'
	 },
	 modelText: {
	 	// padding:10,
	 	 width:30,
	 },
	 zitirr:{
		 fontFamily:'MongolianWhite'
	 },
	 xuanzhuan:{
		transform:[
			{rotateZ:'90deg'}
		] 
	 }

});