import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    ToastAndroid,
    DeviceEventEmitter,
    Dimensions
} from 'react-native';
import LoginCSS from '../../../css/register';
import api from '../../../api'
import RadioGroup  from '../../../common/RadioGroup';
import CountDownButton from 'react-native-smscode-count-down'
const { width, height } = Dimensions.get('window');
export default class Expert extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props)
        this.state = {
            sexArray: [
                {
                    title: 'ᠮᠠᠯᠴᠢᠨ',
                    image:  require('../../../img/xuan.png'),
                    image2:require('../../../img/yixuan.png'),
                },
                {
                    title: 'ᠮᠡᠷᠭᠡᠵᠢᠯᠲᠡᠨ',
                    image:  require('../../../img/xuan.png'),
                    image2:require('../../../img/yixuan.png'),
                },
                {
                    title: 'ᠡᠮ  ᠦᠨ  ᠳᠡᠯᠭᠡᠭᠦᠷ',
                    image:  require('../../../img/xuan.png'),
                    image2:require('../../../img/yixuan.png'),
                }
            ],
            rpass:null,
            state: '这里显示状态',
            form:{
               username: "",
               code: "",
               pass: "",
               type: "",
               rid: ""
            }
        };
    }
    submit(){
        // console.warn(this.state.form)
        if(this.state.form.username=="" || this.state.form.code =="" || this.state.form.pass == "" || this.state.rpass=="" || this.state.form.type == "" || this.state.form.rid == ""){
            ToastAndroid.show('所有的内容都是必填项', ToastAndroid.SHORT);
            return; 
        }

        if(this.state.form.pass != this.state.rpass){
            ToastAndroid.show('两次密码不一致', ToastAndroid.SHORT);
            return; 
        }

        api.user.checkMsg(this.state.form).then(response => {
             //console.warn(response)  
             if(response.code == 401){
                ToastAndroid.show(response.message, ToastAndroid.SHORT);
                //DeviceEventEmitter.emit('register', {username:this.state.form.username,pass:this.state.form.pass})
               // this.props.navigation.goBack(null);
             }else{
                ToastAndroid.show("注册成功，重新登录", ToastAndroid.SHORT);
                DeviceEventEmitter.emit('register', {username:this.state.form.username,pass:this.state.form.pass})
                this.props.navigation.goBack(null);
             }
            
        })
       
    }
    render() {
        const { navigate } = this.props.navigation;
        const {username} = this.state.form;
        const {pass} = this.state.form;
        const {rpass} = this.state;
        const {code} = this.state.form;
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}></Text>
                        </View>         
                    </View>
                    <View style={{transform:[
                            {rotateX:'180deg'},
                            {rotateZ:'270deg'}
                        ],width:height-80,height:515,marginTop:(height-80)/2-515/2,marginLeft:515/2-(height-80)/2,}}>
                        <View style={[LoginCSS.loginInformation]}>
                            <Text style={[LoginCSS.loginText,LoginCSS.Mongolia]}>ᠨᠡᠪᠲᠡᠷᠡᠬᠦ ᠨᠡᠷ᠎ᠡ᠄</Text>
                            <TextInput
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput]}
                                value={username} onChangeText={(value)=>{
                                    let form = this.state.form
                                       form.username = value
                                    this.setState({
                                   
                                        form: form
                                       
                                    })
                                  }}
                            ></TextInput>
                        </View>
                        <View style={[LoginCSS.loginInformation]}>
                            <Text style={[LoginCSS.loginText,LoginCSS.Mongolia]}>ᠤᠬᠤᠷ ᠴᠢᠮᠡᠭᠡ</Text>
                            <TextInput
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.verificationInput]}
                                value={code} onChangeText={(value)=>{
                                    let form = this.state.form
                                       form.code = value
                                    this.setState({
                                   
                                        form: form
                                       
                                    })
                                  }}
                                >
                                </TextInput>
                            <TouchableOpacity style={{flex:1,}}>

                              
                                <CountDownButton
                                style={LoginCSS.Mongolia}
                                style={{width: 110,marginRight: 10}}
                                textStyle={{color: 'blue'}}
                                enable={username != ""}
                                timerCount={60}
                                timerTitle={'ᠱᠢᠯᠭᠠᠬᠤ  ᠨᠤᠮᠸ᠋ᠷ  ᠤᠯᠤᠬᠤ'}
                                timerActiveTitle={['（','s） ᠬᠤᠢᠨ᠎ᠠ  ᠲᠤᠷᠰᠢ']}
                                onClick={(shouldStartCounting)=>{
                                    //随机模拟发送验证码成功或失败
                                   // const requestSucc = Math.random() + 0.5 > 1; 
                                 
                                   api.user.sendMsg({tel:username}).then(response => {
                                   
                                   })
                                   shouldStartCounting(true);
                                   //this._requestAPI(shouldStartCounting)
                                    
                                }}
                              />
                            </TouchableOpacity>
                        </View>
                        <View style={[LoginCSS.loginInformation]}>
                            <Text style={[LoginCSS.loginText,LoginCSS.Mongolia]}>ᠨᠢᠭᠤᠴᠠ ᠦᠭᠡ᠄</Text>
                            <TextInput
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput]}
                                value={pass} onChangeText={(value)=>{
                                    let form = this.state.form
                                    form.pass = value
                                 this.setState({
                                
                                     form: form
                                    
                                 })
                                  }}
                                
                                >
                                </TextInput>
                        </View>
                        <View style={[LoginCSS.loginInformation]}>
                            <Text style={[LoginCSS.loginText,LoginCSS.Mongolia]}>ᠨᠢᠭᠤᠴᠠ ᠪᠠᠲᠠᠯᠠᠬᠤ</Text>
                            <TextInput
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput]}
                                value={rpass} onChangeText={(value)=>{
                                    this.setState({
                                        rpass: value 
                                    })
                                  }}
                                >
                                </TextInput>
                        </View>
                        <View style={[LoginCSS.loginInformation]}>
                            <RadioGroup
                                style={{flexDirection: 'row',}}//整个组件的样式----这样可以垂直和水平
                                conTainStyle={{height: 44, width:width/3}}//图片和文字的容器样式
                                imageStyle={{width: 25, height: 25}}//图片样式
                                textStyle={{color: '#666',fontFamily:'MongolianWhitePuaMirror',}}//文字样式
                                selectIndex={''}//空字符串,表示不选中,数组索引表示默认选中
                                data={this.state.sexArray}//数据源
                                onPress={(index, item)=> {
                                    if(index == 1){
                                    
                                        let form = this.state.form
                                        form.type = 2
                                        form.rid = 5
                                     this.setState({
                                    
                                         form: form
                                        
                                     })
                                        
                                     }else if(index == 2){
                                       
                                        let form = this.state.form
                                        form.type = 3
                                        form.rid = 6
                                     this.setState({
                                    
                                         form: form
                                        
                                     })
                                      
                                     }else{
                                       
                                        let form = this.state.form
                                        form.type = 1
                                        form.rid = 4
                                     this.setState({
                                    
                                         form: form
                                        
                                     })
                                     }
                                    }}
                            />
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]} onPress={()=>this.submit()}>
                                <Text style={[LoginCSS.loginbtnText,LoginCSS.Mongolia]}>ᠳᠠᠩᠰᠠᠯᠠᠬᠤ</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}