import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    Linking,
    ToastAndroid
} from 'react-native';
import ExpertCSS from '../../css/expert/MExpert';
import api from '../../api';
import config from '../../config';
export default class Expert extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
        headerTitle: '专家',
        tabBarLabel: '专家',
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../img/zs.png')}
            style={{ width: 40, height: 40, tintColor: tintColor }}
          />
        ),
    };

    constructor(props) {
        super(props)
        this.state = {
          data: []
        }
      }
    
      componentDidMount() {
        this.getList()
      }

      getList() {
        api.categoriesm.hanzhuanjia().then(response => {
         // console.warn(response)
          this.setState({
            data: response
          })
        })
      }
      //拨打电话
      gettell(name){
        if(localStorage.type != 1){
            ToastAndroid.show('只有牧民才有通话！', ToastAndroid.SHORT);
          return;
        }  
        if(localStorage.wanshan == 1){
            ToastAndroid.show('请先完善个人信息！', ToastAndroid.SHORT);
            return;
        }
       
       this.linking('tel:'+name);
    }
   linking=(url)=>{
   
   // console.log(url);

    Linking.canOpenURL(url).then(supported => {
        if (!supported) {
            console.warn('不能打电话');
        } else {
            return Linking.openURL(url);
        }
    }).catch(err => console.error('打电话失败', err));

 }
  //发送视频通话
  getwaiting(i){
    if(localStorage.type != 1){
        ToastAndroid.show('只有牧民才有通话！', ToastAndroid.SHORT);
      return;
    }  
    if(localStorage.wanshan == 1){
        ToastAndroid.show('请先完善个人信息！', ToastAndroid.SHORT);
        return;
    }
    if(i.status != 1){
        ToastAndroid.show('抱歉，专家忙碌！', ToastAndroid.SHORT);
        return; 
    }
    this.props.navigation.navigate('Waiting',i)
   }
   //发送文件材料
   getAvatarURL(name){
    if(localStorage.type != 1){
        ToastAndroid.show('只有牧民才有通话！', ToastAndroid.SHORT);
      return;
    }  
    if(localStorage.wanshan == 1){
        ToastAndroid.show('请先完善个人信息！', ToastAndroid.SHORT);
        return;
    }
    
    this.props.navigation.navigate('Consult',name);

   }
    _onPressButton() {
        console.log("You tapped the button!");
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={ExpertCSS.mainStyle}>
                <View style={ExpertCSS.container}>
                    <View style={ExpertCSS.header}>
                        <View style={ExpertCSS.title}>
                            <Text style={ExpertCSS.titleText}>专家</Text>
                        </View>         
                    </View>
                    { 
                      this.state.data.map((item,id) => {
                        let zhuangtai=[];
                        if(item.state == 1 ){
                           zhuangtai.push(<Text style={[ExpertCSS.status, ExpertCSS.online]} key={id}>ᠪᠠᠢᠨ᠎ᠠ</Text>); 
                         }else if(item.state == 2){
                           zhuangtai.push(<Text style={[ExpertCSS.status, ExpertCSS.busy]} key={id}> ᠶᠠᠭᠠᠷᠠᠤ</Text>);
                         }else{
                           zhuangtai.push(<Text style={[ExpertCSS.status, ExpertCSS.offline]} key={id}>ᠦᠭᠡᠢ</Text>);
                         }
                          return (
                    <View style={ExpertCSS.experts} key={id}>
                        <View style={ExpertCSS.portrait} >
                            <TouchableOpacity   onPress={() => navigate('MExpertDetail',{itm:item})}>
                                <Image  source={{uri:config.img_url+item.headerUrl}} style={ExpertCSS.expertimg} />
                                <View style={ExpertCSS.state}>
                                {zhuangtai}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={ExpertCSS.infomation} >
                            <Text style={ExpertCSS.name} >{item.nameMeng}</Text>
                            <Text style={ExpertCSS.department}  >{item.expertiseMeng}</Text>
                        </View>
                        <View style={ExpertCSS.expertintro} >
                            <View style={ExpertCSS.dealing} >
                                <Text style={ExpertCSS.dealingP} >ᠣᠨᠣᠰᠢᠯᠠᠭᠰᠠᠨ</Text>
                                <Text style={ExpertCSS.dealingN}  >{item.dealingProblems}</Text>
                            </View>
                            <View style={ExpertCSS.handle} >
                                <View style={ExpertCSS.video} >
                                    <TouchableOpacity  onPress={() =>this.getwaiting({id:item.uid,name:item.name,img:item.headerUrl,status:item.state})}>
                                        <Image  source={require('../../img/x.png')} style={ExpertCSS.btn} />
                                    </TouchableOpacity>
                                </View>
                                <View style={ExpertCSS.phone} >
                                    <TouchableOpacity   onPress={()=> this.gettell(item.username)}>
                                        <Image source={require('../../img/x.png')} style={ExpertCSS.btn} />
                                    </TouchableOpacity>
                                </View>
                                <View style={ExpertCSS.message} >
                                    <TouchableOpacity  onPress={() =>this.getAvatarURL({id:item.uid})}>
                                        <Image  source={require('../../img/x.png')} style={ExpertCSS.btn} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                          )
                     })
                    }
                </View>
            </ScrollView>
        );
    }

}