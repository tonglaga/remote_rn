import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions
} from 'react-native';
const { width, height } = Dimensions.get('window');
import DrugstoreCSS from '../../../css/drugstore/MDrugstore'
import config from '../../../config';
export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，
    static navigationOptions = {
        
    };
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
        }
       
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={DrugstoreCSS.mainStyle}>
                <View style={DrugstoreCSS.container}>
                    <View style={DrugstoreCSS.header}>
                        <TouchableOpacity style={DrugstoreCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DrugstoreCSS.title}>
                            <Text style={DrugstoreCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={DrugstoreCSS.drugstores}>
                        <View style={[DrugstoreCSS.drugstore,{flex:2}]}>
                            <View style={DrugstoreCSS.portrait}>
                                <Image source={{uri:config.img_url+this.state.con.drugUrl}} style={DrugstoreCSS.drugstoreimg} />
                            </View>
                            <View style={[DrugstoreCSS.drugstoreintro,{flex:2}]}>
                                <Text style={[DrugstoreCSS.storename,{flex:2}]}>{this.state.con.titlemeng}</Text>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],flex:2}}>ᠰᠠᠩ  ᠪᠠᠢᠬᠤ   ᠲᠤᠭ᠎ᠠ : {this.state.con.number}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={DrugstoreCSS.medicinalList}>
                        <View style={DrugstoreCSS.medicinalListTop}>
                            <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],position: 'relative',top:140,width:height-222,}}>ᠡᠮ  ᠦᠨ ᠲᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ</Text>
                        </View>
                        <View style={[DrugstoreCSS.medicinals,{backgroundColor:'#fff'}]}>
                            <TouchableOpacity style={{height:height-235,backgroundColor: '#fff',padding:5,margin:3,}}  onPress={() => navigate('MMedicinal')}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],position: 'relative',width:height-250,height:width-50,left:(width-50)/2-(height-250)/2,top:(height-250)/2-(width-50)/2,}}>{this.state.con.contentmeng}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }
} 