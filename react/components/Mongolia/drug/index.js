import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions
} from 'react-native';
const { width, height } = Dimensions.get('window');
import DrugstoreCSS from '../../../css/drugstore/MDrugstore'
import api from '../../../api'
export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，
    static navigationOptions = {
        
    };
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
                data:[],
               
             
        }
       
    }

    componentDidMount() {
        this.getList()
    }
    
    getList() {
        api.gradem.Drug({uid:this.state.con.uid}).then(response => {
         // console.warn(response)
          this.setState({
            data: response
          })
          
        })
      }
 
    render() {
        const { navigate } = this.props.navigation;

        return (
            <ScrollView style={DrugstoreCSS.mainStyle}>
                <View style={DrugstoreCSS.container}>
                    <View style={DrugstoreCSS.header}>
                        <TouchableOpacity style={DrugstoreCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={DrugstoreCSS.title}>
                            <Text style={DrugstoreCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={DrugstoreCSS.drugstores}>
                        <View style={[DrugstoreCSS.drugstore,{flex:2}]}>
                            <View style={DrugstoreCSS.portrait}>
                                <Image source={{uri:config.img_url+this.state.con.headerUrl}}  style={DrugstoreCSS.drugstoreimg} />
                            </View>
                            <View style={[DrugstoreCSS.drugstoreintro,{flex:2}]}>
                                <Text style={[DrugstoreCSS.storename,{flex:2}]}>{this.state.con.pharmacyNameMeng} </Text>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],flex:2}}>
                                   {this.state.con.provinceNameMeng}
                                   {this.state.con.cityNameMeng}
                                   {this.state.con.areaNameMeng}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={DrugstoreCSS.medicinalList}>
                        <View style={DrugstoreCSS.medicinalListTop}>
                            <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],position: 'relative',top:140,width:height-222,}}> ᠲᠤᠰ ᠳᠡᠯᠭᠡᠭᠦᠷ  ᠦᠨ  ᠡᠮ</Text>
                        </View>
                        <ScrollView horizontal={true} style={DrugstoreCSS.medicinals}>
                        {
                        this.state.data.map((item,id) => {
                            return(
                            <TouchableOpacity key={id} style={DrugstoreCSS.medicinal}  onPress={() => navigate('MMedicinal',{itm:item})}>
                                <Image style={DrugstoreCSS.medicinalImg}  source={{uri:config.img_url+item.drugUrl}}  />
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],position: 'relative',width:height-320,height:20,left:10-(height-320)/2+28,top:(height-320)/2-5,}}>{item.titlemeng}</Text>
                            </TouchableOpacity>
                             )
                             })
                            }
                        </ScrollView>
                    </View>
                </View>
            </ScrollView>
        );
    }
}