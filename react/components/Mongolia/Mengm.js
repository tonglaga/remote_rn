import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput
} from 'react-native';

import LoginCSS from '../../css/login';


export default class Mengm extends Component {
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        this.state = {
           
            form: {
                username: '',
                password: ''
            }
        }
        
    }
    
    //点击事件函数
    login() {    

      }
    //设置手机号
    setPhone(phone) {
        let form = this.state.form
        form.username = phone
        this.setState({
            form: form
        })
    }
    //设置密码
    setPassword(password) {
        let form = this.state.form
        form.password = password
        this.setState({
            form: form
        })
    }
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };
    _onPressButton() {
        console.log("You tapped the button!");
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}>登录</Text>
                        </View>         
                    </View>
                    <View>
                        <View style={[LoginCSS.loginInformation]}>
                            <Text style={[LoginCSS.loginText]}>账号</Text>
                            <TextInput placeholder="请输入手机号"
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput]}
                                onChangeText={(username) => this.setPhone(username)}
                            ></TextInput>
                        </View>
                        <View style={[LoginCSS.loginInformation]}>
                            <Text style={[LoginCSS.loginText]}>密码</Text>
                            <TextInput placeholder="请输入密码"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput]}
                                onChangeText={(password) => this.setPassword(password)}>
                                </TextInput>
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]} 
                            //点击事件，要记得绑定
                            onPress={() => this.login()}>
                                <Text style={[LoginCSS.loginbtnText]}>登录</Text>
                            </TouchableOpacity>
                            <View style={[LoginCSS.handle]}>
                                <TouchableOpacity style={[LoginCSS.register]}>
                                    <Text style={[LoginCSS.registerText]}>注册账号</Text>
                                </TouchableOpacity>
                                <View>
                                    <Text style={[LoginCSS.uprightText]}> | </Text>
                                </View>
                                <TouchableOpacity style={[LoginCSS.forgotpassword]}>
                                    <Text style={[LoginCSS.forgotpasswordText]}>忘记密码</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}