import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ToastAndroid,
    ImageBackground,
    DeviceEventEmitter,
    Dimensions,
 
} from 'react-native';
import PersonalCSS from '../../css/personal/MPersonal';
import { getToken } from '../../utils/token'
import { removeToken } from '../../utils/token'
const { width, height } = Dimensions.get('window');
//为了测试在线输入法而添加的

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
        headerTitle: '个人',
        tabBarLabel: '个人',
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../img/gs.png')}
            style={{ width: 20, height: 20, tintColor: tintColor }}
          />
        )
    }; 
    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state
        this.state = {
            headImgUrl: require('../../img/a8.jpg'),
            loginedstatus:null,
            loginbtn:null,
            siginoutbtn:null,
            
        }
    }
    //跳转个人中心 
    gogo(){
       
        //判断token,失效重新登录
        getToken().then(token => {
           
            if(token !== null) {
               
                if(localStorage.type==1){
                    this.props.navigation.navigate('MFCenter');
                   }else if(localStorage.type == 2){
                    this.props.navigation.navigate('MECenter');
                   }else if(localStorage.type == 3){
                    this.props.navigation.navigate('MDCenter');
                 }
            }
        }).catch(err => {
           
            ToastAndroid.show('请先登录！', ToastAndroid.SHORT);
        })
    
       
    }
    componentDidMount() {

        DeviceEventEmitter.addListener('headImgUrl', (message) => {
          //  console.warn(message)
            this.getMessage(message); 
            
        })

        //判断用户登录状态
        getToken().then(token => {
            if(token !== null) {
                this.state.loginedstatus=1;
                this.setState({
                    siginoutbtn: <TouchableOpacity style={[PersonalCSS.titleCommon]} onPress={() => this.siginout()}>
                    <Text style={[PersonalCSS.leave]}>ᠭᠠᠷᠬᠤ</Text>
                </TouchableOpacity>
                })
                
               }
           }).catch(err => {
            this.state.loginedstatus=0;
           
            this.setState({
                loginbtn: <TouchableOpacity style={[PersonalCSS.titleCommon]}  onPress={() => this.siginin()}>
                <Text style={[PersonalCSS.login]}>ᠨᠡᠪᠲᠡᠷᠡᠬᠦ</Text>
            </TouchableOpacity>
             })

            })
            let states=this.state;
            if(localStorage.headImgUrl == null){
     
             states.headImgUrl=require('../../img/a8.jpg')
             this.setState({
                 states:states
                })
            }else{
               
                states.headImgUrl= localStorage.headImgUrl; 
             this.setState({
                 states:states
                })
            }
     
    }
    //跳转登陆
    siginin(){
        this.props.navigation.navigate('MLogin');
    }
    //退出登录
    siginout(){
        removeToken();
        this.setState({
            siginoutbtn: 0,
            siginoutbtn: null,
            loginbtn: <TouchableOpacity style={[PersonalCSS.titleCommon]}  onPress={() => this.siginin()}>
            <Text style={[PersonalCSS.login]}>ᠨᠡᠪᠲᠡᠷᠡᠬᠦ</Text>
        </TouchableOpacity>
        })
    }
    getMessage(message) {
       // console.warn(message)
        this.setState({
            headImgUrl: message,
            siginoutbtn: <TouchableOpacity style={[PersonalCSS.titleCommon]} onPress={() => this.siginout()}>
            <Text style={[PersonalCSS.leave]}>ᠭᠠᠷᠬᠤ</Text>
        </TouchableOpacity>,
           loginbtn: null
        })
        
    }
    //跳转消息中心
      goMessage(){
        //判断token,失效重新登录
        getToken().then(token => {
            if(token !== null) {
             this.props.navigation.navigate('MMessage');
                
            }
        }).catch(err => {
            ToastAndroid.show('请先登录！', ToastAndroid.SHORT);
        })
    
       
    }
     //跳转收藏中心
     goCollect(){
        //判断token,失效重新登录
        getToken().then(token => {

            if(token !== null) {
                    this.props.navigation.navigate('MCollect');          
            }

        }).catch(err => {
            ToastAndroid.show('请先登录！', ToastAndroid.SHORT);
           // this.props.navigation.navigate('Login');
        })
    
       
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={PersonalCSS.container}>
              
                    <ImageBackground
                        source={require('../../img/bg-top.png')}
                        style={[PersonalCSS.titleBox]}
                      >
                        <View style={[PersonalCSS.titleCommon]}>
                            {this.state.loginbtn}
                        </View>
                        <View style={[PersonalCSS.titleCommon]}>
                          <Image source={this.state.headImgUrl} style={[PersonalCSS.headerImg]} />
                        </View>
                        <View style={[PersonalCSS.titleCommon]}>
                            {this.state.siginoutbtn}
                        </View>
                    </ImageBackground>
                    <View  style={PersonalCSS.personalList}>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]}  onPress={() =>this.gogo()}>
                            <Image source={require('../../img/center.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠲᠦᠪ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]}  onPress={() =>this.goMessage()}>
                            <Image source={require('../../img/message.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠮᠡᠳᠡᠭᠡ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]} onPress={() => this.goCollect()}>
                            <Image source={require('../../img/collect.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠰᠠᠨ</Text>
                        </TouchableOpacity>

               
                    </View>
                </View>
            </ScrollView>
        );
    }
}