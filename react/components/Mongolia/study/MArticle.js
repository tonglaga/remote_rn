import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions
} from 'react-native';
const { width, height } = Dimensions.get('window');
import ArticleCSS from '../../../css/study/MArticle';
import config from '../../../config';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
        }
       
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={[ArticleCSS.container]}>
                    <View style={[ArticleCSS.header]}>
                        <TouchableOpacity style={[ArticleCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ArticleCSS.headerText}>
                            <Text style={[ArticleCSS.titleText]}></Text>
                        </View>
                        <TouchableOpacity style={[ArticleCSS.backImg]}>
                            <Image source={require('../../../img/star-3.png')} style={{height:23,width:23}}/>
                        </TouchableOpacity>  
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor:'#fff',margin:4,borderRadius:6,}}>
                        <View style={[ArticleCSS.content,{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height-85,marginTop:20,marginLeft:-20,}]}>
                                <View>   
                                    <Text style={[ArticleCSS.title]}>{this.state.con.title}</Text>
                                </View>  
                                <View style={[ArticleCSS.infors]}>
                                    <Text style={[ArticleCSS.reading,{fontFamily:'MongolianWhitePuaMirror',}]}>ᠦᠵᠡᠭᠰᠡᠨ：{this.state.con.clicks}</Text>
                                    <Text style={[ArticleCSS.time,{fontFamily:'MongolianWhitePuaMirror',}]}>{this.state.con.createTime}</Text>
                                </View>
                                <View style={{height:300,}}>
                                    <Image source={{uri:config.img_url+this.state.con.coverPhoto}} style={[ArticleCSS.coverImage,{transform:[
                                                {rotateX:'180deg'},
                                                {rotateZ:'270deg'}
                                            ]}]}/>
                                </View>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',fontSize:16,}}>
                                {this.state.con.content}  
                                </Text>
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}