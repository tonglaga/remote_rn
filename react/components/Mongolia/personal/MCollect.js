import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import StudyCSS from '../../../css/personal/MCollect'
import api from '../../../api';
import config from '../../../config';
export default class Study extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    }; 

    constructor(props) {
        super(props)
      //  const { params } = this.props.navigation.state
        this.state = {
          filterNum: 1,
          articles:[],
          videos:[],
          cases:[],
 
        }
    }
    componentDidMount() {
        this.getArticles()
    }  

    getArticles() {
        api.collects.articles({'login-token':localStorage.token}).then(response => {
            //console.warn(response)
            if(response.code != 400){
                this.setState({
                    articles: response
               })
            }
            
        })
        api.collects.videos({'login-token':localStorage.token}).then(response => {
            if(response.code != 400){
                this.setState({
                    videos: response
                })
            }
           
        })
    
        api.collects.cases({'login-token':localStorage.token}).then(response => {
            if(response.code != 400){
                this.setState({
                    cases: response
                })
            }
           
        })
     
    }
    getFilter() {
   
        if(this.state.filterNum === 1) {
            return (
                <ScrollView horizontal={true} style={{marginLeft:55,flexDirection:'row',}}>
                  {
                    this.state.articles.map((item,id) => {
                     return (
                    <TouchableOpacity key={id} style={[StudyCSS.content]} onPress={() => this.props.navigation.navigate('MArticle')}>
                        <Image source={{uri:config.img_url+item.coverPhoto}} style={StudyCSS.drugstoreimg} />
                        <View style={StudyCSS.info}>
                            <View style={{flex:1,}}>
                                <Text style={StudyCSS.articletitle}>{item.title}</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={StudyCSS.reading}>ᠦᠵᠡᠭᠰᠡᠨ：{item.clicks}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                     )
                    })
                   }
               
                </ScrollView>
            )
        } else if(this.state.filterNum === 2) {
            return (
                <ScrollView horizontal={true} style={{marginLeft:55,flexDirection:'row',}}>
                    {
                      this.state.videos.map((item,id) => {
                        return (
                    <TouchableOpacity style={[StudyCSS.content]} onPress={() => this.props.navigation.navigate('MVideo')}>
                        <Image source={{uri:config.img_url+item.coverPhoto}} style={StudyCSS.drugstoreimg} />
                        <View style={StudyCSS.info}>
                            <View style={{flex:1,}}>
                                <Text style={StudyCSS.articletitle}>{item.title}</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={StudyCSS.reading}>ᠦᠵᠡᠭᠰᠡᠨ：{item.clicks}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                      )
                    })
                   }
            
                </ScrollView>
            )
        } else if(this.state.filterNum === 3) {
            return (
                <ScrollView horizontal={true} style={{marginLeft:55,flexDirection:'row',}}>
                     {
                    this.state.cases.map((item,id) => {
                     return (
                    <TouchableOpacity key={id} style={StudyCSS.content}>
                        <View style={StudyCSS.info}>
                            <View style={{flex:1,}}>
                                <Text style={StudyCSS.casetitle}>{item.title}</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={StudyCSS.cause}>{item.content}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                        )
                    })
                   }
                </ScrollView>
            )
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={StudyCSS.mainstyle}>
                <View style={StudyCSS.container}>
                    <View style={StudyCSS.header}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>   
                        <View style={StudyCSS.title}>
                            <Text style={StudyCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={StudyCSS.studys}>
                        <View style={StudyCSS.tabs}>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 1 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.video}>ᠸᠢᠳᠢᠣ᠋</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 2 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.article}>ᠵᠢᠷᠣᠭ</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 3 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.task}>ᠡᠪᠡᠳᠴᠢᠨ ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡ</Text>
                            </TouchableOpacity>
                          
                        </View>
                        { this.getFilter() }
                    </View>
                </View>
            </ScrollView>
        );
    }
}    