import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    Dimensions
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import StudyCSS from '../../../../css/study/MCase'
import RadioGroup  from '../../../../common/RadioGroup';
const { width, height } = Dimensions.get('window');

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 

    constructor(props) {
        super(props)
        this.state = {
            sexArray: [
                {
                    title: 'nan',
                    image:  require('../../../../img/xuan.png'),
                    image2:require('../../../../img/yixuan.png'),
                },
                {
                    title: 'nv',
                    image:  require('../../../../img/xuan.png'),
                    image2:require('../../../../img/yixuan.png'),
                }
            ],
            data: {
                name: null,
                avatar: null,
                avatar_file: null,
                sign: {
                    sex: 0,
                    address: null,
                    sign: null
                }
            }
        };
    }


    changeHeadImage() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            api.file.upload({uri: image.path, name: 'image.jpg', type: image.mime}).then(response => {
                api.user.update({avatar: response.id}).then(() => {
                    this.getData()
                    DeviceEventEmitter.emit('userStatusChange')
                })
            })
        })
    }

    createHeaderImage() {
        if(this.state.data.avatar_file === null) {
            return (
                <Image
                    style={{width: 60,height: 60,borderRadius: 8,}}
                    source={require('../../../../img/yixuan.png')}  >

                </Image>
            )
        } else {
            return (
                <Image
                    style={{width: 60,height: 60,borderRadius: 8,}}
                    source={{ uri: this.state.data.avatar_file.path }}  >

                </Image>
            )
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={StudyCSS.container}>
                    <View style={[StudyCSS.header]}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.headerText}>
                            <Text style={[StudyCSS.titleText]}></Text>
                        </View>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.gosubmit() }}>
                            <Text style={{color:'#ffffff'}}>提交</Text>
                        </TouchableOpacity>       
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor:'#fff',margin:4,borderRadius:6,}}>
                        <View style={[StudyCSS.prescription,{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height-108,marginTop:0,marginLeft:10,}]}>
                            <View style={[StudyCSS.listTop,{height:40,alignItems: 'center',}]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠤᠪᠤᠭ ᠨᠡᠷ᠎ᠡ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',color: '#2fa4e7',fontSize: 16,padding:5}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡ</Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop,{height:40,alignItems: 'center',}]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠴᠢᠨᠠᠷ  ᠤᠨ  ᠢᠯᠭᠠᠯ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <RadioGroup
                                        style={{flexDirection: 'row',}}//整个组件的样式----这样可以垂直和水平
                                        conTainStyle={{height: 44, width: 80}}//图片和文字的容器样式
                                        imageStyle={{width: 25, height: 25}}//图片样式
                                        textStyle={{color: '#666',fontFamily:'MongolianWhitePuaMirror',}}//文字样式
                                        selectIndex={''}//空字符串,表示不选中,数组索引表示默认选中
                                        data={this.state.sexArray}//数据源
                                        onPress={(index, item)=> {
                                        }}
                                    />
                                </View>
                            </View>
                          
                            <View style={[StudyCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠬᠤᠲᠠ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={[StudyCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',padding:5}}></Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠬᠤᠱᠢᠭᠤ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={[StudyCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',padding:5}}></Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠰᠤᠮᠦ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={[StudyCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',padding:5}}></Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠨᠢᠭᠲᠠ ᠤᠷᠤᠨ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={[StudyCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',padding:5}}></Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}> ᠪᠡᠶ᠎ᠡ  ᠶᠢᠨ  ᠦᠨᠡᠮᠯᠡᠯ  ᠦᠨ  ᠨᠤᠮᠸ᠋ᠷ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={[StudyCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',padding:5}}>152622222222222222</Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,justifyContent: 'center',alignItems: 'center',}}> ᠲᠤᠯᠤᠭᠠᠨ ᠰᠡᠭᠦᠳᠡᠷ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260,padding:5}}>
                                    {this.createHeaderImage()}
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}