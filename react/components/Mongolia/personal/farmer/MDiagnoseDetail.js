import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    Dimensions
} from 'react-native';
import StudyCSS from '../../../../css/study/MCase'
import api from '../../../../api';
const { width, height } = Dimensions.get('window');

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    // static navigationOptions = {
	// }; 
	constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
                data:[]
        }
       
    }
    componentDidMount() {
        this.getList()
      }

      getList() {
        api.farmer.prescriptionms({'login-token':localStorage.token,id:this.state.con.id}).then(response => {
         
          this.setState({
            data: response
          })
        })
      }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={StudyCSS.container}>
                    <View style={[StudyCSS.header]}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.headerText}>
                            <Text style={[StudyCSS.titleText]}></Text>
                        </View>        
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor:'#fff',margin:4,borderRadius:6,}}>
                    	<View style={[StudyCSS.prescription,{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height-108,marginTop:0,marginLeft:10,}]}>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠭᠠᠷᠴᠠᠭ:</Text>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',color: '#2fa4e7',fontSize: 16,}}>{this.state.con.title}</Text>
	                        </View>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠠᠩᠭᠢᠯᠠᠯ:</Text>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>888</Text>
	                        </View>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={[StudyCSS.label]} style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠪᠠᠢᠳᠠᠯ:</Text>
	                            <Text style={[StudyCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',}}>{this.state.con.content}</Text>
	                        </View>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠡᠪᠡᠳᠴᠢᠨ ᠵᠠᠰᠠᠬᠤ ᠵᠤᠷ:</Text>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>{this.state.con.processingMethod}</Text>
	                        </View>
	                        <View style={{marginTop:4}}>
	                            <Text style={[StudyCSS.recommend,{fontFamily:'MongolianWhitePuaMirror',}]}>ᠡᠮ  ᠤᠨ ᠵᠤᠷ ᠦᠵᠡᠬᠦ</Text>
	                            <View style={[StudyCSS.prescript]}>
								{
                             this.state.data.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                        
                        return (
							<View key={id}>
	                                <View style={[StudyCSS.listTop,StudyCSS.drugstore]}>
	                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠳᠡᠯᠭᠡᠭᠦᠷ  ᠦᠨ  ᠨᠡᠷ᠎ᠡ᠄{item.drugstoreinfo.pharmacyNameMeng}</Text>
	                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ  ᠤᠲᠠᠰᠤ᠄{item.drugstoreinfo.username}</Text>
	                                </View>
                                     {
										item.prescriptionmDtos.map((m,j)=>{
										 return (  
	                                <View style={[StudyCSS.medicinal]}>
	                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠡᠮ  ᠦᠨ  ᠨᠡᠷ᠎ᠡ᠄{m.drugName}      ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠄{m.drugNumber}</Text>
	     
	                                </View>
								)
							})
						  }
						     </View>
	                                )
                               
                                })
        
                               }
	                            </View>
	                        </View>
	                    </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}