import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import api from '../../../api';
import StudyCSS from '../../../css/personal/MMessage'

export default class Study extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props)
        this.state = {
            data:[]
        }
    }

    componentDidMount() {
        
        this.getList()
    } 

    getList() {

        api.messages.list().then(response => {

            this.setState({
                data: response
            })
        })
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={StudyCSS.mainstyle}>
                <View style={StudyCSS.container}>
                    <View style={StudyCSS.header}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>                                                                                                                                              
                        <View style={StudyCSS.title}>
                            <Text style={StudyCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={StudyCSS.studys}>
                        <ScrollView horizontal={true} style={{flexDirection:'row',}}>
                        { 
                        this.state.data.map((item,id) => {
                            return (
                            <TouchableOpacity key={id} style={[StudyCSS.content]} onPress={() => this.props.navigation.navigate('MMessageDetail',{title:item.title,content:item.content,time:item.createTime})}>
                                <View style={StudyCSS.info}>
                                    <View style={{flex:1,}}>
                                        <Text style={StudyCSS.articletitle}>{item.title}</Text>
                                    </View>
                                    <View style={{flex:1,}}>
                                        <Text style={StudyCSS.Mcontent}>{item.createTime}</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <Text style={StudyCSS.reading}>{item.content}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                            )
                        })
                    }  
                        </ScrollView>
                    </View>
                </View>
            </ScrollView>
        );
    }
}    