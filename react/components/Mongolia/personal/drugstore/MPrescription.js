import React, { Component } from 'react';
import {
    AppRegistry,
    StudyCSSheet,
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    HorizontalScrollView
} from 'react-native';
import StudyCSS from '../../../../css/personal/MMessage'
import PersonalCSS from '../../../../css/personal/MAbout';
import api from '../../../../api';
export default class Study extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props)
        this.state = {
          data: []
        }
      }
    
      componentDidMount() {
        this.getList()
      }

      getList() {
         
        api.drugstore.diagnosef({'login-token':localStorage.token}).then(response => {
           
          this.setState({
            data: response
          })
        })
      }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={StudyCSS.mainstyle}>
                <View style={StudyCSS.container}>
                    <View style={StudyCSS.header}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>                                                                                                                                              
                        <View style={StudyCSS.title}>
                            <Text style={StudyCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={StudyCSS.studys}>
                        <ScrollView horizontal={true} style={{flexDirection:'row',}}>
	                        <View  style={PersonalCSS.personalList}>
                            {
                      this.state.data.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                     
                    return (
		                        <TouchableOpacity key={id} style={[PersonalCSS.lists,{width:50,borderRadius:6,}]} onPress={() => navigate('MPrescriptionDetail',{itm:item})}>
		                            <Text style={[PersonalCSS.modelText,{fontSize:16}]} onPress={() => navigate('MPrescriptionDetail',{itm:item})}>{item.name}</Text>
		                        </TouchableOpacity>
		                        )
                       
                            })
    
                           }
		                    </View>
                        </ScrollView>
                    </View>
                </View>
            </ScrollView>
        );
    }
}    