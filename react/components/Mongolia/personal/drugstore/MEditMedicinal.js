import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    Dimensions
} from 'react-native';
import StudyCSS from '../../../../css/study/MCase'
const { width, height } = Dimensions.get('window');
import RadioGroup  from '../../../../common/RadioGroup';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 

    constructor(props) {
        super(props)
         this.state = {
            sexArray: [
                {
                    title: '᠌ᠭᠠᠷᠬᠤ',
                    image:  require('../../../../img/xuan.png'),
                    image2:require('../../../../img/yixuan.png'),
                },
                {
                    title: ' ᠤᠨᠠᠬᠤ',
                    image:  require('../../../../img/xuan.png'),
                    image2:require('../../../../img/yixuan.png'),
                }
            ],
            data: {
                name: null,
                avatar: null,
                avatar_file: null,
                sign: {
                    sex: 0,
                    address: null,
                    sign: null
                }
            }
        };
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={StudyCSS.container}>
                    <View style={[StudyCSS.header]}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.headerText}>
                            <Text style={[StudyCSS.titleText]}></Text>
                        </View>        
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor:'#fff',margin:4,borderRadius:6,}}>
                        <View style={[StudyCSS.prescription,{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height-108,marginTop:0,marginLeft:10,}]}>
                            <View style={[StudyCSS.listTop,{height:40,alignItems: 'center',}]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠰᠠᠩ  ᠳᠤ  ᠪᠠᠢᠬᠤ  ᠲᠤᠭ᠎ᠠ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-340}}>
                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',color: '#2fa4e7',fontSize: 16,padding:5}}>122</Text>
                                </View>
                                <View style={[StudyCSS.info,{flex:1,width:80,}]}>
                                    <TouchableOpacity onPress={() => navigate('MEditMedicinal')}>
                                        <Text style={[StudyCSS.article,{backgroundColor:'#2fa4e7',color:'#fff',alignSelf:'center',borderRadius:3,width:40,height:30}]}></Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop,{height:40,alignItems: 'center',}]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>yaopinshuliang:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-340}}>
                                    <RadioGroup
	                                    style={{flexDirection: 'row',}}//整个组件的样式----这样可以垂直和水平
	                                    conTainStyle={{height: 44, width: 80}}//图片和文字的容器样式
	                                    imageStyle={{width: 25, height: 25}}//图片样式
	                                    textStyle={{color: '#666',fontFamily:'MongolianWhitePuaMirror',}}//文字样式
	                                    selectIndex={''}//空字符串,表示不选中,数组索引表示默认选中
	                                    data={this.state.sexArray}//数据源
	                                    onPress={(index, item)=> {
	                                    }}
	                                />
                                </View>
                                <View style={[StudyCSS.info,{flex:1,width:80,}]}>
                                    <TouchableOpacity onPress={() => navigate('MEditMedicinal')}>
                                        <Text style={[StudyCSS.article,{backgroundColor:'#2fa4e7',color:'#fff',alignSelf:'center',borderRadius:3,width:40,height:30}]}>ᠲᠤᠰᠢᠶᠠᠬᠤ</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}