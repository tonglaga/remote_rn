import React, { Component } from 'react';
import {
    AppRegistry,
    StudyCSSheet,
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    HorizontalScrollView
} from 'react-native';
import StudyCSS from '../../../../css/personal/MMedicinal'

export default class Study extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={StudyCSS.mainstyle}>
                <View style={StudyCSS.container}>
                    <View style={StudyCSS.header}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.title}>
                            <Text style={StudyCSS.titleText}></Text>
                        </View>  
                         
                        <TouchableOpacity style={[StudyCSS.backImg]}  onPress={() => navigate('MAddMedicinal')}>
                            <Text style={{color:'#ffffff'}}>添加</Text>
                        </TouchableOpacity>    
                    </View>

                    <View style={StudyCSS.studys}>

                        <ScrollView horizontal={true} style={{flexDirection:'row',}}>

                            <TouchableOpacity style={[StudyCSS.content]}>
                                <Image source={require('../../../../img/a8.jpg')} style={[StudyCSS.drugstoreimg,{flex:1}]} />
                                <View style={[StudyCSS.info,{flex:5}]}>
                                    <View style={{flex:1,}}>
                                        <Text style={StudyCSS.articletitle}>ᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠡᠳᠴᠢᠨ  ᠤᠨ </Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <Text style={StudyCSS.reading}>ᠰᠠᠩ  ᠳᠤ  ᠪᠠᠢᠬᠤ  ᠲᠤᠭ᠎ᠠ：1222222222</Text>
                                    </View>
                                    <View style={{flex:1}}>
                                        <Text style={StudyCSS.reading}> ᠪᠠᠢᠳᠡᠯ ： ᠪᠠᠢᠨ᠎ᠠ</Text>
                                    </View>
                                </View>
                                <View style={{flex:1,paddingTop:18,flexDirection:'row',}}>
                                    <TouchableOpacity onPress={() => navigate('MEditMedicinal')} style={{paddingTop:18,}}>
                                        <Text style={[StudyCSS.article,{backgroundColor:'#2fa4e7',color:'#fff',alignSelf:'center',borderRadius:3,width:40,height:30,}]}>bian</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{paddingTop:18,}}>    
                                        <Text style={[StudyCSS.article,{backgroundColor:'#f20000',color:'#fff',alignSelf:'center',borderRadius:3,width:40,height:30,}]}>shanbtn</Text>
                                    </TouchableOpacity>
                                </View>
                            </TouchableOpacity>

                        
                         
                      
                        </ScrollView>
                    </View>
                </View>
            </ScrollView>
        );
    }
}    