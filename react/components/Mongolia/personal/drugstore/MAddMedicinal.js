import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    Dimensions
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import StudyCSS from '../../../../css/study/MCase'
const { width, height } = Dimensions.get('window');

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 

    constructor(props) {
        super(props)
        this.state = {
            data: {
                name: null,
                avatar: null,
                avatar_file: null,
                sign: {
                    sex: 0,
                    address: null,
                    sign: null
                }
            }
        };
    }

    changeHeadImage() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            api.file.upload({uri: image.path, name: 'image.jpg', type: image.mime}).then(response => {
                api.user.update({avatar: response.id}).then(() => {
                    this.getData()
                    DeviceEventEmitter.emit('userStatusChange')
                })
            })
        })
    }

    createHeaderImage() {
        if(this.state.data.avatar_file === null) {
            return (
                <Image
                    style={{width: 60,height: 60,borderRadius: 8,}}
                    source={require('../../../../img/h.png')}  >

                </Image>
            )
        } else {
            return (
                <Image
                    style={{width: 60,height: 60,borderRadius: 8,}}
                    source={{ uri: this.state.data.avatar_file.path }}  >

                </Image>
            )
        }
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={StudyCSS.container}>
                    <View style={[StudyCSS.header]}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.headerText}>
                            <Text style={[StudyCSS.titleText]}></Text>
                        </View>        
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor:'#fff',margin:4,borderRadius:6,}}>
                        <View style={[StudyCSS.prescription,{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height-108,marginTop:0,marginLeft:10,}]}>
                            <View style={[StudyCSS.listTop,{height:40,alignItems: 'center',}]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}> ᠡᠮ  ᠦᠨ  ᠨᠡᠷ᠎ᠡ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',color: '#2fa4e7',fontSize: 16,padding:5}}></Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop,{height:40,alignItems: 'center',}]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}> ᠰᠠᠩ  ᠳᠤ  ᠲᠤᠭ᠎ᠠ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',padding:5}}>7</Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop]}>
                                <Text style={[StudyCSS.label]} style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}> ᠡᠮ  ᠦᠨ  ᠲᠠᠨᠢᠯᠴᠤᠭ᠎ᠠ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={[StudyCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',padding:5}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡ</Text>
                                </View>
                            </View>
                            <View style={[StudyCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠭᠠᠳᠠᠷ  ᠨᠢᠭᠤᠷᠠᠨ  ᠵᠢᠷᠤᠭ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260,padding:5}}>
                                    {this.createHeaderImage()}
                                </View>
                            </View>
                            <TouchableOpacity style={{justifyContent: 'center',
                                alignSelf: 'center',
                                alignItems: 'center',
                                backgroundColor: '#3385ff',
                                width:'80%',
                                borderRadius: 3,
                                marginTop:10,}}>
                                <Text style={{padding:6,
                                    color: '#fff',fontFamily:'MongolianWhitePuaMirror',}}>ᠲᠤᠰᠢᠶᠠᠬᠤ</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}