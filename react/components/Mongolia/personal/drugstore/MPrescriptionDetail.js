import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import StudyCSS from '../../../../css/study/MCase'
const { width, height } = Dimensions.get('window');
import api from '../../../../api';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    // static navigationOptions = {
	// }; 
	constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
        }
       
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={StudyCSS.container}>
                    <View style={[StudyCSS.header]}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.headerText}>
                            <Text style={[StudyCSS.titleText]}></Text>
                        </View>        
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor:'#fff',margin:4,borderRadius:6,}}>
                    	<View style={[StudyCSS.prescription,{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height-108,marginTop:0,marginLeft:10,}]}>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠮᠠᠯᠴᠢᠨ  ᠤ  ᠨᠡᠷ᠎ᠡ :</Text>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',color: '#2fa4e7',fontSize: 16,}}>{this.state.con.nameMeng}</Text>
	                        </View>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ  ᠤᠲᠠᠰᠤ:</Text>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>{this.state.con.username}</Text>
	                        </View>
	                      
	                        <View style={{marginTop:4}}>
	                            <Text style={[StudyCSS.recommend,{fontFamily:'MongolianWhitePuaMirror',}]}>ᠡᠮ  ᠤᠨ ᠵᠤᠷ ᠦᠵᠡᠬᠦ</Text>
	                            <View style={[StudyCSS.prescript]}>

	                                {
                             this.state.con.prescriptions.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                        
                        
                                 return (  
	                                <View key={id} style={[StudyCSS.medicinal]}>
	                                   <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠡᠮ  ᠦᠨ  ᠨᠡᠷ᠎ᠡ᠄{item.drugName}       ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠄{item.drugNumber}</Text>

	                                </View>
	                                )
                               
                               
                                })
        
                               }
	                            </View>
	                        </View>
	                    </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}