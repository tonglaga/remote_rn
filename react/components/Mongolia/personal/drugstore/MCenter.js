import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
} from 'react-native';
import PersonalCSS from '../../../../css/personal/MAbout';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    }; 
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={PersonalCSS.container}>
                    <View style={PersonalCSS.header}>
                        <TouchableOpacity style={[PersonalCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>   
                        <View style={PersonalCSS.title}>
                            <Text style={PersonalCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={PersonalCSS.personalList}>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]} onPress={() => navigate('MPrescription')}>
                            <Image source={require('../../../../img/setting.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠢᠨᠦ  ᠬᠦᠯᠢᠶᠡᠭᠰᠡᠨ  ᠵᠤᠷ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]} onPress={() => navigate('MDrugstoreMedicinal')}>
                            <Image source={require('../../../../img/collect.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]}  onPress={() => navigate('MDrugstorePerfectInformation')}>
                            <Image source={require('../../../../img/about.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ  ᠢᠶᠠᠨ  ᠲᠡᠭᠦᠯᠳᠡᠷᠵᠢᠭᠦᠯᠭᠦ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists,{backgroundColor: '#e7ebec',}]}>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}