import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    ImageBackground
} from 'react-native';
import PersonalCSS from '../../../css/personal/MAbout';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    }; 
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={PersonalCSS.container}>
                    <View style={PersonalCSS.header}>
                        <TouchableOpacity style={[PersonalCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>   
                        <View style={PersonalCSS.title}>
                            <Text style={PersonalCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View  style={PersonalCSS.personalList}>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]} onPress={() => this.props.navigation.navigate('MFeedback')}>
                            <Image source={require('../../../img/setting.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠴᠢᠳᠠᠮᠵᠢ ᠶᠢᠨ ᠲᠠᠨᠢᠯᠴᠠᠭᠣᠯᠭ᠎ᠠ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]}>
                            <Image source={require('../../../img/collect.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠮᠡᠷᠭᠡᠵᠢᠯᠲᠡᠨ ᠦ ᠡᠩᠨᠡᠭᠡ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists, PersonalCSS.borderRi]}>
                            <Image source={require('../../../img/about.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠰᠠᠨᠠᠯ ᠦᠭᠭᠦᠬᠦ</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[PersonalCSS.lists]}>
                            <Image source={require('../../../img/message.png')} style={[PersonalCSS.modelIcon]} />
                            <Text style={[PersonalCSS.modelText]}>ᠬᠡᠪ</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}