import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions
} from 'react-native';
const { width, height } = Dimensions.get('window');
import ArticleCSS from '../../../css/study/MArticle';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    };
    
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
      
        this.state = {
            title:params.title,
            time:params.time,
            content: params.content,
        }
    } 

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={[ArticleCSS.container]}>
                    <View style={[ArticleCSS.header]}>
                        <TouchableOpacity style={[ArticleCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ArticleCSS.headerText}>
                            <Text style={[ArticleCSS.titleText]}></Text>
                        </View>
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor:'#fff',margin:4,borderRadius:6,}}>
                        <View style={[ArticleCSS.content,{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height-85,marginTop:20,marginLeft:-20,}]}>
                                <View>   
                                    <Text style={[ArticleCSS.title]}>{this.state.title}</Text>
                                </View>  
                                <View style={[ArticleCSS.infors]}>
                                    <Text style={[ArticleCSS.time,{fontFamily:'MongolianWhitePuaMirror',}]}>{this.state.time}</Text>
                                </View>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',fontSize:16,lineHeight:25,marginTop:5,}}>
                                {this.state.content}    
                                </Text>
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}