import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import StudyCSS from '../../../../css/study/MCase'
import api from '../../../../api';
const { width, height } = Dimensions.get('window');

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    // static navigationOptions = {
	// }; 
	constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
				con:params.itm,
				data:[]
        }
       
	}
	componentDidMount() {
        this.getList()
      }

      getList() {
        api.expert.prescriptionms({'login-token':localStorage.token,id:this.state.con.id}).then(response => {
         
          this.setState({
            data: response
          })

        })
      }
    render() {
        const { navigate } = this.props.navigation;
        return (
                <View style={StudyCSS.container}>
                    <View style={[StudyCSS.header]}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.headerText}>
                            <Text style={[StudyCSS.titleText]}></Text>
                        </View> 
                         <TouchableOpacity style={[StudyCSS.backImg]}>
                            <Image source={require('../../../../img/star-3.png')} style={{height:23,width:23}}/>
                        </TouchableOpacity>        
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor: '#fff',}}>
                        <View style={{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height+108,height:height+222,position: 'relative',left:(height+222)/2-(height+108)/2, top:(height+116)/2-(height+222)/2,}}>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',width:100,}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠭᠠᠷᠴᠠᠭ:</Text>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',color: '#2fa4e7',fontSize: 16,width:height-188}}>{this.state.con.title}</Text>
	                        </View>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',width:100,}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠠᠩᠭᠢᠯᠠᠯ:</Text>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',width:height-188}}>{this.state.con.illnessCategoryId}</Text>
	                        </View>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={[StudyCSS.label]} style={{fontFamily:'MongolianWhitePuaMirror',width:100,}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠪᠠᠢᠳᠠᠯ:</Text>
	                            <Text style={[StudyCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',width:height-188}}>{this.state.con.content}</Text>
	                        </View>
	                        <View style={[StudyCSS.listTop]}>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',width:100,}}>ᠡᠪᠡᠳᠴᠢᠨ ᠵᠠᠰᠠᠬᠤ ᠵᠤᠷ:</Text>
	                            <Text style={{fontFamily:'MongolianWhitePuaMirror',width:height-188}}>{this.state.con.processingMethod}</Text>
	                        </View>
	                        <View style={{marginTop:4}}>
	                            <Text style={[StudyCSS.recommend,{fontFamily:'MongolianWhitePuaMirror',}]}>ᠡᠮ  ᠤᠨ ᠵᠤᠷ ᠦᠵᠡᠬᠦ</Text>
	                            <View style={[StudyCSS.prescript,{width:height-88}]}>
								{
                             this.state.data.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                        
                        return (
                              <View key={id}>
	                                <View style={[StudyCSS.listTop,StudyCSS.drugstore]}>
	                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠳᠡᠯᠭᠡᠭᠦᠷ  ᠦᠨ  ᠨᠡᠷ᠎ᠡ᠄{item.drugstoreinfo.pharmacyNameMeng}</Text>
	                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ  ᠤᠲᠠᠰᠤ᠄{item.drugstoreinfo.username}</Text>
	                                </View>
									{
                                item.prescriptionmDtos.map((m,j)=>{
                                 return ( 
	                                <View style={[StudyCSS.medicinal]}>
	                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',}}>ᠡᠮ  ᠦᠨ  ᠨᠡᠷ᠎ᠡ᠄{m.drugName}      ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠄{m.drugNumber}</Text>
	                                </View>
								)
                                })
                                }
                                </View>
                                 )
                               
                                })
        
                               }
	                            </View>
	                        </View>
	                    </View>
                    </ScrollView>
                </View>
        );
    }
}