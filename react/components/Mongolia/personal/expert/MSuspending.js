import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    DeviceEventEmitter,
    WebView
} from 'react-native';
import CaseCSS from '../../../../css/study/MCase'
import StudyCSS from '../../../../css/personal/MDiagnose'
import api from '../../../../api';
const { width, height } = Dimensions.get('window');
import PopupDialog, { SlideAnimation,DialogTitle } from 'react-native-popup-dialog';

export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
    }; 

    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
       // console.warn(params)
        
        this.state = { 
            con: {
                json:null,
                id:params.itm.id,
                herdsmanId:params.itm.herdsmanId,
                expertId:params.itm.expertId,
                title:params.itm.title,
                content:params.itm.content,
                illnessCategoryId:params.itm.illnesscategoryid,
                processingMethod:params.itm.pronessingmethod,
                type:params.itm.type,
                good:params.itm.good,
                enclosure:""
            },
               // con:params.itm,
                data:[],
                fenlei:[],
                yaopin:[],
                zhuangtai:1,
        }
       
    }
    componentDidMount() {
        this.getList();
        DeviceEventEmitter.addListener('zuizhong', (message) => {
            
            let yaopin=this.state.yaopin;
       yaopin[yaopin.length]=message;
             this.setState({
                 yaopin:yaopin,
               
             })
             //console.warn(yaopin)
             //this.getMessage(message); 
         })
      }

      getList() {
        api.expert.drugstoreinfos({'login-token':localStorage.token,'id':this.state.con.id}).then(response => {
        // console.warn(response)
           this.setState({
             data: response
           })
        })
        api.expert.illnesscategorys({'login-token':localStorage.token}).then(response => {
        
        //      this.setState({
        //        fenlei: response
        //      })
           })
      }
    
     
      setStatetitle(text){
        let con=this.state.con;
       con.title=text;
         this.setState({
           con:con
        })   
      }
      setStatecontent(text){
        let con=this.state.con;
       con.content=text;
         this.setState({
           con:con
        })   
      }
      setStateprocessingMethod(text){
        let con=this.state.con;
       con.processingMethod=text;
         this.setState({
           con:con
        })   
      }
      //选择药品
      goChooseMedicinal(item){
        this.popupDialog.dismiss();
        this.props.navigation.navigate('MChooseMedicinal',{itm:item}) 
      }
      //提交诊断接口
      gosubmit(){
        let json=[];  
        let k=0;
        this.state.yaopin.map((n,i)=>{
            n.prescriptionmDtos.map((m,j)=>{
                json[k]=m;
                k++;
            })
           
        })  
        let con=this.state.con;
        con.json=JSON.stringify(json);
          this.setState({
            con:con
         })
       
        api.expert.expertinfoszhen(this.state.con).then(response => {
       //  console.warn(response) 
       
          })
         this.props.navigation.navigate("Diagnose");
      }
     // 在线输入
     getshuru(k){
        this.setState({
            zhuangtai:k
        }) 
        this.popupDialogs.show();
     }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView>
                <View style={CaseCSS.container}>
                        <PopupDialog
                            dialogTitle={<DialogTitle title="" />}
                            ref={popupDialog => {
                                this.popupDialog = popupDialog;
                            }}
                            width={300}
                            height={440}
                        >
                            <ScrollView horizontal={true} style={{flexDirection:'row',}}>
                            {
                      this.state.data.map((item,id) => {
                        
                    return (
                                <TouchableOpacity key={id} style={[StudyCSS.listStyle]}  onPress={() => this.goChooseMedicinal(item)}>
                                    <View style={StudyCSS.info}>
                                        <Text style={[StudyCSS.articletitle,{left:-166,}]}>{item.pharmacyNameMeng}</Text>
                                    </View>
                                </TouchableOpacity>
                                )
                           
                            })
        
                           }
                            </ScrollView>
                        </PopupDialog>
                        <PopupDialog
					    dialogTitle={<DialogTitle title="蒙语在线输入" />}
					    ref={popupDialog => {
					        this.popupDialogs = popupDialog;
					    }}
					    width={width}
					    height={height-100}
					>
					   
                     
                        <WebView
                          ref='WebView'
                        
                        source={{uri:'file:///android_asset/index.html'}}
                        onMessage={(event) => {   //监听html发送过来的数据
                        const message = event.nativeEvent.data;
                       // DeviceEventEmitter.emit('imeMsg', message)
                      //  this.props.navigation.navigate('Meng')
                       // alert(message.split(',')[0])
                       if(this.state.zhuangtai==2){
                        this.setStatecontent(message);
                       }else if(this.state.zhuangtai==3){
                        this.setStateprocessingMethod(message);
                       }else{
                        this.setStatetitle(message);
                       }
                        
                        this.popupDialogs.dismiss();
                       } }

                         >
                       </WebView>
                          
					  

					</PopupDialog>
                    <View style={[CaseCSS.header]}>
                        <TouchableOpacity style={[CaseCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={CaseCSS.headerText}>
                            <Text style={[CaseCSS.titleText]}></Text>
                        </View>
                        <TouchableOpacity style={[CaseCSS.backImg]} onPress={() => { this.gosubmit() }}>
                            <Text style={{color:'#ffffff'}}>发送</Text>
                        </TouchableOpacity>         
                    </View>
                    <ScrollView horizontal={true} style={{height:height-82,backgroundColor:'#fff',margin:4,borderRadius:6,}}>
                        <View style={[CaseCSS.prescription,{transform:[
                                            {rotateX:'180deg'},
                                            {rotateZ:'270deg'}
                                        ],width:height-108,marginTop:0,marginLeft:10,}]}>
                            <View style={[CaseCSS.listTop,{height:40,alignItems: 'center',}]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠭᠠᠷᠴᠠᠭ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',color: '#2fa4e7',fontSize: 16,padding:5}} onPress={() => { this.getshuru(1) }}>{this.state.con.title}</Text>
                                </View>
                            </View>
                            <View style={[CaseCSS.listTop,{height:40,alignItems: 'center',}]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠠᠩᠭᠢᠯᠠᠯ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={{fontFamily:'MongolianWhitePuaMirror',padding:5}}>{this.state.fenlei}</Text>
                                </View>
                            </View>
                            <View style={[CaseCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠡᠪᠡᠳᠴᠢᠨ  ᠦ ᠪᠠᠢᠳᠠᠯ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={[CaseCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',padding:5}} onPress={() => { this.getshuru(2) }}>{this.state.con.content}</Text>
                                </View>
                            </View>
                            <View style={[CaseCSS.listTop]}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',width:130,}}>ᠡᠪᠡᠳᠴᠢᠨ ᠵᠠᠰᠠᠬᠤ ᠵᠤᠷ:</Text>
                                <View style={{borderWidth:1,borderColor:'#ccc',width:height-260}}>
                                    <Text style={[CaseCSS.listTop]} style={{fontFamily:'MongolianWhitePuaMirror',padding:5}} onPress={() => { this.getshuru(3) }}>{this.state.con.processingMethod}</Text>
                                </View>
                            </View>

                            <View style={{marginTop:4,}}>
                                <TouchableOpacity style={[CaseCSS.backImg]} onPress={() => { this.popupDialog.show(); }}>
                                    <Text style={[CaseCSS.recommend,{fontFamily:'MongolianWhitePuaMirror',}]} > ᠡᠮ  ᠦᠨ  ᠳᠡᠯᠭᠡᠭᠦᠷ  ᠰᠤᠩᠭᠤᠬᠤ</Text>
                                </TouchableOpacity>
                            </View>

                            <View style={{marginTop:4}}>

                                <View style={[CaseCSS.prescript]}>
                                {
                             this.state.yaopin.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                        
                        return (
                            <View key={id}>
                                    <View style={[CaseCSS.listTop,CaseCSS.drugstore]} >
                                        <Text>ᠳᠡᠯᠭᠡᠭᠦᠷ  ᠦᠨ  ᠨᠡᠷ᠎ᠡ：{item.pharmacyNameMeng}</Text>
                                    </View>
                                    {
                                item.prescriptionmDtos.map((m,j)=>{
                                 return (  
                                    <View style={[CaseCSS.medicinal]} key={j}>
                                        <Text>ᠡᠮ  ᠦᠨ  ᠨᠡᠷ᠎ᠡ：{m.drugName}       ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠄ {m.drugNumber}</Text>
                                    </View>
                                        )
                                    })
                                  }
                                    </View>
                                      )
                               
                                    })
            
                                   }
                                </View>

                            </View>   
                        </View>
                    </ScrollView>
                </View>
            </ScrollView>
        );
    }
}