import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    ToastAndroid
} from 'react-native';
import StudyCSS from '../../../../css/personal/MMedicinal'
import api from '../../../../api';
export default class Study extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props) 
        this.state = {
          filterNum: 1,
          data:[]
        }
    }

    componentDidMount() {

        this.getList();

      }

      getList() {
        api.expert.studyarticlems({'login-token':localStorage.token}).then(response => {  
          //  console.warn(response); 
            if(response.code != 401){
                this.setState({
                    data: response
                  })
            }else {
                ToastAndroid.show(response.message, ToastAndroid.SHORT)
            }
       
        })
      }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={StudyCSS.mainstyle}>
                <View style={StudyCSS.container}>
                    <View style={StudyCSS.header}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.title}>
                            <Text style={StudyCSS.titleText}></Text>
                        </View>  
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => navigate('MAddArticle')}>
                             <Text style={[StudyCSS.title]}>添加</Text>
                        </TouchableOpacity>        
                    </View>

                    <View style={StudyCSS.studys}>
                        <ScrollView horizontal={true} style={{flexDirection:'row',}}>
                        {
                      this.state.data.map((item,id) => {
                        
                       
                    return (

                            <TouchableOpacity key={id} style={[StudyCSS.content,{width:80}]} onPress={() => navigate('MArticleDetail',{itm:item})}>
                                <Image source={require('../../../../img/a8.jpg')} style={[StudyCSS.drugstoreimg,{flex:1,height:70,width:70,}]} />
                                <View style={[StudyCSS.info,{flex:5,}]}>
                                    <View style={{flex:1,}}>
                                        <Text style={StudyCSS.articletitle}>{item.title}</Text>
                                    </View>
		                            <View style={{flex:1}}>
		                                <Text style={[StudyCSS.reading,{color:'#aaa',paddingTop:8,}]}>{item.createTime}</Text>
		                            </View>
                                </View>
                                <TouchableOpacity style={[StudyCSS.info,{flex:1,alignItems: 'center',justifyContent: 'center',}]} onPress={() => navigate('MEditArticle',{itm:item})}>
                                    <View style={{paddingTop:18,}}>
                                        <Image source={require('../../../../img/xuan.png')} style={{ marginLeft: 5,height: 15, width: 15}}/>
                                    </View>
                                </TouchableOpacity>
                            </TouchableOpacity>
                             )
                    
                            })
        
                           }
                        </ScrollView>
                    </View>
                </View>
            </ScrollView>
        );
    }
}    