import React, { Component } from 'react';
import {
    AppRegistry,
    StudyCSSheet,
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    HorizontalScrollView
} from 'react-native';
import StudyCSS from '../../../../css/personal/MDiagnose'
import api from '../../../../api';
export default class Study extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props) 
        this.state = {
          filterNum: 1,
          data:[]
        }
    }

    componentDidMount() {

        this.getList();

      }

      getList() {
        api.expert.diagnose({'login-token':localStorage.token}).then(response => {   
          this.setState({
            data: response
          })
        })
      }

    getFilter() {
        const { navigate } = this.props.navigation;
        if(this.state.filterNum === 1) {
            return (
                <ScrollView horizontal={true} style={{marginLeft:55,flexDirection:'row',}}>
                     {
                      this.state.data.map((item,id) => {
                        
                         if(item.status==2){
                    return (
                    <TouchableOpacity style={[StudyCSS.content]} onPress={() => this.props.navigation.navigate('MSolved',{itm:item})}>
                        <View style={StudyCSS.info}>
                            <Text style={[StudyCSS.articletitle,]}>{item.title}</Text>
                        </View>
                    </TouchableOpacity>
                       )
                    }
                    })

                   }
                </ScrollView>
            )
        } else if(this.state.filterNum === 2) {
            return (
                <ScrollView horizontal={true} style={{marginLeft:55,flexDirection:'row',}}>
                    {
                      this.state.data.map((item,id) => {
                        
                     if(item.status!=2){
                    return (
                    <TouchableOpacity style={[StudyCSS.content]} onPress={() => this.props.navigation.navigate('MSuspending',{itm:item})}>
                        <View style={StudyCSS.info}>
                            <View style={{flex:1,}}>
                                <Text style={[StudyCSS.articletitle]}>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠡ ᠦᠭᠡᠢ</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={[StudyCSS.reading,{color:'#aaa'}]}>ᠮᠠᠯᠴᠢᠨ  ᠤ  ᠤᠲᠠᠰᠤ ：{item.husername}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                       )
                    }
                    })

                   }
                </ScrollView>
            )
        } 
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={StudyCSS.mainstyle}>
                <View style={StudyCSS.container}>
                    <View style={StudyCSS.header}>
                        <TouchableOpacity style={[StudyCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={StudyCSS.title}>
                            <Text style={StudyCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={StudyCSS.studys}>
                        <View style={StudyCSS.tabs}>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 1 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.video}>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠪᠡ</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 2 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.article}>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠡ ᠦᠭᠡᠢ</Text>
                            </TouchableOpacity>
                        </View>
                        { this.getFilter() }
                    </View>
                </View>
            </ScrollView>
        );
    }
}    