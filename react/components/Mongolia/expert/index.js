import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity,
    Dimensions,
    ToastAndroid
} from 'react-native';
const { width, height } = Dimensions.get('window');

import ExpertCSS from '../../../css/expert/MExpert';
import config from '../../../config';
export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，
    static navigationOptions = {
        
    };
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
                con:params.itm,
                zhuangtai:null 
        }
       
    }
    componentDidMount() {
       
        if(this.state.con.state == 1){
           this.setState({
               zhuangtai:<Text style={[ExpertCSS.status, ExpertCSS.online]} >ᠪᠠᠢᠨ᠎ᠠ</Text>
           }) 
          
        }else if(this.state.con.state == 2){
            this.setState({
                zhuangtai:<Text style={[ExpertCSS.status, ExpertCSS.busy]} >ᠶᠠᠭᠠᠷᠠᠤ</Text>
            }) 
        }else{
            this.setState({
                zhuangtai:<Text style={[ExpertCSS.status, ExpertCSS.offline]}>ᠦᠭᠡᠢ</Text>
            }) 
        }
        
    }
      //拨打电话
      gettell(){
        if(localStorage.type != 1){
            ToastAndroid.show('只有牧民才有通话！', ToastAndroid.SHORT);
          return;
        }  
        if(localStorage.wanshan == 1){
            ToastAndroid.show('请先完善个人信息！', ToastAndroid.SHORT);
            return;
        }
       
       this.linking('tel:'+this.state.con.username);
    }
   linking=(url)=>{
   
   // console.log(url);

    Linking.canOpenURL(url).then(supported => {
        if (!supported) {
            console.warn('不能打电话');
        } else {
            return Linking.openURL(url);
        }
    }).catch(err => console.error('打电话失败', err));

 }
  //发送视频通话
  getwaiting(){
    if(localStorage.type != 1){
        ToastAndroid.show('只有牧民才有通话！', ToastAndroid.SHORT);
      return;
    }  
    if(localStorage.wanshan == 1){
        ToastAndroid.show('请先完善个人信息！', ToastAndroid.SHORT);
        return;
    }
    if(this.state.con.state != 1){
        ToastAndroid.show('抱歉，专家忙碌！', ToastAndroid.SHORT);
        return; 
    }
    this.props.navigation.navigate('Waiting',{id:this.state.con.uid,name:this.state.con.name,img:this.state.con.headerUrl})
   }
   //发送文件材料
   getAvatarURL(){
    if(localStorage.type != 1){
        ToastAndroid.show('只有牧民才有通话！', ToastAndroid.SHORT);
      return;
    }  
    if(localStorage.wanshan == 1){
        ToastAndroid.show('请先完善个人信息！', ToastAndroid.SHORT);
        return;
    }
    
    this.props.navigation.navigate('Consult',{itm:this.state.con.uid});

   }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={ExpertCSS.mainStyle}>
                <View style={ExpertCSS.container}>
                    <View style={ExpertCSS.header}>
                        <TouchableOpacity style={ExpertCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={ExpertCSS.title}>
                            <Text style={ExpertCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={ExpertCSS.experts}>
                        <View style={ExpertCSS.portrait} >
                            <TouchableOpacity   onPress={() => navigate('MExpertDetail')}>
                                <Image source={{uri:config.img_url+this.state.con.headerUrl}} style={ExpertCSS.expertimg} />
                                <View style={ExpertCSS.state}>
                                   {this.state.zhuangtai}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={ExpertCSS.infomation} >
                            <Text style={ExpertCSS.name} >{this.state.con.nameMeng}</Text>
                            <Text style={ExpertCSS.department}  >{this.state.con.expertiseMeng}</Text>
                        </View>
                        <View style={ExpertCSS.expertintro} >
                            <View style={ExpertCSS.dealing} >
                                <Text style={ExpertCSS.dealingP} >ᠣᠨᠣᠰᠢᠯᠠᠭᠰᠠᠨ</Text>
                                <Text style={ExpertCSS.dealingN}  >{this.state.con.dealingProblems}</Text>
                            </View>
                            <View style={ExpertCSS.handle} >
                                <View style={ExpertCSS.video}>
                                    <TouchableOpacity  onPress={() =>this.getwaiting()}>
                                        <Image source={require('../../../img/x.png')} style={ExpertCSS.btn} />
                                    </TouchableOpacity>
                                </View>
                                <View style={ExpertCSS.phone}>
                                    <TouchableOpacity  onPress={() =>this.gettell()}>
                                        <Image source={require('../../../img/x.png')} style={ExpertCSS.btn} />
                                    </TouchableOpacity>
                                </View>
                                <View style={ExpertCSS.message}>
                                    <TouchableOpacity  onPress={() =>this.getAvatarURL()}>
                                        <Image source={require('../../../img/x.png')} style={ExpertCSS.btn} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={ExpertCSS.expertinfomation}>
                        <View style={ExpertCSS.expertinformationTop}>
                            <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],position: 'relative',top:140,width:height-222,}}>ᠲᠠᠨᠢᠭᠤᠯᠭ᠎ᠠ</Text>
                        </View>
                        <ScrollView horizontal={true}  style={[ExpertCSS.Einformation,{backgroundColor:'#fff'}]}>
                            <View style={{width:width,height:height-235,backgroundColor: '#fff', padding:5,margin:3,}}>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],position: 'absolute',width:height-250,height:width-50,left:(width-50)/2-(height-250)/2,top:(height-250)/2-(width-50)/2,}}>{this.state.con.personalMeng}</Text>
                            </View>
                        </ScrollView>
                    </View>
                </View>
            </ScrollView>
        );
    }
} 