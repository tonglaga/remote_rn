import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
    ToastAndroid,
    Dimensions,
    WebView
} from 'react-native';
import api from '../../../api';
import ConsultCSS from '../../../css/expert/MConsult';
const { width, height } = Dimensions.get('window');
import PopupDialog, { SlideAnimation,DialogTitle } from 'react-native-popup-dialog';
export default class Personal extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    }; 
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        const { params } = this.props.navigation.state;
        this.state = { 
            form: {
                expertId:params.id,
                content: "tonglagaa"
            }
        }
       
    }
    setStatecontent(text){
        let form=this.state.form;
        form.content=text;
         this.setState({
            form:form
        })   
      }
      //提交文字
      submitbtnText(){
        //console.warn(localStorage.token)
        api.categories.appAddTask({'loken-token':localStorage.token,expertId:this.state.form.expertId,content:this.state.form.content}).then(response => {
            ToastAndroid.show('发送成功，耐心等待专家回复！', ToastAndroid.SHORT);
             this.props.navigation.goBack(null)
        })
    }
    getshuru(){
       
        this.popupDialogs.show();
     }
    render() {
        const { navigate } = this.props.navigation;
        return (
          
            <View style={ConsultCSS.container}>
            	 <PopupDialog
					    dialogTitle={<DialogTitle title="蒙语在线输入" />}
					    ref={popupDialog => {
					        this.popupDialogs = popupDialog;
					    }}
					    width={width}
					    height={height-100}
					>
					   
                     
                        <WebView
                          ref='WebView'
                        
                        source={{uri:'file:///android_asset/index.html'}}
                        onMessage={(event) => {   //监听html发送过来的数据
                        const message = event.nativeEvent.data;
                       // DeviceEventEmitter.emit('imeMsg', message)
                      //  this.props.navigation.navigate('Meng')
                       // alert(message.split(',')[0])
                     
                        this.setStatecontent(message);
                         this.popupDialogs.dismiss();
                        }}

                         >
                       </WebView>
                          
					  

					</PopupDialog>
                <View style={ConsultCSS.header}>
                    <TouchableOpacity style={ConsultCSS.backImg} onPress={() => { this.props.navigation.goBack(null) }}>
                        <Image source={require('../../../img/nav_back01.png')} />
                    </TouchableOpacity>
                    <View style={ConsultCSS.title}>
                        <Text style={ConsultCSS.titleText}>ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠳᠡᠪᠰᠢᠭᠦᠯᠬᠦ</Text>
                    </View>         
                </View>
                <View style={[ConsultCSS.consultcontent]}>
                    <View>
                        <Text  style={[ConsultCSS.consultext,ConsultCSS.consultinput]} onPress={() => {this.getshuru()}}>{this.state.form.content}</Text>
                    </View>
                    <Text style={[ConsultCSS.prompt]}> ᠬᠠᠮᠤᠭ ᠢᠯᠡᠭᠦᠦ 100  ᠦᠰᠦᠭ  ᠪᠢᠴᠢᠨ᠎ᠡ</Text>
                    <TouchableOpacity style={[ConsultCSS.submitbtn]} onPress={() => this.submitbtnText()}>
                        <Text style={[ConsultCSS.submitbtnText]} onPress={() => this.submitbtnText()}>ᠲᠤᠰᠢᠶᠠᠬᠤ</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
        );
    }
}