import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    ToastAndroid,
    DeviceEventEmitter,
    Dimensions
} from 'react-native';
const { width, height } = Dimensions.get('window');
import api from '../../../api'
import config from '../../../config';
import JPushModule from 'jpush-react-native'
import LoginCSS from '../../../css/login';
import { setToken } from '../../../utils/token'
export default class Expert extends Component {
        constructor(props) {
            super(props);
            //两个状态用户输入框文本，密码框文本
            this.state = {
                registrationId:"",
                form: {
                    username: '',
                    password: ''
                }
            }
            //获取registrationId
            JPushModule.getRegistrationID(registrationId => {       
              this.setState({
                registrationId: registrationId
              })
            })
        }
        
        //点击事件函数
        login() {   
           
            api.user.login(this.state.form).then(response => {
                if(response.code == 401){
                    ToastAndroid.show(response.message, ToastAndroid.SHORT);
                }else{
                localStorage.token = response.token;
              //  console.warn(localStorage.token)
                setToken(response.token)
                api.user.user({'login-token':response.token}).then(response => {
                  //  console.warn(response)
                    localStorage.name = response.username;
                    localStorage.pass = response.password;
                    localStorage.id = response.id;
                   // localStorage.createTime = data.createTime;
                   // localStorage.updateTime = data.updateTime;
                    localStorage.nickname = response.nickname; 
                   //localStorage.phone = data.phone;
                   // localStorage.telephone = data.telephone;
                   // localStorage.email = data.email;
                   // localStorage.birthday = data.birthday;
                   // localStorage.sex = data.sex;
                   // localStorage.status = data.status;
                    localStorage.type = response.type;
                   // localStorage.typestatus = data.typestatus;
                    localStorage.userstatus=1;
                    if(localStorage.type == 1){
                     //牧民个人信息
                     api.user.mumin({'login-token':localStorage.token,'id':localStorage.id}).then(response => {
                           //  console.warn(response)

                             if(response.id != null && response.province != null && response.city != null && response.area != null){
                             localStorage.wanshan=2;
                             localStorage.headImgUrl ={uri:config.img_url+response.headerUrl};
                             DeviceEventEmitter.emit('headImgUrl', localStorage.headImgUrl)
                             }else{
                             localStorage.wanshan=1;
                             }
                             api.user.deviceId({'login-token':localStorage.token,'id':localStorage.id,'regsId':this.state.registrationId}).then(response => {
                                console.warn(response)
                             })
                            
                         })
                         //专家个人信息
                        }else if(localStorage.type == 2){
                            api.user.zhuanjia({'login-token':localStorage.token}).then(response => {
                               // console.warn(response)

                                if(response.id != null){
                                localStorage.wanshan=2;
                                localStorage.headImgUrl ={uri:config.img_url+response.headerUrl};
                                DeviceEventEmitter.emit('headImgUrl', localStorage.headImgUrl)
                                }else{
                                localStorage.wanshan=1;
                                }
                                api.user.deviceId({'login-token':localStorage.token,'id':localStorage.id,'regsId':this.state.registrationId}).then(response => {
                                   console.warn(response)
                                })
                               
                            })
    
                        }else if(localStorage.type == 3){
                            api.user.yaodian({'login-token':localStorage.token}).then(response => {
                               // console.warn(response)
                                if(response.id != null && response.province != null && response.city != null && response.area != null){
                                localStorage.wanshan=2;
                                localStorage.headImgUrl ={uri:config.img_url+response.headerUrl};
                                DeviceEventEmitter.emit('headImgUrl', localStorage.headImgUrl)
                                }else{
                                localStorage.wanshan=1;
                                }
                                api.user.deviceId({'login-token':localStorage.token,'id':localStorage.id,'regsId':this.state.registrationId}).then(response => {
                                   console.warn(response)
                                })
                               
                            })
                        }
                         //ToastAndroid.show('登陆成功！', ToastAndroid.SHORT);
                         //this.props.navigation.goBack(null) 
                    
                     
                })
                ToastAndroid.show('登录成功', ToastAndroid.SHORT)
                this.props.navigation.goBack(null)
               }
            })
           
        }
        //设置手机号
        setPhone(phone) {
            let form = this.state.form
            form.username = phone
            this.setState({
                form: form
            })
        }
        //设置密码
        setPassword(password) {
            let form = this.state.form
            form.password = password
            this.setState({
                form: form
            })
        }
        // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
        static navigationOptions = {
        };
        _onPressButton() {
            console.log("You tapped the button!");
        }
        componentDidMount() {
    
            // DeviceEventEmitter.addListener('register', (message) => {
            //    // console.warn(message)
            //    // this.getMessage(message); 
            //    this.setState({
            //        form:{
            //            username:message.username,
            //            pass:message.pass
            //        }
            //    })  
            // })
        }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}></Text>
                        </View>         
                    </View>
                    <View style={{transform:[
				            {rotateX:'180deg'},
				            {rotateZ:'270deg'}
				        ],width:height-80,height:515,marginTop:(height-80)/2-515/2,marginLeft:515/2-(height-80)/2,}}>
                        <View style={[LoginCSS.loginInformation]}>
                            <Text style={[LoginCSS.loginText,LoginCSS.Mongolia]}>ᠨᠡᠪᠲᠡᠷᠡᠬᠦ ᠨᠡᠷ᠎ᠡ᠄</Text>
                            <TextInput placeholder="ᠨᠡᠪᠲᠡᠷᠡᠬᠦ ᠨᠡᠷ᠎ᠡ ᠪᠢᠴᠢᠬᠦ"
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput,LoginCSS.Mongolia]}
                                onChangeText={(username) => this.setPhone(username)}
                            ></TextInput>
                        </View>
                        <View style={[LoginCSS.loginInformation]}>
                            <Text style={[LoginCSS.loginText,LoginCSS.Mongolia]}>ᠨᠢᠭᠤᠴᠠ ᠦᠭᠡ᠄</Text>
                            <TextInput placeholder="ᠨᠢᠭᠤᠴᠠ ᠦᠭᠡ ᠪᠢᠴᠢᠬᠦ"
                                secureTextEntry={true}
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput,LoginCSS.Mongolia]}
                                onChangeText={(password) => this.setPassword(password)}>
                                </TextInput>
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]} 
                            //点击事件，要记得绑定
                            onPress={() => this.login()}>
                                <Text style={[LoginCSS.loginbtnText,LoginCSS.Mongolia]}  onPress={() => this.login()}>ᠨᠡᠪᠲᠡᠷᠡᠬᠦ</Text>
                            </TouchableOpacity>
                            <View style={[LoginCSS.handle]}>
                                <TouchableOpacity style={[LoginCSS.register]} onPress={() => navigate('MRegister')}>
                                    <Text style={[LoginCSS.registerText,LoginCSS.Mongolia]}>ᠳᠠᠩᠰᠠᠯᠠᠬᠤ</Text>
                                </TouchableOpacity>
                                <View>
                                    <Text style={[LoginCSS.uprightText]}> | </Text>
                                </View>
                                <TouchableOpacity style={[LoginCSS.forgotpassword]} onPress={() => navigate('MForgetPassword')}>
                                    <Text style={[LoginCSS.forgotpasswordText,LoginCSS.Mongolia]}>ᠮᠠᠷᠲᠡᠬᠤ</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}