import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import api from '../../api';
import config from '../../config';
import DrugstoreCSS from '../../css/drugstore/MDrugstore'

export default class Drugstore extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式
    static navigationOptions = {
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../img/ys.png')}
            style={{ width: 20, height: 20, tintColor: tintColor }}
          />
        ) 
    }; 
    constructor(props) {
        super(props)
        this.state = {
          data: []
        }
      }
    _onPressButton() {
        console.log("You tapped the button!");
    }
    componentDidMount() {
        this.getList()
    }
    
    getList() {
        api.gradem.drugstoreinfo().then(response => {
         // console.warn(response)
          this.setState({
            data: response
          })
        })
      }
    _onPressButton() {
        console.log("You tapped the button!");
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={DrugstoreCSS.mainStyle}>
                <View style={DrugstoreCSS.container}>
                    <View style={DrugstoreCSS.header}>
                        <View style={DrugstoreCSS.title}>
                            <Text style={DrugstoreCSS.titleText}>药店</Text>
                        </View>         
                    </View>
                    <View style={DrugstoreCSS.drugstores}>
                      
                    {
                      this.state.data.map((item,id) => {
                         // console.warn(config.img_url+item.headerUrl);
                         // var img=config.img_url+item.headerUrl;
                      return (
                        <TouchableOpacity style={[DrugstoreCSS.drugstore,{flex:2}]}   onPress={() => navigate('MDrug',{itm:item})} key={id}>
                            <View style={DrugstoreCSS.portrait}>
                                <Image source={{uri:config.img_url+item.headerUrl}} style={DrugstoreCSS.drugstoreimg} />
                            </View>
                            <View style={[DrugstoreCSS.drugstoreintro,{flex:2}]}>
                                <Text style={[DrugstoreCSS.storename,{flex:2}]}>{item.pharmacyNameMeng} </Text>
                                <Text style={{fontFamily:'MongolianWhitePuaMirror',
                                    transform:[
                                        {rotateX:'180deg'},
                                        {rotateZ:'270deg'}
                                    ],flex:2}}>
                                   {item.provinceNameMeng} 
                                   {item.cityNameMeng} 
                                   {item.areaNameMeng}
                                </Text>
                            </View>
                        </TouchableOpacity>
                              )
                            })
                           }
                    </View>
                </View>
            </ScrollView>
        );
    }
}