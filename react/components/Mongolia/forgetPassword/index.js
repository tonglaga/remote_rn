import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Dimensions
} from 'react-native';
import LoginCSS from '../../../css/register';
const { width, height } = Dimensions.get('window');
export default class Expert extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    // static navigationOptions = {
    // };

   
    constructor(props) {
        super(props);
        //两个状态用户输入框文本，密码框文本
        this.state = {
            
            form: {
                username: '', 
            }
        }
       
     
    }
    render() {
        const { navigate } = this.props.navigation;

        const {username} = this.state.form;
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}></Text>
                        </View>         
                    </View>
                    <View style={{transform:[
                            {rotateX:'180deg'},
                            {rotateZ:'270deg'}
                        ],width:height-80,height:515,marginTop:(height-80)/2-515/2,marginLeft:515/2-(height-80)/2,}}>
                        <Text style={[LoginCSS.loginText,LoginCSS.Mongolia,{paddingLeft:0,padding:10,color:'#aaa',fontSize:12,}]}> ᠭᠠᠷ  ᠤᠲᠠᠰᠤᠨ  ᠨᠤᠮᠸ᠋ᠷ  ᠢᠶᠠᠨ  ᠪᠢᠴᠢᠭᠡᠷᠡᠢ</Text>
                        <View style={[LoginCSS.loginInformation]}>
                            <TextInput
                                 placeholder="ᠭᠠᠷ  ᠤᠲᠠᠰᠤᠨ  ᠨᠤᠮᠸ᠋ᠷ  ᠤᠨ  ᠪᠢᠴᠢᠭᠡᠷᠡᠢ"
                                 
                                underlineColorAndroid='transparent'

                                style={[LoginCSS.LoginInput,LoginCSS.Mongolia]}

                                value={username} onChangeText={(value)=>{

                                    let form = this.state.form

                                       form.username = value

                                    this.setState({
                                   
                                        form: form
                                       
                                    })
                                  }}
                            ></TextInput>
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]} onPress={() => navigate('MVerification',{tel:this.state.form.username})}>
                                <Text style={[LoginCSS.loginbtnText,LoginCSS.Mongolia]} onPress={() => navigate('MVerification',{tel:this.state.form.username})}>ᠳᠠᠷᠠᠬᠢ</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}