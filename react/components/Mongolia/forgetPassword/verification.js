import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    ToastAndroid,
    DeviceEventEmitter,
    Dimensions
} from 'react-native';
import LoginCSS from '../../../css/register';
import api from '../../../api'
import CountDownButton from 'react-native-smscode-count-down'
const { width, height } = Dimensions.get('window');
export default class Expert extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };

    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state;
        this.state = {
        form:{
            username:params.tel,
            code: "",
         }
        }
    }
    render() {
        const {username} = this.state.form;
        const { navigate } = this.props.navigation;
        const {code} = this.state.form;
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}></Text>
                        </View>         
                    </View>
                    <View style={{transform:[
                            {rotateX:'180deg'},
                            {rotateZ:'270deg'}
                        ],width:height-80,height:515,marginTop:(height-80)/2-515/2,marginLeft:515/2-(height-80)/2,}}>
                        <Text style={[LoginCSS.loginText,LoginCSS.Mongolia,{paddingLeft:0,padding:10,color:'#aaa',fontSize:12,}]}> ᠲᠠᠨ  ᠤ  ᠭᠠᠷ  ᠤᠲᠠᠰᠤᠨ  ᠨᠤᠮᠸ᠋ᠷ  ᠪᠤᠯ {username}”</Text>
                        <View style={[LoginCSS.loginInformation]}>
                            <TextInput
                                 placeholder="ᠱᠢᠯᠭᠠᠬᠤ  ᠨᠤᠮᠸ᠋ᠷ  ᠤᠯᠤᠬᠤ"
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput,LoginCSS.Mongolia,{flex:6}]}
                                value={code} onChangeText={(value)=>{
                                    let form = this.state.form
                                       form.code = value
                                    this.setState({
                                   
                                        form: form
                                       
                                    })
                                  }}
                            ></TextInput>
                            <TouchableOpacity style={{flex:2}}>
                            <CountDownButton
                                style={LoginCSS.Mongolia}
                                style={{width: 110,marginRight: 10}}
                                textStyle={{color: 'blue'}}
                                enable={username != ""}
                                timerCount={60}
                                timerTitle={'ᠱᠢᠯᠭᠠᠬᠤ  ᠨᠤᠮᠸ᠋ᠷ  ᠤᠯᠤᠬᠤ'}
                                timerActiveTitle={['（','s） ᠬᠤᠢᠨ᠎ᠠ  ᠲᠤᠷᠰᠢ']}
                                onClick={(shouldStartCounting)=>{
                                    //随机模拟发送验证码成功或失败
                                   // const requestSucc = Math.random() + 0.5 > 1; 
                                 
                                   api.user.sendMsg({tel:username}).then(response => {
                                    if(response.code == 400){
                                        ToastAndroid.show(response.message, ToastAndroid.SHORT);
                                       //DeviceEventEmitter.emit('register', {username:this.state.form.username,pass:this.state.form.pass})
                                     //  this.props.navigation.goBack(null);
                                     }
                                  })
                                  shouldStartCounting(true);
                                    
                                }}
                              />
                            </TouchableOpacity>
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]} onPress={() => navigate('MNewPassword',{tel:username,code:code})}>
                                <Text style={[LoginCSS.loginbtnText,LoginCSS.Mongolia]}>ᠳᠠᠷᠠᠬᠢ</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}