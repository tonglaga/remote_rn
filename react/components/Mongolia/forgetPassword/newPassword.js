import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
    TextInput,
    Dimensions
} from 'react-native';
import LoginCSS from '../../../css/register';
import api from '../../../api'
const { width, height } = Dimensions.get('window');
export default class Expert extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
    };
    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state;
        this.state = {
        form:{
            username:params.tel,
            code: params.code,
            pass:""
         }
        }
    }
    submit(){
        if(this.state.form.pass == ""){
            ToastAndroid.show('密码不能为空', ToastAndroid.SHORT);
            return; 
        }
        api.user.ChangPassword(this.state.form).then(response => {
            //console.warn(response)  
            if(response.code == 401){
               ToastAndroid.show(response.message, ToastAndroid.SHORT);
               //DeviceEventEmitter.emit('register', {username:this.state.form.username,pass:this.state.form.pass})
              // this.props.navigation.goBack(null);
            }else if(response.code == 400){
                ToastAndroid.show(response.message, ToastAndroid.SHORT);
            }else{
               ToastAndroid.show("修改成功，重新登录", ToastAndroid.SHORT);
              // DeviceEventEmitter.emit('register', {username:this.state.form.username,pass:this.state.form.pass})
               this.props.navigation.navigate('login');
            }
           
       })  
    }
    render() {
        const { navigate } = this.props.navigation;
        const {pass} = this.state.form;
        return (
            <ScrollView style={[LoginCSS.mainStyle]}>
                <View style={[LoginCSS.container]}>
                    <View style={[LoginCSS.header]}>
                        <TouchableOpacity style={[LoginCSS.backImg]} onPress={() => { this.props.navigation.goBack(null) }}>
                            <Image source={require('../../../img/nav_back01.png')} />
                        </TouchableOpacity>
                        <View style={LoginCSS.title}>
                            <Text style={[LoginCSS.titleText]}></Text>
                        </View>         
                    </View>
                    <View style={{transform:[
                            {rotateX:'180deg'},
                            {rotateZ:'270deg'}
                        ],width:height-80,height:515,marginTop:(height-80)/2-515/2,marginLeft:515/2-(height-80)/2,marginTop:3,}}>
                        <Text style={[LoginCSS.loginText,LoginCSS.Mongolia,{paddingLeft:0,padding:10,color:'#aaa',fontSize:12,}]}>ᠰᠢᠨ᠎ᠡ  ᠨᠢᠭᠤᠴᠠ  ᠦᠭᠡ᠄</Text>
                        <View style={[LoginCSS.loginInformation]}>
                            <TextInput
                                 placeholder=" ᠰᠢᠨ᠎ᠡ  ᠨᠢᠭᠤᠴᠠ  ᠦᠭᠡᠨ  ᠪᠢᠴᠢᠭᠡᠷᠡᠢ"
                                underlineColorAndroid='transparent'
                                style={[LoginCSS.LoginInput,LoginCSS.Mongolia]}
                                value={pass} onChangeText={(value)=>{
                                    let form = this.state.form
                                    form.pass = value
                                    this.setState({
                                   
                                        form: form
                                       
                                    })
                                  }}
                            ></TextInput>
                        </View>
                        <View style={[LoginCSS.handles]}>
                            <TouchableOpacity style={[LoginCSS.loginbtn]}  onPress={() => this.submit()}>
                                <Text style={[LoginCSS.loginbtnText,LoginCSS.Mongolia]}>ᠲᠤᠰᠢᠶᠠᠬᠤ</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        );
    }

}