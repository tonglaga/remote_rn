import React, { Component } from 'react';
import {
    Text,
    Image,
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import StudyCSS from '../../css/study/MStudy'
import api from '../../api';
import config from '../../config';
export default class Study extends Component {
    // 此处设置 Tab 的名称和一些样式，这里的会覆盖掉配置路由文件的样式，下面会讲
    static navigationOptions = {
        headerTitle: '学习',
        tabBarLabel: '学习',
        tabBarIcon: ({ focused, tintColor }) => (
          <Image
            source={require('../../img/xs.png')}
            style={{ width: 20, height: 20, tintColor: tintColor }}
          />
        )
    };

    constructor(props) {
        super(props)
        const { params } = this.props.navigation.state
        this.state = {
          filterNum: 1,
          StudyArticlem:[],
          StudyVideom:[],
          Taskm:[]
        }
    }
    componentDidMount() {
        this.getList()
    }
    getList() {
        api.gradem.Videom().then(response => {
         // console.warn(response)
          this.setState({
            StudyVideom: response
          })
        })
        api.gradem.Articlem().then(response => {
            // console.warn(response)
             this.setState({
                StudyArticlem: response
             })
           })
        api.gradem.Taskms().then(response => {
            // console.warn(response)
             this.setState({
                Taskm: response
             })
           })   
      }

    getFilter() {
        const { navigate } = this.props.navigation;
        if(this.state.filterNum === 1) {
            return (
                <ScrollView horizontal={true} style={{marginLeft:55,flexDirection:'row',}}>
                     {
                      this.state.StudyVideom.map((item,id) => {
                         
                    return (
                    <TouchableOpacity style={[StudyCSS.content]} key={id} onPress={() => this.props.navigation.navigate('MArticle',{itm:item})}>
                        <Image source={{uri:config.img_url+item.coverPhoto}} style={StudyCSS.drugstoreimg} />
                        <View style={StudyCSS.info}>
                            <View style={{flex:1,}}>
                                <Text style={StudyCSS.articletitle}>{item.title}</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={StudyCSS.reading}>ᠦᠵᠡᠭᠰᠡᠨ：{item.clicks}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                     )
                    })
                   }
                
                </ScrollView>
            )
        } else if(this.state.filterNum === 2) {
            return (
                <ScrollView horizontal={true} style={{marginLeft:55,flexDirection:'row',}}>
                 {
                    this.state.StudyArticlem.map((item,id) => {
                     return (
                    <TouchableOpacity style={[StudyCSS.content]} key={id} onPress={() => this.props.navigation.navigate('MVideo',{itm:item})}>
                        <Image source={{uri:config.img_url+item.coverPhoto}} style={StudyCSS.drugstoreimg} />
                        <View style={StudyCSS.info}>
                            <View style={{flex:1,}}>
                                <Text style={StudyCSS.articletitle}>{item.title}</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={StudyCSS.reading}>ᠦᠵᠡᠭᠰᠡᠨ：{item.clicks}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    )
                })
               }
                
                </ScrollView>
            )
        } else if(this.state.filterNum === 3) {
            return (
                <ScrollView horizontal={true} style={{marginLeft:55,flexDirection:'row',}}>
                 {
                    this.state.Taskm.map((item,id) => {
                     return (
                    <TouchableOpacity key={id} style={[StudyCSS.content]} onPress={() => this.props.navigation.navigate('MCase',{itm:item})}>
                        <View style={StudyCSS.info}>
                            <View style={{flex:1,}}>
                                <Text style={StudyCSS.casetitle}>{item.title}</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={StudyCSS.cause}>{item.content}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                      )
                    })
                   }
                  
                </ScrollView>
            )
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <ScrollView style={StudyCSS.mainstyle}>
                <View style={StudyCSS.container}>
                    <View style={StudyCSS.header}>
                        <View style={StudyCSS.title}>
                            <Text style={StudyCSS.titleText}></Text>
                        </View>         
                    </View>
                    <View style={StudyCSS.studys}>
                        <View style={StudyCSS.tabs}>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 1 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.video}>ᠸᠢᠳᠢᠣ᠋</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 2 }) }} style={[StudyCSS.headeritem,StudyCSS.headeritemborder]}>
                                <Text style={StudyCSS.article}>ᠵᠢᠷᠣᠭ</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ filterNum: 3 }) }} style={StudyCSS.headeritem}>
                                <Text style={StudyCSS.task}>ᠡᠪᠡᠳᠴᠢᠨ  ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡ</Text>
                            </TouchableOpacity>
                        </View>
                        { this.getFilter() }
                    </View>
                </View>
            </ScrollView>
        );
    }
}    