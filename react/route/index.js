import React from 'react';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';
// 汉语
import Home from '../components/Chinese/Home';   // 首页
import Drugstore from '../components/Chinese/Drugstore';   // 药店
import Expert from '../components/Chinese/Expert';          // 专家
import Study from '../components/Chinese/Study';     // 学习
import Personal from '../components/Chinese/Personal';     // 个人中心
import Drug from '../components/Chinese/drug';     // 药店详情
import Medicinal from '../components/Chinese/drug/medicinal';
import ExpertDetail from '../components/Chinese/expert';
import Login from '../components/Chinese/login';
import Message from '../components/Chinese/personal/message';
import MessageDetail from '../components/Chinese/personal/messageDetail ';
import Collect from '../components/Chinese/personal/collect';
import Waiting from '../components/Chinese/expert/waiting'; 
import Handle from '../components/Chinese/expert/handle'; 
import Consult from '../components/Chinese/expert/consult'; 
import Article from '../components/Chinese/study/article';     // 文章
import Video from '../components/Chinese/study/video';     // 视频
import Task from '../components/Chinese/study/task';     // 案例
import About from '../components/Chinese/personal/about';     // 平台
import Introduces from '../components/Chinese/personal/introduces';     // 功能介绍
import Feedbacks from '../components/Chinese/personal/feedbacks';     // 意见反馈
import ECenter from '../components/Chinese/personal/expert/center';     // 专家中心
import FCenter from '../components/Chinese/personal/farmer/center';     // 牧民中心
import DCenter from '../components/Chinese/personal/drugstore/center';     // 药店中心
import Diagnose from '../components/Chinese/personal/expert/diagnose';     // 专家诊断管理
import ArticleManagement from '../components/Chinese/personal/expert/articleManagement';     // 文章管理
import ArticleDetail from '../components/Chinese/personal/expert/articleDetail';     // 文章管理详情
import EditArticle from '../components/Chinese/personal/expert/editArticle';     // 修改文章
import AddArticle from '../components/Chinese/personal/expert/addArticle';     // 添加文章
import PerfectInformation from '../components/Chinese/personal/expert/PerfectInformation';     // 专家完善资料
import FarmerPerfectInformation from '../components/Chinese/personal/farmer/PerfectInformation';     // 牧民完善资料
import DrugstorePerfectInformation from '../components/Chinese/personal/drugstore/PerfectInformation';     //药店完善资料
import Solved from '../components/Chinese/personal/expert/solved';     // 诊断管理的已处理
import Suspending from '../components/Chinese/personal/expert/suspending';     // 诊断管理的已处理
import ChooseMedicinal from '../components/Chinese/personal/expert/chooseMedicinal';     // 选择药品
import FarmerDiagnose from '../components/Chinese/personal/farmer/diagnose';     // 牧民我的诊断
import FarmerDiagnoseDetail from '../components/Chinese/personal/farmer/diagnoseDetail';     // 牧民我的诊断详情
import Prescription from '../components/Chinese/personal/drugstore/prescription';     //药店药方管理
import PrescriptionDetail from '../components/Chinese/personal/drugstore/prescriptionDetail';     //药店药方管理
import DrugstoreMedicinal from '../components/Chinese/personal/drugstore/medicinal';     //药店药品管理
import AddMedicinal from '../components/Chinese/personal/drugstore/addMedicinal';     //添加药品
import EditMedicinal from '../components/Chinese/personal/drugstore/editMedicinal';     //编辑药品
import Appliance from '../components/Chinese/appliance';     //器械详情
import Videocall from '../Video/Videocall';     // 视频通话

//蒙语测试
import Homem from '../components/Mongolia/Homem'; 
import Mengm from '../components/Mongolia/Mengm'; 
// 蒙语


import Ime from '../Meng/ime'

// 注册tabs
const Tabs = createBottomTabNavigator({
    Home: {
        screen: Home
    },
    Drugstore: {
        screen: Drugstore,
    },
    Expert: {
        screen: Expert,
    },
    Study: {
        screen: Study, 
    },
    Personal: {
        screen: Personal,
    }

}, {
    animationEnabled: false, // 切换页面时是否有动画效果
    tabBarPosition: 'bottom', // 显示在底端，android 默认是显示在页面顶端的
    swipeEnabled: false, // 是否可以左右滑动切换tab
    backBehavior: 'none', // 按 back 键是否跳转到第一个Tab(首页)， none 为不跳转
    tabBarOptions: {
        activeTintColor: '#14afff', // 文字和图片选中颜色
        inactiveTintColor: '#999', // 文字和图片未选中颜色
        showIcon: true, // android 默认不显示 icon, 需要设置为 true 才会显示
        showLabel:false,
        indicatorStyle: {
            height: 0  // 如TabBar下面显示有一条线，可以设高度为0后隐藏
        },
        style: {
            backgroundColor: '#fff', // TabBar 背景色
        },
        labelStyle: {
            fontSize: 14, // 文字大小
        },
    },
});


export default createStackNavigator({
    Main: {
        screen: Tabs,
        navigationOptions:{
            header:null,
        }
    },
    Drug: {
        screen: Drug,
        navigationOptions:{
            header:null,
        }
    },
    Medicinal: {
        screen: Medicinal,
        navigationOptions:{
            header:null,
        }
    },
    ExpertDetail: {
        screen: ExpertDetail,
        navigationOptions:{
            header:null,
        }
    },
    Login: {
        screen: Login,
        navigationOptions:{
            header:null,
        }
    },
    Message: {
        screen: Message,
        navigationOptions:{
            header:null,
        }
    },
    MessageDetail: {
        screen: MessageDetail,
        navigationOptions:{
            header:null,
        }
    },
    Collect: {
        screen: Collect,
        navigationOptions:{
            header:null,
        }
    },
    Waiting: {
        screen: Waiting,
        navigationOptions:{
            header:null,
        }
    },
    Handle: {
        screen: Handle,
        navigationOptions:{
            header:null,
        }
    },
    Consult: {
        screen: Consult,
        navigationOptions:{
            header:null,
        }
    },
    Videocall: {
        screen: Videocall,
        navigationOptions:{
            header:null,
        }
    },
    Article: {
        screen: Article,
        navigationOptions:{
            header:null,
        }
    },
    Video: {
        screen: Video,
        navigationOptions:{
            header:null,
        }
    },
    Task: {
        screen: Task,
        navigationOptions:{
            header:null,
        }
    },
    About: {
        screen: About,
        navigationOptions:{
            header:null,
        }
    },
    Introduces: {
        screen: Introduces,
        navigationOptions:{
            header:null,
        }
    },
    Feedbacks: {
        screen: Feedbacks,
        navigationOptions:{
            header:null,
        }
    },
    ECenter: {
        screen: ECenter,
        navigationOptions:{
            header:null,
        }
    },
    DCenter: {
        screen: DCenter,
        navigationOptions:{
            header:null,
        }
    },
    FCenter: {
        screen: FCenter,
        navigationOptions:{
            header:null,
        }
    },
    Diagnose: {
        screen: Diagnose,
        navigationOptions:{
            header:null,
        }
    },
    ArticleManagement: {
        screen: ArticleManagement,
        navigationOptions:{
            header:null,
        }
    },
    ArticleDetail: {
        screen: ArticleDetail,
        navigationOptions:{
            header:null,
        }
    },
    EditArticle: {
        screen: EditArticle,
        navigationOptions:{
            header:null,
        }
    },
    AddArticle: {
        screen: AddArticle,
        navigationOptions:{
            header:null,
        }
    },
    PerfectInformation: {
        screen: PerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    Solved: {
        screen: Solved,
        navigationOptions:{
            header:null,
        }
    },
    Suspending: {
        screen: Suspending,
        navigationOptions:{
            header:null,
        }
    },
    ChooseMedicinal:{
        screen: ChooseMedicinal,
        navigationOptions:{
            header:null,
        }
    }, 
    FarmerDiagnose: {
        screen: FarmerDiagnose,
        navigationOptions:{
            header:null,
        }
    },
    FarmerDiagnoseDetail: {
        screen: FarmerDiagnoseDetail,
        navigationOptions:{
            header:null,
        }
    },
    FarmerPerfectInformation: {
        screen: FarmerPerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    DrugstorePerfectInformation: {
        screen: DrugstorePerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    Prescription: {
        screen: Prescription,
        navigationOptions:{
            header:null,
        }
    },
    PrescriptionDetail: {
        screen: PrescriptionDetail,
        navigationOptions:{
            header:null,
        }
    },
    DrugstoreMedicinal: {
        screen: DrugstoreMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    AddMedicinal: {
        screen: AddMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    EditMedicinal: {
        screen: EditMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    Appliance: {
        screen: Appliance,
        navigationOptions:{
            header:null,
        }
    },
    //蒙语测试
    Homem: {
        screen: Homem,
        navigationOptions:{
            header:null,
        }
    },
    Mengm: {
        screen: Mengm,
        navigationOptions:{
            header:null,
        }
    },
    Ime: {
        screen: Ime,
        navigationOptions:{
            header:null,
        }
    },
  

},
{
    headerMode: 'screen',  // 标题导航
    initialRouteName: 'Main', // 默认先加载的页面组件
    mode: 'modal'       // 定义跳转风格(card、modal)
});