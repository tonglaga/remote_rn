import React from 'react';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';
// 汉语
import Home from '../components/Chinese/Home';   // 首页
import Drugstore from '../components/Chinese/Drugstore';   // 药店
import Expert from '../components/Chinese/Expert';          // 专家
import Study from '../components/Chinese/Study';     // 学习
import Personal from '../components/Chinese/Personal';     // 个人中心
import Drug from '../components/Chinese/drug';     // 药店详情
import Medicinal from '../components/Chinese/drug/medicinal'; //药品详情
import ExpertDetail from '../components/Chinese/expert'; //专家详情
import Login from '../components/Chinese/login';
import Message from '../components/Chinese/personal/message';
import MessageDetail from '../components/Chinese/personal/messageDetail ';
import Collect from '../components/Chinese/personal/collect';
import Waiting from '../components/Chinese/expert/waiting'; 
import Handle from '../components/Chinese/expert/handle'; 
import Consult from '../components/Chinese/expert/consult'; 
import Article from '../components/Chinese/study/article';     // 文章
import Video from '../components/Chinese/study/video';     // 视频
import Task from '../components/Chinese/study/task';     // 案例
import About from '../components/Chinese/personal/about';     // 平台
import Introduces from '../components/Chinese/personal/introduces';     // 功能介绍
import Feedbacks from '../components/Chinese/personal/feedbacks';     // 意见反馈
import ECenter from '../components/Chinese/personal/expert/center';     // 专家中心
import FCenter from '../components/Chinese/personal/farmer/center';     // 牧民中心
import DCenter from '../components/Chinese/personal/drugstore/center';     // 药店中心
import Diagnose from '../components/Chinese/personal/expert/diagnose';     // 专家诊断管理
import ArticleManagement from '../components/Chinese/personal/expert/articleManagement';     // 文章管理
import ArticleDetail from '../components/Chinese/personal/expert/articleDetail';     // 文章管理详情
import EditArticle from '../components/Chinese/personal/expert/editArticle';     // 修改文章
import AddArticle from '../components/Chinese/personal/expert/addArticle';     // 添加文章
import PerfectInformation from '../components/Chinese/personal/expert/PerfectInformation';     // 专家完善资料
import FarmerPerfectInformation from '../components/Chinese/personal/farmer/PerfectInformation';     // 牧民完善资料
import DrugstorePerfectInformation from '../components/Chinese/personal/drugstore/PerfectInformation';     //药店完善资料
import Solved from '../components/Chinese/personal/expert/solved';     // 诊断管理的已处理
import Suspending from '../components/Chinese/personal/expert/suspending';     // 诊断管理的已处理
import ChooseMedicinal from '../components/Chinese/personal/expert/chooseMedicinal';     // 选择药品
import FarmerDiagnose from '../components/Chinese/personal/farmer/diagnose';     // 牧民我的诊断
import FarmerDiagnoseDetail from '../components/Chinese/personal/farmer/diagnoseDetail';     // 牧民我的诊断详情
import Prescription from '../components/Chinese/personal/drugstore/prescription';     //药店药方管理
import PrescriptionDetail from '../components/Chinese/personal/drugstore/prescriptionDetail';     //药店药方管理
import DrugstoreMedicinal from '../components/Chinese/personal/drugstore/medicinal';     //药店药品管理
import AddMedicinal from '../components/Chinese/personal/drugstore/addMedicinal';     //添加药品
import EditMedicinal from '../components/Chinese/personal/drugstore/editMedicinal';     //编辑药品
import Videocall from '../Video/Videocall';     // 视频通话


//蒙语测试
import Homem from '../components/Mongolia/Homem'; 
import Mengm from '../components/Mongolia/Mengm'; 

// 蒙语
import MHome from '../components/Mongolia/MHome'; // 首页
import MDrugstore from '../components/Mongolia/MDrugstore'; // 药店
import MExpert from '../components/Mongolia/MExpert';          // 专家
import MStudy from '../components/Mongolia/MStudy';     // 学习
import MPersonal from '../components/Mongolia/MPersonal';     // 个人中心
import MDrug from '../components/Mongolia/drug';     // 药店详情
import MMedicinal from '../components/Mongolia/drug/MMedicinal'; //药品详情
import MExpertDetail from '../components/Mongolia/expert/index'; //专家详情
import MArticle from '../components/Mongolia/study/MArticle';     // 文章
import MVideo from '../components/Mongolia/study/MVideo';     // 视频
import MCase from '../components/Mongolia/study/MCase';     // 案例
import MCollect from '../components/Mongolia/personal/MCollect';  // 收藏
import MMessage from '../components/Mongolia/personal/MMessage';   //消息
import MMessageDetail from '../components/Mongolia/personal/MMessageDetail';  //消息详情
import MAbout from '../components/Mongolia/personal/MAbout';     // 平台
import MDCenter from '../components/Mongolia/personal/drugstore/MCenter';     // 药店中心
import MFeedback from '../components/Mongolia/personal/MFeedback';     // 意见反馈
import MPrescription from '../components/Mongolia/personal/drugstore/MPrescription';     //药店药方管理
import MPrescriptionDetail from '../components/Mongolia/personal/drugstore/MPrescriptionDetail';     //药店药方管理
import MDrugstoreMedicinal from '../components/Mongolia/personal/drugstore/MMedicinal';     //药店药品管理
import MAddMedicinal from '../components/Mongolia/personal/drugstore/MAddMedicinal';     //添加药品
import MEditMedicinal from '../components/Mongolia/personal/drugstore/MEditMedicinal';     //编辑药品
import MDrugstorePerfectInformation from '../components/Mongolia/personal/drugstore/MPerfectInformation';     //药店完善资料
import MExpertPerfectInformation from '../components/Mongolia/personal/expert/MPerfectInformation';     // 专家完善资料
import MFarmerPerfectInformation from '../components/Mongolia/personal/farmer/MPerfectInformation';     // 牧民完善资料
import MFarmerDiagnose from '../components/Mongolia/personal/farmer/MDiagnose';     // 牧民我的诊断
import MFarmerDiagnoseDetail from '../components/Mongolia/personal/farmer/MDiagnoseDetail';     // 牧民我的诊断详情
import MDiagnose from '../components/Mongolia/personal/expert/MDiagnose';     // 专家诊断管理
import MSolved from '../components/Mongolia/personal/expert/MSolved';     // 诊断管理的已处理
import MSuspending from '../components/Mongolia/personal/expert/MSuspending';     // 诊断管理的已处理
import MChooseMedicinal from '../components/Mongolia/personal/expert/MChooseMedicinal';     // 选择药品
import MArticleManagement from '../components/Mongolia/personal/expert/MArticleManagement';     // 文章管理
import MArticleDetail from '../components/Mongolia/personal/expert/MArticleDetail';     // 文章管理详情
import MEditArticle from '../components/Mongolia/personal/expert/MEditArticle';     // 修改文章
import MAddArticle from '../components/Mongolia/personal/expert/MAddArticle';     // 添加文章
import MConsult from '../components/Mongolia/expert/MConsult'; 
import MLogin from '../components/Mongolia/login';
import MRegister from '../components/Mongolia/register';
import MForgetPassword from '../components/Mongolia/forgetPassword';
import MVerification from '../components/Mongolia/forgetPassword/verification';
import MNewPassword from '../components/Mongolia/forgetPassword/newPassword';

import MECenter from '../components/Mongolia/personal/expert/MCenter';     // 专家中心
import MFCenter from '../components/Mongolia/personal/farmer/MCenter';     // 牧民中心

import Ime from '../Meng/ime'
// 注册tabs
const Tabs = createBottomTabNavigator({
    Home: {
        screen: Home
    },
    Drugstore: {
        screen: Drugstore,
    },
    Expert: {
        screen: Expert,
    },
    Study: {
        screen: Study, 
    },
    Personal: {
        screen: Personal,
    }

}, {
    animationEnabled: false, // 切换页面时是否有动画效果
    tabBarPosition: 'bottom', // 显示在底端，android 默认是显示在页面顶端的
    swipeEnabled: false, // 是否可以左右滑动切换tab
    backBehavior: 'none', // 按 back 键是否跳转到第一个Tab(首页)， none 为不跳转
    tabBarOptions: {
        activeTintColor: '#14afff', // 文字和图片选中颜色
        inactiveTintColor: '#999', // 文字和图片未选中颜色
        showIcon: true, // android 默认不显示 icon, 需要设置为 true 才会显示
        showLabel:false,
        indicatorStyle: {
            height: 0  // 如TabBar下面显示有一条线，可以设高度为0后隐藏
        },
        style: {
            backgroundColor: '#fff', // TabBar 背景色
        },
        labelStyle: {
            fontSize: 14, // 文字大小
        },
    },
});

const MTabs = createBottomTabNavigator({
    MHome: {
        screen: MHome
    },
    MDrugstore: {
        screen: MDrugstore,
    },
    MExpert: {
        screen: MExpert,
    },
    MStudy: {
        screen: MStudy, 
    },
    MPersonal: {
        screen: MPersonal,
    }

}, {
    animationEnabled: false, // 切换页面时是否有动画效果
    tabBarPosition: 'bottom', // 显示在底端，android 默认是显示在页面顶端的
    swipeEnabled: false, // 是否可以左右滑动切换tab
    backBehavior: 'none', // 按 back 键是否跳转到第一个Tab(首页)， none 为不跳转
    tabBarOptions: {
        activeTintColor: '#14afff', // 文字和图片选中颜色
        inactiveTintColor: '#999', // 文字和图片未选中颜色
        showIcon: true, // android 默认不显示 icon, 需要设置为 true 才会显示
        showLabel:false,
        indicatorStyle: {
            height: 0  // 如TabBar下面显示有一条线，可以设高度为0后隐藏
        },
        style: {
            backgroundColor: '#fff', // TabBar 背景色
        },
        labelStyle: {
            fontSize: 14, // 文字大小
        },
    },
});


export default createStackNavigator({
    Main: {
        screen: Tabs,
        navigationOptions:{
            header:null,
        }
    },
    Drug: {
        screen: Drug,
        navigationOptions:{
            header:null,
        }
    },
    Medicinal: {
        screen: Medicinal,
        navigationOptions:{
            header:null,
        }
    },
    ExpertDetail: {
        screen: ExpertDetail,
        navigationOptions:{
            header:null,
        }
    },
    Login: {
        screen: Login,
        navigationOptions:{
            header:null,
        }
    },
    Message: {
        screen: Message,
        navigationOptions:{
            header:null,
        }
    },
    MessageDetail: {
        screen: MessageDetail,
        navigationOptions:{
            header:null,
        }
    },
    Collect: {
        screen: Collect,
        navigationOptions:{
            header:null,
        }
    },
    Waiting: {
        screen: Waiting,
        navigationOptions:{
            header:null,
        }
    },
    Handle: {
        screen: Handle,
        navigationOptions:{
            header:null,
        }
    },
    Consult: {
        screen: Consult,
        navigationOptions:{
            header:null,
        }
    },
    Videocall: {
        screen: Videocall,
        navigationOptions:{
            header:null,
        }
    },
    Article: {
        screen: Article,
        navigationOptions:{
            header:null,
        }
    },
    Video: {
        screen: Video,
        navigationOptions:{
            header:null,
        }
    },
    Task: {
        screen: Task,
        navigationOptions:{
            header:null,
        }
    },
    About: {
        screen: About,
        navigationOptions:{
            header:null,
        }
    },
    Introduces: {
        screen: Introduces,
        navigationOptions:{
            header:null,
        }
    },
    Feedbacks: {
        screen: Feedbacks,
        navigationOptions:{
            header:null,
        }
    },
    ECenter: {
        screen: ECenter,
        navigationOptions:{
            header:null,
        }
    },
    DCenter: {
        screen: DCenter,
        navigationOptions:{
            header:null,
        }
    },
    FCenter: {
        screen: FCenter,
        navigationOptions:{
            header:null,
        }
    },
    Diagnose: {
        screen: Diagnose,
        navigationOptions:{
            header:null,
        }
    },
    ArticleManagement: {
        screen: ArticleManagement,
        navigationOptions:{
            header:null,
        }
    },
    ArticleDetail: {
        screen: ArticleDetail,
        navigationOptions:{
            header:null,
        }
    },
    EditArticle: {
        screen: EditArticle,
        navigationOptions:{
            header:null,
        }
    },
    AddArticle: {
        screen: AddArticle,
        navigationOptions:{
            header:null,
        }
    },
    PerfectInformation: {
        screen: PerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    Solved: {
        screen: Solved,
        navigationOptions:{
            header:null,
        }
    },
    Suspending: {
        screen: Suspending,
        navigationOptions:{
            header:null,
        }
    },
    ChooseMedicinal:{
        screen: ChooseMedicinal,
        navigationOptions:{
            header:null,
        }
    }, 
    FarmerDiagnose: {
        screen: FarmerDiagnose,
        navigationOptions:{
            header:null,
        }
    },
    FarmerDiagnoseDetail: {
        screen: FarmerDiagnoseDetail,
        navigationOptions:{
            header:null,
        }
    },
    FarmerPerfectInformation: {
        screen: FarmerPerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    DrugstorePerfectInformation: {
        screen: DrugstorePerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    Prescription: {
        screen: Prescription,
        navigationOptions:{
            header:null,
        }
    },
    PrescriptionDetail: {
        screen: PrescriptionDetail,
        navigationOptions:{
            header:null,
        }
    },
    DrugstoreMedicinal: {
        screen: DrugstoreMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    AddMedicinal: {
        screen: AddMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    EditMedicinal: {
        screen: EditMedicinal,
        navigationOptions:{
            header:null,
        }
    },

    //蒙语测试
    Homem: {
        screen: Homem,
        navigationOptions:{
            header:null,
        }
    },
    Mengm: {
        screen: Mengm,
        navigationOptions:{
            header:null,
        }
    },
    Ime: {
        screen: Ime,
        navigationOptions:{
            header:null,
        }
    },


    // 蒙语
    MainM: {
        screen: MTabs,
        navigationOptions:{
            header:null,
        }
    },
    MDrug: {
        screen: MDrug,
        navigationOptions:{
            header:null,
        }
    },
    MMedicinal: {
        screen: MMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    MExpertDetail: {
        screen: MExpertDetail,
        navigationOptions:{
            header:null,
        }
    },
    MArticle: {
        screen: MArticle,
        navigationOptions:{
            header:null,
        }
    },
    MVideo: {
        screen: MVideo,
        navigationOptions:{
            header:null,
        }
    },
    MCase: {
        screen: MCase,
        navigationOptions:{
            header:null,
        }
    },
    MCollect: {
        screen: MCollect,
        navigationOptions:{
            header:null,
        }
    },
    MMessage: {
        screen: MMessage,
        navigationOptions:{
            header:null,
        }
    },
    MMessageDetail: {
        screen: MMessageDetail,
        navigationOptions:{
            header:null,
        }
    },
    MAbout: {
        screen: MAbout,
        navigationOptions:{
            header:null,
        }
    },
    MDCenter: {
        screen: MDCenter,
        navigationOptions:{
            header:null,
        }
    },
    MFeedback: {
        screen: MFeedback,
        navigationOptions:{
            header:null,
        }
    },
    MPrescription: {
        screen: MPrescription,
        navigationOptions:{
            header:null,
        }
    },
    MPrescriptionDetail: {
        screen: MPrescriptionDetail,
        navigationOptions:{
            header:null,
        }
    },
    MDrugstoreMedicinal: {
        screen: MDrugstoreMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    MAddMedicinal: {
        screen: MAddMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    MEditMedicinal: {
        screen: MEditMedicinal,
        navigationOptions:{
            header:null,
        }
    },
    MDrugstorePerfectInformation: {
        screen: MDrugstorePerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    MECenter: {
        screen: MECenter,
        navigationOptions:{
            header:null,
        }
    },
    MFCenter: {
        screen: MFCenter,
        navigationOptions:{
            header:null,
        }
    },
    MFarmerDiagnose: {
        screen: MFarmerDiagnose,
        navigationOptions:{
            header:null,
        }
    },
    MFarmerDiagnoseDetail: {
        screen: MFarmerDiagnoseDetail,
        navigationOptions:{
            header:null,
        }
    },
    MExpertPerfectInformation: {
        screen: MExpertPerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    MFarmerPerfectInformation: {
        screen: MFarmerPerfectInformation,
        navigationOptions:{
            header:null,
        }
    },
    MDiagnose: {
        screen: MDiagnose,
        navigationOptions:{
            header:null,
        }
    },
    MSolved: {
        screen: MSolved,
        navigationOptions:{
            header:null,
        }
    },
    MSuspending: {
        screen: MSuspending,
        navigationOptions:{
            header:null,
        }
    },
    MChooseMedicinal:{
        screen: MChooseMedicinal,
        navigationOptions:{
            header:null,
        }
    }, 
    MArticleManagement: {
        screen: MArticleManagement,
        navigationOptions:{
            header:null,
        }
    },
    MArticleDetail: {
        screen: MArticleDetail,
        navigationOptions:{
            header:null,
        }
    },
    MEditArticle: {
        screen: MEditArticle,
        navigationOptions:{
            header:null,
        }
    },
    MAddArticle: {
        screen: MAddArticle,
        navigationOptions:{
            header:null,
        }
    },
    MConsult: {
        screen: MConsult,
        navigationOptions:{
            header:null,
        }
    },
    MLogin: {
        screen: MLogin,
        navigationOptions:{
            header:null,
        }
    },
    MRegister: {
        screen: MRegister,
        navigationOptions:{
            header:null,
        }
    },
    MForgetPassword: {
        screen: MForgetPassword,
        navigationOptions:{
            header:null,
        }
    },
    MVerification: {
        screen: MVerification,
        navigationOptions:{
            header:null,
        }
    },
    MNewPassword: {
        screen: MNewPassword,
        navigationOptions:{
            header:null,
        }
    },
},
{
    headerMode: 'screen',  // 标题导航
    initialRouteName: 'MainM', // 默认先加载的页面组件
    mode: 'modal'       // 定义跳转风格(card、modal)
});