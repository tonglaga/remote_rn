import React, { Component } from 'react';
import { WebView, DeviceEventEmitter } from 'react-native';

export default class Ime extends Component {

	render() {
		return (
			<WebView
                          ref='WebView'
                          style={{
                                  height:100,
                        }}
                        source={{uri:'file:///android_asset/index.html'}}
                        onMessage={(event) => {   //监听html发送过来的数据
              const message = event.nativeEvent.data;
              DeviceEventEmitter.emit('imeMsg', message)
              this.props.navigation.navigate('Meng')
               // alert(message.split(',')[0])
          } }

                         >
                       </WebView>
		)
	}
}