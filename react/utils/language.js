import storage from './storage'
import { DeviceEventEmitter } from 'react-native'
import config from '../config'

export function setLanguage(lang) {
  storage.save({
    key: 'language',
    data: lang
  }).then(() => {
    DeviceEventEmitter.emit('languageChangeEvent')
  })
}

export function getLanguage() {
  return new Promise((resolve, reject) => {
    storage.load({ key: 'language' }).then(lang => {
      resolve(lang)
    }).catch(() => {
      setLanguage(config.language.default)
      resolve(config.language.default)
    })
  })
}
