export const objToUrl = obj => {
    let arr = [];
    for(let i in obj){
        if (obj.hasOwnProperty(i)) {
            let value;
            if(obj[i] == null) {
                value = ''
            } else {
                value = obj[i]//encodeURIComponent(obj[i])
            }
            arr.push(encodeURIComponent(i) + "=" + value);
        }
    }
    return arr.join("&");
}
