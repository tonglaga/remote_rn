import storage from './storage'
import { DeviceEventEmitter } from 'react-native'

export function setCities(data) {
  storage.save({
    key: 'cities',
    data: data
  })
}

export function getCities() {
  return storage.load({key: 'cities'})
}

export function setLocal(city) {
  storage.save({
    key: 'local',
    data: city
  }).then(() => {
    DeviceEventEmitter.emit('localChange')
  })
}

export function getLocal() {
  return storage.load({key: 'local'})
}

export function searchLocal(cityName, regions) {
  for(let i = 0; i < regions.length; i++) {
    if(regions[i].name_cn === cityName) {
      return regions[i]
    }
  }
  return regions[0]
}