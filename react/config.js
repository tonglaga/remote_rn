
export default config = {
   // api_url: 'https://nda.etkqqzhdj.gov.cn:8443',
   // img_url: 'https://nda.etkqqzhdj.gov.cn:8443/files',
     api_url: 'http://192.168.31.102:8081',
    img_url: 'http://192.168.31.102:8081/files',
    token: {
        refreshSecond: 300
    },

    webViewUrl: {
        releaseMessage: 'http://123.57.230.213:9013/#/released_messages/create',
        updateMessage: 'http://123.57.230.213:9013/#/released_messages/update'
    },

    baiduMap: {
        ak: 'VouDQWdTLL8xbxXtC1wl1lDrTx0kyGUK',
        mcode: '9F:A4:FB:54:2B:C7:B2:C6:0C:D3:6F:C8:D0:4A:64:C3:E4:65:FE:C7;com.imnuapp'
    },

    language: {
        default: 0,
        list: {
            cn: 0,
            mn: 1
        }
    }
}