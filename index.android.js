// 创建本地应用程序加载程序的反作用元素的类型字段

//作为数字而不是符号，因为它尚未被聚合体定义。

//我们在定义符号之前导入应用程序渲染器，以便使用

//数字类型。否则，这将导致不变异常，

//因为纤维粘性不能识别根反应原生组分作为反应。

//元素，但作为对象
//
// See node_modules/react-native/Libraries/polyfills/babelHelpers.js
// :babelHelpers.createRawReactElement - 首先反应本地元素
// is created (super early - it's the app loader).
//
// See node_modules/react-native/Libraries/Renderer/ReactNativeFiber-dev.js
// and look for REACT_ELEMENT_TYPE definition - it's defined later when Symbol
// has been defined and type will not match.
//
// As an alternative solution we could stop using/polyfilling Symbols and
// replace with classpath string constants or some kind of a wrapper around
// that.

import 'react-native/Libraries/ReactNative/renderApplication';
//import { View, DeviceEventEmitter } from 'react-native'


// Android不提供符号
import 'es6-symbol/implement';
//import './react/index.native';
import React, { Component } from 'react';
import { AppRegistry,DeviceEventEmitter,View} from 'react-native';
//import Route from './react/route/index';
import RouterCN from './react/route/router-cn'
import RouterMN from './react/route/router-mn'
import config from './react/config'
import { getLanguage } from './react/utils/language'
class Root extends Component {
    constructor(props) {
        super(props)
        this.state = {
         // luyou:null,  
          language: config.language.default
        }
      }
    
      componentDidMount() {
        DeviceEventEmitter.addListener('languageChangeEvent', () => {
          this.getLanguageStatus()
        })
      //  this.getRoutes();
      }
    
      componentDidMounted() {
        this.getLanguageStatus()
      }
    
      getLanguageStatus() {
        getLanguage().then(lang => {
          //  console.warn(lang);
            
          this.setState({
            language: lang
          })
        })
      }
      getRoutes() {
        if(this.state.language === config.language.list.cn) {
          return (
            <RouterCN />
          )
        } else {
          return (
            <RouterMN />
          )
        }
      }
    //   getRoutes() {

    //     if(this.state.language === config.language.list.cn) {

    //         this.setState({
    //             luyou:<RouterCN />
    //         })
            
       
    //     } else {

    //         this.setState({
    //             luyou:<RouterMN />
    //         })
           
        
    //     }
    //   }
    render() {
       // let ly=this.state.luyou;
      //  console.warn(ly)
        return (
            this.getRoutes()
            
        );
        
    }
}
AppRegistry.registerComponent('App', () => Root);



