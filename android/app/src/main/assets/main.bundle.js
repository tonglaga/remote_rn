webpackJsonp([1,5],{

/***/ 343:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 343;


/***/ }),

/***/ 344:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(454);




if (__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=D:/wamp/www/mgl-keyboard/src/main.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
        this.space = ' ';
        this.wordList = [''];
        this.cursor = 0;
        this.key = '';
        this.value = '';
        this.assocUrl = "http://www.tengrinshar.com/api/typewriting/associational";
        this.relationUrl = "http://www.tengrinshar.com/api/typewriting/relation";
        this.page = 1;
        this.addKey = null;
        this.addVal = null;
        this.isMark = 0;
        this.isAuxiliary = 0;
        this.isSpace = new Array();
        this.isEnter = new Array();
    }
    AppComponent.prototype.ngOnInit = function () {
        var request = this.getRequest();
        if (typeof (request['content']) != "undefined") {
            var content = request['content'].replace(/@@@/g, "%");
            var words = decodeURI(content).split(" ");
            for (var i = 0; i < words.length; ++i) {
                this.value = words[i];
                this.add();
            }
        }
    };
    AppComponent.prototype.getRequest = function () {
        var url = location.search; //获取url中"?"符后的字串 
        var theRequest = new Object();
        if (url.indexOf("?") != -1) {
            var str = url.substr(1);
            var strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
            }
        }
        return theRequest;
    };
    AppComponent.prototype.showKeyboard = function () {
        $("#keyboard").slideDown('slow');
    };
    AppComponent.prototype.cursorleft = function (e) {
        this.moveCursor(-1);
    };
    AppComponent.prototype.cursorright = function (e) {
        this.moveCursor(1);
    };
    AppComponent.prototype.getKeyValue = function () {
        this.addKey = $("#add-key").val();
        this.addVal = $("#add-val").val();
        switch (this.addKey) {
            case "DELETE":
                this.delete();
                return false;
            case "SPACE":
                this.add();
                return false;
            case "SHIFT":
                return false;
            case "MARK":
                return false;
            case "CHANGE":
                this.value += '᠎';
                return false;
            case "ENTER":
                this.add();
                this.value = "\r\n";
                this.add();
                return false;
            case "NUMBER":
                return false;
            case "cagan":
                return false;
            case "shift-mark":
                return false;
            case "AUXILIARY":
                this.auxiliary();
                this.getCandidatesAjax();
                return false;
        }
        return true;
    };
    AppComponent.prototype.addWord = function () {
        this.isMark = 0;
        if (!this.getKeyValue())
            return;
        this.key += this.addKey;
        this.value += this.addVal;
        this.page = 1;
        this.getCandidatesAjax();
    };
    AppComponent.prototype.setCursor = function (e) {
        this.cursor = e;
    };
    AppComponent.prototype.moveCursor = function (position) {
        var cursor = this.cursor += position;
        this.cursor = cursor < 0 ? 0 : (cursor > (this.wordList.length) ? (this.wordList.length) : cursor);
    };
    AppComponent.prototype.add = function () {
        if (this.cursor == 0)
            this.cursor = 1;
        if ("" == this.value) {
            if (this.isMark == 0) {
                this.wordList.splice(this.cursor, 0, this.space);
                this.isSpace[this.cursor] = true;
                this.moveCursor(1);
            }
        }
        else if ("\r\n" == this.value) {
            this.wordList.splice(this.cursor, 0, this.value);
            this.isEnter[this.cursor] = true;
            this.moveCursor(1);
        }
        else {
            if (1 == this.isMark) {
                console.log(this.value);
                this.wordList.splice(this.cursor, 0, this.value);
                this.moveCursor(1);
            }
            else {
                this.wordList.splice(this.cursor, 0, this.value, this.space);
                this.moveCursor(2);
            }
        }
        this.key = "";
        this.value = "";
        this.hideCandidates();
    };
    AppComponent.prototype.delete = function () {
        if ("" == this.key || null == this.key) {
            if ((this.cursor - 2) > 0) {
                this.wordList.splice((this.cursor - 2), 2);
                this.moveCursor(-2);
            }
            else if ((this.cursor - 2) == 0) {
                this.wordList.splice((this.cursor - 1), 1);
                this.moveCursor(-1);
            }
            this.isSpace[this.cursor - 1] = false;
            this.isEnter[this.cursor - 1] = false;
        }
        else {
            this.key = this.key.substring(0, (this.key.length - 1));
            this.value = this.value.substring(0, (this.value.length - 1));
            if (this.key == "") {
                this.hideCandidates();
            }
            else {
                this.getCandidatesAjax();
            }
        }
    };
    AppComponent.prototype.showCandidates = function () {
        $("#controller-box").hide();
        $("#relation-box").hide();
        $("#candidate-box").show();
        return true;
    };
    AppComponent.prototype.hideCandidates = function () {
        $("#candidate-box").hide();
        //$("#relation-box").hide();
        $("#controller-box").show();
        return true;
    };
    AppComponent.prototype.setCandidate = function (data) {
        var length = data.length;
        if (length > 9)
            length = 9;
        if (data != null || data != '') {
            for (var i = 0; i < length; ++i) {
                this.appendCandidate((i + 1), data[i]);
            }
            this.appendCandidatePageControl(length);
            this.setChooseWordClick();
        }
    };
    AppComponent.prototype.appendCandidate = function (num, value) {
        var item = '<li>';
        if (typeof (value) == 'string') {
            item += '<span class="mongol choose-word" item-value="' + value + '">';
            item += num + '.';
            item += value;
        }
        else {
            item += '<span class="mongol choose-word" item-id="' + value.id + '" ';
            item += 'item-bqr_biclg="' + value.bqr_biclg + '"';
            item += 'item-table="' + value.table + '"';
            item += 'item-value="' + value.char_word + '"';
            item += '>';
            item += num + '.';
            item += value.char_word;
        }
        item += '</span></li>';
        $("#candidate-box ul").append(item);
    };
    AppComponent.prototype.appendCandidatePageControl = function (length) {
        var control = '<li><span>';
        control += '<div class="updown-btn" id="page-up"><i class="fa fa-angle-up"></i></div>';
        control += '<div class="updown-btn" id="page-down"><i class="fa fa-angle-down"></i></div>';
        control += '</span></li>';
        $("#candidate-box ul").append(control);
        updownbtn();
        this.setTurnPageClick(length);
    };
    AppComponent.prototype.setTurnPageClick = function (length) {
        var _this = this;
        $("#page-up").click(function () {
            if (_this.page > 1) {
                _this.page -= 1;
                _this.getCandidatesAjax();
            }
        });
        $("#page-down").click(function () {
            if (length == 9) {
                _this.page += 1;
                _this.getCandidatesAjax();
            }
        });
    };
    AppComponent.prototype.getCandidatesAjax = function () {
        var _this = this;
        $("#candidate-box ul").html("");
        this.appendCandidate(0, this.value);
        this.showCandidates();
        var key = this.key;
        if (key == "")
            key = 'allauxiliary';
        var url = this.assocUrl + "/" + key + "/" + this.page + "/" + this.isAuxiliary;
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'jsonp',
            success: function (response) {
                if (1 == response.code) {
                    _this.setCandidate(response.data);
                }
            }
        });
    };
    AppComponent.prototype.setChooseWordClick = function () {
        var _this = this;
        $(".choose-word").click(function () {
            var id = $(this).attr("item-id");
            var table = $(this).attr("item-table");
            var value = $(this).attr("item-value");
            if ('' == id || null == id || id == 'undefined') {
                if ('' == _this.value) {
                    _this.value = value;
                }
                if (_this.hideCandidates()) {
                    _this.add();
                }
            }
            else {
                _this.value = value;
                _this.add();
                _this.getRelationsAjax(id, table);
            }
            if (_this.isAuxiliary == 1) {
                _this.auxiliary();
            }
        });
    };
    AppComponent.prototype.getRelationsAjax = function (id, table) {
        var _this = this;
        var url = this.relationUrl + "/" + table + "/" + id;
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'jsonp',
            success: function (response) {
                if (1 == response.code) {
                    _this.setRelations(response.data);
                    _this.showRelations();
                }
            }
        });
    };
    AppComponent.prototype.showRelations = function () {
        $("#controller-box").hide();
        $("#candidate-box").hide();
        $("#relation-box").show();
        return true;
    };
    AppComponent.prototype.hideRelations = function () {
        //$("#candidate-box").hide();
        $("#relation-box").hide();
        $("#controller-box").show();
        return true;
    };
    AppComponent.prototype.setRelations = function (data) {
        $("#relation-box ul").html("");
        var length = data.length;
        if (length > 10)
            length = 10;
        if (data != null || data != '') {
            for (var i = 0; i < length; ++i) {
                this.appendRelation((i + 1), data[i]);
            }
            this.setRelationChooseWordClick();
        }
    };
    AppComponent.prototype.appendRelation = function (num, data) {
        var item = '<li>';
        item += '<span class="mongol choose-relation-word" item-value="' + data + '">';
        item += num + '.';
        item += data;
        item += '</span></li>';
        $("#relation-box ul").append(item);
    };
    AppComponent.prototype.setRelationChooseWordClick = function () {
        var _this = this;
        $(".choose-relation-word").click(function () {
            var value = $(this).attr("item-value");
            if (_this.hideRelations()) {
                _this.value = value;
                _this.add();
                _this.getRelationsAjax(0, value);
            }
        });
    };
    AppComponent.prototype.addNumber = function () {
        this.isMark = 1;
        if (!this.getKeyValue())
            return;
        this.add();
        this.value = this.addVal;
        this.add();
    };
    AppComponent.prototype.auxiliary = function () {
        if (1 == this.isAuxiliary) {
            this.isAuxiliary = 0;
            unAuxiliary();
        }
        else {
            this.isAuxiliary = 1;
            isAuxiliary();
        }
    };
    AppComponent.prototype.typeOf = function (value) {
        return typeof (value);
    };
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(612),
            styles: [__webpack_require__(610)]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=D:/wamp/www/mgl-keyboard/src/app.component.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(453);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=D:/wamp/www/mgl-keyboard/src/app.module.js.map

/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=D:/wamp/www/mgl-keyboard/src/environment.js.map

/***/ }),

/***/ 610:
/***/ (function(module, exports) {

module.exports = "html {}\r\nbody {\r\n    padding:0px;\r\n    margin:0px;\r\n}\r\n.editor-area{\r\n    border:1px solid #ccc;\r\n    border-radius:5px;\r\n    padding:5px;\r\n    -webkit-writing-mode: vertical-lr;\r\n        -ms-writing-mode: tb-lr;\r\n            writing-mode: vertical-lr;\r\n    width:100%;margin:0;\r\n    box-sizing: border-box;\r\n    height:300px;\r\n}\r\n.cursor{\r\n    border-top: 1px solid red;\r\n}\r\n.space-span {\r\n    margin-top: 5px;\r\n}\r\n.display-none {\r\n    display: none;\r\n}\r\n"

/***/ }),

/***/ 612:
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n\r\n  \t<div class='editor-area' id=\"editor-area\" (click)=\"showKeyboard()\">\r\n  \t\t<span *ngFor=\"let item of wordList;let i = index;\" class=\"editor-content\">\r\n  \t\t\t<br class=\"{{(typeOf(isEnter[i]) != 'undefined' && isEnter[i] == true) ? '':'display-none'}}\"><span class=\"{{i==cursor ? 'cursor':''}} {{(typeOf(isSpace[i]) != 'undefined' && isSpace[i] == true) ? 'space-span':''}}\" (click)=\"setCursor(i)\">{{item}}</span>\r\n  \t\t</span>\r\n    \t<span>\r\n    \t\t<span class=\"{{wordList.length==cursor ? 'cursor':''}}\" (click)=\"setCursor(wordList.length)\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>\r\n    \t</span>\r\n  \t</div>\r\n\t<input type=\"hidden\" (click)=\"cursorleft(i)\" id=\"cursor-left-btn\">\r\n\t<input type=\"hidden\" (click)=\"addword(i)\" id=\"add-btn\">\r\n\t<input type=\"hidden\" (click)=\"cursorright(e)\" id=\"cursor-right-btn\">\r\n\t<input type=\"hidden\" (click)=\"getRelation()\" id=\"get-relation\">\r\n\t<input type=\"hidden\" (click)=\"addWord()\" id=\"add-word\">\r\n\t<input type=\"hidden\" (click)=\"addNumber()\" id=\"add-num\">\r\n\t<input type=\"hidden\" id=\"add-key\">\r\n\t<input type=\"hidden\" id=\"add-val\">\r\n</div>"

/***/ }),

/***/ 628:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(344);


/***/ })

},[628]);
//# sourceMappingURL=main.bundle.map