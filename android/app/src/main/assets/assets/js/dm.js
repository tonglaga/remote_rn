



  
  



if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.indexOf(str) == 0;
  };
}

if (typeof String.prototype.endsWith != 'function') {
	String.prototype.endsWith = function(suffix) {
	    return this.indexOf(suffix, this.length - suffix.length) !== -1;
	};
}

var StringBuffer = function(string) {
	this.buffer = [];

	this.append = function(string) {
		this.buffer.push(string);
		return this;
	};
	 
	this.toString = function() {
		return this.buffer.join('');
	};

	this.length = function() {
		return this.buffer.length;
	};

	this.charAt = function(i) {
		return this.buffer[i];
	};

	if (string) {
		for (i=0; i < string.length; i++) {
			this.append(string.charAt(i));
		}
	}
};

var  EXCLAM = '\u0021';
var  M_EXCLAM_ISOL = '\uE81C';
var  M_EXCLAM_QUESTION_ISOL = '\uE81B';

var  COMMA = '\u002C';
var  M_COMMA_ISOL1 = '\uE81F';

var  QUESTION = '\u003F';
var  M_QUESTION_ISOL = '\uE81D';
var  M_QUESTION_EXCLAM_ISOL = '\uE81A';

var  COLON = '\u003A';

var  SEMICOLON = '\u003B';
var  M_SEMICOLON_ISOL = '\uE81E';

var  M_BIRGA = '\u1800';
var  M_BIRGA1 = '\uEA24';
var  M_BIRGA2 = '\uEA25';
var  M_BIRGA3 = '\uEA26';
// var  M_BIRGA4 = '\uEA27';

var  M_ELLIPSIS = '\u1801';
var  M_ELLIPSIS_ISOL = '\uE801';

var  M_COMMA = '\u1802';
var  M_COMMA_ISOL = '\uE802';

var  M_FULL_STOP = '\u1803';
var  M_FULL_STOP_ISOL = '\uE803';

var  M_COLON = '\u1804';
var  M_COLON_ISOL = '\uE804';

var  M_FOUR_DOTS = '\u1805';
var  M_FOUR_DOTS_ISOL = '\uE805';

var  M_TODO_HYPHEN = '\u1806';
var  M_TODO_HYPHEN_ISOL = '\uE806';

var  M_SIBE_SBM = '\u1807';
var  M_SIBE_SBM_ISOL = '\uE807';

var  M_MANCHU_COMMA = '\u1808';
var  M_MANCHU_COMMA_ISOL = '\u1808';

var  M_MANCHU_FULL_STOP = '\u1809';
var  M_MANCHU_FULL_STOP_ISOL = '\uE809';

var  M_NIRUGU = '\u180A';
var  M_NIRUGU_ISOL = '\uE80A';

var  M_FVS1 = '\u180B';
var  M_FVS1_ISOL = '\u180B';

var  M_FVS2 = '\u180C';
var  M_FVS2_ISOL = '\u180C';

var  M_FVS3 = '\u180D';
var  M_FVS3_ISOL = '\u180D';

var  M_MVS = '\u180E';
var  M_MVS_ISOL = '\uE80E';

var  M_DIGIT_ZERO = '\u1810';
var  M_DIGIT_ZERO_ISOL = '\uE810';

var  M_DIGIT_ONE = '\u1811';
var  M_DIGIT_ONE_ISOL = '\uE811';

var  M_DIGIT_TWO = '\u1812';
var  M_DIGIT_TWO_ISOL = '\uE812';

var  M_DIGIT_THREE = '\u1813';
var  M_DIGIT_THREE_ISOL = '\uE813';

var  M_DIGIT_FOUR = '\u1814';
var  M_DIGIT_FOUR_ISOL = '\uE814';

var  M_DIGIT_FIVE = '\u1815';
var  M_DIGIT_FIVE_ISOL = '\uE815';

var  M_DIGIT_SIX = '\u1816';
var  M_DIGIT_SIX_ISOL = '\uE816';

var  M_DIGIT_SEVEN = '\u1817';
var  M_DIGIT_SEVEN_ISOL = '\uE817';

var  M_DIGIT_EIGHT = '\u1818';
var  M_DIGIT_EIGHT_ISOL = '\uE818';

var  M_DIGIT_NINE = '\u1819';
var  M_DIGIT_NINE_ISOL = '\uE819';

var  ML_A = '\u1820';
var  ML_A_ISOL = '\uE820';
var  ML_A_ISOL1 = '\uE821';
var  ML_A_INIT = '\uE822';
var  ML_A_INIT1 = '\uEA21';
var  ML_A_MEDI = '\uE823';
var  ML_A_MEDI1 = '\uE824';
var  ML_A_FINA = '\uE825';
var  ML_A_FINA1 = '\uE826';
var  ML_A_FINA2 = '\uE827';

var  ML_E = '\u1821';
var  ML_E_ISOL = '\uE828';
var  ML_E_ISOL1 = '\uE829';
var  ML_E_INIT = '\uE82A';
var  ML_E_INIT1 = '\uE82B';
var  ML_E_MEDI = '\uE82C';
var  ML_E_FINA = '\uE82D';
var  ML_E_FINA1 = '\uE82E';
var  ML_E_FINA2 = '\uE82F';

var  ML_I = '\u1822';
var  ML_I_ISOL = '\uE830';
var  ML_I_ISOL1 = '\uE831';
var  ML_I_INIT = '\uE832';
var  ML_I_INIT1 = '\uE833';
var  ML_I_MEDI = '\uE834';
var  ML_I_MEDI1 = '\uE835';
var  ML_I_MEDI2 = '\uE836';
var  ML_I_FINA = '\uE837';

var  ML_O = '\u1823';
var  ML_O_ISOL = '\uE838';
var  ML_O_ISOL1 = '\uE839';
var  ML_O_INIT = '\uE83A';
var  ML_O_INIT1 = '\uE83B';
var  ML_O_MEDI = '\uE83C';
var  ML_O_MEDI1 = '\uE83D';
var  ML_O_FINA = '\uE83E';
var  ML_O_FINA1 = '\uE83F';

var  ML_U = '\u1824';
var  ML_U_ISOL = '\uE840';
var  ML_U_ISOL1 = '\uE841';
var  ML_U_INIT = '\uE842';
var  ML_U_INIT1 = '\uE843';
var  ML_U_MEDI = '\uE844';
var  ML_U_MEDI1 = '\uE845';
var  ML_U_FINA = '\uE846';
var  ML_U_FINA1 = '\uE847';

var  ML_OE = '\u1825';
var  ML_OE_ISOL = '\uE848';
var  ML_OE_ISOL1 = '\uE849';
var  ML_OE_INIT = '\uE84A';
var  ML_OE_INIT1 = '\uEA20';
var  ML_OE_MEDI = '\uE84B';
var  ML_OE_MEDI1 = '\uE84C';
var  ML_OE_MEDI2 = '\uE84D';
var  ML_OE_FINA = '\uE84E';
var  ML_OE_FINA1 = '\uE84F';

var  ML_UE = '\u1826';
var  ML_UE_ISOL = '\uE850';
var  ML_UE_ISOL1 = '\uE85F';
var  ML_UE_ISOL2 = '\uE851';
var  ML_UE_INIT = '\uE852';
var  ML_UE_INIT1 = '\uE85E';
var  ML_UE_MEDI = '\uE853';
var  ML_UE_MEDI1 = '\uE854';
var  ML_UE_MEDI2 = '\uE855';
var  ML_UE_FINA = '\uE856';
var  ML_UE_FINA1 = '\uE857';

var  ML_EE = '\u1827';
var  ML_EE_ISOL = '\uE858';
var  ML_EE_ISOL1 = '\uE859';
var  ML_EE_INIT = '\uE85A';
var  ML_EE_INIT1 = '\uE85B';
var  ML_EE_MEDI = '\uE85C';
var  ML_EE_FINA = '\uE85D';

var  ML_N = '\u1828';
var  ML_N_ISOL = '\uE860';
var  ML_N_ISOL1 = '\uE861';
var  ML_N_INIT = '\uE862';
var  ML_N_INIT1 = '\uE863';
var  ML_N_MEDI = '\uE864';
var  ML_N_MEDI1 = '\uE865';
var  ML_N_FINA = '\uE866';
var  ML_N_FINA1 = '\uE867';
var  ML_N_FINA2 = '\uEA23';

var  ML_NG = '\u1829';
var  ML_NG_ISOL = '\uE868';
var  ML_NG_INIT = '\uE869';
var  ML_NG_MEDI = '\uE86A';
var  ML_NG_FINA = '\uE86B';
var  ML_NGN_MEDI = '\uE9E3';
var  ML_NGH_MEDI = '\uE86C';
var  ML_NGH_FINA = '\uE9E4';
var  ML_NGG_MEDI = '\uE86D';
var  ML_NGG_FINA = '\uE9E5';
var  ML_NGM_MEDI = '\uE86E';
var  ML_NGL_MEDI = '\uE86F';

var  ML_B = '\u182A';
var  ML_B_ISOL = '\uE870';
var  ML_B_INIT = '\uE871';
var  ML_B_MEDI = '\uE872';
var  ML_B_FINA = '\uE873';
var  ML_B_FINA1 = '\uE9CE';
var  ML_BA_ISOL = '\uE874';
var  ML_BA_INIT = '\uE875';
var  ML_BA_MEDI = '\uE876';
var  ML_BA_FINA = '\uE877';
var  ML_BE_ISOL = '\uE878';
var  ML_BE_INIT = '\uE879';
var  ML_BE_MEDI = '\uE87A';
var  ML_BE_FINA = '\uE87B';
var  ML_BI_ISOL = '\uE87C';
var  ML_BI_INIT = '\uE87D';
var  ML_BI_MEDI = '\uE87E';
var  ML_BI_FINA = '\uE87F';
var  ML_BO_ISOL = '\uE880';
var  ML_BO_INIT = '\uE881';
var  ML_BO_MEDI = '\uE882';
var  ML_BO_FINA = '\uE883';
var  ML_BU_ISOL = '\uE884';
var  ML_BU_INIT = '\uE885';
var  ML_BU_MEDI = '\uE886';
var  ML_BU_FINA = '\uE887';
var  ML_BOE_ISOL = '\uE888';
var  ML_BOE_INIT = '\uE889';
var  ML_BOE_MEDI = '\uE88A';
var  ML_BOE_MEDI1 = '\uE9D1';
var  ML_BOE_FINA = '\uE88B';
var  ML_BOE_FINA1 = '\uE88C';
var  ML_BUE_ISOL = '\uE88D';
var  ML_BUE_INIT = '\uE88E';
var  ML_BUE_MEDI = '\uE88F';
var  ML_BUE_MEDI1 = '\uE9D2';
var  ML_BUE_FINA = '\uE890';
var  ML_BUE_FINA1 = '\uE891';
var  ML_BEE_ISOL = '\uE892';
var  ML_BEE_INIT = '\uE893';
var  ML_BEE_MEDI = '\uE894';
var  ML_BEE_FINA = '\uE895';
var  ML_BN_MEDI = '\uE9F0';
var  ML_BH_MEDI = '\uE9E6';
var  ML_BG_MEDI = '\uE9E7';
var  ML_BM_MEDI = '\uE896';
var  ML_BL_MEDI = '\uE897';

var  ML_P = '\u182B';
var  ML_P_ISOL = '\uE898';
var  ML_P_INIT = '\uE899';
var  ML_P_MEDI = '\uE89A';
var  ML_P_FINA = '\uE89B';
var  ML_PA_ISOL = '\uE89C';
var  ML_PA_INIT = '\uE89D';
var  ML_PA_MEDI = '\uE89E';
var  ML_PA_FINA = '\uE89F';
var  ML_PE_ISOL = '\uE8A0';
var  ML_PE_INIT = '\uE8A1';
var  ML_PE_MEDI = '\uE8A2';
var  ML_PE_FINA = '\uE8A3';
var  ML_PI_ISOL = '\uE8A4';
var  ML_PI_INIT = '\uE8A5';
var  ML_PI_MEDI = '\uE8A6';
var  ML_PI_FINA = '\uE8A7';
var  ML_PO_ISOL = '\uE8A8';
var  ML_PO_INIT = '\uE8A9';
var  ML_PO_MEDI = '\uE8AA';
var  ML_PO_FINA = '\uE8AB';
var  ML_PU_ISOL = '\uE8AC';
var  ML_PU_INIT = '\uE8AD';
var  ML_PU_MEDI = '\uE8AE';
var  ML_PU_FINA = '\uE8AF';
var  ML_POE_ISOL = '\uE8B0';
var  ML_POE_INIT = '\uE8B1';
var  ML_POE_MEDI = '\uE8B2';
var  ML_POE_MEDI1 = '\uE9D3';
var  ML_POE_FINA = '\uE8B3';
var  ML_POE_FINA1 = '\uE8B4';
var  ML_PUE_ISOL = '\uE8B5';
var  ML_PUE_INIT = '\uE8B6';
var  ML_PUE_MEDI = '\uE8B7';
var  ML_PUE_MEDI1 = '\uE9D4';
var  ML_PUE_FINA = '\uE8B8';
var  ML_PUE_FINA1 = '\uE8B9';
var  ML_PEE_ISOL = '\uE8BA';
var  ML_PEE_INIT = '\uE8BB';
var  ML_PEE_MEDI = '\uE8BC';
var  ML_PEE_FINA = '\uE8BD';
var  ML_PN_MEDI = '\uE9F1';
var  ML_PH_MEDI = '\uE9E8';
var  ML_PG_MEDI = '\uE9E9';
var  ML_PM_MEDI = '\uE8BE';
var  ML_PL_MEDI = '\uE8BF';

var  ML_H = '\u182C';
var  ML_H_ISOL = '\uE8C0';
var  ML_H_ISOL1 = '\uE8C1';
var  ML_H_ISOL2 = '\uEA00';
var  ML_H_ISOL3 = '\uEA02';
var  ML_H_INIT = '\uE8C2';
var  ML_H_INIT1 = '\uE8C3';
var  ML_H_INIT2 = '\uEA01';
var  ML_H_INIT3 = '\uEA03';
var  ML_H_MEDI = '\uE8C4';
var  ML_H_MEDI1 = '\uE8C5';
var  ML_H_MEDI2 = '\uE8C6';
var  ML_H_MEDI3 = '\uEA04';
var  ML_H_FINA = '\uE8C7';
var  ML_H_FINA1 = '\uE8C8';
var  ML_H_FINA2 = '\uE8C9';
var  ML_HE_ISOL = '\uE8CA';
var  ML_HE_ISOL1 = '\uEA05';
var  ML_HE_INIT = '\uE8CB';
var  ML_HE_INIT1 = '\uEA06';
var  ML_HE_MEDI = '\uE8CC';
var  ML_HE_MEDI1 = '\uEA07';
var  ML_HE_FINA = '\uE8CD';
var  ML_HE_FINA1 = '\uEA08';
var  ML_HI_ISOL = '\uE8CE';
var  ML_HI_ISOL1 = '\uEA09';
var  ML_HI_INIT = '\uE8CF';
var  ML_HI_INIT1 = '\uEA0A';
var  ML_HI_MEDI = '\uE8D0';
var  ML_HI_MEDI1 = '\uEA0B';
var  ML_HI_FINA = '\uE8D1';
var  ML_HI_FINA1 = '\uEA0C';
var  ML_HOE_ISOL = '\uE8D2';
var  ML_HOE_ISOL1 = '\uEA0D';
var  ML_HOE_INIT = '\uE8D3';
var  ML_HOE_INIT1 = '\uEA0E';
var  ML_HOE_MEDI = '\uE8D4';
var  ML_HOE_MEDI1 = '\uE9D5';
var  ML_HOE_MEDI2 = '\uEA0F';
var  ML_HOE_FINA = '\uE8D5';
var  ML_HOE_FINA1 = '\uE8D6';
var  ML_HOE_FINA2 = '\uEA10';
var  ML_HOE_FINA3 = '\uEA11';
var  ML_HUE_ISOL = '\uE8D7';
var  ML_HUE_ISOL1 = '\uEA12';
var  ML_HUE_INIT = '\uE8D8';
var  ML_HUE_INIT1 = '\uEA13';
var  ML_HUE_MEDI = '\uE8D9';
var  ML_HUE_MEDI1 = '\uE9D6';
var  ML_HUE_MEDI2 = '\uEA14';
var  ML_HUE_FINA = '\uE8DA';
var  ML_HUE_FINA1 = '\uE8DB';
var  ML_HUE_FINA2 = '\uEA15';
var  ML_HUE_FINA3 = '\uEA16';
var  ML_HEE_ISOL = '\uE8DC';
var  ML_HEE_ISOL1 = '\uEA17';
var  ML_HEE_INIT = '\uE8DD';
var  ML_HEE_INIT1 = '\uEA18';
var  ML_HEE_MEDI = '\uE8DE';
var  ML_HEE_MEDI1 = '\uEA19';
var  ML_HEE_FINA = '\uE8DF';
var  ML_HEE_FINA1 = '\uEA1A';

var  ML_G = '\u182D';
var  ML_G_ISOL = '\uE8E0';
var  ML_G_ISOL1 = '\uE8E1';
var  ML_G_ISOL2 = '\uEA1B';
var  ML_G_ISOL3 = '\uEA1C';
var  ML_G_INIT = '\uE8E2';
var  ML_G_INIT1 = '\uE8E3';
var  ML_G_INIT2 = '\uEA1D';
var  ML_G_INIT3 = '\uEA1E';
var  ML_G_MEDI = '\uE8E4';
var  ML_G_MEDI1 = '\uE8E5';
var  ML_G_MEDI2 = '\uE8E6';
var  ML_G_MEDI3 = '\uEA1F';
//	var  ML_G_MEDI3 = '\uEA04';
var  ML_G_FINA = '\uE8E7';
var  ML_G_FINA1 = '\uE8E8';
var  ML_G_FINA2 = '\uE8E9';
var  ML_GE_ISOL = '\uE8EA';
var  ML_GE_ISOL1 = '\uEA05';
var  ML_GE_INIT = '\uE8EB';
var  ML_GE_INIT1 = '\uEA06';
var  ML_GE_MEDI = '\uE8EC';
var  ML_GE_MEDI1 = '\uEA07';
var  ML_GE_FINA = '\uE8ED';
var  ML_GE_FINA1 = '\uEA08';
var  ML_GI_ISOL = '\uE8EE';
var  ML_GI_ISOL1 = '\uEA09';
var  ML_GI_INIT = '\uE8EF';
var  ML_GI_INIT1 = '\uEA0A';
var  ML_GI_MEDI = '\uE8F0';
var  ML_GI_MEDI1 = '\uEA0B';
var  ML_GI_FINA = '\uE8F1';
var  ML_GI_FINA1 = '\uEA0C';
var  ML_GOE_ISOL = '\uE8F2';
var  ML_GOE_ISOL1 = '\uEA0D';
var  ML_GOE_INIT = '\uE8F3';
var  ML_GOE_INIT1 = '\uEA0E';
var  ML_GOE_MEDI = '\uE8F4';
var  ML_GOE_MEDI1 = '\uE9D7';
var  ML_GOE_MEDI2 = '\uEA0F';
var  ML_GOE_FINA = '\uE8F5';
var  ML_GOE_FINA1 = '\uE8F6';
var  ML_GOE_FINA2 = '\uEA10';
var  ML_GOE_FINA3 = '\uEA11';
var  ML_GUE_ISOL = '\uE8F7';
var  ML_GUE_ISOL1 = '\uEA12';
var  ML_GUE_INIT = '\uE8F8';
var  ML_GUE_INIT1 = '\uEA13';
var  ML_GUE_MEDI = '\uE8F9';
var  ML_GUE_MEDI1 = '\uE9D8';
var  ML_GUE_MEDI2 = '\uEA14';
var  ML_GUE_FINA = '\uE8FA';
var  ML_GUE_FINA1 = '\uE8FB';
var  ML_GUE_FINA2 = '\uEA15';
var  ML_GUE_FINA3 = '\uEA16';
var  ML_GEE_ISOL = '\uE8FC';
var  ML_GEE_INIT = '\uE8FD';
var  ML_GEE_MEDI = '\uE8FE';
var  ML_GEE_FINA = '\uE8FF';
var  ML_GN_MEDI = '\uE9F2';
var  ML_GM_MEDI = '\uE900';
var  ML_GL_MEDI = '\uE901';

var  ML_M = '\u182E';
var  ML_M_ISOL = '\uE902';
var  ML_M_INIT = '\uE903';
var  ML_M_MEDI = '\uE904';
var  ML_M_FINA = '\uE905';
var  ML_MM_MEDI = '\uE9E0';
var  ML_ML_MEDI = '\uE9E1';

var  ML_L = '\u182F';
var  ML_L_ISOL = '\uE906';
var  ML_L_INIT = '\uE907';
var  ML_L_MEDI = '\uE908';
var  ML_L_FINA = '\uE909';
var  ML_LL_MEDI = '\uE9E2';

var  ML_S = '\u1830';
var  ML_S_ISOL = '\uE90A';
var  ML_S_INIT = '\uE90B';
var  ML_S_MEDI = '\uE90C';
var  ML_S_FINA = '\uE90D';
var  ML_S_FINA1 = '\uE9CC';
var  ML_S_FINA2 = '\uE9CD';

var  ML_SH = '\u1831';
var  ML_SH_ISOL = '\uE90E';
var  ML_SH_INIT = '\uE90F';
var  ML_SH_MEDI = '\uE910';
var  ML_SH_FINA = '\uE911';

var  ML_T = '\u1832';
var  ML_T_ISOL = '\uE912';
var  ML_T_ISOL1 = '\uE913';
var  ML_T_INIT = '\uE914';
var  ML_T_MEDI = '\uE915';
var  ML_T_MEDI1 = '\uE916';
var  ML_T_MEDI2 = '\uE917';
var  ML_T_FINA = '\uE918';

var  ML_D = '\u1833';
var  ML_D_ISOL = '\uE919';
var  ML_D_INIT = '\uE91A';
var  ML_D_INIT1 = '\uE91B';
var  ML_D_MEDI = '\uE91C';
var  ML_D_MEDI1 = '\uE91D';
var  ML_D_FINA = '\uE91E';
var  ML_D_FINA1 = '\uE91F';

var  ML_CH = '\u1834';
var  ML_CH_ISOL = '\uE920';
var  ML_CH_INIT = '\uE921';
var  ML_CH_MEDI = '\uE922';
var  ML_CH_FINA = '\uE923';

var  ML_J = '\u1835';
var  ML_J_ISOL = '\uE924';
var  ML_J_ISOL1 = '\uE925';
var  ML_J_INIT = '\uE926';
var  ML_J_MEDI = '\uE927';
var  ML_J_FINA = '\uE928';

var  ML_Y = '\u1836';
var  ML_Y_ISOL = '\uE929';
var  ML_Y_INIT = '\uE92A';
var  ML_Y_INIT1 = '\uE92B';
var  ML_Y_MEDI = '\uE92C';
var  ML_Y_MEDI1 = '\uEA22';
var  ML_Y_FINA = '\uE92D';

var  ML_R = '\u1837';
var  ML_R_ISOL = '\uE92E';
var  ML_R_INIT = '\uE92F';
var  ML_R_MEDI = '\uE930';
var  ML_R_FINA = '\uE931';

var  ML_W = '\u1838';
var  ML_W_ISOL = '\uE932';
var  ML_W_INIT = '\uE933';
var  ML_W_INIT1 = '\uE934';
var  ML_W_MEDI = '\uE935';
var  ML_W_FINA = '\uE936';
var  ML_W_FINA1 = '\uE937';

var  ML_F = '\u1839';
var  ML_F_ISOL = '\uE938';
var  ML_F_INIT = '\uE939';
var  ML_F_MEDI = '\uE93A';
var  ML_F_FINA = '\uE93B';
var  ML_FA_ISOL = '\uE93C';
var  ML_FA_INIT = '\uE93D';
var  ML_FA_MEDI = '\uE93E';
var  ML_FA_FINA = '\uE93F';
var  ML_FE_ISOL = '\uE940';
var  ML_FE_INIT = '\uE941';
var  ML_FE_MEDI = '\uE942';
var  ML_FE_FINA = '\uE943';
var  ML_FI_ISOL = '\uE944';
var  ML_FI_INIT = '\uE945';
var  ML_FI_MEDI = '\uE946';
var  ML_FI_FINA = '\uE947';
var  ML_FO_ISOL = '\uE948';
var  ML_FO_INIT = '\uE949';
var  ML_FO_MEDI = '\uE94A';
var  ML_FO_FINA = '\uE94B';
var  ML_FU_ISOL = '\uE94C';
var  ML_FU_INIT = '\uE94D';
var  ML_FU_MEDI = '\uE94E';
var  ML_FU_FINA = '\uE94F';
var  ML_FOE_ISOL = '\uE950';
var  ML_FOE_INIT = '\uE951';
var  ML_FOE_MEDI = '\uE952';
var  ML_FOE_MEDI1 = '\uE9D9';
var  ML_FOE_FINA = '\uE953';
var  ML_FOE_FINA1 = '\uE954';
var  ML_FUE_ISOL = '\uE955';
var  ML_FUE_INIT = '\uE956';
var  ML_FUE_MEDI = '\uE957';
var  ML_FUE_MEDI1 = '\uE9DA';
var  ML_FUE_FINA = '\uE958';
var  ML_FUE_FINA1 = '\uE959';
var  ML_FEE_ISOL = '\uE95A';
var  ML_FEE_INIT = '\uE95B';
var  ML_FEE_MEDI = '\uE95C';
var  ML_FEE_FINA = '\uE95D';
var  ML_FN_MEDI = '\uE9F3';
var  ML_FH_MEDI = '\uE9EA';
var  ML_FG_MEDI = '\uE9EB';
var  ML_FM_MEDI = '\uE95E';
var  ML_FL_MEDI = '\uE95F';

var  ML_K = '\u183A';
var  ML_K_ISOL = '\uE960';
var  ML_K_INIT = '\uE961';
var  ML_K_MEDI = '\uE962';
var  ML_K_FINA = '\uE963';
var  ML_KA_ISOL = '\uE964';
var  ML_KA_INIT = '\uE965';
var  ML_KA_MEDI = '\uE966';
var  ML_KA_FINA = '\uE967';
var  ML_KE_ISOL = '\uE968';
var  ML_KE_INIT = '\uE969';
var  ML_KE_MEDI = '\uE96A';
var  ML_KE_FINA = '\uE96B';
var  ML_KI_ISOL = '\uE96C';
var  ML_KI_INIT = '\uE96D';
var  ML_KI_MEDI = '\uE96E';
var  ML_KI_FINA = '\uE96F';
var  ML_KO_ISOL = '\uE970';
var  ML_KO_INIT = '\uE971';
var  ML_KO_MEDI = '\uE972';
var  ML_KO_FINA = '\uE973';
var  ML_KU_ISOL = '\uE974';
var  ML_KU_INIT = '\uE975';
var  ML_KU_MEDI = '\uE976';
var  ML_KU_FINA = '\uE977';
var  ML_KOE_ISOL = '\uE978';
var  ML_KOE_INIT = '\uE979';
var  ML_KOE_MEDI = '\uE97A';
var  ML_KOE_MEDI1 = '\uE9DB';
var  ML_KOE_FINA = '\uE97B';
var  ML_KOE_FINA1 = '\uE97C';
var  ML_KUE_ISOL = '\uE97D';
var  ML_KUE_INIT = '\uE97E';
var  ML_KUE_MEDI = '\uE97F';
var  ML_KUE_MEDI1 = '\uE9DC';
var  ML_KUE_FINA = '\uE980';
var  ML_KUE_FINA1 = '\uE981';
var  ML_KEE_ISOL = '\uE982';
var  ML_KEE_INIT = '\uE983';
var  ML_KEE_MEDI = '\uE984';
var  ML_KEE_FINA = '\uE985';
var  ML_KN_MEDI = '\uE9F4';
var  ML_KH_MEDI_ = '\uE9EC';
var  ML_KG_MEDI = '\uE9ED';
var  ML_KM_MEDI = '\uE986';
var  ML_KL_MEDI = '\uE987';

var  ML_KH = '\u183B';
var  ML_KH_ISOL = '\uE988';
var  ML_KH_INIT = '\uE989';
var  ML_KH_MEDI = '\uE98A';
var  ML_KH_FINA = '\uE98B';
var  ML_KHA_ISOL = '\uE98C';
var  ML_KHA_INIT = '\uE98D';
var  ML_KHA_MEDI = '\uE98E';
var  ML_KHA_FINA = '\uE98F';
var  ML_KHE_ISOL = '\uE990';
var  ML_KHE_INIT = '\uE991';
var  ML_KHE_MEDI = '\uE992';
var  ML_KHE_FINA = '\uE993';
var  ML_KHI_ISOL = '\uE994';
var  ML_KHI_INIT = '\uE995';
var  ML_KHI_MEDI = '\uE996';
var  ML_KHI_FINA = '\uE997';
var  ML_KHO_ISOL = '\uE998';
var  ML_KHO_INIT = '\uE999';
var  ML_KHO_MEDI = '\uE99A';
var  ML_KHO_FINA = '\uE99B';
var  ML_KHU_ISOL = '\uE99C';
var  ML_KHU_INIT = '\uE99D';
var  ML_KHU_MEDI = '\uE99E';
var  ML_KHU_FINA = '\uE99F';
var  ML_KHOE_ISOL = '\uE9A0';
var  ML_KHOE_INIT = '\uE9A1';
var  ML_KHOE_MEDI = '\uE9A2';
var  ML_KHOE_MEDI1 = '\uE9DD';
var  ML_KHOE_FINA = '\uE9A3';
var  ML_KHOE_FINA1 = '\uE9A4';
var  ML_KHUE_ISOL = '\uE9A5';
var  ML_KHUE_INIT = '\uE9A6';
var  ML_KHUE_MEDI = '\uE9A7';
var  ML_KHUE_MEDI1 = '\uE9DE';
var  ML_KHUE_FINA = '\uE9A8';
var  ML_KHUE_FINA1 = '\uE9A9';
var  ML_KHEE_ISOL = '\uE9AA';
var  ML_KHEE_INIT = '\uE9AB';
var  ML_KHEE_MEDI = '\uE9AC';
var  ML_KHEE_FINA = '\uE9AD';
var  ML_KHN_MEDI = '\uE9F5';
var  ML_KHH_MEDI = '\uE9EE';
var  ML_KHG_MEDI = '\uE9EF';
var  ML_KHM_MEDI = '\uE9AE';
var  ML_KHL_MEDI = '\uE9AF';

var  ML_C = '\u183C';
var  ML_C_ISOL = '\uE9B0';
var  ML_C_INIT = '\uE9B1';
var  ML_C_MEDI = '\uE9B2';
var  ML_C_FINA = '\uE9B3';

var  ML_Z = '\u183D';
var  ML_Z_ISOL = '\uE9B4';
var  ML_Z_INIT = '\uE9B5';
var  ML_Z_MEDI = '\uE9B6';
var  ML_Z_FINA = '\uE9B7';

var  ML_HH = '\u183E';
var  ML_HH_ISOL = '\uE9B8';
var  ML_HH_INIT = '\uE9B9';
var  ML_HH_MEDI = '\uE9BA';
var  ML_HH_FINA = '\uE9BB';

var  ML_RH = '\u183F';
var  ML_RH_ISOL = '\uE9BC';
var  ML_RH_INIT = '\uE9BD';
var  ML_RH_MEDI = '\uE9BE';
var  ML_RH_FINA = '\uE9BF';

var  ML_LH = '\u1840';
var  ML_LH_ISOL = '\uE9C0';
var  ML_LH_INIT = '\uE9C1';
var  ML_LH_MEDI = '\uE9C2';
var  ML_LH_FINA = '\uE9C3';

var  ML_ZHI = '\u1841';
var  ML_ZHI_ISOL = '\uE9C4';
var  ML_ZHI_INIT = '\uE9C5';
var  ML_ZHI_MEDI = '\uE9C6';
var  ML_ZHI_FINA = '\uE9C7';

var  ML_CHI = '\u1842';
var  ML_CHI_ISOL = '\uE9C8';
var  ML_CHI_INIT = '\uE9C9';
var  ML_CHI_MEDI = '\uE9CA';
var  ML_CHI_FINA = '\uE9CB';

var  ML_T_LVS = '\u1843';
var  ML_T_LVS_ISOL = '\uEB00';
var  ML_T_LVS_INIT = '\uEB01';
var  ML_T_LVS_MEDI = '\uEB02';
var  ML_T_LVS_FINA = '\uEB03';

var  ML_T_LONG_A_ISOL = '\uEAFD';
var  ML_T_LONG_A_INIT = '\uEAFE';
var  ML_T_LONG_A_MEDI = '\uEAFF';
var  ML_T_LONG_A_FINA = '\uEB55';

var  ML_T_E = '\u1844';
var  ML_T_E_ISOL = '\uEB04';
var  ML_T_E_INIT = '\uEB05';
var  ML_T_E_MEDI = '\uEB06';
var  ML_T_E_MEDI1 = '\uEB07';
var  ML_T_E_FINA = '\uEB08';

var  ML_T_I = '\u1845';
var  ML_T_I_ISOL = '\uEB09';
var  ML_T_I_INIT = '\uEB0A';
var  ML_T_I_MEDI = '\uEB0B';
var  ML_T_I_MEDI1 = '\uEB0C';
var  ML_T_I_FINA = '\uEB0D';

var  ML_T_O = '\u1846';
var  ML_T_O_ISOL = '\uEB0E';
var  ML_T_O_INIT = '\uEB0F';
var  ML_T_O_MEDI = '\uEB10';
var  ML_T_O_MEDI1 = '\uEB11';
var  ML_T_O_FINA = '\uEB12';
var  ML_T_LONG_O_ISOL = '\uEB57';
var  ML_T_LONG_O_INIT = '\uEB56';
var  ML_T_LONG_O_MEDI = '\uEBFA';
var  ML_T_LONG_O_FINA = '\uEBFB';

var  ML_T_U = '\u1847';
var  ML_T_U_ISOL = '\uEB13';
var  ML_T_U_ISOL1 = '\uEB14';
var  ML_T_U_INIT = '\uEB15';
var  ML_T_U_MEDI = '\uEB16';
var  ML_T_U_MEDI1 = '\uEB17';
var  ML_T_U_MEDI2 = '\uEB18';
var  ML_T_U_FINA = '\uEB19';
var  ML_T_U_FINA1 = '\uEB1A';

var  ML_T_OE = '\u1848';
var  ML_T_OE_ISOL = '\uEB1B';
var  ML_T_OE_INIT = '\uEB1C';
var  ML_T_OE_MEDI = '\uEB1D';
var  ML_T_OE_MEDI1 = '\uEB1E';
var  ML_T_OE_FINA = '\uEB1F';
var  ML_T_LONG_OE_ISOL = '\uEBFC';
var  ML_T_LONG_OE_INIT = '\uEBFD';
var  ML_T_LONG_OE_MEDI = '\uEBFE';
var  ML_T_LONG_OE_FINA = '\uEBFF';

var  ML_T_UE = '\u1849';
var  ML_T_UE_ISOL = '\uEB20';
var  ML_T_UE_ISOL1 = '\uEB21';
var  ML_T_UE_INIT = '\uEB22';
var  ML_T_UE_MEDI = '\uEB23';
var  ML_T_UE_MEDI1 = '\uEB24';
var  ML_T_UE_FINA = '\uEB25';

var  ML_T_NG = '\u184A';
var  ML_T_NG_ISOL = '\uEB26';
var  ML_T_NG_INIT = '\uEB27';
var  ML_T_NG_MEDI = '\uEB28';
var  ML_T_NG_FINA = '\uEB29';
var  ML_T_NGN_MEDI = '\uEB2A';
var  ML_T_NGM_MEDI = '\uEB2B';
var  ML_T_NGL_MEDI = '\uEB2C';
var  ML_T_NGQ_MEDI = '\uEB2D';
var  ML_T_NGG_MEDI = '\uEB2E';
var  ML_T_NGG_FINA = '\uEB2F';

var  ML_T_B = '\u184B';
var  ML_T_B_ISOL = '\uEB30';
var  ML_T_B_INIT = '\uEB31';
var  ML_T_B_MEDI = '\uEB32';
var  ML_T_B_FINA = '\uEB33';
var  ML_T_BA_ISOL = '\uEB34';
var  ML_T_BA_INIT = '\uEB35';
var  ML_T_BA_MEDI = '\uEB36';
var  ML_T_BA_FINA = '\uEB37';
var  ML_T_BE_ISOL = '\uEB38';
var  ML_T_BE_INIT = '\uEB39';
var  ML_T_BE_MEDI = '\uEB3A';
var  ML_T_BE_FINA = '\uEB3B';
var  ML_T_BI_ISOL = '\uEB3C';
var  ML_T_BI_INIT = '\uEB3D';
var  ML_T_BI_MEDI = '\uEB3E';
var  ML_T_BI_FINA = '\uEB3F';
var  ML_T_BO_ISOL = '\uEB40';
var  ML_T_BO_INIT = '\uEB41';
var  ML_T_BO_MEDI = '\uEB42';
var  ML_T_BO_FINA = '\uEB43';
var  ML_T_BU_ISOL = '\uEB44';
var  ML_T_BU_INIT = '\uEB45';
var  ML_T_BU_MEDI = '\uEB46';
var  ML_T_BU_FINA = '\uEB47';
var  ML_T_BOE_ISOL = '\uEB48';
var  ML_T_BOE_INIT = '\uEB49';
var  ML_T_BOE_MEDI = '\uEB4A';
var  ML_T_BOE_FINA = '\uEB4B';
var  ML_T_BUE_ISOL = '\uEB4C';
var  ML_T_BUE_INIT = '\uEB4D';
var  ML_T_BUE_MEDI = '\uEB4E';
var  ML_T_BUE_FINA = '\uEB4F';
var  ML_T_BN_MEDI = '\uEB50';
var  ML_T_BM_MEDI = '\uEB51';
var  ML_T_BL_MEDI = '\uEB52';
var  ML_T_BQ_MEDI = '\uEB53';
var  ML_T_BG_MEDI = '\uEB54';

var  ML_T_P = '\u184C';
var  ML_T_P_ISOL = '\uEB58';
var  ML_T_P_INIT = '\uEB59';
var  ML_T_P_MEDI = '\uEB5A';
var  ML_T_P_FINA = '\uEB5B';
var  ML_T_PA_ISOL = '\uEB5C';
var  ML_T_PA_INIT = '\uEB5D';
var  ML_T_PA_MEDI = '\uEB5E';
var  ML_T_PA_FINA = '\uEB5F';
var  ML_T_PE_ISOL = '\uEB60';
var  ML_T_PE_INIT = '\uEB61';
var  ML_T_PE_MEDI = '\uEB62';
var  ML_T_PE_FINA = '\uEB63';
var  ML_T_PI_ISOL = '\uEB64';
var  ML_T_PI_INIT = '\uEB65';
var  ML_T_PI_MEDI = '\uEB66';
var  ML_T_PI_FINA = '\uEB67';
var  ML_T_PO_ISOL = '\uEB68';
var  ML_T_PO_INIT = '\uEB69';
var  ML_T_PO_MEDI = '\uEB6A';
var  ML_T_PO_FINA = '\uEB6B';
var  ML_T_PU_ISOL = '\uEB6C';
var  ML_T_PU_INIT = '\uEB6D';
var  ML_T_PU_MEDI = '\uEB6E';
var  ML_T_PU_FINA = '\uEB6F';
var  ML_T_POE_ISOL = '\uEB70';
var  ML_T_POE_INIT = '\uEB71';
var  ML_T_POE_MEDI = '\uEB72';
var  ML_T_POE_FINA = '\uEB73';
var  ML_T_PUE_ISOL = '\uEB74';
var  ML_T_PUE_INIT = '\uEB75';
var  ML_T_PUE_MEDI = '\uEB76';
var  ML_T_PUE_FINA = '\uEB77';

var  ML_T_Q = '\u184D';
var  ML_T_Q_ISOL = '\uEB78';
var  ML_T_Q_INIT = '\uEB79';
var  ML_T_Q_INIT1 = '\uEB7A';
var  ML_T_Q_MEDI = '\uEB7B';
var  ML_T_Q_MEDI1 = '\uEB7C';
var  ML_T_Q_FINA = '\uEB7D';
var  ML_T_QE_ISOL = '\uEB7E';
var  ML_T_QE_INIT = '\uEB7F';
var  ML_T_QE_MEDI = '\uEB80';
var  ML_T_QE_FINA = '\uEB81';
var  ML_T_QI_ISOL = '\uEB82';
var  ML_T_QI_INIT = '\uEB83';
var  ML_T_QI_MEDI = '\uEB84';
var  ML_T_QI_FINA = '\uEB85';
var  ML_T_QOE_ISOL = '\uEB86';
var  ML_T_QOE_INIT = '\uEB87';
var  ML_T_QOE_MEDI = '\uEB88';
var  ML_T_QOE_FINA = '\uEB89';
var  ML_T_QUE_ISOL = '\uEB8A';
var  ML_T_QUE_INIT = '\uEB8B';
var  ML_T_QUE_MEDI = '\uEB8C';
var  ML_T_QUE_FINA = '\uEB8D';

var  ML_T_G = '\u184E';
var  ML_T_G_ISOL = '\uEB8E';
var  ML_T_G_ISOL1 = '\uEB8F';
var  ML_T_G_INIT = '\uEB90';
var  ML_T_G_INIT1 = '\uEB91';
var  ML_T_G_MEDI = '\uEB92';
var  ML_T_G_MEDI1 = '\uEB93';
var  ML_T_G_FINA = '\uEB94';
var  ML_T_GE_ISOL = '\uEB95';
var  ML_T_GE_INIT = '\uEB96';
var  ML_T_GE_MEDI = '\uEB97';
var  ML_T_GE_FINA = '\uEB98';
var  ML_T_GI_ISOL = '\uEB99';
var  ML_T_GI_INIT = '\uEB9A';
var  ML_T_GI_MEDI = '\uEB9B';
var  ML_T_GI_FINA = '\uEB9C';
var  ML_T_GOE_ISOL = '\uEB9D';
var  ML_T_GOE_INIT = '\uEB9E';
var  ML_T_GOE_MEDI = '\uEB9F';
var  ML_T_GOE_FINA = '\uEBA0';
var  ML_T_GUE_ISOL = '\uEBA1';
var  ML_T_GUE_INIT = '\uEBA2';
var  ML_T_GUE_MEDI = '\uEBA3';
var  ML_T_GUE_FINA = '\uEBA4';
var  ML_T_GN_MEDI = '\uEBA5';
var  ML_T_GM_MEDI = '\uEBA6';
var  ML_T_GL_MEDI = '\uEBA7';

var  ML_T_M = '\u184F';
var  ML_T_M_ISOL = '\uEBA8';
var  ML_T_M_INIT = '\uEBA9';
var  ML_T_M_MEDI = '\uEBAA';
var  ML_T_M_FINA = '\uEBAB';
var  ML_T_MM_MEDI = '\uEBAC';
var  ML_T_ML_MEDI = '\uEBAD';

var  ML_T_T = '\u1850';
var  ML_T_T_ISOL = '\uEBAE';
var  ML_T_T_INIT = '\uEBAF';
var  ML_T_T_MEDI = '\uEBB0';
var  ML_T_T_FINA = '\uEBB1';

var  ML_T_D = '\u1851';
var  ML_T_D_ISOL = '\uEBB2';
var  ML_T_D_INIT = '\uEBB3';
var  ML_T_D_MEDI = '\uEBB4';
var  ML_T_D_FINA = '\uEBB5';

var  ML_T_CH = '\u1852';
var  ML_T_CH_ISOL = '\uEBB6';
var  ML_T_CH_INIT = '\uEBB7';
var  ML_T_CH_MEDI = '\uEBB8';
var  ML_T_CH_FINA = '\uEBB9';

var  ML_T_J = '\u1853';
var  ML_T_J_ISOL = '\uEBBA';
var  ML_T_J_INIT = '\uEBBB';
var  ML_T_J_MEDI = '\uEBBC';
var  ML_T_J_FINA = '\uEBBD';

var  ML_T_TS = '\u1854';
var  ML_T_TS_ISOL = '\uEBBE';
var  ML_T_TS_INIT = '\uEBBF';
var  ML_T_TS_MEDI = '\uEBC0';
var  ML_T_TS_FINA = '\uEBC1';

var  ML_T_Y = '\u1855';
var  ML_T_Y_ISOL = '\uEBC2';
var  ML_T_Y_INIT = '\uEBC3';
var  ML_T_Y_MEDI = '\uEBC4';
var  ML_T_Y_FINA = '\uEBC5';

var  ML_T_W = '\u1856';
var  ML_T_W_ISOL = '\uEBC6';
var  ML_T_W_INIT = '\uEBC7';
var  ML_T_W_MEDI = '\uEBC8';
var  ML_T_W_FINA = '\uEBC9';

var  ML_T_K = '\u1857';
var  ML_T_K_ISOL = '\uEBCA';
var  ML_T_K_INIT = '\uEBCB';
var  ML_T_K_MEDI = '\uEBCC';
var  ML_T_K_FINA = '\uEBCD';
var  ML_T_KA_ISOL = '\uEBCE';
var  ML_T_KA_INIT = '\uEBCF';
var  ML_T_KA_MEDI = '\uEBD0';
var  ML_T_KA_FINA = '\uEBD1';
var  ML_T_KO_ISOL = '\uEBD2';
var  ML_T_KO_INIT = '\uEBD3';
var  ML_T_KO_MEDI = '\uEBD4';
var  ML_T_KO_FINA = '\uEBD5';
var  ML_T_KU_ISOL = '\uEBD6';
var  ML_T_KU_INIT = '\uEBD7';
var  ML_T_KU_MEDI = '\uEBD8';
var  ML_T_KU_FINA = '\uEBD9';

var  ML_T_GH = '\u1858';
var  ML_T_GH_ISOL = '\uEBDA';
var  ML_T_GH_INIT = '\uEBDB';
var  ML_T_GH_MEDI = '\uEBDC';
var  ML_T_GH_FINA = '\uEBDD';
var  ML_T_GHA_ISOL = '\uEBDE';
var  ML_T_GHA_INIT = '\uEBDF';
var  ML_T_GHA_MEDI = '\uEBE0';
var  ML_T_GHA_FINA = '\uEBE1';
var  ML_T_GHO_ISOL = '\uEBE2';
var  ML_T_GHO_INIT = '\uEBE3';
var  ML_T_GHO_MEDI = '\uEBE4';
var  ML_T_GHO_FINA = '\uEBE5';
var  ML_T_GHU_ISOL = '\uEBE6';
var  ML_T_GHU_INIT = '\uEBE7';
var  ML_T_GHU_MEDI = '\uEBE8';
var  ML_T_GHU_FINA = '\uEBE9';

var  ML_T_HH = '\u1859';
var  ML_T_HH_ISOL = '\uEBEA';
var  ML_T_HH_INIT = '\uEBEB';
var  ML_T_HH_MEDI = '\uEBEC';
var  ML_T_HH_FINA = '\uEBED';

var  ML_T_JI = '\u185A';
var  ML_T_JI_ISOL = '\uEBEE';
var  ML_T_JI_INIT = '\uEBEF';
var  ML_T_JI_MEDI = '\uEBF0';
var  ML_T_JI_FINA = '\uEBF1';

var  ML_T_NIA = '\u185B';
var  ML_T_NIA_ISOL = '\uEBF2';
var  ML_T_NIA_INIT = '\uEBF3';
var  ML_T_NIA_MEDI = '\uEBF4';
var  ML_T_NIA_FINA = '\uEBF5';

var  ML_T_DZ = '\u185C';
var  ML_T_DZ_ISOL = '\uEBF6';
var  ML_T_DZ_INIT = '\uEBF7';
var  ML_T_DZ_MEDI = '\uEBF8';
var  ML_T_DZ_FINA = '\uEBF9';

var  ML_S_E = '\u185D';
var  ML_S_E_ISOL = '\uEC00';
var  ML_S_E_INIT = '\uEC01';
var  ML_S_E_MEDI = '\uEC02';
var  ML_S_E_MEDI1 = '\uEC03';
var  ML_S_E_FINA = '\uEC04';
var  ML_S_E_FINA1 = '\uEC05';
var  ML_S_E_FINA2 = '\uEC06';
var  ML_S_E_FINA3 = '\uEC07';

var  ML_S_I = '\u185E';
var  ML_S_I_ISOL = '\uEC08';
var  ML_S_I_INIT = '\uEC09';
var  ML_S_I_MEDI = '\uEC0A';
var  ML_S_I_MEDI1 = '\uEC0B';
var  ML_S_I_MEDI2 = '\uEC0C';
var  ML_S_I_FINA = '\uEC0D';
var  ML_S_I_FINA1 = '\uEC0E';
var  ML_S_I_FINA2 = '\uEC0F';
var  ML_S_I_FINA3 = '\uEC10';

var  ML_S_IY = '\u185F';
var  ML_S_IY_ISOL = '\uEC11';
var  ML_S_IY_INIT = '\uEC12';
var  ML_S_IY_MEDI = '\uEC13';
var  ML_S_IY_FINA = '\uEC14';

var  ML_S_UE = '\u1860';
var  ML_S_UE_ISOL = '\uEC15';
var  ML_S_UE_INIT = '\uEC16';
var  ML_S_UE_MEDI = '\uEC17';
var  ML_S_UE_MEDI1 = '\uEC18';
var  ML_S_UE_MEDI2 = '\uEC19';
var  ML_S_UE_FINA = '\uEC1A';
var  ML_S_UE_FINA1 = '\uEC1B';
var  ML_S_UE_FINA2 = '\uEC1C';
var  ML_S_UE_FINA3 = '\uEC1D';

var  ML_S_U = '\u1861';
var  ML_S_U_ISOL = '\uEC1E';
var  ML_S_U_INIT = '\uEC1F';
var  ML_S_U_MEDI = '\uEC20';
var  ML_S_U_FINA = '\uEC21';

var  ML_S_NG = '\u1862';
var  ML_S_NG_ISOL = '\uEC22';
var  ML_S_NG_INIT = '\uEC23';
var  ML_S_NG_MEDI = '\uEC24';
var  ML_S_NG_FINA = '\uEC25';
var  ML_S_NGN_MEDI = '\uEC26';
var  ML_S_NGM_MEDI = '\uEC27';
var  ML_S_NGL_MEDI = '\uEC28';
var  ML_S_NGK_MEDI = '\uEC29';
var  ML_S_NGK_MEDI1 = '\uEC2A';
var  ML_S_NGG_MEDI = '\uEC2B';
var  ML_S_NGH_MEDI = '\uEC2C';

var  ML_S_K = '\u1863';
var  ML_S_K_ISOL = '\uEC2D';
var  ML_S_K_INIT = '\uEC2E';
var  ML_S_K_MEDI = '\uEC2F';
var  ML_S_K_MEDI1 = '\uEC30';
var  ML_S_K_FINA = '\uEC31';
var  ML_S_KE_ISOL = '\uEC32';
var  ML_S_KE_INIT = '\uEC33';
var  ML_S_KE_MEDI = '\uEC34';
var  ML_S_KE_FINA = '\uEC35';
var  ML_S_KI_ISOL = '\uEC36';
var  ML_S_KI_INIT = '\uEC37';
var  ML_S_KI_MEDI = '\uEC38';
var  ML_S_KI_FINA = '\uEC39';
var  ML_S_KUE_ISOL = '\uEC3A';
var  ML_S_KUE_INIT = '\uEC3B';
var  ML_S_KUE_MEDI = '\uEC3C';
var  ML_S_KUE_FINA = '\uEC3D';

var  ML_S_G = '\u1864';
var  ML_S_G_ISOL = '\uEC3E';
var  ML_S_G_ISOL1 = '\uEC3F';
var  ML_S_G_INIT = '\uEC40';
var  ML_S_G_INIT1 = '\uEC41';
var  ML_S_G_MEDI = '\uEC42';
var  ML_S_G_FINA = '\uEC43';
var  ML_S_GE_ISOL = '\uEC44';
var  ML_S_GE_INIT = '\uEC45';
var  ML_S_GE_MEDI = '\uEC46';
var  ML_S_GE_FINA = '\uEC47';
var  ML_S_GI_ISOL = '\uEC48';
var  ML_S_GI_INIT = '\uEC49';
var  ML_S_GI_MEDI = '\uEC4A';
var  ML_S_GI_FINA = '\uEC4B';
var  ML_S_GUE_ISOL = '\uEC4C';
var  ML_S_GUE_INIT = '\uEC4D';
var  ML_S_GUE_MEDI = '\uEC4E';
var  ML_S_GUE_FINA = '\uEC4F';

var  ML_S_H = '\u1865';
var  ML_S_H_ISOL = '\uEC50';
var  ML_S_H_ISOL1 = '\uEC51';
var  ML_S_H_INIT = '\uEC52';
var  ML_S_H_INIT1 = '\uEC53';
var  ML_S_H_MEDI = '\uEC54';
var  ML_S_H_MEDI1 = '\uEC55';
var  ML_S_H_FINA = '\uEC56';
var  ML_S_HE_ISOL = '\uEC58';
var  ML_S_HE_INIT = '\uEC59';
var  ML_S_HE_MEDI = '\uEC5A';
var  ML_S_HE_FINA = '\uEC5B';
var  ML_S_HI_ISOL = '\uEC5C';
var  ML_S_HI_INIT = '\uEC5D';
var  ML_S_HI_MEDI = '\uEC5E';
var  ML_S_HI_FINA = '\uEC5F';
var  ML_S_HUE_ISOL = '\uEC60';
var  ML_S_HUE_INIT = '\uEC61';
var  ML_S_HUE_MEDI = '\uEC62';
var  ML_S_HUE_FINA = '\uEC63';

var  ML_S_BE_ISOL = '\uEC64';
var  ML_S_BE_INIT = '\uEC65';
var  ML_S_BE_MEDI = '\uEC66';
var  ML_S_BE_FINA = '\uEC67';
var  ML_S_BI_ISOL = '\uEC68';
var  ML_S_BI_INIT = '\uEC69';
var  ML_S_BI_MEDI = '\uEC6A';
var  ML_S_BI_FINA = '\uEC6B';
var  ML_S_BUE_ISOL = '\uEC6C';
var  ML_S_BUE_INIT = '\uEC6D';
var  ML_S_BUE_MEDI = '\uEC6E';
var  ML_S_BUE_FINA = '\uEC6F';
var  ML_S_BU_ISOL = '\uEC70';
var  ML_S_BU_INIT = '\uEC71';
var  ML_S_BU_MEDI = '\uEC72';
var  ML_S_BU_FINA = '\uEC73';

var  ML_S_P = '\u1866';
var  ML_S_P_ISOL = '\uEC74';
var  ML_S_P_INIT = '\uEC75';
var  ML_S_P_MEDI = '\uEC76';
var  ML_S_P_FINA = '\uEC77';
var  ML_S_PA_ISOL = '\uEC78';
var  ML_S_PA_INIT = '\uEC79';
var  ML_S_PA_MEDI = '\uEC7A';
var  ML_S_PA_FINA = '\uEC7B';
var  ML_S_PE_ISOL = '\uEC7C';
var  ML_S_PE_INIT = '\uEC7D';
var  ML_S_PE_MEDI = '\uEC7E';
var  ML_S_PE_FINA = '\uEC7F';
var  ML_S_PI_ISOL = '\uEC80';
var  ML_S_PI_INIT = '\uEC81';
var  ML_S_PI_MEDI = '\uEC82';
var  ML_S_PI_FINA = '\uEC83';
var  ML_S_PO_ISOL = '\uEC84';
var  ML_S_PO_INIT = '\uEC85';
var  ML_S_PO_MEDI = '\uEC86';
var  ML_S_PO_FINA = '\uEC87';
var  ML_S_PUE_ISOL = '\uEC88';
var  ML_S_PUE_INIT = '\uEC89';
var  ML_S_PUE_MEDI = '\uEC8A';
var  ML_S_PUE_FINA = '\uEC8B';
var  ML_S_PU_ISOL = '\uEC8C';
var  ML_S_PU_INIT = '\uEC8D';
var  ML_S_PU_MEDI = '\uEC8E';
var  ML_S_PU_FINA = '\uEC8F';

var  ML_S_SH = '\u1867';
var  ML_S_SH_ISOL = '\uEC90';
var  ML_S_SH_INIT = '\uEC91';
var  ML_S_SH_MEDI = '\uEC92';
var  ML_S_SH_FINA = '\uEC93';

var  ML_S_T = '\u1868';
var  ML_S_T_ISOL = '\uEC94';
var  ML_S_T_ISOL1 = '\uEC95';
var  ML_S_T_INIT = '\uEC96';
var  ML_S_T_INIT1 = '\uEC97';
var  ML_S_T_MEDI = '\uEC98';
var  ML_S_T_MEDI1 = '\uEC99';
var  ML_S_T_MEDI2 = '\uEC9A';
var  ML_S_T_FINA = '\uEC9B';

var  ML_S_D = '\u1869';
var  ML_S_D_ISOL = '\uEC9C';
var  ML_S_D_INIT = '\uEC9D';
var  ML_S_D_INIT1 = '\uEC9E';
var  ML_S_D_MEDI = '\uEC9F';
var  ML_S_D_MEDI1 = '\uECA0';
var  ML_S_D_FINA = '\uECA1';

var  ML_S_J = '\u186A';
var  ML_S_J_ISOL = '\uECA2';
var  ML_S_J_INIT = '\uECA3';
var  ML_S_J_MEDI = '\uECA4';
var  ML_S_J_FINA = '\uECA5';

var  ML_S_F = '\u186B';
var  ML_S_F_ISOL = '\uECA6';
var  ML_S_F_INIT = '\uECA7';
var  ML_S_F_MEDI = '\uECA8';
var  ML_S_F_FINA = '\uECA9';

var  ML_S_GH = '\u186C';
var  ML_S_GH_ISOL = '\uECAA';
var  ML_S_GH_INIT = '\uECAB';
var  ML_S_GH_MEDI = '\uECAC';
var  ML_S_GH_FINA = '\uECAD';
var  ML_S_GHA_ISOL = '\uECAE';
var  ML_S_GHA_INIT = '\uECAF';
var  ML_S_GHA_MEDI = '\uECB0';
var  ML_S_GHA_FINA = '\uECB1';
var  ML_S_GHO_ISOL = '\uECB2';
var  ML_S_GHO_INIT = '\uECB3';
var  ML_S_GHO_MEDI = '\uECB4';
var  ML_S_GHO_FINA = '\uECB5';
// var  ML_S_GHE_ISOL = '\uECB6';
// var  ML_S_GHE_INIT = '\uECB7';
// var  ML_S_GHE_MEDI = '\uECB8';
// var  ML_S_GHE_FINA = '\uECB9';
// var  ML_S_GHI_ISOL = '\uECBA';
// var  ML_S_GHI_INIT = '\uECBB';
// var  ML_S_GHI_MEDI = '\uECBC';
// var  ML_S_GHI_FINA = '\uECBD';
// var  ML_S_GHUE_ISOL = '\uECBE';
// var  ML_S_GHUE_INIT = '\uECBF';
// var  ML_S_GHUE_MEDI = '\uECC0';
// var  ML_S_GHUE_FINA = '\uECC1';

var  ML_S_HH = '\u186D';
var  ML_S_HH_ISOL = '\uECC2';
var  ML_S_HH_INIT = '\uECC3';
var  ML_S_HH_MEDI = '\uECC4';
var  ML_S_HH_FINA = '\uECC5';
var  ML_S_HHA_ISOL = '\uECC6';
var  ML_S_HHA_INIT = '\uECC7';
var  ML_S_HHA_MEDI = '\uECC8';
var  ML_S_HHA_FINA = '\uECC9';
var  ML_S_HHO_ISOL = '\uECCA';
var  ML_S_HHO_INIT = '\uECCB';
var  ML_S_HHO_MEDI = '\uECCC';
var  ML_S_HHO_FINA = '\uECCD';
// var  ML_S_HHE_ISOL = '\uECCE';
// var  ML_S_HHE_INIT = '\uECCF';
// var  ML_S_HHE_MEDI = '\uECD0';
// var  ML_S_HHE_FINA = '\uECD1';
// var  ML_S_HHI_ISOL = '\uECD2';
// var  ML_S_HHI_INIT = '\uECD3';
// var  ML_S_HHI_MEDI = '\uECD4';
// var  ML_S_HHI_FINA = '\uECD5';
// var  ML_S_HHUE_ISOL = '\uECD6';
// var  ML_S_HHUE_INIT = '\uECD7';
// var  ML_S_HHUE_MEDI = '\uECD8';
// var  ML_S_HHUE_FINA = '\uECD9';

var  ML_S_TS = '\u186E';
var  ML_S_TS_ISOL = '\uECDA';
var  ML_S_TS_INIT = '\uECDB';
var  ML_S_TS_MEDI = '\uECDC';
var  ML_S_TS_FINA = '\uECDD';

var  ML_S_Z = '\u186F';
var  ML_S_Z_ISOL = '\uECDE';
var  ML_S_Z_INIT = '\uECDF';
var  ML_S_Z_INIT1 = '\uECE0';
var  ML_S_Z_MEDI = '\uECE1';
var  ML_S_Z_MEDI1 = '\uECE2';
var  ML_S_Z_FINA = '\uECE3';

var  ML_S_RH = '\u1870';
var  ML_S_RH_ISOL = '\uECE4';
var  ML_S_RH_INIT = '\uECE5';
var  ML_S_RH_MEDI = '\uECE6';
var  ML_S_RH_FINA = '\uECE7';

var  ML_S_CH = '\u1871';
var  ML_S_CH_ISOL = '\uECE8';
var  ML_S_CH_INIT = '\uECE9';
var  ML_S_CH_MEDI = '\uECEA';
var  ML_S_CH_FINA = '\uECEB';

var  ML_S_ZH = '\u1872';
var  ML_S_ZH_ISOL = '\uECEC';
var  ML_S_ZH_INIT = '\uECED';
var  ML_S_ZH_MEDI = '\uECEE';
var  ML_S_ZH_FINA = '\uECEF';

var  ML_M_I = '\u1873';
var  ML_M_I_ISOL = '\uED00';
var  ML_M_I_INIT = '\uED01';
var  ML_M_I_MEDI = '\uED02';
var  ML_M_I_MEDI1 = '\uED03';
var  ML_M_I_MEDI2 = '\uED04';
var  ML_M_I_MEDI3 = '\uED05';
var  ML_M_I_FINA = '\uED06';
var  ML_M_I_FINA1 = '\uED07';
var  ML_M_I_FINA2 = '\uED08';
var  ML_M_I_FINA3 = '\uED09';
var  ML_M_I_FINA4 = '\uED0A';

var  ML_M_K = '\u1874';
var  ML_M_K_ISOL = '\uED0B';
var  ML_M_K_INIT = '\uED0C';
var  ML_M_K_MEDI = '\uED0D';
var  ML_M_K_MEDI1 = '\uED0E';
var  ML_M_K_MEDI2 = '\uED0F';
var  ML_M_K_MEDI3 = '\uED10';
var  ML_M_K_FINA = '\uED11';
var  ML_M_K_FINA1 = '\uED12';
var  ML_M_K_FINA2 = '\uED13';
var  ML_M_KE_ISOL = '\uED14';
var  ML_M_KE_INIT = '\uED15';
var  ML_M_KE_MEDI = '\uED16';
var  ML_M_KE_FINA = '\uED17';
var  ML_M_KI_ISOL = '\uED18';
var  ML_M_KI_INIT = '\uED19';
var  ML_M_KI_MEDI = '\uED1A';
var  ML_M_KI_FINA = '\uED1B';
var  ML_M_KUE_ISOL = '\uED1C';
var  ML_M_KUE_INIT = '\uED1D';
var  ML_M_KUE_MEDI = '\uED1E';
var  ML_M_KUE_FINA = '\uED1F';

var  ML_M_GI_ISOL = '\uED20';
var  ML_M_GI_INIT = '\uED21';
var  ML_M_GI_MEDI = '\uED22';
var  ML_M_GI_FINA = '\uED23';

var  ML_M_BI_ISOL = '\uED24';
var  ML_M_BI_INIT = '\uED25';
var  ML_M_BI_MEDI = '\uED26';
var  ML_M_BI_FINA = '\uED27';

var  ML_M_PI_ISOL = '\uED28';
var  ML_M_PI_INIT = '\uED29';
var  ML_M_PI_MEDI = '\uED2A';
var  ML_M_PI_FINA = '\uED2B';

var  ML_M_R = '\u1875';
var  ML_M_R_ISOL = '\uED2C';
var  ML_M_R_INIT = '\uED2D';
var  ML_M_R_MEDI = '\uED2E';
var  ML_M_R_FINA = '\uED2F';

var  ML_M_F = '\u1876';
var  ML_M_F_ISOL = '\uED30';
var  ML_M_F_INIT = '\uED31';
var  ML_M_F_MEDI = '\uED32';
var  ML_M_F_MEDI1 = '\uED33';
var  ML_M_F_FINA = '\uED34';

var  ML_M_ZH = '\u1877';
var  ML_M_ZH_ISOL = '\uED35';
var  ML_M_ZH_INIT = '\uED36';
var  ML_M_ZH_MEDI = '\uED37';
var  ML_M_ZH_FINA = '\uED38';

// var  ML_ALI_GALI_ANUSVARA_ONE = '\u1880';
// var  ML_ALI_GALI_VISARGA_ONE = '\u1881';
// var  ML_ALI_GALI_DAMARU = '\u1882';
// var  ML_ALI_GALI_UBADAMA = '\u1883';
// var  ML_ALI_GALI_INVERTED_UBADAMA = '\u1884';
// var  ML_ALI_GALI_BALUDA = '\u1885';
// var  ML_ALI_GALI_THREE_BALUDA = '\u1886';
// var  ML_ALI_GALI_A = '\u1887';
// var  ML_ALI_GALI_I = '\u1888';
// var  ML_ALI_GALI_KA = '\u1889';
// var  ML_ALI_GALI_NGA = '\u188A';
// var  ML_ALI_GALI_CA = '\u188B';
// var  ML_ALI_GALI_TTA = '\u188C';
// var  ML_ALI_GALI_TTHA = '\u188D';
// var  ML_ALI_GALI_DDA = '\u188E';
// var  ML_ALI_GALI_NNA = '\u188F';
// var  ML_ALI_GALI_TA = '\u1890';
// var  ML_ALI_GALI_DA = '\u1891';
// var  ML_ALI_GALI_PA = '\u1892';
// var  ML_ALI_GALI_PHA = '\u1893';
// var  ML_ALI_GALI_SSA = '\u1894';
// var  ML_ALI_GALI_ZHA = '\u1895';
// var  ML_ALI_GALI_ZA = '\u1896';
// var  ML_ALI_GALI_AH = '\u1897';
// var  ML_T_ALI_GALI_TA = '\u1898';
// var  ML_T_ALI_GALI_ZHA = '\u1899';
// var  ML_M_ALI_GALI_GHA = '\u189A';
// var  ML_M_ALI_GALI_NGA = '\u189B';
// var  ML_M_ALI_GALI_CA = '\u189C';
// var  ML_M_ALI_GALI_JHA = '\u189D';
// var  ML_M_ALI_GALI_TTA = '\u189E';
// var  ML_M_ALI_GALI_DDHA = '\u189F';
// var  ML_M_ALI_GALI_TA = '\u18A0';
// var  ML_M_ALI_GALI_DHA = '\u18A1';
// var  ML_M_ALI_GALI_SSA = '\u18A2';
// var  ML_M_ALI_GALI_CYA = '\u18A3';
// var  ML_M_ALI_GALI_ZHA = '\u18A4';
// var  ML_M_ALI_GALI_ZA = '\u18A5';
// var  ML_ALI_GALI_HALF_U = '\u18A6';
// var  ML_ALI_GALI_HALF_YA = '\u18A7';
// var  ML_M_ALI_GALI_BHA = '\u18A8';
// var  ML_ALI_GALI_DAGALGA = '\u18A9';
// var  ML_M_ALI_GALI_LHA = '\u18AA';

var  ML_ZWJ = '\u200D';
var  ML_NNBS = '\u202F';

var ML_NO_BREAK = "" + ML_NNBS;

var MS_MVS_A = "" + M_MVS + ML_A;
var MS_MVS_E = "" + M_MVS + ML_E;

var DGBR_MAP = new Object();
// A
DGBR_MAP["" + ML_A] = "" + ML_A_FINA2;
// ACHA
DGBR_MAP["" + ML_A + ML_CH + ML_A] = "" + ML_A_INIT1 + ML_CH_MEDI + ML_A_FINA;
// ACHAGAN
DGBR_MAP["" + ML_A + ML_CH + ML_A + ML_G + ML_A + ML_N] = "" + ML_A_INIT1 + ML_CH_MEDI + ML_A_MEDI + ML_G_MEDI1 + ML_A_MEDI + ML_N_FINA;
// E
DGBR_MAP["" + ML_E] = "" + ML_E_FINA2;
// ECHE
DGBR_MAP["" + ML_E + ML_CH + ML_E] = "" + ML_E_INIT + ML_CH_MEDI + ML_E_FINA;
// ECHEGEN
DGBR_MAP["" + ML_E + ML_CH + ML_E + ML_G + ML_E + ML_N] = "" + ML_E_INIT + ML_CH_MEDI + ML_E_MEDI + ML_GE_MEDI + ML_N_FINA;
// I
DGBR_MAP["" + ML_I] = "" + ML_I_ISOL1;
// IYAN
DGBR_MAP["" + ML_I + ML_Y + ML_A + ML_N] = "" + ML_I_INIT1 + ML_Y_MEDI1 + ML_A_MEDI + ML_N_FINA;
// IYEN
DGBR_MAP["" + ML_I + ML_Y + ML_E + ML_N] = "" + ML_I_INIT1 + ML_Y_MEDI1 + ML_E_MEDI + ML_N_FINA;
// IYAR
DGBR_MAP["" + ML_I + ML_Y + ML_A + ML_R] = "" + ML_I_INIT1 + ML_Y_MEDI1 + ML_A_MEDI + ML_R_FINA;
// IYER
DGBR_MAP["" + ML_I + ML_Y + ML_E + ML_R] = "" + ML_I_INIT1 + ML_Y_MEDI1 + ML_E_MEDI + ML_R_FINA;
// YIAN
DGBR_MAP["" + ML_Y + ML_I + ML_A + ML_N] = "" + ML_Y_INIT1 + ML_I_MEDI + ML_A_MEDI + ML_N_FINA;
// YIEN
DGBR_MAP["" + ML_Y + ML_I + ML_E + ML_N] = "" + ML_Y_INIT1 + ML_I_MEDI + ML_E_MEDI + ML_N_FINA;
// YIAR
DGBR_MAP["" + ML_Y + ML_I + ML_A + ML_R] = "" + ML_Y_INIT1 + ML_I_MEDI + ML_A_MEDI + ML_R_FINA;
// YIER
DGBR_MAP["" + ML_Y + ML_I + ML_E + ML_R] = "" + ML_Y_INIT1 + ML_I_MEDI + ML_E_MEDI + ML_R_FINA;
// U
DGBR_MAP["" + ML_U] = "" + ML_U_ISOL1;
// UU
DGBR_MAP["" + ML_U + ML_U] = "" + ML_U_INIT1 + ML_U_FINA;
// UE
DGBR_MAP["" + ML_UE] = "" + ML_UE_ISOL2;
// UEUE
DGBR_MAP["" + ML_UE + ML_UE] = "" + ML_UE_INIT1 + ML_UE_FINA;
// UN
DGBR_MAP["" + ML_U + ML_N] = "" + ML_U_INIT1 + ML_N_FINA;
// UEN
DGBR_MAP["" + ML_UE + ML_N] = "" + ML_UE_INIT1 + ML_N_FINA;
// UD
DGBR_MAP["" + ML_U + ML_D] = "" + ML_U_INIT1 + ML_D_FINA;
// UED
DGBR_MAP["" + ML_UE + ML_D] = "" + ML_UE_INIT1 + ML_D_FINA;
// BAN
DGBR_MAP["" + ML_B + ML_A + ML_N] = "" + ML_BA_INIT + ML_N_FINA;
// BEN
DGBR_MAP["" + ML_B + ML_E + ML_N] = "" + ML_BA_INIT + ML_N_FINA;
// BAR
DGBR_MAP["" + ML_B + ML_A + ML_R] = "" + ML_BA_INIT + ML_R_FINA;
// BER
DGBR_MAP["" + ML_B + ML_E + ML_R] = "" + ML_BA_INIT + ML_R_FINA;
// NAR
DGBR_MAP["" + ML_N + ML_A + ML_R] = "" + ML_N_INIT + ML_A_MEDI + ML_R_FINA;
// NER
DGBR_MAP["" + ML_N + ML_E + ML_R] = "" + ML_N_INIT + ML_E_MEDI + ML_R_FINA;
// NUGAN
DGBR_MAP["" + ML_N + ML_U + ML_G + ML_A + ML_N] = "" + ML_N_INIT + ML_U_MEDI + ML_G_MEDI1 + ML_A_MEDI + ML_N_FINA;
// NUEGEN
DGBR_MAP["" + ML_N + ML_UE + ML_G + ML_E + ML_N] = "" + ML_N_INIT + ML_UE_MEDI + ML_GE_MEDI + ML_N_FINA;
// NUGUD
DGBR_MAP["" + ML_N + ML_U + ML_G + ML_U + ML_D] = "" + ML_N_INIT + ML_U_MEDI + ML_G_MEDI1 + ML_U_MEDI + ML_D_FINA;
// NUEGUED
DGBR_MAP["" + ML_N + ML_UE + ML_G + ML_UE + ML_D] = "" + ML_N_INIT + ML_UE_MEDI + ML_GUE_MEDI + ML_D_FINA;
// DA
DGBR_MAP["" + ML_D + ML_A] = "" + ML_D_INIT1 + ML_A_FINA;
// DAHI
DGBR_MAP["" + ML_D + ML_A + ML_H + ML_I] = "" + ML_D_INIT1 + ML_A_MEDI + ML_HI_FINA;
// DAGAN
DGBR_MAP["" + ML_D + ML_A + ML_G + ML_A + ML_N] = "" + ML_D_INIT1 + ML_A_MEDI + ML_G_MEDI1 + ML_A_MEDI + ML_N_FINA;
// DE
DGBR_MAP["" + ML_D + ML_E] = "" + ML_D_INIT1 + ML_E_FINA;
// DEHI
DGBR_MAP["" + ML_D + ML_E + ML_H + ML_I] = "" + ML_D_INIT1 + ML_E_MEDI + ML_HI_FINA;
// DEGEN
DGBR_MAP["" + ML_D + ML_E + ML_G + ML_E + ML_N] = "" + ML_D_INIT1 + ML_E_MEDI + ML_GE_MEDI + ML_N_FINA;
// DU
DGBR_MAP["" + ML_D + ML_U] = "" + ML_D_INIT1 + ML_U_FINA;
// DUE
DGBR_MAP["" + ML_D + ML_UE] = "" + ML_D_INIT1 + ML_UE_FINA;
// DUNI
DGBR_MAP["" + ML_D + ML_U + ML_N + ML_I] = "" + ML_D_INIT1 + ML_U_MEDI + ML_N_MEDI1 + ML_I_FINA;
// DUENI
DGBR_MAP["" + ML_D + ML_UE + ML_N + ML_I] = "" + ML_D_INIT1 + ML_UE_MEDI + ML_N_MEDI1 + ML_I_FINA;
// DUR
DGBR_MAP["" + ML_D + ML_U + ML_R] = "" + ML_D_INIT1 + ML_U_MEDI + ML_R_FINA;
// DUER
DGBR_MAP["" + ML_D + ML_UE + ML_R] = "" + ML_D_INIT1 + ML_U_MEDI + ML_R_FINA;
// MINI
DGBR_MAP["" + ML_M + ML_I + ML_N + ML_I] = "" + ML_M_INIT + ML_I_MEDI + ML_N_MEDI1 + ML_I_FINA;
// CHINI
DGBR_MAP["" + ML_CH + ML_I + ML_N + ML_I] = "" + ML_CH_INIT + ML_I_MEDI + ML_N_MEDI1 + ML_I_FINA;
// NI
DGBR_MAP["" + ML_N + ML_I] = "" + ML_N_INIT + ML_I_FINA;
// LUGA
DGBR_MAP["" + ML_L + ML_U + ML_G + M_MVS + ML_A] = "" + ML_L_INIT + ML_U_MEDI + ML_G_FINA1 + ML_A_FINA2;
// LUEGE
DGBR_MAP["" + ML_L + ML_UE + ML_G + ML_E] = "" + ML_L_INIT + ML_U_MEDI + ML_GE_FINA;
// TAI
DGBR_MAP["" + ML_T + ML_A + ML_I] = "" + ML_T_INIT + ML_A_MEDI + ML_I_FINA;
// TEI
DGBR_MAP["" + ML_T + ML_E + ML_I] = "" + ML_T_INIT + ML_E_MEDI + ML_I_FINA;
// TU
DGBR_MAP["" + ML_T + ML_U] = "" + ML_T_INIT + ML_U_FINA;
// TUNI
DGBR_MAP["" + ML_T + ML_U + ML_N + ML_I] = "" + ML_T_INIT + ML_U_MEDI + ML_N_MEDI1 + ML_I_FINA;
// TUE
DGBR_MAP["" + ML_T + ML_UE] = "" + ML_T_INIT + ML_UE_FINA;
// TUENI
DGBR_MAP["" + ML_T + ML_UE + ML_N + ML_I] = "" + ML_T_INIT + ML_U_MEDI + ML_N_MEDI1 + ML_I_FINA;
// TUR
DGBR_MAP["" + ML_T + ML_U + ML_R] = "" + ML_T_INIT + ML_U_MEDI + ML_R_FINA;
// TUER
DGBR_MAP["" + ML_T + ML_UE + ML_R] = "" + ML_T_INIT + ML_U_MEDI + ML_R_FINA;
// YI
DGBR_MAP["" + ML_Y + ML_I] = "" + ML_Y_INIT1 + ML_I_FINA;
// YIN
DGBR_MAP["" + ML_Y + ML_I + ML_N] = "" + ML_Y_INIT1 + ML_I_MEDI + ML_N_FINA;
// YUEGEN
DGBR_MAP["" + ML_Y + ML_UE + ML_G + ML_E + ML_N] = "" + ML_Y_INIT + ML_U_MEDI + ML_GE_MEDI + ML_N_FINA;
// UEGEI
DGBR_MAP["" + ML_UE + ML_G + ML_E + ML_I] = "" + ML_UE_INIT + ML_GE_MEDI + ML_I_FINA;

var ISOL = 0;
var INIT = 1;
var MEDI = 2;
var FINA = 3;

function  advance(srcdoc) {
	var map = new Object();
	var sbConvertedDoc = new StringBuffer();
	var sb = new StringBuffer();
	var nextChar = ' ';
	var len = srcdoc.length;
	for (var i = 0; i < len; i++) {
		var ch = srcdoc.charAt(i);
		if ( i< (len-1) ) {
			nextChar = srcdoc.charAt(i+1);
		}
		if (isMongolian(ch)) {
			sb.append(ch);
		} else {
			if (sb.length() > 0) {
				var words = sb.toString();
				if( map[words] === undefined ) {
					var convertedWord = convertWords(words);
					sbConvertedDoc.append(convertedWord);
					map[words] = convertedWord;
				} else {
					sbConvertedDoc.append(map[words]);
				}
				sb = new StringBuffer("");
			}
			switch( ch ) {
			case EXCLAM:
				if (nextChar == QUESTION) {
					sbConvertedDoc.append(M_EXCLAM_QUESTION_ISOL);
					i++;
				} else {
					sbConvertedDoc.append(M_EXCLAM_ISOL);
				}
				break;

			case COMMA:
				//sbConvertedDoc.append(M_COMMA_ISOL1);
				sbConvertedDoc.append(COMMA);
				break;

			case QUESTION:
				if (nextChar == EXCLAM) {
					sbConvertedDoc.append(M_QUESTION_EXCLAM_ISOL);
					i++;
				} else {
					sbConvertedDoc.append(M_QUESTION_ISOL);
				}
				break;

			case COLON:
				//sbConvertedDoc.append(M_COLON_ISOL);
				sbConvertedDoc.append(COLON);
				break;

			case SEMICOLON:
				//sbConvertedDoc.append(M_SEMICOLON_ISOL);
				sbConvertedDoc.append(SEMICOLON);
				break;

			case M_BIRGA:
				if (nextChar == M_FVS1) {
					sbConvertedDoc.append(M_BIRGA1);
					i++;
				} else if (nextChar == M_FVS2) {
					sbConvertedDoc.append(M_BIRGA2);
					i++;
				} else if (nextChar == M_FVS3) {
					sbConvertedDoc.append(M_BIRGA3);
					i++;
				} else {
					sbConvertedDoc.append(M_BIRGA);
				}
				break;

			case M_ELLIPSIS:
				sbConvertedDoc.append(M_ELLIPSIS_ISOL);
				break;

			case M_COMMA:
				sbConvertedDoc.append(M_COMMA_ISOL);
				break;

			case M_FULL_STOP:
				sbConvertedDoc.append(M_FULL_STOP_ISOL);
				break;

			case M_COLON:
				sbConvertedDoc.append(M_COLON_ISOL);
				break;

			case M_FOUR_DOTS:
				sbConvertedDoc.append(M_FOUR_DOTS_ISOL);
				break;

			case M_TODO_HYPHEN:
				sbConvertedDoc.append(M_TODO_HYPHEN_ISOL);
				break;

			case M_SIBE_SBM:
				sbConvertedDoc.append(M_SIBE_SBM_ISOL);
				break;

			case M_MANCHU_COMMA:
				sbConvertedDoc.append(M_MANCHU_COMMA_ISOL);
				break;

			case M_MANCHU_FULL_STOP:
				sbConvertedDoc.append(M_MANCHU_FULL_STOP_ISOL);
				break;

			case M_DIGIT_ZERO:
				sbConvertedDoc.append(M_DIGIT_ZERO_ISOL);
				break;

			case M_DIGIT_ONE:
				sbConvertedDoc.append(M_DIGIT_ONE_ISOL);
				break;

			case M_DIGIT_TWO:
				sbConvertedDoc.append(M_DIGIT_TWO_ISOL);
				break;

			case M_DIGIT_THREE:
				sbConvertedDoc.append(M_DIGIT_THREE_ISOL);
				break;

			case M_DIGIT_FOUR:
				sbConvertedDoc.append(M_DIGIT_FOUR_ISOL);
				break;

			case M_DIGIT_FIVE:
				sbConvertedDoc.append(M_DIGIT_FIVE_ISOL);
				break;

			case M_DIGIT_SIX:
				sbConvertedDoc.append(M_DIGIT_SIX_ISOL);
				break;

			case M_DIGIT_SEVEN:
				sbConvertedDoc.append(M_DIGIT_SEVEN_ISOL);
				break;

			case M_DIGIT_EIGHT:
				sbConvertedDoc.append(M_DIGIT_EIGHT_ISOL);
				break;

			case M_DIGIT_NINE:
				sbConvertedDoc.append(M_DIGIT_NINE_ISOL);
				break;

			default :
				sbConvertedDoc.append(ch);
				break;
			}
		}
	}
	if (sb.length() > 0) {
		var words = sb.toString();
		if( map[words] === undefined ) {
			var convertedWord = convertWords(words);
			sbConvertedDoc.append(convertedWord);
			map[words] = convertedWord;
		} else {
			sbConvertedDoc.append(map[words]);
		}
		sb = new StringBuffer("");
	}
	map = null;

	return sbConvertedDoc.toString();

}

function convertWords(wordsString) {
	var sb = new StringBuffer();
	var words = wordsString.split(ML_NO_BREAK);
	if (words.length == 0) {
		return wordsString;
	}
	for (var i = 0; i < words.length; i++) {
		if (i == 0) {
			if (wordsString.startsWith(ML_NO_BREAK)) {
				sb.append(ML_NO_BREAK);
				if (DGBR_MAP[words[i]] === undefined  ) {
					sb.append(convertWord(words[i]));
				} else {
					sb.append(DGBR_MAP[words[i]]);
				}
			} else {
				sb.append(convertWord(words[i]));
			}
		} else {
			sb.append(ML_NO_BREAK);
			if (DGBR_MAP[words[i]] === undefined  ) {
				sb.append(convertWord(words[i]));
			} else {
				sb.append(DGBR_MAP[words[i]]);
			}
		}
	}
	if (wordsString.endsWith(ML_NO_BREAK)) {
		sb.append(ML_NO_BREAK);
	}
	return sb.toString();
}

function convertWord(word) {
	var lastIndex = word.length - 1;
	var ae = "";
	if (word.endsWith(MS_MVS_A) || word.endsWith(MS_MVS_E)) {
		if (word.endsWith(MS_MVS_A)) {
			ae = "" + ML_A_FINA2;
		} else {
			ae = "" + ML_E_FINA2;
		}
		word = word.substring(0, lastIndex - 1);
		lastIndex -= 2;
	}
	var ISOLATE = isIsolate(word, lastIndex);
	var result = new StringBuffer();
	for (var i = 0; i <= lastIndex; i++) {
		var currentChar = word.charAt(i);
		var nextChar = ' ';
		if (i < lastIndex) {
			nextChar = word.charAt(i + 1);
		}

		switch (currentChar) {
		case M_NIRUGU:
			result.append(M_NIRUGU);
			break;

		case M_FVS1:
			// result.append(M_FVS1_ISOL);
			break;

		case M_FVS2:
			// result.append(M_FVS2_ISOL);
			break;

		case M_FVS3:
			// result.append(M_FVS3_ISOL);
			break;

		case M_MVS:
			// result.append(M_MVS_ISOL);
			break;

		case ML_A:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_A_ISOL1);
				} else {
					result.append(ML_A_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_A_INIT1);
				} else {
					if (nextChar == ML_T_LVS) {
						if (i == (lastIndex - 1)) { // ISOL
							result.append(ML_T_LONG_A_ISOL);
						} else { // INIT
							result.append(ML_T_LONG_A_INIT);
						}
						i++;
					} else {
						result.append(ML_A_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_A_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_A_FINA2);
				} else {
					result.append(ML_A_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_A_MEDI1);
				} else {
					if (nextChar == ML_T_LVS) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(ML_T_LONG_A_FINA);
						} else { // MEDI
							result.append(ML_T_LONG_A_MEDI);
						}
						i++;
					} else {
						result.append(ML_A_MEDI);
					}
				}
			}
			break;

		case ML_E:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_E_ISOL1);
				} else {
					result.append(ML_E_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_E_INIT1);
				} else {
					result.append(ML_E_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_E_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_E_FINA2);
				} else {
					result.append(ML_E_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_E_MEDI);
				} else {
					result.append(ML_E_MEDI);
				}
			}
			break;

		case ML_I:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_I_ISOL1);
				} else {
					result.append(ML_I_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_I_INIT1);
				} else {
					result.append(ML_I_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_I_FINA);
				} else {
					result.append(ML_I_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_I_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_I_MEDI2);
				} else {
					var prevChar = ' ';
					if (i > 0) {
						prevChar = word.charAt(i - 1);
					}
					if (prevChar == ML_A || prevChar == ML_E || prevChar == ML_O || prevChar == ML_U || ((prevChar == ML_OE || prevChar == ML_UE) && i > 2) || prevChar == ML_EE ) {
						result.append(ML_I_MEDI2);
					} else {
						result.append(ML_I_MEDI);
					}
				}
			}
			break;

		case ML_O:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_O_ISOL1);
				} else {
					result.append(ML_O_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_O_INIT1);
				} else {
					result.append(ML_O_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_O_FINA1);
				} else {
					result.append(ML_O_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_O_MEDI1);
				} else {
					result.append(ML_O_MEDI);
				}
			}
			break;

		case ML_U:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_U_ISOL1);
				} else {
					result.append(ML_U_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_U_INIT1);
				} else {
					result.append(ML_U_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_U_FINA1);
				} else {
					result.append(ML_U_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_U_MEDI1);
				} else {
					result.append(ML_U_MEDI);
				}
			}
			break;

		case ML_OE:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_OE_ISOL1);
				} else {
					result.append(ML_OE_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_OE_INIT1);
				} else {
					result.append(ML_OE_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_OE_FINA1);
				} else {
					result.append(ML_OE_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_OE_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_OE_MEDI2);
				} else {
					if (i == 1) {
						result.append(ML_OE_MEDI1);
					} else {
						result.append(ML_OE_MEDI);
					}
				}
			}
			break;

		case ML_UE:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_UE_ISOL1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_UE_ISOL2);
				} else {
					result.append(ML_UE_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_UE_INIT1);
				} else {
					result.append(ML_UE_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_UE_FINA1);
				} else {
					result.append(ML_UE_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_UE_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_UE_MEDI2);
				} else {
					if (i == 1) {
						result.append(ML_UE_MEDI1);
					} else {
						result.append(ML_UE_MEDI);
					}
				}
			}
			break;

		case ML_EE:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_EE_ISOL1);
				} else {
					result.append(ML_EE_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_EE_INIT1);
				} else {
					result.append(ML_EE_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_EE_FINA);
			} else { // MEDI
				result.append(ML_EE_MEDI);
			}
			break;

		case ML_N:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_N_ISOL1);
				} else {
					result.append(ML_N_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_N_INIT1);
				} else {
					result.append(ML_N_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_N_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_N_FINA2);
				} else {
					if (ae.length > 0) {
						result.append(ML_N_FINA1);
					} else {
						result.append(ML_N_FINA);
					}
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_N_MEDI1);
				} else {
					if (isVowel(nextChar) || isTodoVowel(nextChar) || isSibeVowel(nextChar)) {
						result.append(ML_N_MEDI1);
					} else {
						result.append(ML_N_MEDI);
					}
				}
			}
			break;

		case ML_NG:
			if (ISOLATE) { // ISOL
				result.append(ML_NG_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_NG_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_NG_FINA);
			} else { // MEDI
				if (( isGNML(nextChar) || nextChar == ML_M_K ||  nextChar == ML_S_G ||  nextChar == ML_S_H ) && i < (lastIndex - 1) && isMVowel(word.charAt(i + 2))) {
					result.append(getConCon(currentChar, nextChar, MEDI));
					i++;
				} else {
					result.append(ML_NG_MEDI);
				}
			}
			break;

		case ML_B:
			if (ISOLATE) { // ISOL
				result.append(ML_B_ISOL);
			} else if (i == 0) { // INIT
				if (isVowel(nextChar) || isSibeVowel(nextChar) || nextChar == ML_M_I) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1) { // ISOL1
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_B_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_B_FINA1);
				} else {
					result.append(ML_B_FINA);
				}
			} else { // MEDI
				if (isVowel(nextChar) || isSibeVowel(nextChar) || nextChar == ML_M_I) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // FINA1
						if (nextChar == ML_OE) {
							result.append(ML_BOE_FINA1);
						} else {
							result.append(ML_BUE_FINA1);
						}
					} else if (i < (lastIndex - 2) && word.charAt(i + 2) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // MEDI1
						if (nextChar == ML_OE) {
							result.append(ML_BOE_MEDI1);
						} else {
							result.append(ML_BUE_MEDI1);
						}
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					if (isGNML(nextChar) && i < (lastIndex - 1)) {
						if ( ( nextChar == ML_H || nextChar == ML_G ) && ( word.charAt(i+2) == ML_E || word.charAt(i+2) == ML_I  || word.charAt(i+2) == ML_OE  || word.charAt(i+2) == ML_UE ) ) {
							result.append(ML_B_MEDI);
						} else {
							result.append(getConCon(currentChar, nextChar, MEDI));
							i++;
						}
					} else {
						result.append(ML_B_MEDI);
					}
				}
			}
			break;

		case ML_P:
			if (ISOLATE) { // ISOL
				result.append(ML_P_ISOL);
			} else if (i == 0) { // INIT
				if (isVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1) { // ISOL1
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_P_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_P_FINA);
			} else { // MEDI
				if (isVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // FINA1
						if (nextChar == ML_OE) {
							result.append(ML_POE_FINA1);
						} else {
							result.append(ML_PUE_FINA1);
						}
					} else if (i < (lastIndex - 2) && word.charAt(i + 2) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // MEDI1
						if (nextChar == ML_OE) {
							result.append(ML_POE_MEDI1);
						} else {
							result.append(ML_PUE_MEDI1);
						}
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					if (isGNML(nextChar) && i < (lastIndex - 1)) {
						result.append(getConCon(currentChar, nextChar, MEDI));
						i++;
					} else {
						result.append(ML_P_MEDI);
					}
				}
			}
			break;

		case ML_H:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_H_ISOL1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_H_ISOL2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_H_ISOL3);
				} else {
					result.append(ML_H_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_H_INIT1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_H_INIT2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_H_INIT3);
				} else {
					if (isFMVowel(nextChar) || nextChar == ML_I) {
						if (i == (lastIndex - 1)) { // ISOL
							result.append(getConVowel(currentChar, ISOL, nextChar));
						} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1) { // ISOL1
							result.append(getConVowel(currentChar, ISOL, nextChar));
						} else { // INIT
							result.append(getConVowel(currentChar, INIT, nextChar));
						}
						i++;
					} else {
						result.append(ML_H_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_H_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_H_FINA2);
				} else {
					if (isGNML(nextChar) && i == (lastIndex - 1)) {
						result.append(getConCon(currentChar, nextChar, FINA));
						i++;
					} else {
						result.append(ML_H_FINA);
					}
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_H_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_H_MEDI2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_H_MEDI3);
				} else {
					if (isFMVowel(nextChar) || nextChar == ML_I) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(getConVowel(currentChar, FINA, nextChar));
						} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // FINA1
							if (nextChar == ML_OE) {
								result.append(ML_HOE_FINA1);
							} else {
								result.append(ML_HUE_FINA1);
							}
						} else if (i < (lastIndex - 2) && word.charAt(i + 2) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // MEDI1
							if (nextChar == ML_OE) {
								result.append(ML_HOE_MEDI1);
							} else {
								result.append(ML_HUE_MEDI1);
							}
						} else { // MEDI
							result.append(getConVowel(currentChar, MEDI, nextChar));
						}
						i++;
					} else {
						if (isGNML(nextChar) && i < (lastIndex - 1)) {
							result.append(getConCon(currentChar, nextChar, MEDI));
							i++;
						} else {
							result.append(ML_H_MEDI);
						}
					}
				}
			}
			break;

		case ML_G:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_G_ISOL1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_G_ISOL2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_G_ISOL3);
				} else {
					result.append(ML_G_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_G_INIT1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_G_INIT2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_G_INIT3);
				} else {
					if (isFMVowel(nextChar) || nextChar == ML_I) {
						if (i == (lastIndex - 1)) { // ISOL
							result.append(getConVowel(currentChar, ISOL, nextChar));
						} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1) { // ISOL1
							result.append(getConVowel(currentChar, ISOL, nextChar));
						} else { // INIT
							result.append(getConVowel(currentChar, INIT, nextChar));
						}
						i++;
					} else if ( nextChar == ML_H || nextChar == ML_SH || nextChar == ML_R ) {
						result.append(ML_G_INIT3);
					} else {
						result.append(ML_G_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_G_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_G_FINA2);
				} else {
					if (ae.length > 0) {
						result.append(ML_G_FINA1);
					} else {
						if (isGNML(nextChar) && i == (lastIndex - 1)) {
							result.append(getConCon(currentChar, nextChar, FINA));
							i++;
						} else {
							if (i > 0 && isFMVowel(word.charAt(i - 1))) {
								result.append(ML_G_FINA2);
							} else if (i > 0 && ML_I == word.charAt(i - 1)) {
								if (i > 2 && (isFMVowel(word.charAt(i - 3)) || ML_G_MEDI2 == word.charAt(i - 3))) {
									result.append(ML_G_FINA2);
								} else if (i > 3 && (isFMVowel(word.charAt(i - 4)) || ML_G_MEDI2 == word.charAt(i - 4))) {
									result.append(ML_G_FINA2);
								} else if (i > 4 && (isFMVowel(word.charAt(i - 5)) || ML_G_MEDI2 == word.charAt(i - 5))) {
									result.append(ML_G_FINA2);
								} else if (i > 5 && (isFMVowel(word.charAt(i - 6)) || ML_G_MEDI2 == word.charAt(i - 6))) {
									result.append(ML_G_FINA2);
								} else {
									result.append(ML_G_FINA);
								}
							} else {
								result.append(ML_G_FINA);
							}
						}
					}
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_G_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_G_MEDI2);
				} else if (nextChar == M_FVS3) {
					if (isGNML(word.charAt(i + 2)) && i < (lastIndex - 1)) {
						result.append(getConCon(currentChar, word.charAt(i + 2), MEDI));
						i++;
						i++;
					} else {
						result.append(ML_G_MEDI3);
					}
				} else {
					if (isMVowel(nextChar)) {
						if (i > 0 && ( word.charAt(i - 1) == ML_S || word.charAt(i - 1) == ML_D )) {
							result.append(ML_G_MEDI);
						} else {
							result.append(ML_G_MEDI1);
						}
					} else {
						if (isFMVowel(nextChar) || nextChar == ML_I) {
							if (i == (lastIndex - 1)) { // FINA
								result.append(getConVowel(currentChar, FINA, nextChar));
							} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // FINA1
								if (nextChar == ML_OE) {
									result.append(ML_GOE_FINA1);
								} else {
									result.append(ML_GUE_FINA1);
								}
							} else if (i < (lastIndex - 2) && word.charAt(i + 2) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // MEDI1
								if (nextChar == ML_OE) {
									result.append(ML_GOE_MEDI1);
								} else {
									result.append(ML_GUE_MEDI1);
								}
							} else { // MEDI
								result.append(getConVowel(currentChar, MEDI, nextChar));
							}
							i++;
						} else {
							if (isGNML(nextChar) && i < (lastIndex - 1) && word.charAt(i + 2) != ML_A && word.charAt(i + 2) != ML_O && word.charAt(i + 2) != ML_U) {
								if ( ( nextChar == ML_G || nextChar == ML_H ) && ( word.charAt(i + 2) == ML_E || word.charAt(i + 2) == ML_OE || word.charAt(i + 2) == ML_UE ) ) {
									result.append(ML_G_MEDI3);
								} else {
									result.append(getConCon(currentChar, nextChar, MEDI));
									i++;
								}
							} else {
								if (i > 0 && isFMVowel(word.charAt(i - 1))) {
									result.append(ML_G_MEDI3);
								} else if (i > 0 && ML_I == word.charAt(i - 1)) {
									if (i > 2 && (isFMVowel(word.charAt(i - 3)) || ML_G_MEDI3 == word.charAt(i - 3))) {
										result.append(ML_G_MEDI3);
									} else if (i > 3 && (isFMVowel(word.charAt(i - 4)) || ML_G_MEDI3 == word.charAt(i - 4))) {
										result.append(ML_G_MEDI3);
									} else if (i > 4 && (isFMVowel(word.charAt(i - 5)) || ML_G_MEDI3 == word.charAt(i - 5))) {
										result.append(ML_G_MEDI3);
									} else if (i > 5 && (isFMVowel(word.charAt(i - 6)) || ML_G_MEDI3 == word.charAt(i - 6))) {
										result.append(ML_G_MEDI3);
									} else if (lastIndex >= (i + 2) && (isFMVowel(word.charAt(i + 2)) || ML_G_MEDI3 == word.charAt(i + 2))) {
										result.append(ML_G_MEDI3);
									} else if (lastIndex >= (i + 3) && (isFMVowel(word.charAt(i + 3)) || ML_G_MEDI3 == word.charAt(i + 3))) {
										result.append(ML_G_MEDI3);
									} else if (lastIndex >= (i + 4) && (isFMVowel(word.charAt(i + 4)) || ML_G_MEDI3 == word.charAt(i + 4))) {
										result.append(ML_G_MEDI3);
									} else {
										result.append(ML_G_MEDI);
									}
								} else {
									result.append(ML_G_MEDI);
								}
							}
						}
					}
				}
			}
			break;
		case ML_M:
			if (ISOLATE) { // ISOL
				result.append(ML_M_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_M_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_M_FINA);
			} else { // MEDI
				if (nextChar == ML_L && i < (lastIndex - 1)) {
					result.append(ML_ML_MEDI);
					i++;
				} else {
					result.append(ML_M_MEDI);
				}
			}
			break;

		case ML_L:
			if (ISOLATE) { // ISOL
				result.append(ML_L_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_L_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_L_FINA);
			} else { // MEDI
				if (nextChar == ML_L && i < (lastIndex - 1)) {
					result.append(ML_LL_MEDI);
					i++;
				} else {
					result.append(ML_L_MEDI);
				}
			}
			break;

		case ML_S:
			if (ISOLATE) { // ISOL
				result.append(ML_S_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_S_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_S_FINA2);
				} else {
					result.append(ML_S_FINA);
				}
			} else { // MEDI
				result.append(ML_S_MEDI);
			}
			break;

		case ML_SH:
			if (ISOLATE) { // ISOL
				result.append(ML_SH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_SH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_SH_FINA);
			} else { // MEDI
				result.append(ML_SH_MEDI);
			}
			break;

		case ML_T:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_T_ISOL1);
				} else {
					result.append(ML_T_ISOL);
				}
			} else if (i == 0) { // INIT
				result.append(ML_T_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_T_MEDI2);
				} else {
					result.append(ML_T_MEDI);
				}
			}
			break;

		case ML_D:
			if (ISOLATE) { // ISOL
				result.append(ML_D_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_D_INIT1);
				} else {
					result.append(ML_D_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_D_FINA1);
				} else {
					result.append(ML_D_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_D_MEDI1);
				} else {
					if (isVowel(nextChar)) {
						result.append(ML_D_MEDI1);
					} else {
						result.append(ML_D_MEDI);
					}
				}
			}
			break;

		case ML_CH:
			if (ISOLATE) { // ISOL
				result.append(ML_CH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_CH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_CH_FINA);
			} else { // MEDI
				result.append(ML_CH_MEDI);
			}
			break;

		case ML_J:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_J_ISOL1);
				} else if ( ae.length > 0) {
						result.append(ML_J_ISOL1);
				} else {
					result.append(ML_J_ISOL);
				}
			} else if (i == 0) { // INIT
				result.append(ML_J_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_J_FINA);
			} else { // MEDI
				result.append(ML_J_MEDI);
			}
			break;

		case ML_Y:
			if (ISOLATE) { // ISOL
				result.append(ML_Y_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_Y_INIT1);
				} else {
					result.append(ML_Y_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_Y_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_Y_MEDI1);
				} else {
					var prevChar = ' ';
					if (i > 0) {
						prevChar = word.charAt(i - 1);
					}
					var nextNextChar = ' ';
					if ( i < (lastIndex-1) ) {
						nextNextChar = word.charAt( i+2 );
					}
					if (nextChar == ML_I && i<(lastIndex-1) && !isFVS(nextNextChar) && ( prevChar == ML_A || prevChar == ML_E || prevChar == ML_O || prevChar == ML_U || ((prevChar == ML_OE || prevChar == ML_UE) && i > 1) ) ) {
						result.append(ML_Y_MEDI1);
					} else {
						result.append(ML_Y_MEDI);
					}
				}
			}
			break;

		case ML_R:
			if (ISOLATE) { // ISOL
				result.append(ML_R_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_R_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_R_FINA);
			} else { // MEDI
				result.append(ML_R_MEDI);
			}
			break;

		case ML_W:
			if (ISOLATE) { // ISOL
				result.append(ML_W_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_W_INIT1);
				} else {
					result.append(ML_W_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_W_FINA1);
				} else if ( ae.length > 0) {
					result.append(ML_W_FINA1);
				} else {
					result.append(ML_W_FINA);
				}
			} else { // MEDI
				result.append(ML_W_MEDI);
			}
			break;

		case ML_F:
			if (ISOLATE) { // ISOL
				result.append(ML_F_ISOL);
			} else if (i == 0) { // INIT
				if (isVowel(nextChar) || nextChar == ML_I) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1) { // ISOL1
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_F_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_F_FINA);
			} else { // MEDI
				if (isVowel(nextChar) || nextChar == ML_I) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // FINA1
						if (nextChar == ML_OE) {
							result.append(ML_FOE_FINA1);
						} else {
							result.append(ML_FUE_FINA1);
						}
					} else if (i < (lastIndex - 2) && word.charAt(i + 2) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // MEDI1
						if (nextChar == ML_OE) {
							result.append(ML_FOE_MEDI1);
						} else {
							result.append(ML_FUE_MEDI1);
						}
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					if (isGNML(nextChar) && i < (lastIndex - 1)) {
						result.append(getConCon(currentChar, nextChar, MEDI));
						i++;
					} else {
						result.append(ML_F_MEDI);
					}
				}
			}
			break;

		case ML_K:
			if (ISOLATE) { // ISOL
				result.append(ML_K_ISOL);
			} else if (i == 0) { // INIT
				if (isVowel(nextChar) || nextChar == ML_I) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1) { // ISOL1
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_K_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_K_FINA);
			} else { // MEDI
				if (isVowel(nextChar) || nextChar == ML_I) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // FINA1
						if (nextChar == ML_OE) {
							result.append(ML_KOE_FINA1);
						} else {
							result.append(ML_KUE_FINA1);
						}
					} else if (i < (lastIndex - 2) && word.charAt(i + 2) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // MEDI1
						if (nextChar == ML_OE) {
							result.append(ML_KOE_MEDI1);
						} else {
							result.append(ML_KUE_MEDI1);
						}
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					if (isGNML(nextChar) && i < (lastIndex - 1)) {
						result.append(getConCon(currentChar, nextChar, MEDI));
						i++;
					} else {
						result.append(ML_K_MEDI);
					}
				}
			}
			break;

		case ML_KH:
			if (ISOLATE) { // ISOL
				result.append(ML_KH_ISOL);
			} else if (i == 0) { // INIT
				if (isVowel(nextChar) || nextChar == ML_I) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1) { // ISOL1
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_KH_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_KH_FINA);
			} else { // MEDI
				if (isVowel(nextChar) || nextChar == ML_I) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else if (i == (lastIndex - 2) && word.charAt(lastIndex) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // FINA1
						if (nextChar == ML_OE) {
							result.append(ML_KHOE_FINA1);
						} else {
							result.append(ML_KHUE_FINA1);
						}
					} else if (i < (lastIndex - 2) && word.charAt(i + 2) == M_FVS1 && (nextChar == ML_OE || nextChar == ML_UE)) { // MEDI1
						if (nextChar == ML_OE) {
							result.append(ML_KHOE_MEDI1);
						} else {
							result.append(ML_KHUE_MEDI1);
						}
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					if (isGNML(nextChar) && i < (lastIndex - 1)) {
						result.append(getConCon(currentChar, nextChar, MEDI));
						i++;
					} else {
						result.append(ML_KH_MEDI);
					}
				}
			}
			break;

		case ML_C:
			if (ISOLATE) { // ISOL
				result.append(ML_C_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_C_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_C_FINA);
			} else { // MEDI
				result.append(ML_C_MEDI);
			}
			break;

		case ML_Z:
			if (ISOLATE) { // ISOL
				result.append(ML_Z_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_Z_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_Z_FINA);
			} else { // MEDI
				result.append(ML_Z_MEDI);
			}
			break;

		case ML_HH:
			if (ISOLATE) { // ISOL
				result.append(ML_HH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_HH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_HH_FINA);
			} else { // MEDI
				result.append(ML_HH_MEDI);
			}
			break;

		case ML_RH:
			if (ISOLATE) { // ISOL
				result.append(ML_RH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_RH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_RH_FINA);
			} else { // MEDI
				result.append(ML_RH_MEDI);
			}
			break;

		case ML_LH:
			if (ISOLATE) { // ISOL
				result.append(ML_LH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_LH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_LH_FINA);
			} else { // MEDI
				result.append(ML_LH_MEDI);
			}
			break;

		case ML_ZHI:
			if (ISOLATE) { // ISOL
				result.append(ML_ZHI_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_ZHI_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_ZHI_FINA);
			} else { // MEDI
				result.append(ML_ZHI_MEDI);
			}
			break;

		case ML_CHI:
			if (ISOLATE) { // ISOL
				result.append(ML_CHI_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_CHI_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_CHI_FINA);
			} else { // MEDI
				result.append(ML_CHI_MEDI);
			}
			break;

		case ML_T_LVS:
			if (ISOLATE) { // ISOL
				result.append(ML_T_LVS_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_LVS_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_LVS_FINA);
			} else { // MEDI
				result.append(ML_T_LVS_MEDI);
			}
			break;

		case ML_T_E:
			if (ISOLATE) { // ISOL
				result.append(ML_T_E_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_E_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_E_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_E_MEDI1);
				} else {
					result.append(ML_T_E_MEDI);
				}
			}
			break;

		case ML_T_I:
			if (ISOLATE) { // ISOL
				result.append(ML_T_I_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_I_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_I_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_I_MEDI1);
				} else {
					var prevChar = ' ';
					if (i > 0) {
						prevChar = word.charAt(i - 1);
					}
					if (prevChar == ML_A || prevChar == ML_T_E || prevChar == ML_T_I || prevChar == ML_T_O || prevChar == ML_T_U || prevChar == ML_T_OE || prevChar == ML_T_UE ) {
						result.append(ML_T_I_MEDI1);
					} else {
						result.append(ML_T_I_MEDI);
					}
				}
			}
			break;

		case ML_T_O:
			if (ISOLATE) { // ISOL
				result.append(ML_T_O_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == ML_T_LVS) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(ML_T_LONG_O_ISOL);
					} else { // INIT
						result.append(ML_T_LONG_O_INIT);
					}
					i++;
				} else {
					result.append(ML_T_O_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_O_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_O_MEDI1);
				} else {
					if (nextChar == ML_T_LVS) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(ML_T_LONG_O_FINA);
						} else { // MEDI
							result.append(ML_T_LONG_O_MEDI);
						}
						i++;
					} else {
						result.append(ML_T_O_MEDI);
					}
				}
			}
			break;

		case ML_T_U:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_T_U_ISOL1);
				} else {
					result.append(ML_T_U_ISOL);
				}
			} else if (i == 0) { // INIT
				result.append(ML_T_U_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_T_U_FINA1);
				} else {
					result.append(ML_T_U_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_U_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_T_U_MEDI2);
				} else {
					result.append(ML_T_U_MEDI);
				}
			}
			break;

		case ML_T_OE:
			if (ISOLATE) { // ISOL
				result.append(ML_T_OE_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == ML_T_LVS) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(ML_T_LONG_OE_ISOL);
					} else { // INIT
						result.append(ML_T_LONG_OE_INIT);
					}
					i++;
				} else {
					result.append(ML_T_OE_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_OE_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_OE_MEDI1);
				} else {
					if (nextChar == ML_T_LVS) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(ML_T_LONG_OE_FINA);
						} else { // MEDI
							result.append(ML_T_LONG_OE_MEDI);
						}
						i++;
					} else {
						result.append(ML_T_OE_MEDI);
					}
				}
			}
			break;

		case ML_T_UE:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_T_UE_ISOL1);
				} else {
					result.append(ML_T_UE_ISOL);
				}
			} else if (i == 0) { // INIT
				result.append(ML_T_UE_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_UE_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_UE_MEDI1);
				} else {
					result.append(ML_T_UE_MEDI);
				}
			}
			break;

		case ML_T_NG:
			if (ISOLATE) { // ISOL
				result.append(ML_T_NG_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_NG_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if ((nextChar == ML_T_G) && i == (lastIndex - 1) && isTodoMVowel(word.charAt(lastIndex))) {
					result.append(getConCon(currentChar, nextChar, MEDI));
					i++;
				} else {
					result.append(ML_T_NG_FINA);
				}
			} else { // MEDI
				if (isTodoNMLQG(nextChar) && i < (lastIndex - 1) && isTodoMVowel(word.charAt(i + 2))) {
					result.append(getConCon(currentChar, nextChar, MEDI));
					i++;
				} else {
					result.append(ML_T_NG_MEDI);
				}
			}
			break;

		case ML_T_B:
			if (ISOLATE) { // ISOL
				result.append(ML_T_B_ISOL);
			} else if (i == 0) { // INIT
				if (isTodoVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_T_B_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_B_FINA);
			} else { // MEDI
				if (isTodoVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					if (isTodoNMLQG(nextChar) && i < (lastIndex - 1)) {
						result.append(getConCon(currentChar, nextChar, MEDI));
						i++;
					} else {
						result.append(ML_T_B_MEDI);
					}
				}
			}
			break;

		case ML_T_P:
			if (ISOLATE) { // ISOL
				result.append(ML_T_P_ISOL);
			} else if (i == 0) { // INIT
				if (isTodoVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_T_P_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_P_FINA);
			} else { // MEDI
				if (isTodoVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					result.append(ML_T_P_MEDI);
				}
			}
			break;

		case ML_T_Q:
			if (ISOLATE) { // ISOL
				result.append(ML_T_Q_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_T_Q_INIT1);
				} else {
					if (isTodoFMVowel(nextChar) || nextChar == ML_T_I) {
						if (i == (lastIndex - 1)) { // ISOL
							result.append(getConVowel(currentChar, ISOL, nextChar));
						} else { // INIT
							result.append(getConVowel(currentChar, INIT, nextChar));
						}
						i++;
					} else {
						result.append(ML_T_Q_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_Q_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_Q_MEDI1);
				} else {
					if (isTodoFMVowel(nextChar) || nextChar == ML_T_I) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(getConVowel(currentChar, FINA, nextChar));
						} else { // MEDI
							result.append(getConVowel(currentChar, MEDI, nextChar));
						}
						i++;
					} else {
						result.append(ML_T_Q_MEDI);
					}
				}
			}
			break;

		case ML_T_G:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_T_G_ISOL1);
				} else {
					result.append(ML_T_G_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_T_G_INIT1);
				} else {
					if (isTodoFMVowel(nextChar) || nextChar == ML_T_I) {
						if (i == (lastIndex - 1)) { // ISOL
							result.append(getConVowel(currentChar, ISOL, nextChar));
						} else { // INIT
							result.append(getConVowel(currentChar, INIT, nextChar));
						}
						i++;
					} else {
						result.append(ML_T_G_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_G_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_T_G_MEDI1);
				} else {
					if (isTodoFMVowel(nextChar) || nextChar == ML_T_I) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(getConVowel(currentChar, FINA, nextChar));
						} else { // MEDI
							result.append(getConVowel(currentChar, MEDI, nextChar));
						}
						i++;
					} else {
						if ((nextChar == ML_N || nextChar == ML_T_M || nextChar == ML_L) && i < (lastIndex - 1)) {
							result.append(getConCon(currentChar, nextChar, MEDI));
							i++;
						} else {
							result.append(ML_T_G_MEDI);
						}
					}
				}
			}
			break;

		case ML_T_M:
			if (ISOLATE) { // ISOL
				result.append(ML_T_M_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_M_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_M_FINA);
			} else { // MEDI
				if ((nextChar == ML_T_M || nextChar == ML_L) && i < (lastIndex - 1)) {
					result.append(getConCon(currentChar, nextChar, MEDI));
					i++;
				} else {
					result.append(ML_T_M_MEDI);
				}
			}
			break;

		case ML_T_T:
			if (ISOLATE) { // ISOL
				result.append(ML_T_T_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_T_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_T_FINA);
			} else { // MEDI
				result.append(ML_T_T_MEDI);
			}
			break;

		case ML_T_D:
			if (ISOLATE) { // ISOL
				result.append(ML_T_D_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_D_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_D_FINA);
			} else { // MEDI
				result.append(ML_T_D_MEDI);
			}
			break;

		case ML_T_CH:
			if (ISOLATE) { // ISOL
				result.append(ML_T_CH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_CH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_CH_FINA);
			} else { // MEDI
				result.append(ML_T_CH_MEDI);
			}
			break;

		case ML_T_J:
			if (ISOLATE) { // ISOL
				result.append(ML_T_J_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_J_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_J_FINA);
			} else { // MEDI
				result.append(ML_T_J_MEDI);
			}
			break;

		case ML_T_TS:
			if (ISOLATE) { // ISOL
				result.append(ML_T_TS_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_TS_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_TS_FINA);
			} else { // MEDI
				result.append(ML_T_TS_MEDI);
			}
			break;

		case ML_T_Y:
			if (ISOLATE) { // ISOL
				result.append(ML_T_Y_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_Y_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_Y_FINA);
			} else { // MEDI
				result.append(ML_T_Y_MEDI);
			}
			break;

		case ML_T_W:
			if (ISOLATE) { // ISOL
				result.append(ML_T_W_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_W_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_W_FINA);
			} else { // MEDI
				result.append(ML_T_W_MEDI);
			}
			break;

		case ML_T_K:
			if (ISOLATE) { // ISOL
				result.append(ML_T_K_ISOL);
			} else if (i == 0) { // INIT
				if (isTodoMVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_T_K_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_K_FINA);
			} else { // MEDI
				if (isTodoMVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					result.append(ML_T_K_MEDI);
				}
			}
			break;

		case ML_T_GH:
			if (ISOLATE) { // ISOL
				result.append(ML_T_GH_ISOL);
			} else if (i == 0) { // INIT
				if (isTodoMVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_T_GH_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_GH_FINA);
			} else { // MEDI
				if (isTodoMVowel(nextChar)) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					result.append(ML_T_GH_MEDI);
				}
			}
			break;

		case ML_T_HH:
			if (ISOLATE) { // ISOL
				result.append(ML_T_HH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_HH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_HH_FINA);
			} else { // MEDI
				result.append(ML_T_HH_MEDI);
			}
			break;

		case ML_T_JI:
			if (ISOLATE) { // ISOL
				result.append(ML_T_JI_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_JI_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_JI_FINA);
			} else { // MEDI
				result.append(ML_T_JI_MEDI);
			}
			break;

		case ML_T_NIA:
			if (ISOLATE) { // ISOL
				result.append(ML_T_NIA_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_NIA_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_NIA_FINA);
			} else { // MEDI
				result.append(ML_T_NIA_MEDI);
			}
			break;

		case ML_T_DZ:
			if (ISOLATE) { // ISOL
				result.append(ML_T_DZ_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_T_DZ_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_T_DZ_FINA);
			} else { // MEDI
				result.append(ML_T_DZ_MEDI);
			}
			break;

		case ML_S_E:
			if (ISOLATE) { // ISOL
				result.append(ML_S_E_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_E_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_S_E_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_S_E_FINA2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_S_E_FINA3);
				} else {
					var prevChar = ' ';
					if (i > 0) {
						prevChar = word.charAt(i - 1);
					}
					if (prevChar == ML_S_T || prevChar == ML_S_D ) {
						result.append(ML_S_E_FINA1);
					} else {
						result.append(ML_S_E_FINA);
					}
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_S_E_MEDI1);
				} else {
					var prevChar = ' ';
					if (i > 0) {
						prevChar = word.charAt(i - 1);
					}
					if (prevChar == ML_S_T || prevChar == ML_S_D ) {
						result.append(ML_S_E_MEDI1);
					} else {
						result.append(ML_S_E_MEDI);
					}
				}
			}
			break;

		case ML_S_I:
			if (ISOLATE) { // ISOL
				result.append(ML_S_I_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_I_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_S_I_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_S_I_FINA2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_S_I_FINA3);
				} else {
					result.append(ML_S_I_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_S_I_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_S_I_MEDI2);
				} else {
					result.append(ML_S_I_MEDI);
				}
			}
			break;

		case ML_S_IY:
			if (ISOLATE) { // ISOL
				result.append(ML_S_IY_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_IY_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_IY_FINA);
			} else { // MEDI
				result.append(ML_S_IY_MEDI);
			}
			break;

		case ML_S_UE:
			if (ISOLATE) { // ISOL
				result.append(ML_S_UE_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_UE_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_S_UE_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_S_UE_FINA2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_S_UE_FINA3);
				} else {
					var prevChar = ' ';
					if (i > 0) {
						prevChar = word.charAt(i - 1);
					}
					if (prevChar == ML_S_T || prevChar == ML_S_D ) {
						result.append(ML_S_UE_FINA1);
					} else {
						result.append(ML_S_UE_FINA);
					}
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_S_UE_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_S_UE_MEDI2);
				} else {
					var prevChar = ' ';
					if (i > 0) {
						prevChar = word.charAt(i - 1);
					}
					if (prevChar == ML_S_T || prevChar == ML_S_D ) {
						result.append(ML_S_UE_MEDI1);
					} else {
						result.append(ML_S_UE_MEDI);
					}
				}
			}
			break;

		case ML_S_U:
			if (ISOLATE) { // ISOL
				result.append(ML_S_U_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_U_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_U_FINA);
			} else { // MEDI
				result.append(ML_S_U_MEDI);
			}
			break;

		case ML_S_NG:
			if (ISOLATE) { // ISOL
				result.append(ML_S_NG_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_NG_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_NG_FINA);
			} else { // MEDI
				if ((nextChar == ML_N || nextChar == ML_M || nextChar == ML_L || nextChar == ML_S_K || nextChar == ML_S_G || nextChar == ML_S_H) && i < (lastIndex - 1)
						&& isTodoMVowel(word.charAt(i + 2))) {
					switch (nextChar) {
					case ML_N:
						result.append(ML_S_NGN_MEDI);
						break;
					case ML_M:
						result.append(ML_S_NGM_MEDI);
						break;
					case ML_L:
						result.append(ML_S_NGL_MEDI);
						break;
					case ML_S_K:
						if (i < (lastIndex - 1) && word.charAt(i + 2) == M_FVS1) {
							result.append(ML_S_NGK_MEDI1);
						} else {
							result.append(ML_S_NGK_MEDI);
						}
						break;
					case ML_S_G:
						result.append(ML_S_NGG_MEDI);
						break;
					case ML_S_H:
						result.append(ML_S_NGH_MEDI);
						break;
					}
					i++;
				} else {
					result.append(ML_S_NG_MEDI);
				}
			}
			break;

		case ML_S_K:
			if (ISOLATE) { // ISOL
				result.append(ML_S_K_ISOL);
			} else if (i == 0) { // INIT
				var nextNextChar = ' ';
				if ( i < (lastIndex -1) ) {
					nextNextChar = word.charAt( i+2 );
				}
				if ( ( nextChar == ML_S_E || nextChar == ML_S_I || nextChar == ML_S_UE) && !isFVS(nextNextChar)) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_S_K_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_K_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_S_K_MEDI1);
				} else {
					var nextNextChar = ' ';
					if ( i < (lastIndex -1) ) {
						nextNextChar = word.charAt( i+2 );
					}
					if ( ( nextChar == ML_S_E || nextChar == ML_S_I || nextChar == ML_S_UE) && !isFVS(nextNextChar)) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(getConVowel(currentChar, FINA, nextChar));
						} else { // MEDI
							result.append(getConVowel(currentChar, MEDI, nextChar));
						}
						i++;
					} else {
						result.append(ML_S_K_MEDI);
					}
				}
			}
			break;

		case ML_S_G:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_S_G_ISOL1);
				} else {
					result.append(ML_S_G_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_S_G_INIT1);
				} else {
					var nextNextChar = ' ';
					if ( i < (lastIndex -1) ) {
						nextNextChar = word.charAt( i+2 );
					}
					if ( ( nextChar == ML_S_E || nextChar == ML_S_I || nextChar == ML_S_UE) && !isFVS(nextNextChar)) {
						if (i == (lastIndex - 1)) { // ISOL
							result.append(getConVowel(currentChar, ISOL, nextChar));
						} else { // INIT
							result.append(getConVowel(currentChar, INIT, nextChar));
						}
						i++;
					} else {
						result.append(ML_S_G_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_G_FINA);
			} else { // MEDI
				var nextNextChar = ' ';
				if ( i < (lastIndex -1) ) {
					nextNextChar = word.charAt( i+2 );
				}
				if ( ( nextChar == ML_S_E || nextChar == ML_S_I || nextChar == ML_S_UE) && !isFVS(nextNextChar)) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					result.append(ML_S_G_MEDI);
				}
			}
			break;

		case ML_S_H:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_S_H_ISOL1);
				} else {
					result.append(ML_S_H_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_S_H_INIT1);
				} else {
					var nextNextChar = ' ';
					if ( i < (lastIndex -1) ) {
						nextNextChar = word.charAt( i+2 );
					}
					if ( ( nextChar == ML_S_E || nextChar == ML_S_I || nextChar == ML_S_UE) && !isFVS(nextNextChar)) {
						if (i == (lastIndex - 1)) { // ISOL
							result.append(getConVowel(currentChar, ISOL, nextChar));
						} else { // INIT
							result.append(getConVowel(currentChar, INIT, nextChar));
						}
						i++;
					} else {
						result.append(ML_S_H_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_H_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_S_H_MEDI1);
				} else {
					var nextNextChar = ' ';
					if ( i < (lastIndex -1) ) {
						nextNextChar = word.charAt( i+2 );
					}
					if ( ( nextChar == ML_S_E || nextChar == ML_S_I || nextChar == ML_S_UE) && !isFVS(nextNextChar)) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(getConVowel(currentChar, FINA, nextChar));
						} else { // MEDI
							result.append(getConVowel(currentChar, MEDI, nextChar));
						}
						i++;
					} else {
						result.append(ML_S_H_MEDI);
					}
				}
			}
			break;

		case ML_S_P:
			if (ISOLATE) { // ISOL
				result.append(ML_S_P_ISOL);
			} else if (i == 0) { // INIT
				if (isSibeVowel(nextChar) || nextChar == ML_M_I) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_S_P_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_P_FINA);
			} else { // MEDI
				if (isSibeVowel(nextChar) || nextChar == ML_M_I) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					result.append(ML_S_P_MEDI);
				}
			}
			break;

		case ML_S_SH:
			if (ISOLATE) { // ISOL
				result.append(ML_S_SH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_SH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_SH_FINA);
			} else { // MEDI
				result.append(ML_S_SH_MEDI);
			}
			break;

		case ML_S_T:
			if (ISOLATE) { // ISOL
				if (nextChar == M_FVS1) {
					result.append(ML_S_T_ISOL1);
				} else {
					result.append(ML_S_T_ISOL);
				}
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_S_T_INIT1);
				} else {
					if ( nextChar == ML_A ||  nextChar == ML_S_I ||  nextChar == ML_O  ) {
						result.append(ML_S_T_INIT1);
					} else {
						result.append(ML_S_T_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_T_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_S_T_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_S_T_MEDI2);
				} else {
					if ( nextChar == ML_A ||  nextChar == ML_S_I ||  nextChar == ML_O  ) {
						result.append(ML_S_T_MEDI1);
					} else {
						result.append(ML_S_T_MEDI);
					}
				}
			}
			break;

		case ML_S_D:
			if (ISOLATE) { // ISOL
				result.append(ML_S_D_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_S_D_INIT1);
				} else {
					if ( nextChar == ML_S_E || nextChar == ML_S_UE  ) {
						result.append(ML_S_D_INIT1);
					} else {
						result.append(ML_S_D_INIT);
					}
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_D_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_S_D_MEDI1);
				} else {
					if ( nextChar == ML_A ||  nextChar == ML_S_I ||  nextChar == ML_O  ) {
						result.append(ML_S_D_MEDI1);
					} else {
						result.append(ML_S_D_MEDI);
					}
				}
			}
			break;

		case ML_S_J:
			if (ISOLATE) { // ISOL
				result.append(ML_S_J_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_J_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_J_FINA);
			} else { // MEDI
				result.append(ML_S_J_MEDI);
			}
			break;

		case ML_S_F:
			if (ISOLATE) { // ISOL
				result.append(ML_S_F_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_F_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_F_FINA);
			} else { // MEDI
				result.append(ML_S_F_MEDI);
			}
			break;

		case ML_S_GH:
			if (ISOLATE) { // ISOL
				result.append(ML_S_GH_ISOL);
			} else if (i == 0) { // INIT
				if ((nextChar == ML_A || nextChar == ML_O)) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_S_GH_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_GH_FINA);
			} else { // MEDI
				if ((nextChar == ML_A || nextChar == ML_O)) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					result.append(ML_S_GH_MEDI);
				}
			}
			break;

		case ML_S_HH:
			if (ISOLATE) { // ISOL
				result.append(ML_S_HH_ISOL);
			} else if (i == 0) { // INIT
				if ((nextChar == ML_A || nextChar == ML_O)) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_S_HH_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_HH_FINA);
			} else { // MEDI
				if ((nextChar == ML_A || nextChar == ML_O)) {
					if (i == (lastIndex - 1)) { // FINA
						result.append(getConVowel(currentChar, FINA, nextChar));
					} else { // MEDI
						result.append(getConVowel(currentChar, MEDI, nextChar));
					}
					i++;
				} else {
					result.append(ML_S_HH_MEDI);
				}
			}
			break;

		case ML_S_TS:
			if (ISOLATE) { // ISOL
				result.append(ML_S_TS_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_TS_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_TS_FINA);
			} else { // MEDI
				result.append(ML_S_TS_MEDI);
			}
			break;

		case ML_S_Z:
			if (ISOLATE) { // ISOL
				result.append(ML_S_Z_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == M_FVS1) {
					result.append(ML_S_Z_INIT1);
				} else {
					result.append(ML_S_Z_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_Z_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_S_Z_MEDI1);
				} else {
					result.append(ML_S_Z_MEDI);
				}
			}
			break;

		case ML_S_RH:
			if (ISOLATE) { // ISOL
				result.append(ML_S_RH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_RH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_RH_FINA);
			} else { // MEDI
				result.append(ML_S_RH_MEDI);
			}
			break;

		case ML_S_CH:
			if (ISOLATE) { // ISOL
				result.append(ML_S_CH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_CH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_CH_FINA);
			} else { // MEDI
				result.append(ML_S_CH_MEDI);
			}
			break;

		case ML_S_ZH:
			if (ISOLATE) { // ISOL
				result.append(ML_S_ZH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_S_ZH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_S_ZH_FINA);
			} else { // MEDI
				result.append(ML_S_ZH_MEDI);
			}
			break;

		case ML_M_I:
			if (ISOLATE) { // ISOL
				result.append(ML_M_I_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_M_I_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_M_I_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_M_I_FINA2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_M_I_FINA3);
				} else if (nextChar == M_MVS) {
					result.append(ML_M_I_FINA4);
				} else {
					result.append(ML_M_I_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_M_I_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_M_I_MEDI2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_M_I_MEDI3);
				} else {
					result.append(ML_M_I_MEDI);
				}
			}
			break;

		case ML_M_K:
			if (ISOLATE) { // ISOL
				result.append(ML_M_K_ISOL);
			} else if (i == 0) { // INIT
				if (nextChar == ML_S_E || nextChar == ML_M_I || nextChar == ML_S_UE) {
					if (i == (lastIndex - 1)) { // ISOL
						result.append(getConVowel(currentChar, ISOL, nextChar));
					} else { // INIT
						result.append(getConVowel(currentChar, INIT, nextChar));
					}
					i++;
				} else {
					result.append(ML_M_K_INIT);
				}
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				if (nextChar == M_FVS1) {
					result.append(ML_M_K_FINA1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_M_K_FINA2);
				} else {
					result.append(ML_M_K_FINA);
				}
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_M_K_MEDI1);
				} else if (nextChar == M_FVS2) {
					result.append(ML_M_K_MEDI2);
				} else if (nextChar == M_FVS3) {
					result.append(ML_M_K_MEDI3);
				} else {
					if (nextChar == ML_S_E || nextChar == ML_M_I || nextChar == ML_S_UE) {
						if (i == (lastIndex - 1)) { // FINA
							result.append(getConVowel(currentChar, FINA, nextChar));
						} else { // MEDI
							result.append(getConVowel(currentChar, MEDI, nextChar));
						}
						i++;
					} else {
						result.append(ML_M_K_MEDI);
					}
				}
			}
			break;

		case ML_M_R:
			if (ISOLATE) { // ISOL
				result.append(ML_M_R_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_M_R_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_M_R_FINA);
			} else { // MEDI
				result.append(ML_M_R_MEDI);
			}
			break;

		case ML_M_F:
			if (ISOLATE) { // ISOL
				result.append(ML_M_F_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_M_F_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_M_F_FINA);
			} else { // MEDI
				if (nextChar == M_FVS1) {
					result.append(ML_M_F_MEDI1);
				} else {
					result.append(ML_M_F_MEDI);
				}
			}
			break;

		case ML_M_ZH:
			if (ISOLATE) { // ISOL
				result.append(ML_M_ZH_ISOL);
			} else if (i == 0) { // INIT
				result.append(ML_M_ZH_INIT);
			} else if (isFinal(nextChar, i, lastIndex)) { // FINA
				result.append(ML_M_ZH_FINA);
			} else { // MEDI
				result.append(ML_M_ZH_MEDI);
			}
			break;

		default:
			result.append(currentChar);
			break;
		}
	}

	if (ae.length > 0) {
		result.append(ae);
	}

	return result.toString();
}

function isIsolate( word, lastIndex) {
	var result = false;
	if (lastIndex == 0 || (lastIndex == 1 && isFVS(word.charAt(lastIndex)))) {
		result = true;
	}
	return result;
}

function isFinal( nextChar, i, lastIndex) {
	var result = false;
	if (i == lastIndex) {
		result = true;
	} else if ((i == (lastIndex - 1)) && isFVS(nextChar)) {
		result = true;
	}
	return result;
}

function isFVS(ch) {
	var result = false;
	if (ch == M_FVS1 || ch == M_FVS2 || ch == M_FVS3) {
		result = true;
	}
	return result;
}

function isMongolian(ch) {
	var result = false;

	if ((ch >= '\u180A') && (ch <= '\u180E')) {
		result = true;
	} else if ((ch >= '\u1820') && (ch <= '\u1877')) {
		result = true;
	} else if (ch == ML_NNBS) {
		result = true;
	} else if (ch == ML_ZWJ) {
		result = true;
	}

	return result;
}

function isTodoVowel(ch) {
	var result = false;
	if (isTodoMVowel(ch) || isTodoFMVowel(ch) || ch == ML_T_I) {
		result = true;
	}
	return result;
}

function isTodoMVowel(ch) {
	var result = false;
	if (ch == ML_A || ch == ML_T_O || ch == ML_T_U) {
		result = true;
	}
	return result;
}

function isTodoFMVowel(ch) {
	var result = false;
	if (ch == ML_T_E || ch == ML_T_OE || ch == ML_T_UE) {
		result = true;
	}
	return result;
}

function isVowel(ch) {
	var result = false;
	if (isMVowel(ch) || isFMVowel(ch) || ch == ML_I) {
		result = true;
	}
	return result;
}

function isMVowel(ch) {
	var result = false;
	if (ch == ML_A || ch == ML_O || ch == ML_U) {
		result = true;
	}
	return result;
}

function isFMVowel(ch) {
	var result = false;
	if (ch == ML_E || ch == ML_OE || ch == ML_UE || ch == ML_EE) {
		result = true;
	}
	return result;
}

function isSibeVowel(ch) {
	var result = false;
	if (ch == ML_A || ch == ML_S_E || ch == ML_S_I || ch == ML_O || ch == ML_S_UE || ch == ML_S_U) {
		result = true;
	}
	return result;
}

function isGNML(ch) {
	var result = false;
	if (ch == ML_H || ch == ML_G || ch == ML_N || ch == ML_M || ch == ML_L) {
		result = true;
	}
	return result;
}

function isTodoNMLQG(ch) {
	var result = false;
	if (ch == ML_N || ch == ML_T_M || ch == ML_L | ch == ML_T_Q || ch == ML_T_G) {
		result = true;
	}
	return result;
}

function getConVowel( con, form, vowel) {
	var sb = new StringBuffer();
	switch (con) {
	case ML_B:
		sb.append(getBVowel(form, vowel));
		break;
	case ML_P:
		sb.append(getPVowel(form, vowel));
		break;
	case ML_H:
		sb.append(getHVowel(form, vowel));
		break;
	case ML_G:
		sb.append(getGVowel(form, vowel));
		break;
	case ML_F:
		sb.append(getFVowel(form, vowel));
		break;
	case ML_K:
		sb.append(getKVowel(form, vowel));
		break;
	case ML_KH:
		sb.append(getKHVowel(form, vowel));
		break;
	case ML_T_B:
		sb.append(getTBVowel(form, vowel));
		break;
	case ML_T_P:
		sb.append(getTPVowel(form, vowel));
		break;
	case ML_T_Q:
		sb.append(getTQVowel(form, vowel));
		break;
	case ML_T_G:
		sb.append(getTGVowel(form, vowel));
		break;
	case ML_T_K:
		sb.append(getTKVowel(form, vowel));
		break;
	case ML_T_GH:
		sb.append(getTGHVowel(form, vowel));
		break;
	case ML_S_K:
		sb.append(getSKVowel(form, vowel));
		break;
	case ML_S_G:
		sb.append(getSGVowel(form, vowel));
		break;
	case ML_S_H:
		sb.append(getSHVowel(form, vowel));
		break;
	case ML_S_P:
		sb.append(getSPVowel(form, vowel));
		break;
	case ML_S_GH:
		sb.append(getSGHVowel(form, vowel));
		break;
	case ML_S_HH:
		sb.append(getSHHVowel(form, vowel));
		break;
	case ML_M_K:
		sb.append(getMKVowel(form, vowel));
		break;
	}
	return sb.toString();
}

function getBVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_BA_ISOL);
			break;
		case ML_E:
			sb.append(ML_BE_ISOL);
			break;
		case ML_I:
			sb.append(ML_BI_ISOL);
			break;
		case ML_O:
			sb.append(ML_BO_ISOL);
			break;
		case ML_U:
			sb.append(ML_BU_ISOL);
			break;
		case ML_OE:
			sb.append(ML_BOE_ISOL);
			break;
		case ML_UE:
			sb.append(ML_BUE_ISOL);
			break;
		case ML_EE:
			sb.append(ML_BEE_ISOL);
			break;
		case ML_S_E:
			sb.append(ML_S_BE_ISOL);
			break;
		case ML_S_I:
			sb.append(ML_S_BI_ISOL);
			break;
		case ML_S_UE:
			sb.append(ML_S_BUE_ISOL);
			break;
		case ML_S_U:
			sb.append(ML_S_BU_ISOL);
			break;
		case ML_M_I:
			sb.append(ML_M_BI_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_BA_INIT);
			break;
		case ML_E:
			sb.append(ML_BE_INIT);
			break;
		case ML_I:
			sb.append(ML_BI_INIT);
			break;
		case ML_O:
			sb.append(ML_BO_INIT);
			break;
		case ML_U:
			sb.append(ML_BU_INIT);
			break;
		case ML_OE:
			sb.append(ML_BOE_INIT);
			break;
		case ML_UE:
			sb.append(ML_BUE_INIT);
			break;
		case ML_EE:
			sb.append(ML_BEE_INIT);
			break;
		case ML_S_E:
			sb.append(ML_S_BE_INIT);
			break;
		case ML_S_I:
			sb.append(ML_S_BI_INIT);
			break;
		case ML_S_UE:
			sb.append(ML_S_BUE_INIT);
			break;
		case ML_S_U:
			sb.append(ML_S_BU_INIT);
			break;
		case ML_M_I:
			sb.append(ML_M_BI_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_BA_MEDI);
			break;
		case ML_E:
			sb.append(ML_BE_MEDI);
			break;
		case ML_I:
			sb.append(ML_BI_MEDI);
			break;
		case ML_O:
			sb.append(ML_BO_MEDI);
			break;
		case ML_U:
			sb.append(ML_BU_MEDI);
			break;
		case ML_OE:
			sb.append(ML_BOE_MEDI);
			break;
		case ML_UE:
			sb.append(ML_BUE_MEDI);
			break;
		case ML_EE:
			sb.append(ML_BEE_MEDI);
			break;
		case ML_S_E:
			sb.append(ML_S_BE_MEDI);
			break;
		case ML_S_I:
			sb.append(ML_S_BI_MEDI);
			break;
		case ML_S_UE:
			sb.append(ML_S_BUE_MEDI);
			break;
		case ML_S_U:
			sb.append(ML_S_BU_MEDI);
			break;
		case ML_M_I:
			sb.append(ML_M_BI_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_BA_FINA);
			break;
		case ML_E:
			sb.append(ML_BE_FINA);
			break;
		case ML_I:
			sb.append(ML_BI_FINA);
			break;
		case ML_O:
			sb.append(ML_BO_FINA);
			break;
		case ML_U:
			sb.append(ML_BU_FINA);
			break;
		case ML_OE:
			sb.append(ML_BOE_FINA);
			break;
		case ML_UE:
			sb.append(ML_BUE_FINA);
			break;
		case ML_EE:
			sb.append(ML_BEE_FINA);
			break;
		case ML_S_E:
			sb.append(ML_S_BE_FINA);
			break;
		case ML_S_I:
			sb.append(ML_S_BI_FINA);
			break;
		case ML_S_UE:
			sb.append(ML_S_BUE_FINA);
			break;
		case ML_S_U:
			sb.append(ML_S_BU_FINA);
			break;
		case ML_M_I:
			sb.append(ML_M_BI_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getPVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_PA_ISOL);
			break;
		case ML_E:
			sb.append(ML_PE_ISOL);
			break;
		case ML_I:
			sb.append(ML_PI_ISOL);
			break;
		case ML_O:
			sb.append(ML_PO_ISOL);
			break;
		case ML_U:
			sb.append(ML_PU_ISOL);
			break;
		case ML_OE:
			sb.append(ML_POE_ISOL);
			break;
		case ML_UE:
			sb.append(ML_PUE_ISOL);
			break;
		case ML_EE:
			sb.append(ML_PEE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_PA_INIT);
			break;
		case ML_E:
			sb.append(ML_PE_INIT);
			break;
		case ML_I:
			sb.append(ML_PI_INIT);
			break;
		case ML_O:
			sb.append(ML_PO_INIT);
			break;
		case ML_U:
			sb.append(ML_PU_INIT);
			break;
		case ML_OE:
			sb.append(ML_POE_INIT);
			break;
		case ML_UE:
			sb.append(ML_PUE_INIT);
			break;
		case ML_EE:
			sb.append(ML_PEE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_PA_MEDI);
			break;
		case ML_E:
			sb.append(ML_PE_MEDI);
			break;
		case ML_I:
			sb.append(ML_PI_MEDI);
			break;
		case ML_O:
			sb.append(ML_PO_MEDI);
			break;
		case ML_U:
			sb.append(ML_PU_MEDI);
			break;
		case ML_OE:
			sb.append(ML_POE_MEDI);
			break;
		case ML_UE:
			sb.append(ML_PUE_MEDI);
			break;
		case ML_EE:
			sb.append(ML_PEE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_PA_FINA);
			break;
		case ML_E:
			sb.append(ML_PE_FINA);
			break;
		case ML_I:
			sb.append(ML_PI_FINA);
			break;
		case ML_O:
			sb.append(ML_PO_FINA);
			break;
		case ML_U:
			sb.append(ML_PU_FINA);
			break;
		case ML_OE:
			sb.append(ML_POE_FINA);
			break;
		case ML_UE:
			sb.append(ML_PUE_FINA);
			break;
		case ML_EE:
			sb.append(ML_PEE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getHVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_E:
			sb.append(ML_HE_ISOL);
			break;
		case ML_I:
			sb.append(ML_HI_ISOL);
			break;
		case ML_OE:
			sb.append(ML_HOE_ISOL);
			break;
		case ML_UE:
			sb.append(ML_HUE_ISOL);
			break;
		case ML_EE:
			sb.append(ML_HEE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_E:
			sb.append(ML_HE_INIT);
			break;
		case ML_I:
			sb.append(ML_HI_INIT);
			break;
		case ML_OE:
			sb.append(ML_HOE_INIT);
			break;
		case ML_UE:
			sb.append(ML_HUE_INIT);
			break;
		case ML_EE:
			sb.append(ML_HEE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_E:
			sb.append(ML_HE_MEDI);
			break;
		case ML_I:
			sb.append(ML_HI_MEDI);
			break;
		case ML_OE:
			sb.append(ML_HOE_MEDI);
			break;
		case ML_UE:
			sb.append(ML_HUE_MEDI);
			break;
		case ML_EE:
			sb.append(ML_HEE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_E:
			sb.append(ML_HE_FINA);
			break;
		case ML_I:
			sb.append(ML_HI_FINA);
			break;
		case ML_OE:
			sb.append(ML_HOE_FINA);
			break;
		case ML_UE:
			sb.append(ML_HUE_FINA);
			break;
		case ML_EE:
			sb.append(ML_HEE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getGVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_E:
			sb.append(ML_GE_ISOL);
			break;
		case ML_I:
			sb.append(ML_GI_ISOL);
			break;
		case ML_OE:
			sb.append(ML_GOE_ISOL);
			break;
		case ML_UE:
			sb.append(ML_GUE_ISOL);
			break;
		case ML_EE:
			sb.append(ML_GEE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_E:
			sb.append(ML_GE_INIT);
			break;
		case ML_I:
			sb.append(ML_GI_INIT);
			break;
		case ML_OE:
			sb.append(ML_GOE_INIT);
			break;
		case ML_UE:
			sb.append(ML_GUE_INIT);
			break;
		case ML_EE:
			sb.append(ML_GEE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_E:
			sb.append(ML_GE_MEDI);
			break;
		case ML_I:
			sb.append(ML_GI_MEDI);
			break;
		case ML_OE:
			sb.append(ML_GOE_MEDI);
			break;
		case ML_UE:
			sb.append(ML_GUE_MEDI);
			break;
		case ML_EE:
			sb.append(ML_GEE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_E:
			sb.append(ML_GE_FINA);
			break;
		case ML_I:
			sb.append(ML_GI_FINA);
			break;
		case ML_OE:
			sb.append(ML_GOE_FINA);
			break;
		case ML_UE:
			sb.append(ML_GUE_FINA);
			break;
		case ML_EE:
			sb.append(ML_GEE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getFVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_FA_ISOL);
			break;
		case ML_E:
			sb.append(ML_FE_ISOL);
			break;
		case ML_I:
			sb.append(ML_FI_ISOL);
			break;
		case ML_O:
			sb.append(ML_FO_ISOL);
			break;
		case ML_U:
			sb.append(ML_FU_ISOL);
			break;
		case ML_OE:
			sb.append(ML_FOE_ISOL);
			break;
		case ML_UE:
			sb.append(ML_FUE_ISOL);
			break;
		case ML_EE:
			sb.append(ML_FEE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_FA_INIT);
			break;
		case ML_E:
			sb.append(ML_FE_INIT);
			break;
		case ML_I:
			sb.append(ML_FI_INIT);
			break;
		case ML_O:
			sb.append(ML_FO_INIT);
			break;
		case ML_U:
			sb.append(ML_FU_INIT);
			break;
		case ML_OE:
			sb.append(ML_FOE_INIT);
			break;
		case ML_UE:
			sb.append(ML_FUE_INIT);
			break;
		case ML_EE:
			sb.append(ML_FEE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_FA_MEDI);
			break;
		case ML_E:
			sb.append(ML_FE_MEDI);
			break;
		case ML_I:
			sb.append(ML_FI_MEDI);
			break;
		case ML_O:
			sb.append(ML_FO_MEDI);
			break;
		case ML_U:
			sb.append(ML_FU_MEDI);
			break;
		case ML_OE:
			sb.append(ML_FOE_MEDI);
			break;
		case ML_UE:
			sb.append(ML_FUE_MEDI);
			break;
		case ML_EE:
			sb.append(ML_FEE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_FA_FINA);
			break;
		case ML_E:
			sb.append(ML_FE_FINA);
			break;
		case ML_I:
			sb.append(ML_FI_FINA);
			break;
		case ML_O:
			sb.append(ML_FO_FINA);
			break;
		case ML_U:
			sb.append(ML_FU_FINA);
			break;
		case ML_OE:
			sb.append(ML_FOE_FINA);
			break;
		case ML_UE:
			sb.append(ML_FUE_FINA);
			break;
		case ML_EE:
			sb.append(ML_FEE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getKVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_KA_ISOL);
			break;
		case ML_E:
			sb.append(ML_KE_ISOL);
			break;
		case ML_I:
			sb.append(ML_KI_ISOL);
			break;
		case ML_O:
			sb.append(ML_KO_ISOL);
			break;
		case ML_U:
			sb.append(ML_KU_ISOL);
			break;
		case ML_OE:
			sb.append(ML_KOE_ISOL);
			break;
		case ML_UE:
			sb.append(ML_KUE_ISOL);
			break;
		case ML_EE:
			sb.append(ML_KEE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_KA_INIT);
			break;
		case ML_E:
			sb.append(ML_KE_INIT);
			break;
		case ML_I:
			sb.append(ML_KI_INIT);
			break;
		case ML_O:
			sb.append(ML_KO_INIT);
			break;
		case ML_U:
			sb.append(ML_KU_INIT);
			break;
		case ML_OE:
			sb.append(ML_KOE_INIT);
			break;
		case ML_UE:
			sb.append(ML_KUE_INIT);
			break;
		case ML_EE:
			sb.append(ML_KEE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_KA_MEDI);
			break;
		case ML_E:
			sb.append(ML_KE_MEDI);
			break;
		case ML_I:
			sb.append(ML_KI_MEDI);
			break;
		case ML_O:
			sb.append(ML_KO_MEDI);
			break;
		case ML_U:
			sb.append(ML_KU_MEDI);
			break;
		case ML_OE:
			sb.append(ML_KOE_MEDI);
			break;
		case ML_UE:
			sb.append(ML_KUE_MEDI);
			break;
		case ML_EE:
			sb.append(ML_KEE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_KA_FINA);
			break;
		case ML_E:
			sb.append(ML_KE_FINA);
			break;
		case ML_I:
			sb.append(ML_KI_FINA);
			break;
		case ML_O:
			sb.append(ML_KO_FINA);
			break;
		case ML_U:
			sb.append(ML_KU_FINA);
			break;
		case ML_OE:
			sb.append(ML_KOE_FINA);
			break;
		case ML_UE:
			sb.append(ML_KUE_FINA);
			break;
		case ML_EE:
			sb.append(ML_KEE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getKHVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_KHA_ISOL);
			break;
		case ML_E:
			sb.append(ML_KHE_ISOL);
			break;
		case ML_I:
			sb.append(ML_KHI_ISOL);
			break;
		case ML_O:
			sb.append(ML_KHO_ISOL);
			break;
		case ML_U:
			sb.append(ML_KHU_ISOL);
			break;
		case ML_OE:
			sb.append(ML_KHOE_ISOL);
			break;
		case ML_UE:
			sb.append(ML_KHUE_ISOL);
			break;
		case ML_EE:
			sb.append(ML_KHEE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_KHA_INIT);
			break;
		case ML_E:
			sb.append(ML_KHE_INIT);
			break;
		case ML_I:
			sb.append(ML_KHI_INIT);
			break;
		case ML_O:
			sb.append(ML_KHO_INIT);
			break;
		case ML_U:
			sb.append(ML_KHU_INIT);
			break;
		case ML_OE:
			sb.append(ML_KHOE_INIT);
			break;
		case ML_UE:
			sb.append(ML_KHUE_INIT);
			break;
		case ML_EE:
			sb.append(ML_KHEE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_KHA_MEDI);
			break;
		case ML_E:
			sb.append(ML_KHE_MEDI);
			break;
		case ML_I:
			sb.append(ML_KHI_MEDI);
			break;
		case ML_O:
			sb.append(ML_KHO_MEDI);
			break;
		case ML_U:
			sb.append(ML_KHU_MEDI);
			break;
		case ML_OE:
			sb.append(ML_KHOE_MEDI);
			break;
		case ML_UE:
			sb.append(ML_KHUE_MEDI);
			break;
		case ML_EE:
			sb.append(ML_KHEE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_KHA_FINA);
			break;
		case ML_E:
			sb.append(ML_KHE_FINA);
			break;
		case ML_I:
			sb.append(ML_KHI_FINA);
			break;
		case ML_O:
			sb.append(ML_KHO_FINA);
			break;
		case ML_U:
			sb.append(ML_KHU_FINA);
			break;
		case ML_OE:
			sb.append(ML_KHOE_FINA);
			break;
		case ML_UE:
			sb.append(ML_KHUE_FINA);
			break;
		case ML_EE:
			sb.append(ML_KHEE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getTBVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_BA_ISOL);
			break;
		case ML_T_E:
			sb.append(ML_T_BE_ISOL);
			break;
		case ML_T_I:
			sb.append(ML_T_BI_ISOL);
			break;
		case ML_T_O:
			sb.append(ML_T_BO_ISOL);
			break;
		case ML_T_U:
			sb.append(ML_T_BU_ISOL);
			break;
		case ML_T_OE:
			sb.append(ML_T_BOE_ISOL);
			break;
		case ML_T_UE:
			sb.append(ML_T_BUE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_BA_INIT);
			break;
		case ML_T_E:
			sb.append(ML_T_BE_INIT);
			break;
		case ML_T_I:
			sb.append(ML_T_BI_INIT);
			break;
		case ML_T_O:
			sb.append(ML_T_BO_INIT);
			break;
		case ML_T_U:
			sb.append(ML_T_BU_INIT);
			break;
		case ML_T_OE:
			sb.append(ML_T_BOE_INIT);
			break;
		case ML_T_UE:
			sb.append(ML_T_BUE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_BA_MEDI);
			break;
		case ML_T_E:
			sb.append(ML_T_BE_MEDI);
			break;
		case ML_T_I:
			sb.append(ML_T_BI_MEDI);
			break;
		case ML_T_O:
			sb.append(ML_T_BO_MEDI);
			break;
		case ML_T_U:
			sb.append(ML_T_BU_MEDI);
			break;
		case ML_T_OE:
			sb.append(ML_T_BOE_MEDI);
			break;
		case ML_T_UE:
			sb.append(ML_T_BUE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_BA_FINA);
			break;
		case ML_T_E:
			sb.append(ML_T_BE_FINA);
			break;
		case ML_T_I:
			sb.append(ML_T_BI_FINA);
			break;
		case ML_T_O:
			sb.append(ML_T_BO_FINA);
			break;
		case ML_T_U:
			sb.append(ML_T_BU_FINA);
			break;
		case ML_T_OE:
			sb.append(ML_T_BOE_FINA);
			break;
		case ML_T_UE:
			sb.append(ML_T_BUE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getTPVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_PA_ISOL);
			break;
		case ML_T_E:
			sb.append(ML_T_PE_ISOL);
			break;
		case ML_T_I:
			sb.append(ML_T_PI_ISOL);
			break;
		case ML_T_O:
			sb.append(ML_T_PO_ISOL);
			break;
		case ML_T_U:
			sb.append(ML_T_PU_ISOL);
			break;
		case ML_T_OE:
			sb.append(ML_T_POE_ISOL);
			break;
		case ML_T_UE:
			sb.append(ML_T_PUE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_PA_INIT);
			break;
		case ML_T_E:
			sb.append(ML_T_PE_INIT);
			break;
		case ML_T_I:
			sb.append(ML_T_PI_INIT);
			break;
		case ML_T_O:
			sb.append(ML_T_PO_INIT);
			break;
		case ML_T_U:
			sb.append(ML_T_PU_INIT);
			break;
		case ML_T_OE:
			sb.append(ML_T_POE_INIT);
			break;
		case ML_T_UE:
			sb.append(ML_T_PUE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_PA_MEDI);
			break;
		case ML_T_E:
			sb.append(ML_T_PE_MEDI);
			break;
		case ML_T_I:
			sb.append(ML_T_PI_MEDI);
			break;
		case ML_T_O:
			sb.append(ML_T_PO_MEDI);
			break;
		case ML_T_U:
			sb.append(ML_T_PU_MEDI);
			break;
		case ML_T_OE:
			sb.append(ML_T_POE_MEDI);
			break;
		case ML_T_UE:
			sb.append(ML_T_PUE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_PA_FINA);
			break;
		case ML_T_E:
			sb.append(ML_T_PE_FINA);
			break;
		case ML_T_I:
			sb.append(ML_T_PI_FINA);
			break;
		case ML_T_O:
			sb.append(ML_T_PO_FINA);
			break;
		case ML_T_U:
			sb.append(ML_T_PU_FINA);
			break;
		case ML_T_OE:
			sb.append(ML_T_POE_FINA);
			break;
		case ML_T_UE:
			sb.append(ML_T_PUE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getTQVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_T_E:
			sb.append(ML_T_QE_ISOL);
			break;
		case ML_T_I:
			sb.append(ML_T_QI_ISOL);
			break;
		case ML_T_OE:
			sb.append(ML_T_QOE_ISOL);
			break;
		case ML_T_UE:
			sb.append(ML_T_QUE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_T_E:
			sb.append(ML_T_QE_INIT);
			break;
		case ML_T_I:
			sb.append(ML_T_QI_INIT);
			break;
		case ML_T_OE:
			sb.append(ML_T_QOE_INIT);
			break;
		case ML_T_UE:
			sb.append(ML_T_QUE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_T_E:
			sb.append(ML_T_QE_MEDI);
			break;
		case ML_T_I:
			sb.append(ML_T_QI_MEDI);
			break;
		case ML_T_OE:
			sb.append(ML_T_QOE_MEDI);
			break;
		case ML_T_UE:
			sb.append(ML_T_QUE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_T_E:
			sb.append(ML_T_QE_FINA);
			break;
		case ML_T_I:
			sb.append(ML_T_QI_FINA);
			break;
		case ML_T_OE:
			sb.append(ML_T_QOE_FINA);
			break;
		case ML_T_UE:
			sb.append(ML_T_QUE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getTGVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_T_E:
			sb.append(ML_T_GE_ISOL);
			break;
		case ML_T_I:
			sb.append(ML_T_GI_ISOL);
			break;
		case ML_T_OE:
			sb.append(ML_T_GOE_ISOL);
			break;
		case ML_T_UE:
			sb.append(ML_T_GUE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_T_E:
			sb.append(ML_T_GE_INIT);
			break;
		case ML_T_I:
			sb.append(ML_T_GI_INIT);
			break;
		case ML_T_OE:
			sb.append(ML_T_GOE_INIT);
			break;
		case ML_T_UE:
			sb.append(ML_T_GUE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_T_E:
			sb.append(ML_T_GE_MEDI);
			break;
		case ML_T_I:
			sb.append(ML_T_GI_MEDI);
			break;
		case ML_T_OE:
			sb.append(ML_T_GOE_MEDI);
			break;
		case ML_T_UE:
			sb.append(ML_T_GUE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_T_E:
			sb.append(ML_T_GE_FINA);
			break;
		case ML_T_I:
			sb.append(ML_T_GI_FINA);
			break;
		case ML_T_OE:
			sb.append(ML_T_GOE_FINA);
			break;
		case ML_T_UE:
			sb.append(ML_T_GUE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getTKVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_KA_ISOL);
			break;
		case ML_T_O:
			sb.append(ML_T_KO_ISOL);
			break;
		case ML_T_U:
			sb.append(ML_T_KU_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_KA_INIT);
			break;
		case ML_T_O:
			sb.append(ML_T_KO_INIT);
			break;
		case ML_T_U:
			sb.append(ML_T_KU_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_KA_MEDI);
			break;
		case ML_T_O:
			sb.append(ML_T_KO_MEDI);
			break;
		case ML_T_U:
			sb.append(ML_T_KU_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_KA_FINA);
			break;
		case ML_T_O:
			sb.append(ML_T_KO_FINA);
			break;
		case ML_T_U:
			sb.append(ML_T_KU_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getTGHVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_GHA_ISOL);
			break;
		case ML_T_O:
			sb.append(ML_T_GHO_ISOL);
			break;
		case ML_T_U:
			sb.append(ML_T_GHU_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_GHA_INIT);
			break;
		case ML_T_O:
			sb.append(ML_T_GHO_INIT);
			break;
		case ML_T_U:
			sb.append(ML_T_GHU_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_GHA_MEDI);
			break;
		case ML_T_O:
			sb.append(ML_T_GHO_MEDI);
			break;
		case ML_T_U:
			sb.append(ML_T_GHU_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_T_GHA_FINA);
			break;
		case ML_T_O:
			sb.append(ML_T_GHO_FINA);
			break;
		case ML_T_U:
			sb.append(ML_T_GHU_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getSKVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_KE_ISOL);
			break;
		case ML_S_I:
			sb.append(ML_S_KI_ISOL);
			break;
		case ML_S_UE:
			sb.append(ML_S_KUE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_KE_INIT);
			break;
		case ML_S_I:
			sb.append(ML_S_KI_INIT);
			break;
		case ML_S_UE:
			sb.append(ML_S_KUE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_KE_MEDI);
			break;
		case ML_S_I:
			sb.append(ML_S_KI_MEDI);
			break;
		case ML_S_UE:
			sb.append(ML_S_KUE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_KE_FINA);
			break;
		case ML_S_I:
			sb.append(ML_S_KI_FINA);
			break;
		case ML_S_UE:
			sb.append(ML_S_KUE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getSGVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_GE_ISOL);
			break;
		case ML_S_I:
			sb.append(ML_S_GI_ISOL);
			break;
		case ML_M_I:
			sb.append(ML_M_GI_ISOL);
			break;
		case ML_S_UE:
			sb.append(ML_S_GUE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_GE_INIT);
			break;
		case ML_S_I:
			sb.append(ML_S_GI_INIT);
			break;
		case ML_M_I:
			sb.append(ML_M_GI_INIT);
			break;
		case ML_S_UE:
			sb.append(ML_S_GUE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_GE_MEDI);
			break;
		case ML_S_I:
			sb.append(ML_S_GI_MEDI);
			break;
		case ML_M_I:
			sb.append(ML_M_GI_MEDI);
			break;
		case ML_S_UE:
			sb.append(ML_S_GUE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_GE_FINA);
			break;
		case ML_S_I:
			sb.append(ML_S_GI_FINA);
			break;
		case ML_M_I:
			sb.append(ML_M_GI_FINA);
			break;
		case ML_S_UE:
			sb.append(ML_S_GUE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getSHVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_HE_ISOL);
			break;
		case ML_S_I:
		case ML_M_I:
			sb.append(ML_S_HI_ISOL);
			break;
		case ML_S_UE:
			sb.append(ML_S_HUE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_HE_INIT);
			break;
		case ML_S_I:
		case ML_M_I:
			sb.append(ML_S_HI_INIT);
			break;
		case ML_S_UE:
			sb.append(ML_S_HUE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_HE_MEDI);
			break;
		case ML_S_I:
		case ML_M_I:
			sb.append(ML_S_HI_MEDI);
			break;
		case ML_S_UE:
			sb.append(ML_S_HUE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_S_HE_FINA);
			break;
		case ML_S_I:
		case ML_M_I:
			sb.append(ML_S_HI_FINA);
			break;
		case ML_S_UE:
			sb.append(ML_S_HUE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getSPVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_PA_ISOL);
			break;
		case ML_S_E:
			sb.append(ML_S_PE_ISOL);
			break;
		case ML_S_I:
			sb.append(ML_S_PI_ISOL);
			break;
		case ML_O:
			sb.append(ML_S_PO_ISOL);
			break;
		case ML_S_UE:
			sb.append(ML_S_PUE_ISOL);
			break;
		case ML_S_U:
			sb.append(ML_S_PU_ISOL);
			break;
		case ML_M_I:
			sb.append(ML_M_PI_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_PA_INIT);
			break;
		case ML_S_E:
			sb.append(ML_S_PE_INIT);
			break;
		case ML_S_I:
			sb.append(ML_S_PI_INIT);
			break;
		case ML_O:
			sb.append(ML_S_PO_INIT);
			break;
		case ML_S_UE:
			sb.append(ML_S_PUE_INIT);
			break;
		case ML_S_U:
			sb.append(ML_S_PU_INIT);
			break;
		case ML_M_I:
			sb.append(ML_M_PI_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_PA_MEDI);
			break;
		case ML_S_E:
			sb.append(ML_S_PE_MEDI);
			break;
		case ML_S_I:
			sb.append(ML_S_PI_MEDI);
			break;
		case ML_O:
			sb.append(ML_S_PO_MEDI);
			break;
		case ML_S_UE:
			sb.append(ML_S_PUE_MEDI);
			break;
		case ML_S_U:
			sb.append(ML_S_PU_MEDI);
			break;
		case ML_M_I:
			sb.append(ML_M_PI_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_PA_FINA);
			break;
		case ML_S_E:
			sb.append(ML_S_PE_FINA);
			break;
		case ML_S_I:
			sb.append(ML_S_PI_FINA);
			break;
		case ML_O:
			sb.append(ML_S_PO_FINA);
			break;
		case ML_S_UE:
			sb.append(ML_S_PUE_FINA);
			break;
		case ML_S_U:
			sb.append(ML_S_PU_FINA);
			break;
		case ML_M_I:
			sb.append(ML_M_PI_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getSGHVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_GHA_ISOL);
			break;
		case ML_O:
			sb.append(ML_S_GHO_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_GHA_INIT);
			break;
		case ML_O:
			sb.append(ML_S_GHO_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_GHA_MEDI);
			break;
		case ML_O:
			sb.append(ML_S_GHO_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_GHA_FINA);
			break;
		case ML_O:
			sb.append(ML_S_GHO_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getSHHVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_HHA_ISOL);
			break;
		case ML_O:
			sb.append(ML_S_HHO_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_HHA_INIT);
			break;
		case ML_O:
			sb.append(ML_S_HHO_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_HHA_MEDI);
			break;
		case ML_O:
			sb.append(ML_S_HHO_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_A:
			sb.append(ML_S_HHA_FINA);
			break;
		case ML_O:
			sb.append(ML_S_HHO_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getMKVowel( form, vowel) {
	var sb = new StringBuffer();
	switch (form) {
	case ISOL:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_M_KE_ISOL);
			break;
		case ML_M_I:
			sb.append(ML_M_KI_ISOL);
			break;
		case ML_S_UE:
			sb.append(ML_M_KUE_ISOL);
			break;
		}
		break;
	case INIT:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_M_KE_INIT);
			break;
		case ML_M_I:
			sb.append(ML_M_KI_INIT);
			break;
		case ML_S_UE:
			sb.append(ML_M_KUE_INIT);
			break;
		}
		break;
	case MEDI:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_M_KE_MEDI);
			break;
		case ML_M_I:
			sb.append(ML_M_KI_MEDI);
			break;
		case ML_S_UE:
			sb.append(ML_M_KUE_MEDI);
			break;
		}
		break;
	case FINA:
		switch (vowel) {
		case ML_S_E:
			sb.append(ML_M_KE_FINA);
			break;
		case ML_M_I:
			sb.append(ML_M_KI_FINA);
			break;
		case ML_S_UE:
			sb.append(ML_M_KUE_FINA);
			break;
		}
		break;
	}
	return sb.toString();
}

function getConCon(currentChar, nextChar, form) {
	var sb = new StringBuffer();
	switch (currentChar) {
	case ML_NG:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_NGN_MEDI);
			break;
		case ML_H:
			if (form == MEDI) {
				sb.append(ML_NGH_MEDI);
			} else {
				sb.append(ML_NGH_FINA);
			}
			break;
		case ML_G:
			if (form == MEDI) {
				sb.append(ML_NGG_MEDI);
			} else {
				sb.append(ML_NGG_FINA);
			}
			break;
		case ML_M:
			sb.append(ML_NGM_MEDI);
			break;
		case ML_L:
			sb.append(ML_NGL_MEDI);
			break;
		case ML_M_K:
			sb.append(ML_S_NGK_MEDI);
			break;
		case ML_S_G:
			sb.append(ML_S_NGG_MEDI);
			break;
		case ML_S_H:
			sb.append(ML_S_NGH_MEDI);
			break;
		}
		break;
	case ML_B:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_BN_MEDI);
			break;
		case ML_H:
			sb.append(ML_BH_MEDI);
			break;
		case ML_G:
			sb.append(ML_BG_MEDI);
			break;
		case ML_M:
			sb.append(ML_BM_MEDI);
			break;
		case ML_L:
			sb.append(ML_BL_MEDI);
			break;
		}
		break;
	case ML_P:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_PN_MEDI);
			break;
		case ML_H:
			sb.append(ML_PH_MEDI);
			break;
		case ML_G:
			sb.append(ML_PG_MEDI);
			break;
		case ML_M:
			sb.append(ML_PM_MEDI);
			break;
		case ML_L:
			sb.append(ML_PL_MEDI);
			break;
		}
		break;
	case ML_G:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_GN_MEDI);
			break;
		// case ML_G:
		// sb.append(ML_GG_MEDI);
		// break;
		case ML_M:
			sb.append(ML_GM_MEDI);
			break;
		case ML_L:
			sb.append(ML_GL_MEDI);
			break;
		}
		break;
	case ML_M:
		switch (nextChar) {
		case ML_M:
			sb.append(ML_MM_MEDI);
			break;
		case ML_L:
			sb.append(ML_ML_MEDI);
			break;
		default:
			sb.append(nextChar);
			break;
		}
		break;
	case ML_L:
		switch (nextChar) {
		case ML_L:
			sb.append(ML_LL_MEDI);
			break;
		default:
			sb.append(nextChar);
			break;
		}
		break;
	case ML_F:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_FN_MEDI);
			break;
		case ML_H:
			sb.append(ML_FH_MEDI);
			break;
		case ML_G:
			sb.append(ML_FG_MEDI);
			break;
		case ML_M:
			sb.append(ML_FM_MEDI);
			break;
		case ML_L:
			sb.append(ML_FL_MEDI);
			break;
		}
		break;
	case ML_K:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_KN_MEDI);
			break;
		case ML_H:
			sb.append(ML_KH_MEDI_);
			break;
		case ML_G:
			sb.append(ML_KG_MEDI);
			break;
		case ML_M:
			sb.append(ML_KM_MEDI);
			break;
		case ML_L:
			sb.append(ML_KL_MEDI);
			break;
		}
		break;
	case ML_KH:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_KHN_MEDI);
			break;
		case ML_H:
			sb.append(ML_KHH_MEDI);
			break;
		case ML_G:
			sb.append(ML_KHG_MEDI);
			break;
		case ML_M:
			sb.append(ML_KHM_MEDI);
			break;
		case ML_L:
			sb.append(ML_KHL_MEDI);
			break;
		}
		break;
	case ML_T_B:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_T_BN_MEDI);
			break;
		case ML_M:
			sb.append(ML_T_BM_MEDI);
			break;
		case ML_L:
			sb.append(ML_T_BL_MEDI);
			break;
		case ML_T_Q:
			sb.append(ML_T_BQ_MEDI);
			break;
		case ML_T_G:
			sb.append(ML_T_BG_MEDI);
			break;
		}
		break;
	case ML_T_NG:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_T_NGN_MEDI);
			break;
		case ML_T_M:
			sb.append(ML_T_NGM_MEDI);
			break;
		case ML_L:
			sb.append(ML_T_NGL_MEDI);
			break;
		case ML_T_Q:
			sb.append(ML_T_NGQ_MEDI);
			break;
		case ML_T_G:
			if (form == MEDI) {
				sb.append(ML_T_NGG_MEDI);
			} else {
				sb.append(ML_T_NGG_FINA);
			}
			break;
		}
		break;
	case ML_T_G:
		switch (nextChar) {
		case ML_N:
			sb.append(ML_T_GN_MEDI);
			break;
		case ML_T_M:
			sb.append(ML_T_GM_MEDI);
			break;
		case ML_L:
			sb.append(ML_T_GL_MEDI);
			break;
		}
		break;
	case ML_T_M:
		switch (nextChar) {
		case ML_T_M:
			sb.append(ML_T_MM_MEDI);
			break;
		case ML_L:
			sb.append(ML_T_ML_MEDI);
			break;
		default:
			sb.append(nextChar);
			break;
		}
		break;
	}
	return sb.toString();
}

function reverse( text ) {
	var sb = new StringBuffer();
	var len = text.length();
	for (var i = 0; i < len; i++) {
		var ch = text.charAt(i);
		switch( ch ) {
		case M_EXCLAM_ISOL:
			sb.append( EXCLAM );
			break;

		case M_EXCLAM_QUESTION_ISOL:
			sb.append( EXCLAM );
			sb.append( QUESTION );
			break;

		case M_COMMA_ISOL1:
			sb.append( COMMA );
			break;

		case M_QUESTION_ISOL:
			sb.append( QUESTION );
			break;

		case M_QUESTION_EXCLAM_ISOL:
			sb.append( QUESTION );
			sb.append( EXCLAM );
			break;

//			COLON = '\u003A';

		case M_SEMICOLON_ISOL:
			sb.append( SEMICOLON );
			break;

		case M_BIRGA1:
		case M_BIRGA2:
		case M_BIRGA3:
//		case M_BIRGA4:
			sb.append( M_BIRGA );
			break;

		case M_ELLIPSIS_ISOL:
			sb.append( M_ELLIPSIS );
			break;

		case M_COMMA_ISOL:
			sb.append( M_COMMA );
			break;

		case M_FULL_STOP_ISOL:
			sb.append( M_FULL_STOP );
			break;

		case M_COLON_ISOL:
			sb.append( M_COLON );
			break;

		case M_FOUR_DOTS_ISOL:
			sb.append( M_FOUR_DOTS );
			break;

		case M_TODO_HYPHEN_ISOL:
			sb.append( M_TODO_HYPHEN );
			break;

		case M_SIBE_SBM_ISOL:
			sb.append( M_SIBE_SBM );
			break;

		case M_MANCHU_COMMA_ISOL:
			sb.append( M_MANCHU_COMMA );
			break;

		case M_NIRUGU_ISOL:
			sb.append( M_NIRUGU );
			break;

		case M_MANCHU_FULL_STOP_ISOL:
			sb.append( M_MANCHU_FULL_STOP );
			break;

		case M_FVS1_ISOL:
			sb.append( M_FVS1 );
			break;

		case M_FVS2_ISOL:
			sb.append( M_FVS2 );
			break;

		case M_FVS3_ISOL:
			sb.append( M_FVS3 );
			break;

		case M_MVS_ISOL:
			sb.append( M_MVS );
			break;

		case M_DIGIT_ZERO_ISOL:
			sb.append( M_DIGIT_ZERO );
			break;

		case M_DIGIT_ONE_ISOL:
			sb.append( M_DIGIT_ONE );
			break;

		case M_DIGIT_TWO_ISOL:
			sb.append( M_DIGIT_TWO );
			break;

		case M_DIGIT_THREE_ISOL:
			sb.append( M_DIGIT_THREE );
			break;

		case M_DIGIT_FOUR_ISOL:
			sb.append( M_DIGIT_FOUR );
			break;

		case M_DIGIT_FIVE_ISOL:
			sb.append( M_DIGIT_FIVE );
			break;

		case M_DIGIT_SIX_ISOL:
			sb.append( M_DIGIT_SIX );
			break;

		case M_DIGIT_SEVEN_ISOL:
			sb.append( M_DIGIT_SEVEN );
			break;

		case M_DIGIT_EIGHT_ISOL:
			sb.append( M_DIGIT_EIGHT );
			break;

		case M_DIGIT_NINE_ISOL:
			sb.append( M_DIGIT_NINE );
			break;

		case ML_A_ISOL:
		case ML_A_ISOL1:
		case ML_A_INIT:
		case ML_A_INIT1:
		case ML_A_MEDI:
		case ML_A_MEDI1:
		case ML_A_FINA:
		case ML_A_FINA1:
		case ML_A_FINA2:
			sb.append( ML_A );
			break;

		case ML_E_ISOL:
		case ML_E_ISOL1:
		case ML_E_INIT:
		case ML_E_INIT1:
		case ML_E_MEDI:
		case ML_E_FINA:
		case ML_E_FINA1:
		case ML_E_FINA2:
			sb.append( ML_E );
			break;

		case ML_I_ISOL:
		case ML_I_ISOL1:
		case ML_I_INIT:
		case ML_I_INIT1:
		case ML_I_MEDI:
		case ML_I_MEDI1:
		case ML_I_MEDI2:
		case ML_I_FINA:
			sb.append( ML_I );
			break;

		case ML_O_ISOL:
		case ML_O_ISOL1:
		case ML_O_INIT:
		case ML_O_INIT1:
		case ML_O_MEDI:
		case ML_O_MEDI1:
		case ML_O_FINA:
		case ML_O_FINA1:
			sb.append( ML_O );
			break;

		case ML_U_ISOL:
		case ML_U_ISOL1:
		case ML_U_INIT:
		case ML_U_INIT1:
		case ML_U_MEDI:
		case ML_U_MEDI1:
		case ML_U_FINA:
		case ML_U_FINA1:
			sb.append( ML_U );
			break;

		case ML_OE_ISOL:
		case ML_OE_ISOL1:
		case ML_OE_INIT:
		case ML_OE_INIT1:
		case ML_OE_MEDI:
		case ML_OE_MEDI1:
		case ML_OE_MEDI2:
		case ML_OE_FINA:
		case ML_OE_FINA1:
			sb.append( ML_OE );
			break;

		case ML_UE_ISOL:
		case ML_UE_ISOL1:
		case ML_UE_ISOL2:
		case ML_UE_INIT:
		case ML_UE_INIT1:
		case ML_UE_MEDI:
		case ML_UE_MEDI1:
		case ML_UE_MEDI2:
		case ML_UE_FINA:
		case ML_UE_FINA1:
			sb.append( ML_UE );
			break;

		case ML_EE_ISOL:
		case ML_EE_ISOL1:
		case ML_EE_INIT:
		case ML_EE_INIT1:
		case ML_EE_MEDI:
		case ML_EE_FINA:
			sb.append( ML_EE );
			break;

		case ML_N_ISOL:
		case ML_N_ISOL1:
		case ML_N_INIT:
		case ML_N_INIT1:
		case ML_N_MEDI:
		case ML_N_MEDI1:
		case ML_N_FINA:
		case ML_N_FINA1:
		case ML_N_FINA2:
			sb.append( ML_N );
			break;

		case ML_NG_ISOL:
		case ML_NG_INIT:
		case ML_NG_MEDI:
		case ML_NG_FINA:
			sb.append( ML_NG );
			break;
		case ML_NGN_MEDI:
			sb.append( ML_NG + ML_N );
			break;
		case ML_NGH_MEDI:
		case ML_NGH_FINA:
			sb.append( ML_NG + ML_H );
			break;
		case ML_NGG_MEDI:
		case ML_NGG_FINA:
			sb.append( ML_NG + ML_G );
			break;
		case ML_NGM_MEDI:
			sb.append( ML_NG + ML_M );
			break;
		case ML_NGL_MEDI:
			sb.append( ML_NG + ML_L );
			break;

		case ML_B_ISOL:
		case ML_B_INIT:
		case ML_B_MEDI:
		case ML_B_FINA:
		case ML_B_FINA1:
			sb.append( ML_B );
			break;
		case ML_BA_ISOL:
		case ML_BA_INIT:
		case ML_BA_MEDI:
		case ML_BA_FINA:
			sb.append( ML_B + ML_A );
			break;
		case ML_BE_ISOL:
		case ML_BE_INIT:
		case ML_BE_MEDI:
		case ML_BE_FINA:
			sb.append( ML_B + ML_E );
			break;
		case ML_BI_ISOL:
		case ML_BI_INIT:
		case ML_BI_MEDI:
		case ML_BI_FINA:
			sb.append( ML_B + ML_I );
			break;
		case ML_BO_ISOL:
		case ML_BO_INIT:
		case ML_BO_MEDI:
		case ML_BO_FINA:
			sb.append( ML_B + ML_O );
			break;
		case ML_BU_ISOL:
		case ML_BU_INIT:
		case ML_BU_MEDI:
		case ML_BU_FINA:
			sb.append( ML_B + ML_U );
			break;
		case ML_BOE_ISOL:
		case ML_BOE_INIT:
		case ML_BOE_MEDI:
		case ML_BOE_MEDI1:
		case ML_BOE_FINA:
		case ML_BOE_FINA1:
			sb.append( ML_B + ML_OE );
			break;
		case ML_BUE_ISOL:
		case ML_BUE_INIT:
		case ML_BUE_MEDI:
		case ML_BUE_MEDI1:
		case ML_BUE_FINA:
		case ML_BUE_FINA1:
			sb.append( ML_B + ML_UE );
			break;
		case ML_BEE_ISOL:
		case ML_BEE_INIT:
		case ML_BEE_MEDI:
		case ML_BEE_FINA:
			sb.append( ML_B + ML_EE );
			break;
		case ML_BN_MEDI:
			sb.append( ML_B + ML_N );
			break;
		case ML_BH_MEDI:
			sb.append( ML_B + ML_H );
			break;
		case ML_BG_MEDI:
			sb.append( ML_B + ML_G );
			break;
		case ML_BM_MEDI:
			sb.append( ML_B + ML_M );
			break;
		case ML_BL_MEDI:
			sb.append( ML_B + ML_L );
			break;

		case ML_P_ISOL:
		case ML_P_INIT:
		case ML_P_MEDI:
		case ML_P_FINA:
			sb.append( ML_P );
			break;
		case ML_PA_ISOL:
		case ML_PA_INIT:
		case ML_PA_MEDI:
		case ML_PA_FINA:
			sb.append( ML_P + ML_A );
			break;
		case ML_PE_ISOL:
		case ML_PE_INIT:
		case ML_PE_MEDI:
		case ML_PE_FINA:
			sb.append( ML_P + ML_E );
			break;
		case ML_PI_ISOL:
		case ML_PI_INIT:
		case ML_PI_MEDI:
		case ML_PI_FINA:
			sb.append( ML_P + ML_I );
			break;
		case ML_PO_ISOL:
		case ML_PO_INIT:
		case ML_PO_MEDI:
		case ML_PO_FINA:
			sb.append( ML_P + ML_O );
			break;
		case ML_PU_ISOL:
		case ML_PU_INIT:
		case ML_PU_MEDI:
		case ML_PU_FINA:
			sb.append( ML_P + ML_U );
			break;
		case ML_POE_ISOL:
		case ML_POE_INIT:
		case ML_POE_MEDI:
		case ML_POE_MEDI1:
		case ML_POE_FINA:
		case ML_POE_FINA1:
			sb.append( ML_P + ML_OE );
			break;
		case ML_PUE_ISOL:
		case ML_PUE_INIT:
		case ML_PUE_MEDI:
		case ML_PUE_MEDI1:
		case ML_PUE_FINA:
		case ML_PUE_FINA1:
			sb.append( ML_P + ML_UE );
			break;
		case ML_PEE_ISOL:
		case ML_PEE_INIT:
		case ML_PEE_MEDI:
		case ML_PEE_FINA:
			sb.append( ML_P + ML_EE );
			break;
		case ML_PN_MEDI:
			sb.append( ML_P + ML_N );
			break;
		case ML_PH_MEDI:
			sb.append( ML_P + ML_H );
			break;
		case ML_PG_MEDI:
			sb.append( ML_P + ML_G );
			break;
		case ML_PM_MEDI:
			sb.append( ML_P + ML_M );
			break;
		case ML_PL_MEDI:
			sb.append( ML_P + ML_L );
			break;

		case ML_H_ISOL:
		case ML_H_ISOL1:
		case ML_H_ISOL2:
		case ML_H_ISOL3:
		case ML_H_INIT:
		case ML_H_INIT1:
		case ML_H_INIT2:
		case ML_H_INIT3:
		case ML_H_MEDI:
		case ML_H_MEDI1:
		case ML_H_MEDI2:
		case ML_H_MEDI3:
		case ML_H_FINA:
		case ML_H_FINA1:
		case ML_H_FINA2:
			sb.append( ML_H );
			break;
		case ML_HE_ISOL:
		case ML_HE_ISOL1:
		case ML_HE_INIT:
		case ML_HE_INIT1:
		case ML_HE_MEDI:
		case ML_HE_MEDI1:
		case ML_HE_FINA:
		case ML_HE_FINA1:
			sb.append( ML_H + ML_E );
			break;
		case ML_HI_ISOL:
		case ML_HI_ISOL1:
		case ML_HI_INIT:
		case ML_HI_INIT1:
		case ML_HI_MEDI:
		case ML_HI_MEDI1:
		case ML_HI_FINA:
		case ML_HI_FINA1:
			sb.append( ML_H + ML_I );
			break;
		case ML_HOE_ISOL:
		case ML_HOE_ISOL1:
		case ML_HOE_INIT:
		case ML_HOE_INIT1:
		case ML_HOE_MEDI:
		case ML_HOE_MEDI1:
		case ML_HOE_MEDI2:
		case ML_HOE_FINA:
		case ML_HOE_FINA1:
		case ML_HOE_FINA2:
		case ML_HOE_FINA3:
			sb.append( ML_H + ML_OE );
			break;
		case ML_HUE_ISOL:
		case ML_HUE_ISOL1:
		case ML_HUE_INIT:
		case ML_HUE_INIT1:
		case ML_HUE_MEDI:
		case ML_HUE_MEDI1:
		case ML_HUE_MEDI2:
		case ML_HUE_FINA:
		case ML_HUE_FINA1:
		case ML_HUE_FINA2:
		case ML_HUE_FINA3:
			sb.append( ML_H + ML_UE );
			break;
		case ML_HEE_ISOL:
		case ML_HEE_ISOL1:
		case ML_HEE_INIT:
		case ML_HEE_INIT1:
		case ML_HEE_MEDI:
		case ML_HEE_MEDI1:
		case ML_HEE_FINA:
		case ML_HEE_FINA1:
			sb.append( ML_H + ML_EE );
			break;

		case ML_G_ISOL:
		case ML_G_ISOL1:
//		case ML_G_ISOL2:
//		case ML_G_ISOL3:
		case ML_G_INIT:
		case ML_G_INIT1:
//		case ML_G_INIT2:
//		case ML_G_INIT3:
		case ML_G_MEDI:
		case ML_G_MEDI1:
		case ML_G_MEDI2:
//		case ML_G_MEDI3:
		case ML_G_FINA:
		case ML_G_FINA1:
		case ML_G_FINA2:
			sb.append( ML_G );
			break;
		case ML_GE_ISOL:
//		case ML_GE_ISOL1:
		case ML_GE_INIT:
//		case ML_GE_INIT1:
		case ML_GE_MEDI:
//		case ML_GE_MEDI1:
		case ML_GE_FINA:
//		case ML_GE_FINA1:
			sb.append( ML_G + ML_E );
			break;
		case ML_GI_ISOL:
//		case ML_GI_ISOL1:
		case ML_GI_INIT:
//		case ML_GI_INIT1:
		case ML_GI_MEDI:
//		case ML_GI_MEDI1:
		case ML_GI_FINA:
//		case ML_GI_FINA1:
			sb.append( ML_G + ML_I );
			break;
		case ML_GOE_ISOL:
//		case ML_GOE_ISOL1:
		case ML_GOE_INIT:
//		case ML_GOE_INIT1:
		case ML_GOE_MEDI:
		case ML_GOE_MEDI1:
//		case ML_GOE_MEDI2:
		case ML_GOE_FINA:
		case ML_GOE_FINA1:
//		case ML_GOE_FINA2:
//		case ML_GOE_FINA3:
			sb.append( ML_G + ML_OE );
			break;
		case ML_GUE_ISOL:
//		case ML_GUE_ISOL1:
		case ML_GUE_INIT:
//		case ML_GUE_INIT1:
		case ML_GUE_MEDI:
		case ML_GUE_MEDI1:
//		case ML_GUE_MEDI2:
		case ML_GUE_FINA:
		case ML_GUE_FINA1:
//		case ML_GUE_FINA2:
//		case ML_GUE_FINA3:
			sb.append( ML_G + ML_UE );
			break;
		case ML_GEE_ISOL:
		case ML_GEE_INIT:
		case ML_GEE_MEDI:
		case ML_GEE_FINA:
			sb.append( ML_G + ML_EE );
			break;
		case ML_GN_MEDI:
			sb.append( ML_G + ML_N );
			break;
		case ML_GM_MEDI:
			sb.append( ML_G + ML_M );
			break;
		case ML_GL_MEDI:
			sb.append( ML_G + ML_L );
			break;

		case ML_M_ISOL:
		case ML_M_INIT:
		case ML_M_MEDI:
		case ML_M_FINA:
			sb.append( ML_M );
			break;
		case ML_MM_MEDI:
			sb.append( ML_M + ML_M );
			break;
		case ML_ML_MEDI:
			sb.append( ML_M + ML_M );
			break;

		case ML_L_ISOL:
		case ML_L_INIT:
		case ML_L_MEDI:
		case ML_L_FINA:
			sb.append( ML_L );
			break;
		case ML_LL_MEDI:
			sb.append( ML_L + ML_L );
			break;

		case ML_S_ISOL:
		case ML_S_INIT:
		case ML_S_MEDI:
		case ML_S_FINA:
		case ML_S_FINA1:
		case ML_S_FINA2:
			sb.append( ML_S );
			break;

		case ML_SH_ISOL:
		case ML_SH_INIT:
		case ML_SH_MEDI:
		case ML_SH_FINA:
			sb.append( ML_SH );
			break;

		case ML_T_ISOL:
		case ML_T_ISOL1:
		case ML_T_INIT:
		case ML_T_MEDI:
		case ML_T_MEDI1:
		case ML_T_MEDI2:
		case ML_T_FINA:
			sb.append( ML_T );
			break;

		case ML_D_ISOL:
		case ML_D_INIT:
		case ML_D_INIT1:
		case ML_D_MEDI:
		case ML_D_MEDI1:
		case ML_D_FINA:
		case ML_D_FINA1:
			sb.append( ML_D );
			break;

		case ML_CH_ISOL:
		case ML_CH_INIT:
		case ML_CH_MEDI:
		case ML_CH_FINA:
			sb.append( ML_CH );
			break;

		case ML_J_ISOL:
		case ML_J_ISOL1:
		case ML_J_INIT:
		case ML_J_MEDI:
		case ML_J_FINA:
			sb.append( ML_J );
			break;

		case ML_Y_ISOL:
		case ML_Y_INIT:
		case ML_Y_INIT1:
		case ML_Y_MEDI:
		case ML_Y_MEDI1:
		case ML_Y_FINA:
			sb.append( ML_Y );
			break;

		case ML_R_ISOL:
		case ML_R_INIT:
		case ML_R_MEDI:
		case ML_R_FINA:
			sb.append( ML_R );
			break;

		case ML_W_ISOL:
		case ML_W_INIT:
		case ML_W_INIT1:
		case ML_W_MEDI:
		case ML_W_FINA:
		case ML_W_FINA1:
			sb.append( ML_W );
			break;

		case ML_F_ISOL:
		case ML_F_INIT:
		case ML_F_MEDI:
		case ML_F_FINA:
			sb.append( ML_F );
			break;
		case ML_FA_ISOL:
		case ML_FA_INIT:
		case ML_FA_MEDI:
		case ML_FA_FINA:
			sb.append( ML_F + ML_A );
			break;
		case ML_FE_ISOL:
		case ML_FE_INIT:
		case ML_FE_MEDI:
		case ML_FE_FINA:
			sb.append( ML_F + ML_E );
			break;
		case ML_FI_ISOL:
		case ML_FI_INIT:
		case ML_FI_MEDI:
		case ML_FI_FINA:
			sb.append( ML_F + ML_I );
			break;
		case ML_FO_ISOL:
		case ML_FO_INIT:
		case ML_FO_MEDI:
		case ML_FO_FINA:
			sb.append( ML_F + ML_O );
			break;
		case ML_FU_ISOL:
		case ML_FU_INIT:
		case ML_FU_MEDI:
		case ML_FU_FINA:
			sb.append( ML_F + ML_U );
			break;
		case ML_FOE_ISOL:
		case ML_FOE_INIT:
		case ML_FOE_MEDI:
		case ML_FOE_MEDI1:
		case ML_FOE_FINA:
		case ML_FOE_FINA1:
			sb.append( ML_F + ML_OE );
			break;
		case ML_FUE_ISOL:
		case ML_FUE_INIT:
		case ML_FUE_MEDI:
		case ML_FUE_MEDI1:
		case ML_FUE_FINA:
		case ML_FUE_FINA1:
			sb.append( ML_F + ML_UE );
			break;
		case ML_FEE_ISOL:
		case ML_FEE_INIT:
		case ML_FEE_MEDI:
		case ML_FEE_FINA:
			sb.append( ML_F + ML_EE );
			break;
		case ML_FN_MEDI:
			sb.append( ML_F + ML_N );
			break;
		case ML_FH_MEDI:
			sb.append( ML_F + ML_H );
			break;
		case ML_FG_MEDI:
			sb.append( ML_F + ML_G );
			break;
		case ML_FM_MEDI:
			sb.append( ML_F + ML_M );
			break;
		case ML_FL_MEDI:
			sb.append( ML_F + ML_L );
			break;

		case ML_K_ISOL:
		case ML_K_INIT:
		case ML_K_MEDI:
		case ML_K_FINA:
			sb.append( ML_K );
			break;
		case ML_KA_ISOL:
		case ML_KA_INIT:
		case ML_KA_MEDI:
		case ML_KA_FINA:
			sb.append( ML_K + ML_A );
			break;
		case ML_KE_ISOL:
		case ML_KE_INIT:
		case ML_KE_MEDI:
		case ML_KE_FINA:
			sb.append( ML_K + ML_E );
			break;
		case ML_KI_ISOL:
		case ML_KI_INIT:
		case ML_KI_MEDI:
		case ML_KI_FINA:
			sb.append( ML_K + ML_I );
			break;
		case ML_KO_ISOL:
		case ML_KO_INIT:
		case ML_KO_MEDI:
		case ML_KO_FINA:
			sb.append( ML_K + ML_O );
			break;
		case ML_KU_ISOL:
		case ML_KU_INIT:
		case ML_KU_MEDI:
		case ML_KU_FINA:
			sb.append( ML_K + ML_U );
			break;
		case ML_KOE_ISOL:
		case ML_KOE_INIT:
		case ML_KOE_MEDI:
		case ML_KOE_MEDI1:
		case ML_KOE_FINA:
		case ML_KOE_FINA1:
			sb.append( ML_K + ML_OE );
			break;
		case ML_KUE_ISOL:
		case ML_KUE_INIT:
		case ML_KUE_MEDI:
		case ML_KUE_MEDI1:
		case ML_KUE_FINA:
		case ML_KUE_FINA1:
			sb.append( ML_K + ML_UE );
			break;
		case ML_KEE_ISOL:
		case ML_KEE_INIT:
		case ML_KEE_MEDI:
		case ML_KEE_FINA:
			sb.append( ML_K + ML_EE );
			break;
		case ML_KN_MEDI:
			sb.append( ML_K + ML_N );
			break;
		case ML_KH_MEDI:
			sb.append( ML_K + ML_H );
			break;
		case ML_KG_MEDI:
			sb.append( ML_K + ML_G );
			break;
		case ML_KM_MEDI:
			sb.append( ML_K + ML_M );
			break;
		case ML_KL_MEDI:
			sb.append( ML_K + ML_L );
			break;

		case ML_KH_ISOL:
		case ML_KH_INIT:
		case ML_KHH_MEDI:
		case ML_KH_FINA:
			sb.append( ML_KH );
			break;
		case ML_KHA_ISOL:
		case ML_KHA_INIT:
		case ML_KHA_MEDI:
		case ML_KHA_FINA:
			sb.append( ML_H + ML_A );
			break;
		case ML_KHE_ISOL:
		case ML_KHE_INIT:
		case ML_KHE_MEDI:
		case ML_KHE_FINA:
			sb.append( ML_KH + ML_E );
			break;
		case ML_KHI_ISOL:
		case ML_KHI_INIT:
		case ML_KHI_MEDI:
		case ML_KHI_FINA:
			sb.append( ML_KH + ML_I );
			break;
		case ML_KHO_ISOL:
		case ML_KHO_INIT:
		case ML_KHO_MEDI:
		case ML_KHO_FINA:
			sb.append( ML_KH + ML_O );
			break;
		case ML_KHU_ISOL:
		case ML_KHU_INIT:
		case ML_KHU_MEDI:
		case ML_KHU_FINA:
			sb.append( ML_KH + ML_U );
			break;
		case ML_KHOE_ISOL:
		case ML_KHOE_INIT:
		case ML_KHOE_MEDI:
		case ML_KHOE_MEDI1:
		case ML_KHOE_FINA:
		case ML_KHOE_FINA1:
			sb.append( ML_KH + ML_OE );
			break;
		case ML_KHUE_ISOL:
		case ML_KHUE_INIT:
		case ML_KHUE_MEDI:
		case ML_KHUE_MEDI1:
		case ML_KHUE_FINA:
		case ML_KHUE_FINA1:
			sb.append( ML_KH + ML_UE );
			break;
		case ML_KHEE_ISOL:
		case ML_KHEE_INIT:
		case ML_KHEE_MEDI:
		case ML_KHEE_FINA:
			sb.append( ML_KH + ML_EE );
			break;
		case ML_KHN_MEDI:
			sb.append( ML_KH + ML_N );
			break;
		case ML_KH_MEDI_:
			sb.append( ML_KH + ML_H );
			break;
		case ML_KHG_MEDI:
			sb.append( ML_KH + ML_G );
			break;
		case ML_KHM_MEDI:
			sb.append( ML_KH + ML_M );
			break;
		case ML_KHL_MEDI:
			sb.append( ML_KH + ML_L );
			break;

		case ML_C_ISOL:
		case ML_C_INIT:
		case ML_C_MEDI:
		case ML_C_FINA:
			sb.append( ML_C );
			break;

		case ML_Z_ISOL:
		case ML_Z_INIT:
		case ML_Z_MEDI:
		case ML_Z_FINA:
			sb.append( ML_Z );
			break;

		case ML_HH_ISOL:
		case ML_HH_INIT:
		case ML_HH_MEDI:
		case ML_HH_FINA:
			sb.append( ML_HH );
			break;

		case ML_RH_ISOL:
		case ML_RH_INIT:
		case ML_RH_MEDI:
		case ML_RH_FINA:
			sb.append( ML_RH );
			break;

		case ML_LH_ISOL:
		case ML_LH_INIT:
		case ML_LH_MEDI:
		case ML_LH_FINA:
			sb.append( ML_LH );
			break;

		case ML_ZHI_ISOL:
		case ML_ZHI_INIT:
		case ML_ZHI_MEDI:
		case ML_ZHI_FINA:
			sb.append( ML_ZHI );
			break;

		case ML_CHI_ISOL:
		case ML_CHI_INIT:
		case ML_CHI_MEDI:
		case ML_CHI_FINA:
			sb.append( ML_CHI );
			break;

		case ML_T_LVS_ISOL:
		case ML_T_LVS_INIT:
		case ML_T_LVS_MEDI:
		case ML_T_LVS_FINA:
			sb.append( ML_T_LVS );
			break;

		case ML_T_LONG_A_ISOL:
		case ML_T_LONG_A_INIT:
		case ML_T_LONG_A_MEDI:
		case ML_T_LONG_A_FINA:
			sb.append( ML_A + ML_T_LVS );
			break;

		case ML_T_E_ISOL:
		case ML_T_E_INIT:
		case ML_T_E_MEDI:
		case ML_T_E_MEDI1:
		case ML_T_E_FINA:
			sb.append( ML_T_E );
			break;

		case ML_T_I_ISOL:
		case ML_T_I_INIT:
		case ML_T_I_MEDI:
		case ML_T_I_MEDI1:
		case ML_T_I_FINA:
			sb.append( ML_T_I );
			break;

		case ML_T_O_ISOL:
		case ML_T_O_INIT:
		case ML_T_O_MEDI:
		case ML_T_O_MEDI1:
		case ML_T_O_FINA:
			sb.append( ML_T_O );
			break;
		case ML_T_LONG_O_ISOL:
		case ML_T_LONG_O_INIT:
		case ML_T_LONG_O_MEDI:
		case ML_T_LONG_O_FINA:
			sb.append( ML_T_O + ML_T_LVS );
			break;

		case ML_T_U_ISOL:
		case ML_T_U_ISOL1:
		case ML_T_U_INIT:
		case ML_T_U_MEDI:
		case ML_T_U_MEDI1:
		case ML_T_U_MEDI2:
		case ML_T_U_FINA:
		case ML_T_U_FINA1:
			sb.append( ML_T_U );
			break;

		case ML_T_OE_ISOL:
		case ML_T_OE_INIT:
		case ML_T_OE_MEDI:
		case ML_T_OE_MEDI1:
		case ML_T_OE_FINA:
			sb.append( ML_T_OE );
			break;
		case ML_T_LONG_OE_ISOL:
		case ML_T_LONG_OE_INIT:
		case ML_T_LONG_OE_MEDI:
		case ML_T_LONG_OE_FINA:
			sb.append( ML_T_OE + ML_T_LVS );
			break;

		case ML_T_UE_ISOL:
		case ML_T_UE_ISOL1:
		case ML_T_UE_INIT:
		case ML_T_UE_MEDI:
		case ML_T_UE_MEDI1:
		case ML_T_UE_FINA:
			sb.append( ML_T_UE );
			break;

		case ML_T_NG_ISOL:
		case ML_T_NG_INIT:
		case ML_T_NG_MEDI:
		case ML_T_NG_FINA:
			sb.append( ML_T_NG );
			break;
		case ML_T_NGN_MEDI:
			sb.append( ML_T_NG + ML_N );
			break;
		case ML_T_NGM_MEDI:
			sb.append( ML_T_NG + ML_T_M );
			break;
		case ML_T_NGL_MEDI:
			sb.append( ML_T_NG + ML_L );
			break;
		case ML_T_NGQ_MEDI:
			sb.append( ML_T_NG + ML_T_Q );
			break;
		case ML_T_NGG_MEDI:
			sb.append( ML_T_NG + ML_T_G );
			break;
		case ML_T_NGG_FINA:
			sb.append( ML_T_NG + ML_T_G );
			break;

		case ML_T_B_ISOL:
		case ML_T_B_INIT:
		case ML_T_B_MEDI:
		case ML_T_B_FINA:
			sb.append( ML_T_B );
			break;
		case ML_T_BA_ISOL:
		case ML_T_BA_INIT:
		case ML_T_BA_MEDI:
		case ML_T_BA_FINA:
			sb.append( ML_T_B + ML_A );
			break;
		case ML_T_BE_ISOL:
		case ML_T_BE_INIT:
		case ML_T_BE_MEDI:
		case ML_T_BE_FINA:
			sb.append( ML_T_B + ML_T_E );
			break;
		case ML_T_BI_ISOL:
		case ML_T_BI_INIT:
		case ML_T_BI_MEDI:
		case ML_T_BI_FINA:
			sb.append( ML_T_B + ML_T_I);
			break;
		case ML_T_BO_ISOL:
		case ML_T_BO_INIT:
		case ML_T_BO_MEDI:
		case ML_T_BO_FINA:
			sb.append( ML_T_B + ML_T_O);
			break;
		case ML_T_BU_ISOL:
		case ML_T_BU_INIT:
		case ML_T_BU_MEDI:
		case ML_T_BU_FINA:
			sb.append( ML_T_B + ML_T_U);
			break;
		case ML_T_BOE_ISOL:
		case ML_T_BOE_INIT:
		case ML_T_BOE_MEDI:
		case ML_T_BOE_FINA:
			sb.append( ML_T_B + ML_T_OE);
			break;
		case ML_T_BUE_ISOL:
		case ML_T_BUE_INIT:
		case ML_T_BUE_MEDI:
		case ML_T_BUE_FINA:
			sb.append( ML_T_B + ML_T_UE);
			break;
		case ML_T_BN_MEDI:
			sb.append( ML_T_B + ML_N);
			break;
		case ML_T_BM_MEDI:
			sb.append( ML_T_B + ML_T_M);
			break;
		case ML_T_BL_MEDI:
			sb.append( ML_T_B + ML_L);
			break;
		case ML_T_BQ_MEDI:
			sb.append( ML_T_B + ML_T_Q);
			break;
		case ML_T_BG_MEDI:
			sb.append( ML_T_B + ML_T_G);
			break;

		case ML_T_P_ISOL:
		case ML_T_P_INIT:
		case ML_T_P_MEDI:
		case ML_T_P_FINA:
			sb.append( ML_T_P );
			break;
		case ML_T_PA_ISOL:
		case ML_T_PA_INIT:
		case ML_T_PA_MEDI:
		case ML_T_PA_FINA:
			sb.append( ML_T_P + ML_A );
			break;
		case ML_T_PE_ISOL:
		case ML_T_PE_INIT:
		case ML_T_PE_MEDI:
		case ML_T_PE_FINA:
			sb.append( ML_T_P + ML_T_E );
			break;
		case ML_T_PI_ISOL:
		case ML_T_PI_INIT:
		case ML_T_PI_MEDI:
		case ML_T_PI_FINA:
			sb.append( ML_T_P + ML_T_I);
			break;
		case ML_T_PO_ISOL:
		case ML_T_PO_INIT:
		case ML_T_PO_MEDI:
		case ML_T_PO_FINA:
			sb.append( ML_T_P + ML_T_O);
			break;
		case ML_T_PU_ISOL:
		case ML_T_PU_INIT:
		case ML_T_PU_MEDI:
		case ML_T_PU_FINA:
			sb.append( ML_T_P + ML_T_U);
			break;
		case ML_T_POE_ISOL:
		case ML_T_POE_INIT:
		case ML_T_POE_MEDI:
		case ML_T_POE_FINA:
			sb.append( ML_T_P + ML_T_OE);
			break;
		case ML_T_PUE_ISOL:
		case ML_T_PUE_INIT:
		case ML_T_PUE_MEDI:
		case ML_T_PUE_FINA:
			sb.append( ML_T_P + ML_T_UE);
			break;

		case ML_T_Q_ISOL:
		case ML_T_Q_INIT:
		case ML_T_Q_INIT1:
		case ML_T_Q_MEDI:
		case ML_T_Q_MEDI1:
		case ML_T_Q_FINA:
			sb.append( ML_T_Q );
			break;
		case ML_T_QE_ISOL:
		case ML_T_QE_INIT:
		case ML_T_QE_MEDI:
		case ML_T_QE_FINA:
			sb.append( ML_T_Q + ML_T_E);
			break;
		case ML_T_QI_ISOL:
		case ML_T_QI_INIT:
		case ML_T_QI_MEDI:
		case ML_T_QI_FINA:
			sb.append( ML_T_Q + ML_T_I);
			break;
		case ML_T_QOE_ISOL:
		case ML_T_QOE_INIT:
		case ML_T_QOE_MEDI:
		case ML_T_QOE_FINA:
			sb.append( ML_T_Q + ML_T_OE);
			break;
		case ML_T_QUE_ISOL:
		case ML_T_QUE_INIT:
		case ML_T_QUE_MEDI:
		case ML_T_QUE_FINA:
			sb.append( ML_T_Q + ML_T_UE);
			break;

		case ML_T_G_ISOL:
		case ML_T_G_ISOL1:
		case ML_T_G_INIT:
		case ML_T_G_INIT1:
		case ML_T_G_MEDI:
		case ML_T_G_MEDI1:
		case ML_T_G_FINA:
			sb.append( ML_T_G );
			break;
		case ML_T_GE_ISOL:
		case ML_T_GE_INIT:
		case ML_T_GE_MEDI:
		case ML_T_GE_FINA:
			sb.append( ML_T_G + ML_T_E);
			break;
		case ML_T_GI_ISOL:
		case ML_T_GI_INIT:
		case ML_T_GI_MEDI:
		case ML_T_GI_FINA:
			sb.append( ML_T_G + ML_T_I);
			break;
		case ML_T_GOE_ISOL:
		case ML_T_GOE_INIT:
		case ML_T_GOE_MEDI:
		case ML_T_GOE_FINA:
			sb.append( ML_T_G + ML_T_OE);
			break;
		case ML_T_GUE_ISOL:
		case ML_T_GUE_INIT:
		case ML_T_GUE_MEDI:
		case ML_T_GUE_FINA:
			sb.append( ML_T_G + ML_T_UE);
			break;
		case ML_T_GN_MEDI:
			sb.append( ML_T_G + ML_N);
			break;
		case ML_T_GM_MEDI:
			sb.append( ML_T_G + ML_T_M);
			break;
		case ML_T_GL_MEDI:
			sb.append( ML_T_G + ML_L);
			break;

		case ML_T_M_ISOL:
		case ML_T_M_INIT:
		case ML_T_M_MEDI:
		case ML_T_M_FINA:
			sb.append( ML_T_M );
			break;
		case ML_T_MM_MEDI:
			sb.append( ML_T_G + ML_T_M);
			break;
		case ML_T_ML_MEDI:
			sb.append( ML_T_G + ML_L);
			break;

		case ML_T_T_ISOL:
		case ML_T_T_INIT:
		case ML_T_T_MEDI:
		case ML_T_T_FINA:
			sb.append( ML_T_T );
			break;

		case ML_T_D_ISOL:
		case ML_T_D_INIT:
		case ML_T_D_MEDI:
		case ML_T_D_FINA:
			sb.append( ML_T_D );
			break;

		case ML_T_CH_ISOL:
		case ML_T_CH_INIT:
		case ML_T_CH_MEDI:
		case ML_T_CH_FINA:
			sb.append( ML_T_CH );
			break;

		case ML_T_J_ISOL:
		case ML_T_J_INIT:
		case ML_T_J_MEDI:
		case ML_T_J_FINA:
			sb.append( ML_T_J );
			break;

		case ML_T_TS_ISOL:
		case ML_T_TS_INIT:
		case ML_T_TS_MEDI:
		case ML_T_TS_FINA:
			sb.append( ML_T_TS );
			break;

		case ML_T_Y_ISOL:
		case ML_T_Y_INIT:
		case ML_T_Y_MEDI:
		case ML_T_Y_FINA:
			sb.append( ML_T_Y );
			break;

		case ML_T_W_ISOL:
		case ML_T_W_INIT:
		case ML_T_W_MEDI:
		case ML_T_W_FINA:
			sb.append( ML_T_W );
			break;

		case ML_T_K_ISOL:
		case ML_T_K_INIT:
		case ML_T_K_MEDI:
		case ML_T_K_FINA:
			sb.append( ML_T_K );
			break;
		case ML_T_KA_ISOL:
		case ML_T_KA_INIT:
		case ML_T_KA_MEDI:
		case ML_T_KA_FINA:
			sb.append( ML_T_W + ML_A );
			break;
		case ML_T_KO_ISOL:
		case ML_T_KO_INIT:
		case ML_T_KO_MEDI:
		case ML_T_KO_FINA:
			sb.append( ML_T_W + ML_T_O );
			break;
		case ML_T_KU_ISOL:
		case ML_T_KU_INIT:
		case ML_T_KU_MEDI:
		case ML_T_KU_FINA:
			sb.append( ML_T_W + ML_T_U );
			break;

		case ML_T_GH_ISOL:
		case ML_T_GH_INIT:
		case ML_T_GH_MEDI:
		case ML_T_GH_FINA:
			sb.append( ML_T_GH );
			break;
		case ML_T_GHA_ISOL:
		case ML_T_GHA_INIT:
		case ML_T_GHA_MEDI:
		case ML_T_GHA_FINA:
			sb.append( ML_T_GH + ML_A );
			break;
		case ML_T_GHO_ISOL:
		case ML_T_GHO_INIT:
		case ML_T_GHO_MEDI:
		case ML_T_GHO_FINA:
			sb.append( ML_T_GH + ML_T_O );
			break;
		case ML_T_GHU_ISOL:
		case ML_T_GHU_INIT:
		case ML_T_GHU_MEDI:
		case ML_T_GHU_FINA:
			sb.append( ML_T_GH + ML_T_U );
			break;

		case ML_T_HH_ISOL:
		case ML_T_HH_INIT:
		case ML_T_HH_MEDI:
		case ML_T_HH_FINA:
			sb.append( ML_T_HH );
			break;

		case ML_T_JI_ISOL:
		case ML_T_JI_INIT:
		case ML_T_JI_MEDI:
		case ML_T_JI_FINA:
			sb.append( ML_T_JI );
			break;

		case ML_T_NIA_ISOL:
		case ML_T_NIA_INIT:
		case ML_T_NIA_MEDI:
		case ML_T_NIA_FINA:
			sb.append( ML_T_NIA );
			break;

		case ML_T_DZ_ISOL:
		case ML_T_DZ_INIT:
		case ML_T_DZ_MEDI:
		case ML_T_DZ_FINA:
			sb.append( ML_T_DZ );
			break;

		case ML_S_E_ISOL:
		case ML_S_E_INIT:
		case ML_S_E_MEDI:
		case ML_S_E_MEDI1:
		case ML_S_E_FINA:
		case ML_S_E_FINA1:
		case ML_S_E_FINA2:
		case ML_S_E_FINA3:
			sb.append( ML_S_E );
			break;

		case ML_S_I_ISOL:
		case ML_S_I_INIT:
		case ML_S_I_MEDI:
		case ML_S_I_MEDI1:
		case ML_S_I_MEDI2:
		case ML_S_I_FINA:
		case ML_S_I_FINA1:
		case ML_S_I_FINA2:
		case ML_S_I_FINA3:
			sb.append( ML_S_I );
			break;

		case ML_S_IY_ISOL:
		case ML_S_IY_INIT:
		case ML_S_IY_MEDI:
		case ML_S_IY_FINA:
			sb.append( ML_S_IY );
			break;

		case ML_S_UE_ISOL:
		case ML_S_UE_INIT:
		case ML_S_UE_MEDI:
		case ML_S_UE_MEDI1:
		case ML_S_UE_MEDI2:
		case ML_S_UE_FINA:
		case ML_S_UE_FINA1:
		case ML_S_UE_FINA2:
		case ML_S_UE_FINA3:
			sb.append( ML_S_UE );
			break;

		case ML_S_U_ISOL:
		case ML_S_U_INIT:
		case ML_S_U_MEDI:
		case ML_S_U_FINA:
			sb.append( ML_S_U );
			break;

		case ML_S_NG_ISOL:
		case ML_S_NG_INIT:
		case ML_S_NG_MEDI:
		case ML_S_NG_FINA:
			sb.append( ML_S_NG );
			break;
		case ML_S_NGN_MEDI:
			sb.append( ML_S_NG + ML_N );
			break;
		case ML_S_NGM_MEDI:
			sb.append( ML_S_NG + ML_M );
			break;
		case ML_S_NGL_MEDI:
			sb.append( ML_S_NG + ML_L );
			break;
		case ML_S_NGK_MEDI:
		case ML_S_NGK_MEDI1:
			sb.append( ML_S_NG + ML_S_K );
			break;
		case ML_S_NGG_MEDI:
			sb.append( ML_S_NG + ML_S_G );
			break;
		case ML_S_NGH_MEDI:
			sb.append( ML_S_NG + ML_S_H );
			break;

		case ML_S_K_ISOL:
		case ML_S_K_INIT:
		case ML_S_K_MEDI:
		case ML_S_K_MEDI1:
		case ML_S_K_FINA:
			sb.append( ML_S_K );
			break;
		case ML_S_KE_ISOL:
		case ML_S_KE_INIT:
		case ML_S_KE_MEDI:
		case ML_S_KE_FINA:
			sb.append( ML_S_K + ML_S_E );
			break;
		case ML_S_KI_ISOL:
		case ML_S_KI_INIT:
		case ML_S_KI_MEDI:
		case ML_S_KI_FINA:
			sb.append( ML_S_K + ML_S_I );
			break;
		case ML_S_KUE_ISOL:
		case ML_S_KUE_INIT:
		case ML_S_KUE_MEDI:
		case ML_S_KUE_FINA:
			sb.append( ML_S_K + ML_S_UE );
			break;

		case ML_S_G_ISOL:
		case ML_S_G_ISOL1:
		case ML_S_G_INIT:
		case ML_S_G_INIT1:
		case ML_S_G_MEDI:
		case ML_S_G_FINA:
			sb.append( ML_S_G );
			break;
		case ML_S_GE_ISOL:
		case ML_S_GE_INIT:
		case ML_S_GE_MEDI:
		case ML_S_GE_FINA:
			sb.append( ML_S_G + ML_S_E );
			break;
		case ML_S_GI_ISOL:
		case ML_S_GI_INIT:
		case ML_S_GI_MEDI:
		case ML_S_GI_FINA:
			sb.append( ML_S_G + ML_S_I );
			break;
		case ML_S_GUE_ISOL:
		case ML_S_GUE_INIT:
		case ML_S_GUE_MEDI:
		case ML_S_GUE_FINA:
			sb.append( ML_S_G + ML_S_UE );
			break;

		case ML_S_H_ISOL:
		case ML_S_H_ISOL1:
		case ML_S_H_INIT:
		case ML_S_H_INIT1:
		case ML_S_H_MEDI:
		case ML_S_H_MEDI1:
		case ML_S_H_FINA:
			sb.append( ML_S_H );
			break;
		case ML_S_HE_ISOL:
		case ML_S_HE_INIT:
		case ML_S_HE_MEDI:
		case ML_S_HE_FINA:
			sb.append( ML_S_G + ML_S_E );
			break;
		case ML_S_HI_ISOL:
		case ML_S_HI_INIT:
		case ML_S_HI_MEDI:
		case ML_S_HI_FINA:
			sb.append( ML_S_H + ML_S_I );
			break;
		case ML_S_HUE_ISOL:
		case ML_S_HUE_INIT:
		case ML_S_HUE_MEDI:
		case ML_S_HUE_FINA:
			sb.append( ML_S_H + ML_S_UE );
			break;

		case ML_S_BE_ISOL:
		case ML_S_BE_INIT:
		case ML_S_BE_MEDI:
		case ML_S_BE_FINA:
			sb.append( ML_B + ML_S_E );
			break;
		case ML_S_BI_ISOL:
		case ML_S_BI_INIT:
		case ML_S_BI_MEDI:
		case ML_S_BI_FINA:
			sb.append( ML_B + ML_S_I );
			break;
		case ML_S_BUE_ISOL:
		case ML_S_BUE_INIT:
		case ML_S_BUE_MEDI:
		case ML_S_BUE_FINA:
			sb.append( ML_B + ML_S_UE );
			break;
		case ML_S_BU_ISOL:
		case ML_S_BU_INIT:
		case ML_S_BU_MEDI:
		case ML_S_BU_FINA:
			sb.append( ML_B + ML_S_UE );
			break;

		case ML_S_P_ISOL:
		case ML_S_P_INIT:
		case ML_S_P_MEDI:
		case ML_S_P_FINA:
			sb.append( ML_S_P );
			break;
		case ML_S_PA_ISOL:
		case ML_S_PA_INIT:
		case ML_S_PA_MEDI:
		case ML_S_PA_FINA:
			sb.append( ML_P + ML_A );
			break;
		case ML_S_PE_ISOL:
		case ML_S_PE_INIT:
		case ML_S_PE_MEDI:
		case ML_S_PE_FINA:
			sb.append( ML_P + ML_S_E );
			break;
		case ML_S_PI_ISOL:
		case ML_S_PI_INIT:
		case ML_S_PI_MEDI:
		case ML_S_PI_FINA:
			sb.append( ML_P + ML_S_I );
			break;
		case ML_S_PO_ISOL:
		case ML_S_PO_INIT:
		case ML_S_PO_MEDI:
		case ML_S_PO_FINA:
			sb.append( ML_P + ML_S_UE );
			break;
		case ML_S_PUE_ISOL:
		case ML_S_PUE_INIT:
		case ML_S_PUE_MEDI:
		case ML_S_PUE_FINA:
			sb.append( ML_P + ML_S_UE );
			break;
		case ML_S_PU_ISOL:
		case ML_S_PU_INIT:
		case ML_S_PU_MEDI:
		case ML_S_PU_FINA:
			sb.append( ML_P + ML_S_UE );
			break;

		case ML_S_SH_ISOL:
		case ML_S_SH_INIT:
		case ML_S_SH_MEDI:
		case ML_S_SH_FINA:
			sb.append( ML_S_SH );
			break;

		case ML_S_T_ISOL:
		case ML_S_T_ISOL1:
		case ML_S_T_INIT:
		case ML_S_T_INIT1:
		case ML_S_T_MEDI:
		case ML_S_T_MEDI1:
		case ML_S_T_MEDI2:
		case ML_S_T_FINA:
			sb.append( ML_S_T );
			break;

		case ML_S_D_ISOL:
		case ML_S_D_INIT:
		case ML_S_D_INIT1:
		case ML_S_D_MEDI:
		case ML_S_D_MEDI1:
		case ML_S_D_FINA:
			sb.append( ML_S_D );
			break;

		case ML_S_J_ISOL:
		case ML_S_J_INIT:
		case ML_S_J_MEDI:
		case ML_S_J_FINA:
			sb.append( ML_S_J );
			break;

		case ML_S_F_ISOL:
		case ML_S_F_INIT:
		case ML_S_F_MEDI:
		case ML_S_F_FINA:
			sb.append( ML_S_F );
			break;

		case ML_S_GH_ISOL:
		case ML_S_GH_INIT:
		case ML_S_GH_MEDI:
		case ML_S_GH_FINA:
			sb.append( ML_S_GH );
			break;
		case ML_S_GHA_ISOL:
		case ML_S_GHA_INIT:
		case ML_S_GHA_MEDI:
		case ML_S_GHA_FINA:
			sb.append( ML_S_GH + ML_A );
			break;
		case ML_S_GHO_ISOL:
		case ML_S_GHO_INIT:
		case ML_S_GHO_MEDI:
		case ML_S_GHO_FINA:
			sb.append( ML_S_GH + ML_O );
			break;
			// ML_S_GHE_ISOL = '\uECB6';
			// ML_S_GHE_INIT = '\uECB7';
			// ML_S_GHE_MEDI = '\uECB8';
			// ML_S_GHE_FINA = '\uECB9';
			// ML_S_GHI_ISOL = '\uECBA';
			// ML_S_GHI_INIT = '\uECBB';
			// ML_S_GHI_MEDI = '\uECBC';
			// ML_S_GHI_FINA = '\uECBD';
			// ML_S_GHUE_ISOL = '\uECBE';
			// ML_S_GHUE_INIT = '\uECBF';
			// ML_S_GHUE_MEDI = '\uECC0';
			// ML_S_GHUE_FINA = '\uECC1';

		case ML_S_HH_ISOL:
		case ML_S_HH_INIT:
		case ML_S_HH_MEDI:
		case ML_S_HH_FINA:
			sb.append( ML_S_HH );
			break;
		case ML_S_HHA_ISOL:
		case ML_S_HHA_INIT:
		case ML_S_HHA_MEDI:
		case ML_S_HHA_FINA:
			sb.append( ML_S_HH + ML_A );
			break;
		case ML_S_HHO_ISOL:
		case ML_S_HHO_INIT:
		case ML_S_HHO_MEDI:
		case ML_S_HHO_FINA:
			sb.append( ML_S_GH + ML_O );
			break;

			// ML_S_HHE_ISOL = '\uECCE';
			// ML_S_HHE_INIT = '\uECCF';
			// ML_S_HHE_MEDI = '\uECD0';
			// ML_S_HHE_FINA = '\uECD1';
			// ML_S_HHI_ISOL = '\uECD2';
			// ML_S_HHI_INIT = '\uECD3';
			// ML_S_HHI_MEDI = '\uECD4';
			// ML_S_HHI_FINA = '\uECD5';
			// ML_S_HHUE_ISOL = '\uECD6';
			// ML_S_HHUE_INIT = '\uECD7';
			// ML_S_HHUE_MEDI = '\uECD8';
			// ML_S_HHUE_FINA = '\uECD9';

		case ML_S_TS_ISOL:
		case ML_S_TS_INIT:
		case ML_S_TS_MEDI:
		case ML_S_TS_FINA:
			sb.append( ML_S_TS );
			break;

		case ML_S_Z_ISOL:
		case ML_S_Z_INIT:
		case ML_S_Z_INIT1:
		case ML_S_Z_MEDI:
		case ML_S_Z_MEDI1:
		case ML_S_Z_FINA:
			sb.append( ML_S_Z );
			break;

		case ML_S_RH_ISOL:
		case ML_S_RH_INIT:
		case ML_S_RH_MEDI:
		case ML_S_RH_FINA:
			sb.append( ML_S_RH );
			break;

		case ML_S_CH_ISOL:
		case ML_S_CH_INIT:
		case ML_S_CH_MEDI:
		case ML_S_CH_FINA:
			sb.append( ML_S_CH );
			break;

		case ML_S_ZH_ISOL:
		case ML_S_ZH_INIT:
		case ML_S_ZH_MEDI:
		case ML_S_ZH_FINA:
			sb.append( ML_S_ZH );
			break;

		case ML_M_I_ISOL:
		case ML_M_I_INIT:
		case ML_M_I_MEDI:
		case ML_M_I_MEDI1:
		case ML_M_I_MEDI2:
		case ML_M_I_MEDI3:
		case ML_M_I_FINA:
		case ML_M_I_FINA1:
		case ML_M_I_FINA2:
		case ML_M_I_FINA3:
		case ML_M_I_FINA4:
			sb.append( ML_M_I );
			break;

		case ML_M_K_ISOL:
		case ML_M_K_INIT:
		case ML_M_K_MEDI:
		case ML_M_K_MEDI1:
		case ML_M_K_MEDI2:
		case ML_M_K_MEDI3:
		case ML_M_K_FINA:
		case ML_M_K_FINA1:
		case ML_M_K_FINA2:
			sb.append( ML_M_K );
			break;
		case ML_M_KE_ISOL:
		case ML_M_KE_INIT:
		case ML_M_KE_MEDI:
		case ML_M_KE_FINA:
			sb.append( ML_M_K + ML_S_E );
			break;
		case ML_M_KI_ISOL:
		case ML_M_KI_INIT:
		case ML_M_KI_MEDI:
		case ML_M_KI_FINA:
			sb.append( ML_M_K + ML_M_I );
			break;
		case ML_M_KUE_ISOL:
		case ML_M_KUE_INIT:
		case ML_M_KUE_MEDI:
		case ML_M_KUE_FINA:
			sb.append( ML_M_K + ML_S_UE );
			break;

		case ML_M_GI_ISOL:
		case ML_M_GI_INIT:
		case ML_M_GI_MEDI:
		case ML_M_GI_FINA:
			sb.append( ML_S_G + ML_M_I );
			break;

		case ML_M_BI_ISOL:
		case ML_M_BI_INIT:
		case ML_M_BI_MEDI:
		case ML_M_BI_FINA:
			sb.append( ML_B + ML_M_I );
			break;

		case ML_M_PI_ISOL:
		case ML_M_PI_INIT:
		case ML_M_PI_MEDI:
		case ML_M_PI_FINA:
			sb.append( ML_P + ML_M_I );
			break;

		case ML_M_R_ISOL:
		case ML_M_R_INIT:
		case ML_M_R_MEDI:
		case ML_M_R_FINA:
			sb.append( ML_M_R );
			break;

		case ML_M_F_ISOL:
		case ML_M_F_INIT:
		case ML_M_F_MEDI:
		case ML_M_F_MEDI1:
		case ML_M_F_FINA:
			sb.append( ML_M_F );
			break;

		case ML_M_ZH_ISOL:
		case ML_M_ZH_INIT:
		case ML_M_ZH_MEDI:
		case ML_M_ZH_FINA:
			sb.append( ML_M_ZH );
			break;

			// ML_ALI_GALI_ANUSVARA_ONE = '\u1880';
			// ML_ALI_GALI_VISARGA_ONE = '\u1881';
			// ML_ALI_GALI_DAMARU = '\u1882';
			// ML_ALI_GALI_UBADAMA = '\u1883';
			// ML_ALI_GALI_INVERTED_UBADAMA = '\u1884';
			// ML_ALI_GALI_BALUDA = '\u1885';
			// ML_ALI_GALI_THREE_BALUDA = '\u1886';
			// ML_ALI_GALI_A = '\u1887';
			// ML_ALI_GALI_I = '\u1888';
			// ML_ALI_GALI_KA = '\u1889';
			// ML_ALI_GALI_NGA = '\u188A';
			// ML_ALI_GALI_CA = '\u188B';
			// ML_ALI_GALI_TTA = '\u188C';
			// ML_ALI_GALI_TTHA = '\u188D';
			// ML_ALI_GALI_DDA = '\u188E';
			// ML_ALI_GALI_NNA = '\u188F';
			// ML_ALI_GALI_TA = '\u1890';
			// ML_ALI_GALI_DA = '\u1891';
			// ML_ALI_GALI_PA = '\u1892';
			// ML_ALI_GALI_PHA = '\u1893';
			// ML_ALI_GALI_SSA = '\u1894';
			// ML_ALI_GALI_ZHA = '\u1895';
			// ML_ALI_GALI_ZA = '\u1896';
			// ML_ALI_GALI_AH = '\u1897';
			// ML_T_ALI_GALI_TA = '\u1898';
			// ML_T_ALI_GALI_ZHA = '\u1899';
			// ML_M_ALI_GALI_GHA = '\u189A';
			// ML_M_ALI_GALI_NGA = '\u189B';
			// ML_M_ALI_GALI_CA = '\u189C';
			// ML_M_ALI_GALI_JHA = '\u189D';
			// ML_M_ALI_GALI_TTA = '\u189E';
			// ML_M_ALI_GALI_DDHA = '\u189F';
			// ML_M_ALI_GALI_TA = '\u18A0';
			// ML_M_ALI_GALI_DHA = '\u18A1';
			// ML_M_ALI_GALI_SSA = '\u18A2';
			// ML_M_ALI_GALI_CYA = '\u18A3';
			// ML_M_ALI_GALI_ZHA = '\u18A4';
			// ML_M_ALI_GALI_ZA = '\u18A5';
			// ML_ALI_GALI_HALF_U = '\u18A6';
			// ML_ALI_GALI_HALF_YA = '\u18A7';
			// ML_M_ALI_GALI_BHA = '\u18A8';
			// ML_ALI_GALI_DAGALGA = '\u18A9';
			// ML_M_ALI_GALI_LHA = '\u18AA';

		default :
			sb.append(ch);
			break;
		}
	}
	
	return sb.toString();
}
