window.onload = function() {

	function addKeyValue(obj) {
		key = obj.attr("value");
        if(key == "CHANGEWORD") {
            value = obj.attr("svalue");
        } else {
		    value = obj.find(".button").find(".alt").html();
        }
		$("#add-key").val(key);
		$("#add-val").val(value);
	}

	$("#keyboard #caganBoard li div").live("touchend", function() {
        addKeyValue($(this));
        $("#add-word").click();
    });

    $("#keyboard #shiftBoard li div").live("touchend", function() {
        addKeyValue($(this));
        $("#add-word").click();
    });

    $("#keyboard #numberBoard li div").live("touchend", function() {
    	addKeyValue($(this));
    	$("#add-num").click();
    });

    $("#keyboard #markBoard li div").live("touchend", function() {
    	addKeyValue($(this));
    	$("#add-num").click();
    });

    $("#cursor-left").live("touchend", function() {
    	$("#cursor-left-btn").click();
    });

    $("#cursor-right").live("touchend", function() {
    	$("#cursor-right-btn").click();
    });

    $("#hide-keyboard").live("touchend", function() {
    	$("#keyboard").slideUp('slow');
    });

}