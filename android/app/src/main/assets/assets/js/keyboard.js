keyboardSetting={
	//keypressAudioMp3:'Button31.wav',
	keypressAudioWav:'Button31.wav'	
};

function createStyle(){
	var css = '';
	css+="div#keyboard{";
	css+="position:absolute;";
	css+="bottom:0;";
	css+="background:#CCC;";
	css+="height:216px;";
	css+="width:"+(document.documentElement.clientWidth)+"px;";
	css+="-moz-user-select:none;";
	css+="-webkit-user-select:none;";
	css+="user-select:none"; 
	css+="}";
	
	css+="ul,li{";
	css+="list-style:none;";
	css+="height:54px;";
	css+="}";
	
	
	css+="div#keyboard>ul>li>div{";
	css+="display:inline-block;";
	css+="}";
	
	css+="div#keyboard>ul>li>div.A{";
	css+="padding-left:"+(document.documentElement.clientWidth*1/20)+"px;";
	css+="}";
	css+="div#keyboard>ul>li>div.L{";
	css+="padding-right:"+(document.documentElement.clientWidth*1/20)+"px;";
	css+="}";
	css+="a.AA,a.BB,a.CC,a.DD,a.EE,a.FF,a.GG,a.HH,a.II,a.JJ,a.KK,a.LL,a.MM,a.NN,a.OO,a.PP,a.QQ,a.RR,a.SS,a.TT,a.UU,a.VV,a.WW,a.XX,a.YY,a.ZZ{";
		css+="margin:4px 2px;";
		css+="position:relative;";
		css+="width:"+(document.documentElement.clientWidth*1/10-4)+"px;";
		css+="line-height:"+(document.documentElement.clientWidth*1/10-4)+"px;";
	css+="}";
	css+="li.row2{";
	//css+="padding-left:"+(document.documentElement.clientWidth/20)+"px;";
	css+="}";
	css+="a.SHIFT,a.DELETE{";
	css+="position:relative;";
	css+="width:"+(document.documentElement.clientWidth*1.5/10-8)+"px;";
	css+="margin:4px 4px;";
	css+="}";
	css+="a.AUXILIARY,a.CHANGE,a.NUMBER,a.MARK{";
	css+="position:relative;";
	css+="width:"+(document.documentElement.clientWidth*1.25/10-4)+"px;";
	css+="margin:2px 2px;";
	css+="}";
	
	css+="a.SPACE{";
	css+="width:"+(document.documentElement.clientWidth*2.75/10-4)+"px;";
	css+="margin:2px 2px;";
	css+="}";
	css+="a.SPACE3{";
	css+="width:"+(document.documentElement.clientWidth*5.5/10-4)+"px;";
	css+="margin:2px 2px;";
	css+="}";
	css+="a.ENTER{";
	css+="width:"+(document.documentElement.clientWidth*2/10-4)+"px;";
	css+="margin:2px 2px;";
	css+="}";
	
	
	
	css+="*{";
	css+="margin:0;";
	css+="padding:0;";
	css+="font-family: 'MongolianArt', Serif;"; 
	css+="}";
		
	css+="span.small-top{";
	css+="font-family:Serif;";
	css+="font-size:12px;";
	css+="display:inline-block;";
	css+="position:absolute;";
	css+="right:3px;";
	css+="top:3px;";
	css+="text-align:right;";
	css+="line-height:10px;";
	css+="}";
	
	css+="span.small-bottom{";
	css+="font-family:Serif;";
	css+="font-size:12px;";
	css+="display:inline-block;";
	css+="position:absolute;";
	css+="left:"+(document.documentElement.clientWidth*1/20+4)+"px;";
	css+="bottom:10px;";
	css+="text-align:right;";
	css+="line-height:10px;";
	css+="}";

	css+="span.mongol{";
	css+="font-family: 'MongolianWhite', Serif;";
	css+="font-size : 20px;";
	css+="-moz-writing-mode: vertical-lr;";
	css+="writing-mode: vertical-lr;";
	css+="-webkit-writing-mode: vertical-lr;";
	css+="-o-writing-mode: vertical-lr;";
	css+="-ms-writing-mode: tb-lr;";
	css+="writing-mode: tb-lr;";
	css+="color:#000;";
	css+="line-height:inherit;";
	css+="width:inherit;";
	css+="height:inherit;";
	css+="}";
	
	css+="span.alt-on{";
	css+="color:#FFF;";
	css+="position:absolute;";
	css+="left:0px;";
	css+="top:-55px;";
	css+="background-color: #1E90FF;";
	css+="background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #1E90FF), color-stop(100%, #4169E1));";
	css+="background: -webkit-linear-gradient(top, #1E90FF, #4169E1);";
	css+="background: -moz-linear-gradient(top, #1E90FF, #4169E1);";
	css+="background: -o-linear-gradient(top, #1E90FF, #4169E1);";
	css+="background: linear-gradient(top, #1E90FF, #4169E1);";
	css+="vertical-align: middle;";
	css+="text-decoration: none;";
	css+="text-align: center;";
	
	css+="-webkit-border-radius: 5px;";
	css+="-moz-border-radius: 5px;";
	css+="-ms-border-radius: 5px;";
	css+="-o-border-radius: 5px;";
	css+="border-radius: 5px;";

	
	css+="line-height:inherit;";
	css+="height: 55px;";
	css+="}";
	
	css+="span.number{";
	css+="line-height:50px;";
	css+="width:inherit;";
	css+="height:inherit;";
	css+="font-size:20px;";
	css+="color:#000;";
	css+="}";
	
	css+="span.unicode{";	
	css+="font-family:Serif;";
	css+="position:absolute;";
	css+="line-height:12px;";
	css+="border:1px dotted;";
	css+="top:15px;";
	css+="left:5px;";
	css+="font-size:14px;";
	css+="width:18px;height:25px;";
	css+="color:#000;";
	css+="}";	
	
	css+=".button-key{";
	css+="-webkit-border-radius: 5px;";
	css+="-moz-border-radius: 5px;";
	css+="-ms-border-radius: 5px;";
	css+="-o-border-radius: 5px;";
	css+="border-radius: 5px;";
	css+="-webkit-box-shadow: inset 0px 1px 1px rgba(255, 255, 255, 0.5), 0px 1px 2px rgba(0, 0, 0, 0.2);";
	css+="-moz-box-shadow: inset 0px 1px 1px rgba(255, 255, 255, 0.5), 0px 1px 2px rgba(0, 0, 0, 0.2);";
	css+="box-shadow: inset 0px 1px 1px rgba(255, 255, 255, 0.5), 0px 1px 2px rgba(0, 0, 0, 0.2);";
	css+="height: 46px;";
	css+="padding: 0px;";
	css+="border-width: 0px;";
	css+="font-size: 24px;";
	css+="}";

	css+=".candidate-box{";
	// css+="display:none;";
	css+="width:100%;";
	css+="height:80px;";
	css+="margin-top:-80px;";
	css+="background:#ccc;";
	css+="overflow: hidden;";
	css+="text-overflow:ellipsis;";
	css+="white-space: nowrap;";
	css+="}";

	css+=".candidate-box ul li{";
	// css+="border:1px solid #fff;";
	css+="padding:10px;";
	css+="float:left;";
	css+="width:3%;";
	css+="text-align:left;";
	css+="}";

	css+="#controller-box{";
	css+="width:100%;";
	css+="height:40px;";
	css+="margin-top:-40px;";
	css+="background:#ccc;";
	css+="}";

	css+=".buttons{";
	css+="width:30px;";
	css+="height:40px;";
	css+="line-height:40px;";
	css+="text-align:center;";
	css+="}";

	css+=".left-button{";
	css+="float:left;";
	css+="}";

	css+=".right-button{";
	css+="float:right";
	css+="}";

	css+=".updown-btn{";
	css+="width:100%;";
	css+="height:50%;";
	css+="text-align:center;";
	css+="font-size:30px;";
	css+="}";

	$("#keyboard-style").remove();
	$("<style id='keyboard-style'></style>").html(css).appendTo($("head"));
	
}
createStyle();
(function createKeyboard(){
	var keyboardHtml=''
	 +'<div id="keyboard">'
	 // 候选词
	 +'<div class="candidate-box" id="candidate-box" style="display:none;">'
	 +'<ul class="unhide">'
	 +'</ul>'
	 +'</div>'
	 // 关联词
	 +'<div class="candidate-box" id="relation-box" style="display:none;">'
	 +'<ul class="unhide">'
	 +'</ul>'
	 +'</div>'
	 // 控制台
	 +'<div id="controller-box">'
	 +'<div class="buttons left-button" id="cursor-left">↑</div>'
	 +'<div class="buttons left-button" id="cursor-right">↓</div>'
	 +'<div class="buttons left-button"></div>'
	 +'<div class="buttons right-button" id="hide-keyboard">∨</div>'
	 +'</div>'

	 +'<ul id="caganBoard" >'
	 +'<li class="row1">'
	 +'<div value="q"><a class="QQ button button-key"><span class="small-top">q</span><span class="mongol alt">ᠣ</span><span class="small-bottom">4</span></a></div>'
	 +'<div value="w"><a class="WW button button-key"><span class="small-top">w</span><span class="mongol alt">ᠸ</span></a></div>'
	 +'<div value="e"><a class="EE button button-key"><span class="small-top">e</span><span class="mongol alt">ᠡ</span></a></div>'
	 +'<div value="r"><a class="RR button button-key"><span class="small-top">r</span><span class="mongol alt">ᠷ</span></a></div>'
	 +'<div value="t"><a class="TT button button-key"><span class="small-top">t</span><span class="mongol alt">ᠲ</span></a></div>'
	 +'<div value="y"><a class="YY button button-key"><span class="small-top">y</span><span class="mongol alt">ᠶ</span></a></div>'
	 +'<div value="u"><a class="UU button button-key"><span class="small-top">u</span><span class="mongol alt">ᠦ</span><span class="small-bottom">7</span></a></div>'
	 +'<div value="i"><a class="II button button-key"><span class="small-top">i</span><span class="mongol alt">ᠢ</span></a></div>'
	 +'<div value="o"><a class="OO button button-key"><span class="small-top">o</span><span class="mongol alt">ᠥ</span><span class="small-bottom">6</span></a></div>'
	 +'<div value="p"><a class="PP button button-key"><span class="small-top">p</span><span class="mongol alt">ᠫ</span></a></div>'
	 +'</li>'	
	 +'<li class="row2">'
	 +'<div value="a" class="A"><a class="AA button button-key"><span class="small-top">a</span><span class="mongol alt">ᠠ</span></a></div>'
	 +'<div value="s"><a class="SS button button-key"><span class="small-top">s</span><span class="mongol alt">ᠰ</span></a></div>'
	 +'<div value="d"><a class="DD button button-key"><span class="small-top">d</span><span class="mongol alt">ᠳ</span></a></div>'
	 +'<div value="f"><a class="FF button button-key"><span class="small-top">f</span><span class="mongol alt">ᠹ</span></a></div>'
	 +'<div value="g"><a class="GG button button-key"><span class="small-top">g</span><span class="mongol alt">ᠭ</span></a></div>'
	 +'<div value="h"><a class="HH button button-key"><span class="small-top">h</span><span class="mongol alt">ᠬ</span></a></div>'
	 +'<div value="j"><a class="JJ button button-key"><span class="small-top">j</span><span class="mongol alt">ᠵ</span></a></div>'
	 +'<div value="k"><a class="KK button button-key"><span class="small-top">k</span><span class="mongol alt">ᠺ</span></a></div>'
	 +'<div value="l" class="L"><a class="LL button button-key"><span class="small-top">l</span><span class="mongol alt">ᠯ</span></a></div>'
	 +'</li>'
	 +'<li class="row3">'
	 +'<div id="shift-on" value="SHIFT" class="SHIFT"><a class="SHIFT button button-key"><span class="mongol">⇐</span></a></div>'
	 +'<div value="z"><a class="ZZ button button-key"><span class="small-top">z</span><span class="mongol alt">ᠽ</span></a></div>'
	 +'<div value="x"><a class="XX button button-key"><span class="small-top">x</span><span class="mongol alt">ᠱ</span></a></div>'
	 +'<div value="c"><a class="CC button button-key"><span class="small-top">c</span><span class="mongol alt">ᠴ</span></a></div>'
	 +'<div value="v"><a class="VV button button-key"><span class="small-top">v</span><span class="mongol alt">ᠤ</span><span class="small-bottom">5</span></a></div>'
	 +'<div value="b"><a class="BB button button-key"><span class="small-top">b</span><span class="mongol alt">ᠪ</span></a></div>'
	 +'<div value="n"><a class="NN button button-key"><span class="small-top">n</span><span class="mongol alt">ᠨ</span></a></div>'
	 +'<div value="m"><a class="MM button button-key"><span class="small-top">m</span><span class="mongol alt">ᠮ</span></a></div>'
	 +'<div id="delete" value="DELETE" class="DELETE"><a class="DELETE button button-key"><span class="mongol">×</span></a></div>'
	 +'</li>'
	 +'<li class="row4">'
	 +'<div id="number-on" value="NUMBER"><a class="NUMBER button button-key"><span class="number">123</span></a></div>'
	 +'<div id="ime-toggle" value="MARK"><a class="MARK button button-key"><span class="mongol">ᠪᠦᠷᠢᠨ</span></a></div>'
	 +'<div id="auxiliary" value="AUXILIARY"><a class="AUXILIARY button button-key"><span class="mongol alt">ᠲᠠᠭᠠᠪᠤᠷᠢ</span></a></div>'
	 +'<div id="change" value="CHANGE"><a class="CHANGE button button-key"><span class="number alt">\'</span></a></div>'
	 +'<div id="space" value="SPACE"><a class="SPACE button button-key"><span class="number">Space</span></a></div>'
	 +'<div value="ENTER"><a class="ENTER button button-key"><span class="number">Enter</span></a></div>'
	 +'</li>'
	 +'</ul>'
	 
	 

	 +'<ul id="shiftBoard" style="display:none;">'
	 +'<li class="row1">'
	 +'<div value="Q"><a class="QQ button button-key"><span class="small-top">Q</span><span class="mongol alt">ᡂᠢ</span></a></div>'
	 +'<div value="W"><a class="WW button button-key"><span class="small-top">W</span><span class="mongol alt">᠊ᠣ᠎ᠠ</span></a></div>'
	 +'<div value="E"><a class="EE button button-key"><span class="small-top">E</span><span class="mongol alt">ᠧ</span><span class="small-bottom">2</span></a></div>'
	 +'<div value="R"><a class="RR button button-key"><span class="small-top">R</span><span class="mongol alt">ᠿᠠ</span></a></div>'
	 +'<div value="T"><a class="TT button button-key"><span class="small-top">T</span><span class="mongol alt">ᠲᠠ</span></a></div>'
	 +'<div value="Y"><a class="YY button button-key"><span class="small-top">Y</span><span class="mongol alt">ᠶᠠ</span></a></div>'
	 +'<div value="U"><a class="UU button button-key"><span class="small-top">U</span><span class="mongol alt">᠊ᠦ᠋᠊</span><span class="small-bottom">7</span></a></div>'
	 +'<div value="I"><a class="II button button-key"><span class="small-top">I</span><span class="mongol alt">᠊᠊᠊᠊</span></a></div>'
	 +'<div value="O"><a class="OO button button-key"><span class="small-top">O</span><span class="mongol alt">᠊ᠥ᠋᠊</span><span class="small-bottom">6</span></a></div>'
	 +'<div value="P"><a class="PP button button-key"><span class="small-top">P</span><span class="mongol alt">ᠫᠠ</span></a></div>'
	 +'</li>'
	 +'<li class="row2">'
	 +'<div class="A" value="CHANGEWORD" svalue="᠎"><a class="AA button button-key"><span class="small-top">A</span><span class="unicode alt">mv<br>s</span></a></div>'
	 +'<div value="CHANGEWORD" svalue=" "><a class="SS button button-key"><span class="small-top">S</span><span class="unicode alt">20<br>2f</span></a></div>'
	 +'<div value="CHANGEWORD" svalue="᠎᠋"><a class="DD button button-key"><span class="small-top">D</span><span class="unicode alt">fv<br>s1</span></a></div>'
	 +'<div value="CHANGEWORD" svalue="᠎᠌"><a class="FF button button-key"><span class="small-top">F</span><span class="unicode alt">fv<br>s2</span></a></div>'
	 +'<div value="CHANGEWORD" svalue="᠎᠍"><a class="GG button button-key"><span class="small-top">G</span><span class="unicode alt">fv<br>s3</span></a></div>'
	 +'<div value="H"><a class="HH button button-key"><span class="small-top">H</span><span class="mongol alt">ᠾᠠ</span></a></div>'
	 +'<div value="J"><a class="JJ button button-key"><span class="small-top">J</span><span class="mongol alt">᠊ᠭ᠍᠊</span><span class="small-bottom">g</span></a></div>'
	 +'<div value="K"><a class="KK button button-key"><span class="small-top">K</span><span class="mongol alt">ᠻᠠ</span></a></div>'
	 +'<div class="L" value="L"><a class="LL button button-key"><span class="small-top">L</span><span class="mongol alt">ᡀᠠ</span></a></div>'
	 +'</li>'
	 +'<li class="row3">'
	 +'<div id="shift-off" value="SHIFT" class="SHIFT"><a class="SHIFT button button-key"><span class="mongol">⇐∣</span></a></div>'
	 +'<div value="Z"><a class="ZZ button button-key"><span class="small-top">Z</span><span class="mongol alt">ᡁᠢ</span></a></div>'
	 +'<div value="X"><a class="XX button button-key"><span class="small-top">X</span><span class="mongol alt">ᠱᠠ</span></a></div>'
	 +'<div value="C"><a class="CC button button-key"><span class="small-top">C</span><span class="mongol alt">ᠼᠠ</span></a></div>'
	 +'<div value="V"><a class="VV button button-key"><span class="small-top">V</span><span class="mongol alt">ᠤ</span><span class="small-bottom">5</span></a></div>'
	 +'<div value="B"><a class="BB button button-key"><span class="small-top">B</span><span class="mongol alt">ᠪᠠ</span></a></div>'
	 +'<div value="N"><a class="NN button button-key"><span class="small-top">N</span><span class="mongol alt">᠊ᠩ</span></a></div>'
	 +'<div value="M"><a class="MM button button-key"><span class="small-top">M</span><span class="mongol alt">᠊ᠨ᠋᠊</span></a></div>'
	 +'<div id="DELETE" value="DELETE"><a class="DELETE button button-key"><span class="mongol">×</span></a></div>'
	 +'</li>'
	 +'<li class="row4">'
	 +'<div id="number-on" value="NUMBER"><a class="NUMBER button button-key"><span class="number">123</span></a></div>'
	 +'<div value="MARK"><a class="MARK button button-key"><span class="mongol">ᠪᠦᠷᠢᠨ</span></a></div>'
	 +'<div value="SPACE"><a class="SPACE3 button button-key"><span class="number">Space</span></a></div>'
	 +'<div value="ENTER"><a class="ENTER button button-key"><span class="number">Enter</span></a></div>'
	 +'</li>'
	 +'</ul>'
	 
	 
	 +'<ul id="numberBoard" style="display:none;">'
	 +'<li class="row1">'
	 +'<div value="1"><a class="QQ button button-key"><span class="number alt">1</span></a></div>'
	 +'<div value="2"><a class="WW button button-key"><span class="number alt">2</span></a></div>'
	 +'<div value="3"><a class="EE button button-key"><span class="number alt">3</span></a></div>'
	 +'<div value="4"><a class="RR button button-key"><span class="number alt">4</span></a></div>'
	 +'<div value="5"><a class="TT button button-key"><span class="number alt">5</span></a></div>'
	 +'<div value="6"><a class="YY button button-key"><span class="number alt">6</span></a></div>'
	 +'<div value="7"><a class="UU button button-key"><span class="number alt">7</span></a></div>'
	 +'<div value="8"><a class="II button button-key"><span class="number alt">8</span></a></div>'
	 +'<div value="9"><a class="OO button button-key"><span class="number alt">9</span></a></div>'
	 +'<div value="0"><a class="PP button button-key"><span class="number alt">0</span></a></div>'
	 +'</li>'
	 +'<li class="row2">'
	 +'<div value="A"><a class="AA button button-key"><span class="number alt">-</span></a></div>'
	 +'<div value="S"><a class="SS button button-key"><span class="number alt">"</span></a></div>'
	 +'<div value="D"><a class="DD button button-key"><span class="number alt">\'</span></a></div>'
	 +'<div value="F"><a class="FF button button-key"><span class="number alt">/</span></a></div>'
	 +'<div value="G"><a class="GG button button-key"><span class="number alt">$</span></a></div>'
	 +'<div value="H"><a class="HH button button-key"><span class="number alt">@</span></a></div>'
	 +'<div value="J"><a class="JJ button button-key"><span class="number alt">《</span></a></div>'
	 +'<div value="K"><a class="KK button button-key"><span class="number alt">(</span></a></div>'
	 +'<div value="L"><a class="LL button button-key"><span class="number alt">)</span></a></div>'
	 +'<div value="L"><a class="LL button button-key"><span class="number alt">~</span></a></div>'
	 +'</li>'
	 +'<li class="row3">'
	 +'<div id="mark-on" value="shift-mark"><a class="SHIFT button button-key"><span class="number alt">#+=</span></a></div>'
	 +'<div value="Z"><a class="ZZ button button-key"><span class="mongol alt">᠂</span></a></div>'
	 +'<div value="X"><a class="XX button button-key"><span class="mongol alt">᠃ </span></a></div>'
	 +'<div value="C"><a class="CC button button-key"><span class="mongol alt">᠁</span></a></div>'
	 +'<div value="V"><a class="VV button button-key"><span class="mongol alt">᠄</span></a></div>'
	 +'<div value="B"><a class="BB button button-key"><span class="mongol alt">?</span></a></div>'
	 +'<div value="N"><a class="NN button button-key"><span class="mongol alt">!</span></a></div>'
	 +'<div value="M"><a class="MM button button-key"><span class="mongol alt">;</span></a></div>'
	 +'<div value="DELETE"><a class="DELETE button button-key"><span class="mongol">×</span></a></div>'
	 +'</li>'
	 +'<li class="row4">'
	 +'<div id="cagan-on" value="cagan"><a class="NUMBER button button-key" ><span class="mongol">ᠴᠠᠭᠠᠨ</span></a></div>'
	 +'<div value="MARK"><a class="MARK button button-key"><span class="mongol">ᠪᠦᠷᠢᠨ</span></a></div>'
	 +'<div value="SPACE"><a class="SPACE3 button button-key"><span class="number">Space</span></a></div>'
	 +'<div value="ENTER"><a class="ENTER button button-key"><span class="number">Enter</span></a></div>'
	 +'</li>'
	 +'</ul>'
	 
	 +'<ul id="markBoard" style="display:none;">'
	 +'<li class="row1">'
	 +'<div value="1"><a class="QQ button button-key"><span class="number alt">q</span></a></div>'
	 +'<div value="2"><a class="WW button button-key"><span class="number alt">w</span></a></div>'
	 +'<div value="3"><a class="EE button button-key"><span class="number alt">e</span></a></div>'
	 +'<div value="4"><a class="RR button button-key"><span class="number alt">r</span></a></div>'
	 +'<div value="5"><a class="TT button button-key"><span class="number alt">t</span></a></div>'
	 +'<div value="6"><a class="YY button button-key"><span class="number alt">y</span></a></div>'
	 +'<div value="7"><a class="UU button button-key"><span class="number alt">u</span></a></div>'
	 +'<div value="8"><a class="II button button-key"><span class="number alt">i</span></a></div>'
	 +'<div value="9"><a class="OO button button-key"><span class="number alt">o</span></a></div>'
	 +'<div value="0"><a class="PP button button-key"><span class="number alt">p</span></a></div>'
	 +'</li>'
	 +'<li class="row2">'
	 +'<div value="A"><a class="AA button button-key"><span class="number alt">-</span></a></div>'
	 +'<div value="S"><a class="SS button button-key"><span class="number alt">"</span></a></div>'
	 +'<div value="D"><a class="DD button button-key"><span class="number alt">\'</span></a></div>'
	 +'<div value="F"><a class="FF button button-key"><span class="number alt">/</span></a></div>'
	 +'<div value="G"><a class="GG button button-key"><span class="number alt">$</span></a></div>'
	 +'<div value="H"><a class="HH button button-key"><span class="number alt">@</span></a></div>'
	 +'<div value="J"><a class="JJ button button-key"><span class="number alt">《</span></a></div>'
	 +'<div value="K"><a class="KK button button-key"><span class="number alt">(</span></a></div>'
	 +'<div value="L"><a class="LL button button-key"><span class="number alt">)</span></a></div>'
	 +'<div value="L"><a class="LL button button-key"><span class="number alt">~</span></a></div>'
	 +'</li>'
	 +'<li class="row3">'
	 +'<div id="number-on" value="shift-mark"><a class="SHIFT button button-key"><span class="number">123</span></a></div>'
	 +'<div value="Z"><a class="ZZ button button-key"><span class="number alt">᠂</span></a></div>'
	 +'<div value="X"><a class="XX button button-key"><span class="number alt">᠃ </span></a></div>'
	 +'<div value="C"><a class="CC button button-key"><span class="number alt">᠁</span></a></div>'
	 +'<div value="V"><a class="VV button button-key"><span class="number alt">᠄</span></a></div>'
	 +'<div value="B"><a class="BB button button-key"><span class="number alt">?</span></a></div>'
	 +'<div value="N"><a class="NN button button-key"><span class="number alt">!</span></a></div>'
	 +'<div value="M"><a class="MM button button-key"><span class="number alt">;</span></a></div>'
	 +'<div value="DELETE"><a class="DELETE button button-key"><span class="mongol">×</span></a></div>'
	 +'</li>'
	 +'<li class="row4">'
	 +'<div id="cagan-on" value="cagan"><a class="NUMBER button button-key" ><span class="mongol">ᠴᠠᠭᠠᠨ</span></a></div>'
	 +'<div value="MARK"><a class="MARK button button-key"><span class="mongol">ᠪᠦᠷᠢᠨ</span></a></div>'
	 +'<div value="SPACE"><a class="SPACE3 button button-key"><span class="number">Space</span></a></div>'
	 +'<div value="ENTER"><a class="ENTER button button-key"><span class="number">Enter</span></a></div>'
	 +'</li>'
	 +'</ul>'
	 +'</div>';
	// +'<audio id="keypressAudio">'
	 //+'<source src="./button-16.wav" type="audio/mp3">'
	 //+'<source src="./button-16.wav" type="audio/wav">'
	 //+'</audio>';
	$("div#shift-on").live("touchstart",function(){//打开shift键盘
		$("#keyboard ul:not(.unhide)").hide();
		$("ul#shiftBoard").show();
	});
	$("div#shift-off").live("touchstart",function(){//关闭shift键盘
		$("#keyboard ul:not(.unhide)").hide();
		$("ul#caganBoard").show();
	});
	$("div#number-on").live("touchstart",function(){//打开数字键盘
		$("#keyboard ul:not(.unhide)").hide();
		$("ul#numberBoard").show();
	});
	$("div#cagan-on").live("touchstart",function(){//关闭数字键盘 回到cagan键盘
		$("#keyboard ul:not(.unhide)").hide();
		$("ul#caganBoard").show();
	});
	
	$("div#mark-on").live("touchstart",function(){//打开符号键盘
		$("#keyboard ul:not(.unhide)").hide();
		$("ul#markBoard").show();
	});
	
	
	$("#keyboard").find('ul>li>div').live("touchstart",function(){
		$(this).find('span.alt').removeClass("alt").addClass("alt-on");
		$("#mongol-textarea").trigger("keydown",$(this).attr('vlaue'));
	}).live("touchend",function(){
		$(this).find('span.alt-on').removeClass("alt-on").addClass("alt");
		$("#mongol-textarea").trigger("keypress",$(this).attr('vlaue'));
		$("#mongol-textarea").text($("#mongol-textarea").text()+$(this).attr("value"));
	});
	$("#keyboard").find('ul>li>div').live("mousedown",function(){
		$(this).find('span.alt').removeClass("alt").addClass("alt-on");
		$("#mongol-textarea").trigger("keydown",$(this).attr('vlaue'));
	}).live("mouseup",function(){
		$(this).find('span.alt-on').removeClass("alt-on").addClass("alt");
		$("#mongol-textarea").trigger("keypress",$(this).attr('vlaue'));
	});

	
	
	$("body").append(keyboardHtml);
	
})(jQuery);

(function(){
	$(window).bind("orientationchange",function(){
		createStyle();
	})
})(jQuery);

var updownbtn = function() {
	$(".updown-btn").live("touchstart", function() {
		$(this).css({
			'color': '#fff'
		});
	});

	$(".updown-btn").live("touchend", function() {
		$(this).css({
			'color': ''
		});
	});
}

var isAuxiliary = function() {
	$("#auxiliary .button").css({
		'background': '#ccc'
	});
}

var unAuxiliary = function() {
	$("#auxiliary .button").css({
		'background': ''
	});
}