// import './react/index.native';


import 'react-native/Libraries/ReactNative/renderApplication';
//import { View, DeviceEventEmitter } from 'react-native'


// Android不提供符号
import 'es6-symbol/implement';
//import './react/index.native';
import React, { Component } from 'react';
import { AppRegistry,DeviceEventEmitter,View} from 'react-native';
//import Route from './react/route/index';
import RouterCN from './react/route/router-cn'
import RouterMN from './react/route/router-mn'
import config from './react/config'
import { getLanguage } from './react/utils/language'
class Root extends Component {
    constructor(props) {
        super(props)
        this.state = {
         // luyou:null,  
          language: config.language.default
        }
      }
    
      componentDidMount() {
        DeviceEventEmitter.addListener('languageChangeEvent', () => {
          this.getLanguageStatus()
        })
      //  this.getRoutes();
      }
    
      componentDidMounted() {
        this.getLanguageStatus()
      }
    
      getLanguageStatus() {
        getLanguage().then(lang => {
          //  console.warn(lang);
            
          this.setState({
            language: lang
          })
        })
      }
      getRoutes() {
        if(this.state.language === config.language.list.cn) {
          return (
            <RouterCN />
          )
        } else {
          return (
            <RouterMN />
          )
        }
      }
    //   getRoutes() {

    //     if(this.state.language === config.language.list.cn) {

    //         this.setState({
    //             luyou:<RouterCN />
    //         })
            
       
    //     } else {

    //         this.setState({
    //             luyou:<RouterMN />
    //         })
           
        
    //     }
    //   }
    render() {
       // let ly=this.state.luyou;
      //  console.warn(ly)
        return (
            this.getRoutes()
            
        );
        
    }
}
AppRegistry.registerComponent('App', () => Root);

